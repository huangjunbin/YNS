package com.yns.net;

/**
 * 业务数据URL地址（注意标注地址注释）
 *
 * @author allen
 * @version 1.0.0
 * @created 2014-7-10
 */
public class Urls {
    public static final boolean TEST_MODE = true;// 当前连接服务器模式，测试模式还是产线模式
    /**
     * 默认的API头地址
     */
    public static final String TEST_HEAD_URL = "http://www.dxsapp.com/api/";
    public static final String ONLINE_HEAD_URL = "http://www.dxsapp.com/api/";
    public static final String HEAD_URL = TEST_MODE ? TEST_HEAD_URL
            : ONLINE_HEAD_URL;
    public static final String TEST_IMAGE_URL = "http://www.dxsapp.com/";
    public static final String ONLINE_IMAGE_URL = "http://www.dxsapp.com/";
    public static final String IMAGE_URL = TEST_MODE ? TEST_IMAGE_URL
            : ONLINE_IMAGE_URL;

    // 获取新闻列表
    public static final String GETMESSAGELIST = HEAD_URL
            + "article/selectArticleByCondition.spv";
    // 收藏
    public static final String ADDCOLLECT = HEAD_URL
            + "favorite/addMemberFavorite.spv";

    //企业收藏
    public static final String addEnterpriseFavorite = HEAD_URL
            + "favorite/addEnterpriseFavorite.spv";
    // 取消收藏
    public static final String REMOVECOLLECT = HEAD_URL
            + "favorite/deleteMemberFavorite.spv";
    // 获取收藏列表
    public static final String GETCOLLECT = HEAD_URL
            + "favorite/selectFavoriteByCondition.spv";

    // 获取求职列表
    public static final String GETJOB = HEAD_URL
            + "job/selectJobByCondition.spv";

    // 申请职位
    public static final String APPLYJOB = HEAD_URL + "job/addJobApply.spv";
    // 登录
    public static final String login_action = HEAD_URL + "user/appLogin.spv";

    // 获取职位列表
    public static final String GETAPPLY = HEAD_URL
            + "job/getJobApplyByCondition.spv";

    // 职位类别类型列表
    public static final String GETTRADETYPE = HEAD_URL
            + "job/getJobSortByCondition.spv";

    // 通知列表
    public static final String GETNOTICE = HEAD_URL
            + "user/selectNoticeByCondition.spv";

    // 企业通知列表
    public static final String selectEnterpriseNoticeByCondition = HEAD_URL
            + "user/selectEnterpriseNoticeByCondition.spv";

    // 简历列表
    public static final String GETRESUME = HEAD_URL
            + "job/getResumeByCondition.spv";

    //
    //根据企业获取应聘简历
    public static final String getResumeByEnterpriseId = HEAD_URL
            + "job/getResumeByEnterpriseId.spv";
    // 注册
    public static final String REGISTERUSER = HEAD_URL + "user/register.spv";
    // 企业注册
    public static final String REGISTERCOMPANY = HEAD_URL
            + "user/enterpriseRegister.spv";
    // 图片上传
    public static final String UPLOADIMG = HEAD_URL + "user/uploadFile.spv";

    // 设置默认简历
    public static final String setUpdefaultFlag = HEAD_URL
            + "job/setUpdefaultFlag.spv";
    // 删除简历
    public static final String deleteResume = HEAD_URL + "job/deleteResume.spv";
    // 编辑简历
    public static final String editResume = HEAD_URL + "job/editResume.spv";
    // 首页广告
    public static final String selectBannerByCondition = HEAD_URL
            + "banner/selectBannerByCondition.spv";
    // 搜索简历
    public static final String searchResumeByCondition = HEAD_URL
            + "job/searchResumeByCondition.spv";
    // 发出通知
    public static final String sendJobMsg = HEAD_URL + "job/sendJobMsg.spv";

    // 企业获得发布的招聘列表
    public static final String selectJobByUserId = HEAD_URL
            + "job/selectJobByUserId.spv";

    public static final String editJob = HEAD_URL + "job/editJob.spv";

    public static final String deleteJob = HEAD_URL + "job/deleteJob.spv";

    public static final String updatePwd = HEAD_URL + "user/updatePwd.spv";

    // 获得某个招聘信息的工作申请列表 job/getJobApplyByJobId.spv
    public static final String getJobApplyByJobId = HEAD_URL
            + "job/getJobApplyByJobId.spv";

    // 找回密码
    public static final String forgetPassword_action = HEAD_URL
            + "user/getBackPwd.spv";
    // 编辑个人信息
    public static final String modifUserinfo_action = HEAD_URL
            + "user/updateUser.spv";

    public static final String updateEnterprise = HEAD_URL
            + "user/updateEnterprise.spv";
    //购买简历
    public static final String buy_resume = HEAD_URL + "job/buyResumeInfo.spv";

    //获取购物车分类
    public static final String selectProductSortByCondition = HEAD_URL + "product/selectProductSortByCondition.spv";

    //获取商品列表
    public static final String selectProductByCondition = HEAD_URL + "product/selectProductByCondition.spv";

    //获取购物车列表
    public static final String selectShopCarByCondition = HEAD_URL + "product/selectShopCarByCondition.spv";

    //添加购物车
    public static final String addShopCar = HEAD_URL + "product/addShopCar.spv";

    //删除购物车
    public static final String deleteShopCar = HEAD_URL + "product/deleteShopCar.spv";

    //收货地址列表
    public static final String selectConsigneeByCondition = HEAD_URL + "product/selectConsigneeByCondition.spv";

    //编辑收货地址
    public static final String editConsignee = HEAD_URL + "product/editConsignee.spv";

    //删除收货地址
    public static final String deleteConsignee = HEAD_URL + "product/deleteConsignee.spv";

    //获得订单列表 product/selectOrderByCondition.spv
    public static final String selectOrderByCondition = HEAD_URL + "product/selectOrderByCondition.spv";

    //结算
    public static final String settleAccounts = HEAD_URL + "product/settleAccounts.spv";

    //订单确认
    public static final String confirmOrder = HEAD_URL + "product/confirmOrder.spv";

    //立即购买 product/buy.spv
    public static final String buy = HEAD_URL + "product/buy.spv";

    //根据id获得职位详情 job/selectJobById.spv
    public static final String selectJobById = HEAD_URL + "job/selectJobById.spv";

    //根据id获取简历详情 job/getResumeById.spv
    public static final String getResumeById = HEAD_URL + "job/getResumeById.spv";

    //favorite/addEnterpriseComment.spv 企业评论
    public static final String addEnterpriseComment = HEAD_URL + "favorite/addEnterpriseComment.spv";

    //获取评论列表
    public static final String selectEnterpriseCommentByCondition = HEAD_URL + "favorite/selectEnterpriseCommentByCondition.spv";

    //job/isTopJob.spv 职位置顶
    public static final String isTopJob = HEAD_URL + "job/isTopJob.spv";

    public static final String deleteMarketInfo = HEAD_URL + "market/deleteMarketInfo.spv";

    public static final String selectMarketInfoByCondition = HEAD_URL
            + "market/selectMarketInfoByCondition.spv";

    public static final String editMarketInfo = HEAD_URL + "market/editMarketInfo.spv";

    //market/selectMarketSortByCondition.spv
    public static final String selectMarketSortByCondition = HEAD_URL + "market/selectMarketSortByCondition.spv";

    //market/getSchool.spv
    public static final String getSchool = HEAD_URL + "market/getSchool.spv";
    // ------------------

    // 获取验证码
    public static final String getcheckcode_adtion = HEAD_URL
            + "userCode/getCode.do";

    // 首页
    public static final String home_action = HEAD_URL
            + "goodsQueryIndex/queryIndex.do";

    // 获取商品列表
    public static final String queryGoodsList_action = HEAD_URL
            + "goodsQueryList/queryList.do";

    // 获取个人信息
    public static final String getuserinfo_action = HEAD_URL
            + "userQueryById/queryById.do";

    // 提交意见
    public static final String commitOption_action = HEAD_URL
            + "userOpinions/opinions.do";

    // 我的车库
    public static final String getMyGradeCars_action = HEAD_URL
            + "carQueryList/queryList.do";

    // 添加到我的车库
    public static final String addToMyGrade_action = HEAD_URL
            + "carInsert/insert.do";

    // 获取我的车库详情
    public static final String getCarDetail_action = HEAD_URL
            + "carQueryDetail/queryDetail.do";

    // 移除汽车
    public static final String removeCar_action = HEAD_URL
            + "carDelete/delete.do";

    // 修改我的车库详情
    public static final String modifyCarDetail_action = HEAD_URL
            + "carUpdate/update.do";

    public static final String order_insert = HEAD_URL
            + "/orderGoodsInsert/orderInsert.do";

    // order back
    public static final String order_back = HEAD_URL
            + "/orderGoodsBack/orderBack.do";

    public static final String order_comment = HEAD_URL
            + "/commentInsert/insert.do";

    // 支付宝回调
    public static final String get_sign = HEAD_URL + "/alipay/getSign.do";

    public static final String money_pay = HEAD_URL + "/yiDingPay/yiDingPay.do";

    public static final String score_money = HEAD_URL
            + "rechargeScore/queryInfo.do";

    public static final String app_version = HEAD_URL
            + "appSource/getAppSource.do";

    public static final String update_city = HEAD_URL
            + "/userUpdateCityId/cityidUpdate.do";

    public static final String get_code = HEAD_URL
            + "/yiDingPay/getOrderCodes.do";

}
