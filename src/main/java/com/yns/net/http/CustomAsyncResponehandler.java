package com.yns.net.http;

import com.yns.model.ResponeModel;

public class CustomAsyncResponehandler extends AsyncHttpResponseHandler {
	private CustomAsyncResponehandler customAsyncResponehandler;

	public CustomAsyncResponehandler() {
		customAsyncResponehandler = null;
	}

	public CustomAsyncResponehandler(
			CustomAsyncResponehandler customAsyncResponehandler) {
		if (customAsyncResponehandler != null)
			this.customAsyncResponehandler = customAsyncResponehandler;
	}

	public void onSuccess(ResponeModel baseModel) {
		if (customAsyncResponehandler != null)
			customAsyncResponehandler.onSuccess(baseModel);
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if (customAsyncResponehandler != null)
			customAsyncResponehandler.onStart();
	}

	@Override
	public void onFinish() {
		// TODO Auto-generated method stub
		super.onFinish();
		if (customAsyncResponehandler != null)
			customAsyncResponehandler.onFinish();
	}

	@Override
	public void onFailure(Throwable error, String content) {
		// TODO Auto-generated method stub
		super.onFailure(error, content);
		if (customAsyncResponehandler != null)
			customAsyncResponehandler.onFailure(error, content);
	}

}
