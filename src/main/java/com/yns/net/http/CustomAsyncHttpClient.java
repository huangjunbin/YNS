package com.yns.net.http;

import android.content.Context;
import android.view.View;

import com.yns.app.AppContext;
import com.yns.model.RequestModel;
import com.yns.model.ResponeModel;
import com.yns.util.JsonUtil;
import com.yns.util.LogUtil;
import com.yns.util.UIHelper;
import com.yns.widget.MyProgressDialog;

public class CustomAsyncHttpClient {
    private String TAG = "CustomAsyncHttpClient";
    private boolean isTag = true;

    private AsyncHttpClient asyncHttpClient;
    private MyProgressDialog dialog;
    private Context mContext;
    private ResponeModel baseModel;

    public CustomAsyncHttpClient(Context context) {
        asyncHttpClient = new AsyncHttpClient();
        mContext = context;
        if (mContext != null) {
            dialog = new MyProgressDialog(mContext, "", true);
            dialog.tv_value.setVisibility(View.GONE);
        }
        baseModel = new ResponeModel();
    }

    public void post(final RequestModel requestModel,
                     final CustomAsyncResponehandler responseHandler) {
        /*
         * RequestParams newParams = new RequestParams();
		 * com.alibaba.fastjson.JSONObject jsonObject = new
		 * com.alibaba.fastjson.JSONObject(); for (BasicNameValuePair param :
		 * requestModel.getParams() .getParamsList()) {
		 * jsonObject.put(param.getName(), param.getValue()); }
		 * 
		 * 
		 * 
		 * newParams.put("p", jsonObject.toString());
		 */


        if (AppContext.currentUser != null && AppContext.currentUser.getUserToken() != null) {
            requestModel.getParams().put("userToken", AppContext.currentUser.getUserToken());
        }
        LogUtil.d(TAG, requestModel.getUrl() + "?"
                + requestModel.getParams().toString(), isTag);
        asyncHttpClient.post(requestModel.getUrl(), requestModel.getParams(),
                new AsyncHttpResponseHandler() {
                    @Override
                    public void onStart() {
                        super.onStart();
                        LogUtil.d(TAG, "onStart___", isTag);
                        if (requestModel.isShowDialog()) {// 显示网络对话框
                            if (mContext != null) {
                                dialog.show();
                            }
                        }
                        responseHandler.onStart();
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                        LogUtil.d(TAG, "onFinish___", isTag);
                        if (requestModel.isShowDialog()) {// 隐藏网络对话框
                            if (mContext != null) {
                                dialog.dismiss();
                            }
                        }
                        responseHandler.onFinish();
                    }

                    @Override
                    public void onSuccess(String content) {
                        super.onSuccess(content);
                        LogUtil.d(TAG, "onSuccess___" + content, isTag);

                        // TODO:解密返回的参数
                        baseModel = JsonUtil.convertJsonToObject(content,
                                ResponeModel.class);

                        if (baseModel != null) {
                            baseModel.setCls(requestModel.getCls());
                            baseModel.setEasyName(requestModel.getEasyName());
                            baseModel.setJson(content);
                            baseModel.setList(requestModel.isList());
                            baseModel.setNew(requestModel.isNew());
                            baseModel.setListCountKey(requestModel
                                    .getListCountKey());
                            baseModel.init();
                        }

                        if (requestModel.isShowErrorMessage()&&!"200".equals(baseModel.getCode())) {
                            if (mContext != null) {
                                UIHelper.ShowMessage(mContext,
                                        baseModel.getMsg());
                            }
                        }

                        responseHandler.onSuccess(baseModel);
                    }

                    @Override
                    public void onFailure(Throwable error, String content) {
                        super.onFailure(error, content);
                        LogUtil.d(TAG, "onFailure___" + content, isTag);
                        responseHandler.onFailure(error, content);
                    }
                });
    }
}
