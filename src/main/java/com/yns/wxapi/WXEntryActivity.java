package com.yns.wxapi;

import android.content.Intent;
import android.widget.Toast;

import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.umeng.socialize.weixin.view.WXCallbackActivity;

public class WXEntryActivity extends WXCallbackActivity {
	@Override
	protected IWXAPI getWXApi() {
		return super.getWXApi();
	}

	@Override
	protected void handleIntent(Intent intent) {
		super.handleIntent(intent);
	}

	@Override
	protected void initWXHandler() {
		super.initWXHandler();
	}

	@Override
	public void onReq(BaseReq req) {
		super.onReq(req);
	}

	@Override
	public void onResp(BaseResp resp) {
		super.onResp(resp);

		switch (resp.errCode) {
		case BaseResp.ErrCode.ERR_OK:
			Toast.makeText(this, "分享成功", Toast.LENGTH_SHORT).show();
			break;
		case BaseResp.ErrCode.ERR_USER_CANCEL:
			Toast.makeText(this, "取消分享", Toast.LENGTH_SHORT).show();
			break;
		case BaseResp.ErrCode.ERR_AUTH_DENIED:
			Toast.makeText(this, "分享被拒绝", Toast.LENGTH_SHORT).show();
			break;
		default:
			Toast.makeText(this, "分享失败", Toast.LENGTH_SHORT).show();
			break;
		}
		// TODO 微信分享 成功之后调用接口
		this.finish();
	}
}
