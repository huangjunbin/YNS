package com.yns.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.yns.app.ui.JobDetailActivity;
import com.yns.app.ui.MainActivity2;
import com.yns.app.ui.ResumeDetailActivity;
import com.yns.model.Job;
import com.yns.model.ResponeModel;
import com.yns.model.Resume;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.ComonService;
import com.yns.util.JsonUtil;
import com.yns.util.SPUtil;

import org.json.JSONException;
import org.json.JSONObject;

import cn.jpush.android.api.JPushInterface;

/**
 * 自定义接收器
 * <p/>
 * 如果不定义这个 Receiver，则： 1) 默认用户会打开主界面 2) 接收不到自定义消息
 */
public class MyReceiver extends BroadcastReceiver {
    private static final String TAG = "JPush";

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        Log.d(TAG, "[MyReceiver] onReceive - " + intent.getAction()
                + ", extras: " + printBundle(bundle));

        if (JPushInterface.ACTION_REGISTRATION_ID.equals(intent.getAction())) {
            String regId = bundle
                    .getString(JPushInterface.EXTRA_REGISTRATION_ID);
            Log.d(TAG, "[MyReceiver] 接收Registration Id : " + regId);
            // send the Registration Id to your server...

        } else if (JPushInterface.ACTION_MESSAGE_RECEIVED.equals(intent
                .getAction())) {
            Log.d(TAG,
                    "[MyReceiver] 接收到推送下来的自定义消息: "
                            + bundle.getString(JPushInterface.EXTRA_MESSAGE));
           // processCustomMessage(context, bundle);

        } else if (JPushInterface.ACTION_NOTIFICATION_RECEIVED.equals(intent
                .getAction())) {
            Log.d(TAG, "[MyReceiver] 接收到推送下来的通知");
            int notifactionId = bundle
                    .getInt(JPushInterface.EXTRA_NOTIFICATION_ID);
            Log.d(TAG, "[MyReceiver] 接收到推送下来的通知的ID: " + notifactionId);
           // processCustomMessage(context, bundle);
            SPUtil.saveboolean("d1",true);
            processCustomMessage2(context, bundle);

        } else if (JPushInterface.ACTION_NOTIFICATION_OPENED.equals(intent
                .getAction())) {
            Log.d(TAG, "[MyReceiver] 用户点击打开了通知");
            processCustomMessage(context, bundle);

        } else if (JPushInterface.ACTION_RICHPUSH_CALLBACK.equals(intent
                .getAction())) {
            Log.d(TAG,
                    "[MyReceiver] 用户收到到RICH PUSH CALLBACK: "
                            + bundle.getString(JPushInterface.EXTRA_EXTRA));
            // 在这里根据 JPushInterface.EXTRA_EXTRA 的内容处理代码，比如打开新的Activity，
            // 打开一个网页等..

        } else if (JPushInterface.ACTION_CONNECTION_CHANGE.equals(intent
                .getAction())) {
            boolean connected = intent.getBooleanExtra(
                    JPushInterface.EXTRA_CONNECTION_CHANGE, false);
            Log.w(TAG, "[MyReceiver]" + intent.getAction()
                    + " connected state change to " + connected);
        } else {
            Log.d(TAG, "[MyReceiver] Unhandled intent - " + intent.getAction());
        }
    }

    // 打印所有的 intent extra 数据
    private static String printBundle(Bundle bundle) {
        StringBuilder sb = new StringBuilder();
        for (String key : bundle.keySet()) {
            if (key.equals(JPushInterface.EXTRA_NOTIFICATION_ID)) {
                sb.append("\nkey:" + key + ", value:" + bundle.getInt(key));
            } else if (key.equals(JPushInterface.EXTRA_CONNECTION_CHANGE)) {
                sb.append("\nkey:" + key + ", value:" + bundle.getBoolean(key));
            } else {
                sb.append("\nkey:" + key + ", value:" + bundle.getString(key));
            }
        }
        return sb.toString();
    }
    // send msg to MainActivity
    private void processCustomMessage2(Context context, Bundle bundle) {
        if (MainActivity2.isForeground) {
            String message = bundle.getString(JPushInterface.EXTRA_MESSAGE);
            String extras = bundle.getString(JPushInterface.EXTRA_EXTRA);
            Intent msgIntent = new Intent(MainActivity2.MESSAGE_RECEIVED_ACTION);
            msgIntent.putExtra(MainActivity2.KEY_MESSAGE, message);
            if (extras == null) {
                try {
                    JSONObject extraJson = new JSONObject(extras);
                    if (null != extraJson && extraJson.length() > 0) {
                        msgIntent.putExtra(MainActivity2.KEY_EXTRAS, extras);
                    }
                } catch (JSONException e) {

                }

            }
            context.sendBroadcast(msgIntent);
        }
    }
    // send msg to MainActivity
    private void processCustomMessage(final Context context, final Bundle bundle) {
        String message = bundle.getString(JPushInterface.EXTRA_MESSAGE);
        String extras = bundle.getString(JPushInterface.EXTRA_EXTRA);
        if (extras!= null) {
            try {
                JSONObject extraJson = new JSONObject(extras);
                if (null != extraJson && extraJson.length() > 0) {
                    String id = extraJson.getString("id");
                    int type = extraJson.getInt("type");
                    System.out.println(id);
                    if (type == 0) {//简历详情
                        ComonService.getInstance(context).getResumeById(id + "",
                                new CustomAsyncResponehandler() {
                                    @Override
                                    public void onSuccess(ResponeModel baseModel) {
                                        super.onSuccess(baseModel);
                                        if (baseModel != null
                                                && baseModel.isStatus()) {
                                            Resume resume = JsonUtil.convertJsonToObject(baseModel.getResult(), Resume.class);
                                            SPUtil.saveboolean("d1",false);
                                            Intent i = new Intent(context, ResumeDetailActivity.class);
                                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            i.putExtra("resume", resume);
                                            context.startActivity(i);
                                        }
                                    }
                                });


                    }
                    if (type == 1) {//职位详情
                        ComonService.getInstance(context).getJobById(id + "",
                                new CustomAsyncResponehandler() {
                                    @Override
                                    public void onSuccess(ResponeModel baseModel) {
                                        super.onSuccess(baseModel);
                                        if (baseModel != null
                                                && baseModel.isStatus()) {
                                            SPUtil.saveboolean("d1",false);
                                            Job job = JsonUtil.convertJsonToObject(baseModel.getResult(), Job.class);

                                            Intent i = new Intent(context, JobDetailActivity.class);
                                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            i.putExtra("job", job);
                                            context.startActivity(i);
                                        }
                                    }
                                });
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}
