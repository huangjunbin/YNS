package com.yns.model;


import java.io.Serializable;

public class Market implements Serializable{

    String  marketInfoId	;//信息Id
    String title		;//标题
    String isPublish	;//是否发布 1 是 0 否
    String details			;//详细描述
    String pic		;//商品图片路径
    String  price		;//价格
    String  userId			;//发布者userId
    String  nickName		;//发布者昵称
    String	 createDate	;//创建时间
    String  mobile;
    String  schoolId		;//	学校id
    String schoolName		;//	学校名称
    String typeSortId;
    String typeSortName;
    public String getMarketInfoId() {
        return marketInfoId;
    }

    public void setMarketInfoId(String marketInfoId) {
        this.marketInfoId = marketInfoId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIsPublish() {
        return isPublish;
    }

    public void setIsPublish(String isPublish) {
        this.isPublish = isPublish;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getTypeSortId() {
        return typeSortId;
    }

    public void setTypeSortId(String typeSortId) {
        this.typeSortId = typeSortId;
    }

    public String getTypeSortName() {
        return typeSortName;
    }

    public void setTypeSortName(String typeSortName) {
        this.typeSortName = typeSortName;
    }

    @Override
    public String toString() {
        return "Market{" +
                "marketInfoId='" + marketInfoId + '\'' +
                ", title='" + title + '\'' +
                ", isPublish='" + isPublish + '\'' +
                ", details='" + details + '\'' +
                ", pic='" + pic + '\'' +
                ", price='" + price + '\'' +
                ", userId='" + userId + '\'' +
                ", nickName='" + nickName + '\'' +
                ", createDate='" + createDate + '\'' +
                ", mobile='" + mobile + '\'' +
                ", schoolId='" + schoolId + '\'' +
                ", schoolName='" + schoolName + '\'' +
                ", typeSortId='" + typeSortId + '\'' +
                ", typeSortName='" + typeSortName + '\'' +
                '}';
    }
}
