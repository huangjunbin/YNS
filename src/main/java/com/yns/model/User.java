package com.yns.model;

import com.simple.util.db.annotation.SimpleColumn;
import com.simple.util.db.annotation.SimpleId;
import com.simple.util.db.annotation.SimpleTable;

import java.io.Serializable;

@SimpleTable(name = "t_user")
public class User implements Serializable {
	private static final long serialVersionUID = 2365701157715369155L;

	public static Integer REMBER_PASSWORD = 1;
	public static Integer AUTO_LOGIN = 1;

	@SimpleId
	@SimpleColumn(name = "userId")
	private String userId;// 用户id
	@SimpleColumn(name = "password")
	private String password;// 密码
	@SimpleColumn(name = "createDate")
	private String createDate;// 创建时间
	@SimpleColumn(name = "headerPic")
	private String headerPic;// 头像图片路径
	@SimpleColumn(name = "nickName")
	private String nickName;// 昵称
	@SimpleColumn(name = "syllabusPic")
	private String syllabusPic;// 课程表图片
	@SimpleColumn(name = "userName")
	private String userName;// 用户名
	@SimpleColumn(name = "email")
	private String email;// 邮箱地址
	@SimpleColumn(name = "mobile")
	private String mobile;// 手机号
	@SimpleColumn(name = "remberPassword")
	private Integer remberPassword;// 记住密码 1：记住，其它不记住
	@SimpleColumn(name = "autoLogin")
	private Integer autoLogin; // 自动登陆 1：自动登陆，其它不登陆
	@SimpleColumn(name = "type")
	private Integer type; // 1个人 2企业
	@SimpleColumn(name = "deptId")
	private Integer deptId;
	@SimpleColumn(name = "freeReadCount")
	private String freeReadCount;
	@SimpleColumn(name = "remark")
	private String remark;
	@SimpleColumn(name = "remainingSum")
	private String remainingSum;
	@SimpleColumn(name = "userToken")
	private String userToken;

	public static Integer getREMBER_PASSWORD() {
		return REMBER_PASSWORD;
	}

	public static void setREMBER_PASSWORD(Integer rEMBER_PASSWORD) {
		REMBER_PASSWORD = rEMBER_PASSWORD;
	}

	public static Integer getAUTO_LOGIN() {
		return AUTO_LOGIN;
	}

	public static void setAUTO_LOGIN(Integer aUTO_LOGIN) {
		AUTO_LOGIN = aUTO_LOGIN;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getHeaderPic() {
		return headerPic;
	}

	public void setHeaderPic(String headerPic) {
		this.headerPic = headerPic;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getSyllabusPic() {
		return syllabusPic;
	}

	public void setSyllabusPic(String syllabusPic) {
		this.syllabusPic = syllabusPic;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Integer getRemberPassword() {
		return remberPassword;
	}

	public void setRemberPassword(Integer remberPassword) {
		this.remberPassword = remberPassword;
	}

	public Integer getAutoLogin() {
		return autoLogin;
	}

	public void setAutoLogin(Integer autoLogin) {
		this.autoLogin = autoLogin;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getDeptId() {
		return deptId;
	}

	public void setDeptId(Integer deptId) {
		this.deptId = deptId;
	}

	public String getFreeReadCount() {
		return freeReadCount;
	}

	public void setFreeReadCount(String freeReadCount) {
		this.freeReadCount = freeReadCount;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getRemainingSum() {
		return remainingSum;
	}

	public void setRemainingSum(String remainingSum) {
		this.remainingSum = remainingSum;
	}

	public String getUserToken() {
		return userToken;
	}

	public void setUserToken(String userToken) {
		this.userToken = userToken;
	}

	@Override
	public String toString() {
		return "User{" +
				"userId='" + userId + '\'' +
				", password='" + password + '\'' +
				", createDate='" + createDate + '\'' +
				", headerPic='" + headerPic + '\'' +
				", nickName='" + nickName + '\'' +
				", syllabusPic='" + syllabusPic + '\'' +
				", userName='" + userName + '\'' +
				", email='" + email + '\'' +
				", mobile='" + mobile + '\'' +
				", remberPassword=" + remberPassword +
				", autoLogin=" + autoLogin +
				", type=" + type +
				", deptId=" + deptId +
				", freeReadCount='" + freeReadCount + '\'' +
				", remark='" + remark + '\'' +
				", remainingSum='" + remainingSum + '\'' +
				", userToken='" + userToken + '\'' +
				'}';
	}
}
