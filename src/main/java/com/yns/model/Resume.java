package com.yns.model;

import java.io.Serializable;
import java.util.List;

public class Resume implements Serializable {

	private static final long serialVersionUID = 1L;
	private String appMemberResumeInfoId; // Id
	private String name;// 简历名称
	private String headerPic;// 简历头像
	private String sex;// 性别 1 为男 0为女
	private String hunYinQingKuang;// 婚姻情况 1 已婚 0未婚
	private String chuShengNianYue;// 出生年月
	private String mingZu;// 民族
	private String createDate;// 创建时间
	private String shenGao;// 身高
	private String xueLi;// 学历id
	private String xueLiName;// 学历名称
	private String huJi;// 户籍
	private String biYeXueXiao;// 毕业学校
	private String zhuanYe;// 专业
	private String yuYanNengLi;// 语言能力
	private String jiNengZhuanChang;// 技能专长
	private String gongZuoJingYan;// 工作经验
	private String gongZuoNainXian;// 工作年限id
	private String gongZuoNainXianName;// 工作年限
	private String gongZuoLeiXing;// 工作类型
	private String qiDaiXingZi;// 期待薪资id
	private String qiDaiXingZiName;// 期待薪资
	private String daoGangShiJian;// 到岗时间
	private String ziWoPingJia;// 自我评价
	private String xiaoYuanChengZhangJingLi;// 校园成长经历
	private String defaultFlag;// 默认投档简历 ture 为是 flase不是
	private String toUserId;
	private String qq;
	private String email;
	private String mobile;
	private List<Education> educations; // 教育经历
    private boolean butFlag=false;
	private String appJobInfoId;
    public boolean isButFlag() {
        return butFlag;
    }

    public void setButFlag(boolean butFlag) {
        this.butFlag = butFlag;
    }



    public String getAppMemberResumeInfoId() {
		return appMemberResumeInfoId;
	}

	public void setAppMemberResumeInfoId(String appMemberResumeInfoId) {
		this.appMemberResumeInfoId = appMemberResumeInfoId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHeaderPic() {
		return headerPic;
	}

	public void setHeaderPic(String headerPic) {
		this.headerPic = headerPic;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getHunYinQingKuang() {
		return hunYinQingKuang;
	}

	public void setHunYinQingKuang(String hunYinQingKuang) {
		this.hunYinQingKuang = hunYinQingKuang;
	}

	public String getChuShengNianYue() {
		return chuShengNianYue;
	}

	public void setChuShengNianYue(String chuShengNianYue) {
		this.chuShengNianYue = chuShengNianYue;
	}

	public String getMingZu() {
		return mingZu;
	}

	public void setMingZu(String mingZu) {
		this.mingZu = mingZu;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getShenGao() {
		return shenGao;
	}

	public void setShenGao(String shenGao) {
		this.shenGao = shenGao;
	}

	public String getXueLi() {
		return xueLi;
	}

	public void setXueLi(String xueLi) {
		this.xueLi = xueLi;
	}

	public String getXueLiName() {
		return xueLiName;
	}

	public void setXueLiName(String xueLiName) {
		this.xueLiName = xueLiName;
	}

	public String getHuJi() {
		return huJi;
	}

	public void setHuJi(String huJi) {
		this.huJi = huJi;
	}

	public String getBiYeXueXiao() {
		return biYeXueXiao;
	}

	public void setBiYeXueXiao(String biYeXueXiao) {
		this.biYeXueXiao = biYeXueXiao;
	}

	public String getZhuanYe() {
		return zhuanYe;
	}

	public void setZhuanYe(String zhuanYe) {
		this.zhuanYe = zhuanYe;
	}

	public String getYuYanNengLi() {
		return yuYanNengLi;
	}

	public void setYuYanNengLi(String yuYanNengLi) {
		this.yuYanNengLi = yuYanNengLi;
	}

	public String getJiNengZhuanChang() {
		return jiNengZhuanChang;
	}

	public void setJiNengZhuanChang(String jiNengZhuanChang) {
		this.jiNengZhuanChang = jiNengZhuanChang;
	}

	public String getGongZuoJingYan() {
		return gongZuoJingYan;
	}

	public void setGongZuoJingYan(String gongZuoJingYan) {
		this.gongZuoJingYan = gongZuoJingYan;
	}

	public String getGongZuoNainXian() {
		return gongZuoNainXian;
	}

	public void setGongZuoNainXian(String gongZuoNainXian) {
		this.gongZuoNainXian = gongZuoNainXian;
	}

	public String getGongZuoNainXianName() {
		return gongZuoNainXianName;
	}

	public void setGongZuoNainXianName(String gongZuoNainXianName) {
		this.gongZuoNainXianName = gongZuoNainXianName;
	}

	public String getGongZuoLeiXing() {
		return gongZuoLeiXing;
	}

	public void setGongZuoLeiXing(String gongZuoLeiXing) {
		this.gongZuoLeiXing = gongZuoLeiXing;
	}

	public String getQiDaiXingZi() {
		return qiDaiXingZi;
	}

	public void setQiDaiXingZi(String qiDaiXingZi) {
		this.qiDaiXingZi = qiDaiXingZi;
	}

	public String getQiDaiXingZiName() {
		return qiDaiXingZiName;
	}

	public void setQiDaiXingZiName(String qiDaiXingZiName) {
		this.qiDaiXingZiName = qiDaiXingZiName;
	}

	public String getDaoGangShiJian() {
		return daoGangShiJian;
	}

	public void setDaoGangShiJian(String daoGangShiJian) {
		this.daoGangShiJian = daoGangShiJian;
	}

	public String getZiWoPingJia() {
		return ziWoPingJia;
	}

	public void setZiWoPingJia(String ziWoPingJia) {
		this.ziWoPingJia = ziWoPingJia;
	}

	public String getXiaoYuanChengZhangJingLi() {
		return xiaoYuanChengZhangJingLi;
	}

	public void setXiaoYuanChengZhangJingLi(String xiaoYuanChengZhangJingLi) {
		this.xiaoYuanChengZhangJingLi = xiaoYuanChengZhangJingLi;
	}

	public String getDefaultFlag() {
		return defaultFlag;
	}

	public void setDefaultFlag(String defaultFlag) {
		this.defaultFlag = defaultFlag;
	}

	public List<Education> getEducations() {
		return educations;
	}

	public void setEducations(List<Education> educations) {
		this.educations = educations;
	}



    public class Education implements Serializable {

		private static final long serialVersionUID = 1L;
		private String educationLogInfoId;// 教育经历id
		private String beginDate;// 工作开始时间
		private String endDate;// 工作截止时间
		private String name;// 院校/培训机构名称
		private String zhuanYe;// 专业
		private String xueLi;// 学历

		public String getEducationLogInfoId() {
			return educationLogInfoId;
		}

		public void setEducationLogInfoId(String educationLogInfoId) {
			this.educationLogInfoId = educationLogInfoId;
		}

		public String getBeginDate() {
			return beginDate;
		}

		public void setBeginDate(String beginDate) {
			this.beginDate = beginDate;
		}

		public String getEndDate() {
			return endDate;
		}

		public void setEndDate(String endDate) {
			this.endDate = endDate;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getZhuanYe() {
			return zhuanYe;
		}

		public void setZhuanYe(String zhuanYe) {
			this.zhuanYe = zhuanYe;
		}

		public String getXueLi() {
			return xueLi;
		}

		public void setXueLi(String xueLi) {
			this.xueLi = xueLi;
		}

	}

	public String getToUserId() {
		return toUserId;
	}

	public void setToUserId(String toUserId) {
		this.toUserId = toUserId;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getAppJobInfoId() {
		return appJobInfoId;
	}

	public void setAppJobInfoId(String appJobInfoId) {
		this.appJobInfoId = appJobInfoId;
	}

	private String deptName;// 公司名称
	private String industry;// 所属行业
	private String companyType;// 公司性质
	private String describe;// 公司描述
	private String title;// 标题
	private String content;// 内容
	private String gongZuoDiDian;// 工作地点
	private String description;// 工作描述
	private String frontCoverPic;// 图片路径
	private String xueLiYaoQiu;// 学历要求
	private String zhaoPingRenShu;// 招聘人数
	private String xinZiDaiYu;// 薪资待遇
	private String fuLiLabels;// 福利待遇

	private String deptId;// 公司id
	private String isFavorite;
	private String deptIsFavorite;
	private String worktype;
	private String jobSortId;
	private String jobPostId;
	private String flag;
	String gongZuoJingYanSortId = "82";
	String xinZiDaiYuSortId = "92";
	String xueLiYaoQiuSortId = "336";
	private String jobSort;
	private boolean topflag;

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getCompanyType() {
		return companyType;
	}

	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}

	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getGongZuoDiDian() {
		return gongZuoDiDian;
	}

	public void setGongZuoDiDian(String gongZuoDiDian) {
		this.gongZuoDiDian = gongZuoDiDian;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFrontCoverPic() {
		return frontCoverPic;
	}

	public void setFrontCoverPic(String frontCoverPic) {
		this.frontCoverPic = frontCoverPic;
	}

	public String getXueLiYaoQiu() {
		return xueLiYaoQiu;
	}

	public void setXueLiYaoQiu(String xueLiYaoQiu) {
		this.xueLiYaoQiu = xueLiYaoQiu;
	}

	public String getZhaoPingRenShu() {
		return zhaoPingRenShu;
	}

	public void setZhaoPingRenShu(String zhaoPingRenShu) {
		this.zhaoPingRenShu = zhaoPingRenShu;
	}

	public String getXinZiDaiYu() {
		return xinZiDaiYu;
	}

	public void setXinZiDaiYu(String xinZiDaiYu) {
		this.xinZiDaiYu = xinZiDaiYu;
	}

	public String getFuLiLabels() {
		return fuLiLabels;
	}

	public void setFuLiLabels(String fuLiLabels) {
		this.fuLiLabels = fuLiLabels;
	}

	public String getDeptId() {
		return deptId;
	}

	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}

	public String getIsFavorite() {
		return isFavorite;
	}

	public void setIsFavorite(String isFavorite) {
		this.isFavorite = isFavorite;
	}

	public String getDeptIsFavorite() {
		return deptIsFavorite;
	}

	public void setDeptIsFavorite(String deptIsFavorite) {
		this.deptIsFavorite = deptIsFavorite;
	}

	public String getWorktype() {
		return worktype;
	}

	public void setWorktype(String worktype) {
		this.worktype = worktype;
	}

	public String getJobSortId() {
		return jobSortId;
	}

	public void setJobSortId(String jobSortId) {
		this.jobSortId = jobSortId;
	}

	public String getJobPostId() {
		return jobPostId;
	}

	public void setJobPostId(String jobPostId) {
		this.jobPostId = jobPostId;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getGongZuoJingYanSortId() {
		return gongZuoJingYanSortId;
	}

	public void setGongZuoJingYanSortId(String gongZuoJingYanSortId) {
		this.gongZuoJingYanSortId = gongZuoJingYanSortId;
	}

	public String getXinZiDaiYuSortId() {
		return xinZiDaiYuSortId;
	}

	public void setXinZiDaiYuSortId(String xinZiDaiYuSortId) {
		this.xinZiDaiYuSortId = xinZiDaiYuSortId;
	}

	public String getXueLiYaoQiuSortId() {
		return xueLiYaoQiuSortId;
	}

	public void setXueLiYaoQiuSortId(String xueLiYaoQiuSortId) {
		this.xueLiYaoQiuSortId = xueLiYaoQiuSortId;
	}

	public String getJobSort() {
		return jobSort;
	}

	public void setJobSort(String jobSort) {
		this.jobSort = jobSort;
	}

	public boolean isTopflag() {
		return topflag;
	}

	public void setTopflag(boolean topflag) {
		this.topflag = topflag;
	}

	@Override
	public String toString() {
		return "Resume{" +
				"appMemberResumeInfoId='" + appMemberResumeInfoId + '\'' +
				", name='" + name + '\'' +
				", headerPic='" + headerPic + '\'' +
				", sex='" + sex + '\'' +
				", hunYinQingKuang='" + hunYinQingKuang + '\'' +
				", chuShengNianYue='" + chuShengNianYue + '\'' +
				", mingZu='" + mingZu + '\'' +
				", createDate='" + createDate + '\'' +
				", shenGao='" + shenGao + '\'' +
				", xueLi='" + xueLi + '\'' +
				", xueLiName='" + xueLiName + '\'' +
				", huJi='" + huJi + '\'' +
				", biYeXueXiao='" + biYeXueXiao + '\'' +
				", zhuanYe='" + zhuanYe + '\'' +
				", yuYanNengLi='" + yuYanNengLi + '\'' +
				", jiNengZhuanChang='" + jiNengZhuanChang + '\'' +
				", gongZuoJingYan='" + gongZuoJingYan + '\'' +
				", gongZuoNainXian='" + gongZuoNainXian + '\'' +
				", gongZuoNainXianName='" + gongZuoNainXianName + '\'' +
				", gongZuoLeiXing='" + gongZuoLeiXing + '\'' +
				", qiDaiXingZi='" + qiDaiXingZi + '\'' +
				", qiDaiXingZiName='" + qiDaiXingZiName + '\'' +
				", daoGangShiJian='" + daoGangShiJian + '\'' +
				", ziWoPingJia='" + ziWoPingJia + '\'' +
				", xiaoYuanChengZhangJingLi='" + xiaoYuanChengZhangJingLi + '\'' +
				", defaultFlag='" + defaultFlag + '\'' +
				", toUserId='" + toUserId + '\'' +
				", qq='" + qq + '\'' +
				", email='" + email + '\'' +
				", mobile='" + mobile + '\'' +
				", educations=" + educations +
				", butFlag=" + butFlag +
				", appJobInfoId='" + appJobInfoId + '\'' +
				'}';
	}
}
