package com.yns.model;

import java.io.Serializable;

public class Agent implements Serializable {
	private static final long serialVersionUID = 6053132753604901932L;
	private String name;
	private String code;

	public Agent(String name, String code) {
		this.name = name;
		this.code = code;
	}

	public Agent() {
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Agent [name=" + name + ", code=" + code + "]";
	}

}
