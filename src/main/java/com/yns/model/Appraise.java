package com.yns.model;

import java.io.Serializable;

public class Appraise implements Serializable {
	private static final long serialVersionUID = 4437762106628501317L;
	private String appraiserName;
	private String appraiseTime;
	private int appraiseStar;
	private String appraiseContent;

	private String commentId;// 评论ID
	private String userNickname;// 昵称
	private String star;// 评价星级 ,5 星10分制
	private String content;// 评论内容
	private String createDate;// 评论时间

	public String getCommentId() {
		return commentId;
	}

	public void setCommentId(String commentId) {
		this.commentId = commentId;
	}

	public String getUserNickname() {
		return userNickname;
	}

	public void setUserNickname(String userNickname) {
		this.userNickname = userNickname;
	}

	public String getStar() {
		return star;
	}

	public void setStar(String star) {
		this.star = star;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	/**
	 * @return the appraiserName
	 */
	public String getAppraiserName() {
		return appraiserName;
	}

	/**
	 * @param appraiserName
	 *            the appraiserName to set
	 */
	public void setAppraiserName(String appraiserName) {
		this.appraiserName = appraiserName;
	}

	/**
	 * @return the appraiseTime
	 */
	public String getAppraiseTime() {
		return appraiseTime;
	}

	/**
	 * @param appraiseTime
	 *            the appraiseTime to set
	 */
	public void setAppraiseTime(String appraiseTime) {
		this.appraiseTime = appraiseTime;
	}

	/**
	 * @return the appraiseStar
	 */
	public int getAppraiseStar() {
		return appraiseStar;
	}

	/**
	 * @param appraiseStar
	 *            the appraiseStar to set
	 */
	public void setAppraiseStar(int appraiseStar) {
		this.appraiseStar = appraiseStar;
	}

	/**
	 * @return the appraiseContent
	 */
	public String getAppraiseContent() {
		return appraiseContent;
	}

	/**
	 * @param appraiseContent
	 *            the appraiseContent to set
	 */
	public void setAppraiseContent(String appraiseContent) {
		this.appraiseContent = appraiseContent;
	}

	@Override
	public String toString() {
		return "Appraise [appraiserName=" + appraiserName + ", appraiseTime="
				+ appraiseTime + ", appraiseStar=" + appraiseStar
				+ ", appraiseContent=" + appraiseContent + ", commentId="
				+ commentId + ", userNickname=" + userNickname + ", star="
				+ star + ", content=" + content + ", createDate=" + createDate
				+ "]";
	}

}
