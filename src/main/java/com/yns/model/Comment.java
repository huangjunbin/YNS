package com.yns.model;


public class Comment {

    String	 commentInfoId	;
    String	 content		;
    String	 fkId		;
    String	 createDate		;
    String	  nickName		;
    String	 headerPic		;

    public String getCommentInfoId() {
        return commentInfoId;
    }

    public void setCommentInfoId(String commentInfoId) {
        this.commentInfoId = commentInfoId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFkId() {
        return fkId;
    }

    public void setFkId(String fkId) {
        this.fkId = fkId;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getHeaderPic() {
        return headerPic;
    }

    public void setHeaderPic(String headerPic) {
        this.headerPic = headerPic;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "commentInfoId='" + commentInfoId + '\'' +
                ", content='" + content + '\'' +
                ", fkId='" + fkId + '\'' +
                ", createDate='" + createDate + '\'' +
                ", nickName='" + nickName + '\'' +
                ", headerPic='" + headerPic + '\'' +
                '}';
    }
}
