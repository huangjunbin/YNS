package com.yns.model;

public class GoodsQueryRequest {
	// 字段名称 类型 是否必须 说明
	private String cityId;// 城市ID
	private String areaId;// 区域ID，默认-1全城
	private String industryId;// 行业ID：1、快捷洗
	// 2、维修保养
	// 3、新车试驾
	// 4、餐饮
	// 5、休闲中心
	// 6、丽人
	// 7、购物中心
	private String serviceId;// 服务ID，默认 -1 全部
	private String codeType;// 条件选择：0：离我最近，
	// 1：评价最高
	// 2：最新发布
	private String pageNum;// 从第几条开始，默认0
	private String pageSize;// 从第几条开始向后查询多少条
	private String longitude;// 经度（codeType为0时必填）
	private String latitude;// 纬度（codeType为0时必填）

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public String getIndustryId() {
		return industryId;
	}

	public void setIndustryId(String industryId) {
		this.industryId = industryId;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getCodeType() {
		return codeType;
	}

	public void setCodeType(String codeType) {
		this.codeType = codeType;
	}

	public String getPageNum() {
		return pageNum;
	}

	public void setPageNum(String pageNum) {
		this.pageNum = pageNum;
	}

	public String getPageSize() {
		return pageSize;
	}

	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	@Override
	public String toString() {
		return "GoodsQueryRequest [cityId=" + cityId + ", areaId=" + areaId
				+ ", industryId=" + industryId + ", serviceId=" + serviceId
				+ ", codeType=" + codeType + ", pageNum=" + pageNum
				+ ", pageSize=" + pageSize + ", longitude=" + longitude
				+ ", latitude=" + latitude + "]";
	}

}
