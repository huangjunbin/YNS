package com.yns.model;

import java.io.Serializable;

public class Job implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String appJobInfoId;// Id
	private String deptName;// 公司名称
	private String industry;// 所属行业
	private String companyType;// 公司性质
	private String describe;// 公司描述
	private String title;// 标题
	private String content;// 内容
	private String createDate;// 创建时间
	private String gongZuoDiDian;// 工作地点
	private String description;// 工作描述
	private String frontCoverPic;// 图片路径
	private String xueLiYaoQiu;// 学历要求
	private String gongZuoJingYan;// 工作经验
	private String zhaoPingRenShu;// 招聘人数
	private String xinZiDaiYu;// 薪资待遇
	private String fuLiLabels;// 福利待遇

	private String deptId;// 公司id
	private String isFavorite;
	private String deptIsFavorite;
	private String worktype;
	private String jobSortId;
	private String jobPostId;
	private String flag;
	String gongZuoJingYanSortId = "82";
	String xinZiDaiYuSortId = "92";
	String xueLiYaoQiuSortId = "336";
	private String jobSort;
	private boolean topflag;

	public String getAppJobInfoId() {
		return appJobInfoId;
	}

	public void setAppJobInfoId(String appJobInfoId) {
		this.appJobInfoId = appJobInfoId;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getCompanyType() {
		return companyType;
	}

	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}

	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getGongZuoDiDian() {
		return gongZuoDiDian;
	}

	public void setGongZuoDiDian(String gongZuoDiDian) {
		this.gongZuoDiDian = gongZuoDiDian;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getFrontCoverPic() {
		return frontCoverPic;
	}

	public void setFrontCoverPic(String frontCoverPic) {
		this.frontCoverPic = frontCoverPic;
	}

	public String getXueLiYaoQiu() {
		return xueLiYaoQiu;
	}

	public void setXueLiYaoQiu(String xueLiYaoQiu) {
		this.xueLiYaoQiu = xueLiYaoQiu;
	}

	public String getGongZuoJingYan() {
		return gongZuoJingYan;
	}

	public void setGongZuoJingYan(String gongZuoJingYan) {
		this.gongZuoJingYan = gongZuoJingYan;
	}

	public String getZhaoPingRenShu() {
		return zhaoPingRenShu;
	}

	public void setZhaoPingRenShu(String zhaoPingRenShu) {
		this.zhaoPingRenShu = zhaoPingRenShu;
	}

	public String getXinZiDaiYu() {
		return xinZiDaiYu;
	}

	public void setXinZiDaiYu(String xinZiDaiYu) {
		this.xinZiDaiYu = xinZiDaiYu;
	}

	public String getFuLiLabels() {
		return fuLiLabels;
	}

	public void setFuLiLabels(String fuLiLabels) {
		this.fuLiLabels = fuLiLabels;
	}

	public String getDeptId() {
		return deptId;
	}

	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}

	public String getIsFavorite() {
		return isFavorite;
	}

	public void setIsFavorite(String isFavorite) {
		this.isFavorite = isFavorite;
	}

	public String getDeptIsFavorite() {
		return deptIsFavorite;
	}

	public void setDeptIsFavorite(String deptIsFavorite) {
		this.deptIsFavorite = deptIsFavorite;
	}

	public String getWorktype() {
		return worktype;
	}

	public void setWorktype(String worktype) {
		this.worktype = worktype;
	}

	public String getJobSortId() {
		return jobSortId;
	}

	public void setJobSortId(String jobSortId) {
		this.jobSortId = jobSortId;
	}

	public String getJobPostId() {
		return jobPostId;
	}

	public void setJobPostId(String jobPostId) {
		this.jobPostId = jobPostId;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getJobSort() {
		return jobSort;
	}

	public void setJobSort(String jobSort) {
		this.jobSort = jobSort;
	}

	public String getGongZuoJingYanSortId() {
		return gongZuoJingYanSortId;
	}

	public void setGongZuoJingYanSortId(String gongZuoJingYanSortId) {
		this.gongZuoJingYanSortId = gongZuoJingYanSortId;
	}

	public String getXinZiDaiYuSortId() {
		return xinZiDaiYuSortId;
	}

	public void setXinZiDaiYuSortId(String xinZiDaiYuSortId) {
		this.xinZiDaiYuSortId = xinZiDaiYuSortId;
	}

	public String getXueLiYaoQiuSortId() {
		return xueLiYaoQiuSortId;
	}

	public void setXueLiYaoQiuSortId(String xueLiYaoQiuSortId) {
		this.xueLiYaoQiuSortId = xueLiYaoQiuSortId;
	}

	public boolean isTopflag() {
		return topflag;
	}

	public void setTopflag(boolean topflag) {
		this.topflag = topflag;
	}

	@Override
	public String toString() {
		return "Job{" +
				"appJobInfoId='" + appJobInfoId + '\'' +
				", deptName='" + deptName + '\'' +
				", industry='" + industry + '\'' +
				", companyType='" + companyType + '\'' +
				", describe='" + describe + '\'' +
				", title='" + title + '\'' +
				", content='" + content + '\'' +
				", createDate='" + createDate + '\'' +
				", gongZuoDiDian='" + gongZuoDiDian + '\'' +
				", description='" + description + '\'' +
				", frontCoverPic='" + frontCoverPic + '\'' +
				", xueLiYaoQiu='" + xueLiYaoQiu + '\'' +
				", gongZuoJingYan='" + gongZuoJingYan + '\'' +
				", zhaoPingRenShu='" + zhaoPingRenShu + '\'' +
				", xinZiDaiYu='" + xinZiDaiYu + '\'' +
				", fuLiLabels='" + fuLiLabels + '\'' +
				", deptId='" + deptId + '\'' +
				", isFavorite='" + isFavorite + '\'' +
				", deptIsFavorite='" + deptIsFavorite + '\'' +
				", worktype='" + worktype + '\'' +
				", jobSortId='" + jobSortId + '\'' +
				", jobPostId='" + jobPostId + '\'' +
				", flag='" + flag + '\'' +
				", gongZuoJingYanSortId='" + gongZuoJingYanSortId + '\'' +
				", xinZiDaiYuSortId='" + xinZiDaiYuSortId + '\'' +
				", xueLiYaoQiuSortId='" + xueLiYaoQiuSortId + '\'' +
				", jobSort='" + jobSort + '\'' +
				", topflag=" + topflag +
				'}';
	}
}
