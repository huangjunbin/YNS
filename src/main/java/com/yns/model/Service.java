package com.yns.model;

public class Service {
	private String bandUrl;
	private String company;
	private String area;
	private String kind;
	private String nowPrice;
	private String oldPrice;
	private String goodRate;
	private String disTance;

	/**
	 * @return the bandUrl
	 */
	public String getBandUrl() {
		return bandUrl;
	}

	/**
	 * @param bandUrl
	 *            the bandUrl to set
	 */
	public void setBandUrl(String bandUrl) {
		this.bandUrl = bandUrl;
	}

	/**
	 * @return the company
	 */
	public String getCompany() {
		return company;
	}

	/**
	 * @param company
	 *            the company to set
	 */
	public void setCompany(String company) {
		this.company = company;
	}

	/**
	 * @return the area
	 */
	public String getArea() {
		return area;
	}

	/**
	 * @param area
	 *            the area to set
	 */
	public void setArea(String area) {
		this.area = area;
	}

	/**
	 * @return the kind
	 */
	public String getKind() {
		return kind;
	}

	/**
	 * @param kind
	 *            the kind to set
	 */
	public void setKind(String kind) {
		this.kind = kind;
	}

	/**
	 * @return the nowPrice
	 */
	public String getNowPrice() {
		return nowPrice;
	}

	/**
	 * @param nowPrice
	 *            the nowPrice to set
	 */
	public void setNowPrice(String nowPrice) {
		this.nowPrice = nowPrice;
	}

	/**
	 * @return the oldPrice
	 */
	public String getOldPrice() {
		return oldPrice;
	}

	/**
	 * @param oldPrice
	 *            the oldPrice to set
	 */
	public void setOldPrice(String oldPrice) {
		this.oldPrice = oldPrice;
	}

	/**
	 * @return the goodRate
	 */
	public String getGoodRate() {
		return goodRate;
	}

	/**
	 * @param goodRate
	 *            the goodRate to set
	 */
	public void setGoodRate(String goodRate) {
		this.goodRate = goodRate;
	}

	/**
	 * @return the disTance
	 */
	public String getDisTance() {
		return disTance;
	}

	/**
	 * @param disTance
	 *            the disTance to set
	 */
	public void setDisTance(String disTance) {
		this.disTance = disTance;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Service [bandUrl=" + bandUrl + ", company=" + company
				+ ", area=" + area + ", kind=" + kind + ", nowPrice="
				+ nowPrice + ", oldPrice=" + oldPrice + ", goodRate="
				+ goodRate + ", disTance=" + disTance + "]";
	}

}
