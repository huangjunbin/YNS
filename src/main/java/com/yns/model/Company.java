package com.yns.model;

import java.io.Serializable;

public class Company implements Serializable {

 
	private static final long serialVersionUID = 1L;
	private String deptName;
	private String remark;
	private String describe;
	private String companyType;
	private String fkId;
	private String industry;
	private String type;
	private String deptId;
	private String appMemberFavoriteInfoCreateDate;
	private String appMemberFavoriteInfoId;
	private String isFavorite;

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe;
	}

	public String getCompanyType() {
		return companyType;
	}

	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}

	public String getFkId() {
		return fkId;
	}

	public void setFkId(String fkId) {
		this.fkId = fkId;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDeptId() {
		return deptId;
	}

	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}

	public String getAppMemberFavoriteInfoCreateDate() {
		return appMemberFavoriteInfoCreateDate;
	}

	public void setAppMemberFavoriteInfoCreateDate(
			String appMemberFavoriteInfoCreateDate) {
		this.appMemberFavoriteInfoCreateDate = appMemberFavoriteInfoCreateDate;
	}

	public String getAppMemberFavoriteInfoId() {
		return appMemberFavoriteInfoId;
	}

	public void setAppMemberFavoriteInfoId(String appMemberFavoriteInfoId) {
		this.appMemberFavoriteInfoId = appMemberFavoriteInfoId;
	}

	public String getIsFavorite() {
		return isFavorite;
	}

	public void setIsFavorite(String isFavorite) {
		this.isFavorite = isFavorite;
	}

	@Override
	public String toString() {
		return "Company [deptName=" + deptName + ", remark=" + remark
				+ ", describe=" + describe + ", companyType=" + companyType
				+ ", fkId=" + fkId + ", industry=" + industry + ", type="
				+ type + ", deptId=" + deptId
				+ ", appMemberFavoriteInfoCreateDate="
				+ appMemberFavoriteInfoCreateDate
				+ ", appMemberFavoriteInfoId=" + appMemberFavoriteInfoId
				+ ", isFavorite=" + isFavorite + "]";
	}

}
