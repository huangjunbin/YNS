package com.yns.model;

public class SubscribeSet {
	private String msgTypeId;// 订阅信息类别id
	private String msgTypeName;// 订阅信息类别名称
	private String flag;// 是否订阅，0否，1是

	public String getMsgTypeId() {
		return msgTypeId;
	}

	public void setMsgTypeId(String msgTypeId) {
		this.msgTypeId = msgTypeId;
	}

	public String getMsgTypeName() {
		return msgTypeName;
	}

	public void setMsgTypeName(String msgTypeName) {
		this.msgTypeName = msgTypeName;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	@Override
	public String toString() {
		return "SubscribeSet [msgTypeId=" + msgTypeId + ", msgTypeName="
				+ msgTypeName + ", flag=" + flag + "]";
	}

}
