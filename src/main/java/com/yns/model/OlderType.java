package com.yns.model;

import java.io.Serializable;

public class OlderType implements Serializable {
	private static final long serialVersionUID = -8287591277120071398L;
	private int olderType;// 订单类型

	/**
	 * @return the olderType
	 */
	public int getOlderType() {
		return olderType;
	}

	/**
	 * @param olderType
	 *            the olderType to set
	 */
	public void setOlderType(int olderType) {
		this.olderType = olderType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "OlderType [olderType=" + olderType + "]";
	}

}
