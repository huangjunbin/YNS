package com.yns.model;


public class ProductType {
   private String name;
    private String sortId;
    private int index;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSortId() {
        return sortId;
    }

    public void setSortId(String sortId) {
        this.sortId = sortId;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public String toString() {
        return "ProductType{" +
                "name='" + name + '\'' +
                ", sortId='" + sortId + '\'' +
                ", index=" + index +
                '}';
    }
}
