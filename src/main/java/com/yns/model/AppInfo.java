package com.yns.model;

public class AppInfo {
	private String appContent;
	private String appName;
	private String appSourceId;
	private String appTitle;
	private String appType;
	private String appUrl;
	private String createDate;
	private String flag;
	private String lastupDat;
	private String remark;
	private String version_num;

	public String getAppContent() {
		return appContent;
	}

	public void setAppContent(String appContent) {
		this.appContent = appContent;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getAppSourceId() {
		return appSourceId;
	}

	public void setAppSourceId(String appSourceId) {
		this.appSourceId = appSourceId;
	}

	public String getAppTitle() {
		return appTitle;
	}

	public void setAppTitle(String appTitle) {
		this.appTitle = appTitle;
	}

	public String getAppType() {
		return appType;
	}

	public void setAppType(String appType) {
		this.appType = appType;
	}

	public String getAppUrl() {
		return appUrl;
	}

	public void setAppUrl(String appUrl) {
		this.appUrl = appUrl;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getLastupDat() {
		return lastupDat;
	}

	public void setLastupDat(String lastupDat) {
		this.lastupDat = lastupDat;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Double getVersion_num() {
		return Double.parseDouble(version_num);
	}

	public void setVersion_num(String version_num) {
		this.version_num = version_num;
	}

	@Override
	public String toString() {
		return "AppInfo [appContent=" + appContent + ", appName=" + appName
				+ ", appSourceId=" + appSourceId + ", appTitle=" + appTitle
				+ ", appType=" + appType + ", appUrl=" + appUrl
				+ ", createDate=" + createDate + ", flag=" + flag
				+ ", lastupDat=" + lastupDat + ", remark=" + remark
				+ ", version_num=" + version_num + "]";
	}

}
