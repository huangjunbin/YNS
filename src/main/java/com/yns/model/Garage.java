package com.yns.model;

import java.io.Serializable;

public class Garage implements Serializable {
	
	private static final long serialVersionUID = -2308008681562913177L;
	
	private String userId;//用户ID
	private String carId;//靓车ID
	private String carBland;//靓车品牌
	private String carNum;//靓车车牌号
	private String buyAddress;//购买4s店
	private String buyTime;//购买时间
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getCarId() {
		return carId;
	}
	public void setCarId(String carId) {
		this.carId = carId;
	}
	public String getCarBland() {
		return carBland;
	}
	public void setCarBland(String carBland) {
		this.carBland = carBland;
	}
	public String getCarNum() {
		return carNum;
	}
	public void setCarNum(String carNum) {
		this.carNum = carNum;
	}
	public String getBuyAddress() {
		return buyAddress;
	}
	public void setBuyAddress(String buyAddress) {
		this.buyAddress = buyAddress;
	}
	public String getBuyTime() {
		return buyTime;
	}
	public void setBuyTime(String buyTime) {
		this.buyTime = buyTime;
	}
	@Override
	public String toString() {
		return "Garage [userId=" + userId + ", carId=" + carId + ", carBland="
				+ carBland + ", carNum=" + carNum + ", buyAddress="
				+ buyAddress + ", buyTime=" + buyTime + "]";
	}
	
}
