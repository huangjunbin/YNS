package com.yns.model;

import java.io.Serializable;
import java.util.List;

public class TradeType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String sortId;// 分类Id
	private String name;// 分类名称
	private List<ChildType> sort;

	public String getSortId() {
		return sortId;
	}

	public void setSortId(String sortId) {
		this.sortId = sortId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ChildType> getSort() {
		return sort;
	}

	public void setSort(List<ChildType> sort) {
		this.sort = sort;
	}

	public class ChildType implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private String sonSortId; // 子分类Id
		private String name; // 子分类名称

		public String getSonSortId() {
			return sonSortId;
		}

		public void setSonSortId(String sonSortId) {
			this.sonSortId = sonSortId;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

	}

	@Override
	public String toString() {
		return "TradeType [sortId=" + sortId + ", name=" + name + ", sort="
				+ sort + "]";
	}

}
