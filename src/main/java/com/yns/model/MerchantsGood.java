package com.yns.model;

import java.io.Serializable;

public class MerchantsGood implements Serializable {
	private static final long serialVersionUID = 5771617870003195242L;
	private String goodsId;// 商品ID
	private String merchantsId;// 商家ID
	private String industryId;// 行业类别id
	private String serviceId;// 服务类别ID
	private String serviceName;// 服务类别名称
	private String goodsName;// 商品名称
	private String oldPrice;// 原价
	private String nowPrice;// 现价
	private String distance;// 距离
	private String goodComment;// 好评率
	private String cityId;// 城市ID
	private String areaId;// 区域ID
	private String areaName;// 区域名称
	private String logoUrl;// 商家图片
	private String longitude;// 经度
	private String latitude;// 纬度

	private String merchantsName;// 商家店铺名称
	private String merchantsAddress;// 商家地址
	private String linkCellphone;// 联系手机
	private String linkPhone;// 联系电话
	private String num;// 商品次数
	private String serviceProcess;// 服务流程
	private String hour;// 时长
	private String detailInfo;// 购买须知
	private String starttime;// 开始时间
	private String endtime;// 结束时间

	public String getGoodsId() {
		return goodsId;
	}

	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}

	public String getMerchantsId() {
		return merchantsId;
	}

	public void setMerchantsId(String merchantsId) {
		this.merchantsId = merchantsId;
	}

	public String getIndustryId() {
		return industryId;
	}

	public void setIndustryId(String industryId) {
		this.industryId = industryId;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public String getOldPrice() {
		return oldPrice;
	}

	public void setOldPrice(String oldPrice) {
		this.oldPrice = oldPrice;
	}

	public String getNowPrice() {
		return nowPrice;
	}

	public void setNowPrice(String nowPrice) {
		this.nowPrice = nowPrice;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public String getGoodComment() {
		return goodComment;
	}

	public void setGoodComment(String goodComment) {
		this.goodComment = goodComment;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getMerchantsName() {
		return merchantsName;
	}

	public void setMerchantsName(String merchantsName) {
		this.merchantsName = merchantsName;
	}

	public String getMerchantsAddress() {
		return merchantsAddress;
	}

	public void setMerchantsAddress(String merchantsAddress) {
		this.merchantsAddress = merchantsAddress;
	}

	public String getLinkCellphone() {
		return linkCellphone;
	}

	public void setLinkCellphone(String linkCellphone) {
		this.linkCellphone = linkCellphone;
	}

	public String getLinkPhone() {
		return linkPhone;
	}

	public void setLinkPhone(String linkPhone) {
		this.linkPhone = linkPhone;
	}

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}

	public String getServiceProcess() {
		return serviceProcess;
	}

	public void setServiceProcess(String serviceProcess) {
		this.serviceProcess = serviceProcess;
	}

	public String getHour() {
		return hour;
	}

	public void setHour(String hour) {
		this.hour = hour;
	}

	public String getDetailInfo() {
		return detailInfo;
	}

	public void setDetailInfo(String detailInfo) {
		this.detailInfo = detailInfo;
	}

	public String getStarttime() {
		return starttime;
	}

	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}

	public String getEndtime() {
		return endtime;
	}

	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}

	@Override
	public String toString() {
		return "MerchantsGood [goodsId=" + goodsId + ", merchantsId="
				+ merchantsId + ", industryId=" + industryId + ", serviceId="
				+ serviceId + ", serviceName=" + serviceName + ", goodsName="
				+ goodsName + ", oldPrice=" + oldPrice + ", nowPrice="
				+ nowPrice + ", distance=" + distance + ", goodComment="
				+ goodComment + ", cityId=" + cityId + ", areaId=" + areaId
				+ ", areaName=" + areaName + ", logoUrl=" + logoUrl
				+ ", longitude=" + longitude + ", latitude=" + latitude
				+ ", merchantsName=" + merchantsName + ", merchantsAddress="
				+ merchantsAddress + ", linkCellphone=" + linkCellphone
				+ ", linkPhone=" + linkPhone + ", num=" + num
				+ ", serviceProcess=" + serviceProcess + ", hour=" + hour
				+ ", detailInfo=" + detailInfo + ", starttime=" + starttime
				+ ", endtime=" + endtime + "]";
	}

}
