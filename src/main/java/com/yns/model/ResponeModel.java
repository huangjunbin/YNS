package com.yns.model;

import org.json.JSONException;
import org.json.JSONObject;

import android.text.TextUtils;

import com.yns.util.JsonUtil;
import com.yns.util.StringUtils;

/**
 * @className：ResponeModel.java
 * @author: allen
 * @Function: 网络响应模型
 * @createDate: 2014-8-29
 * @update:
 */
public class ResponeModel {
	private Object resultObj;// 转换的Json
	private JSONObject dataResult;// 转化为对象的json
	private String json;// 服务器返回的整体Json
	private Integer code = 0;// 错误码
	private boolean status;// 状态
	private String msg = "";// 错误信息
	private String result = "";// 数据
	private int totalCount;// 总条数
	private Class<?> cls;// 请求转换数据模型类
	private String easyName;// 直接解析的名字
	private boolean isList = false;// 是否是数组,默认是对象
	private String listCountKey = "totalCount";// 获取总条数的key
	private boolean isNew = true;// 是否新的解析
    private int buyCount;

	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}

	public String getListCountKey() {
		return listCountKey;
	}

	public void setListCountKey(String listCountKey) {
		this.listCountKey = listCountKey;
	}

	public boolean isList() {
		return isList;
	}

	public void setList(boolean isList) {
		this.isList = isList;
	}

	public String getEasyName() {
		return easyName;
	}

	public void setEasyName(String easyName) {
		this.easyName = easyName;
	}

	public Class<?> getCls() {
		return cls;
	}

	public void setCls(Class<?> cls) {
		this.cls = cls;
	}

	public Object getResultObj() {
		return resultObj;
	}

	public void setResultObj(Object resultObj) {
		this.resultObj = resultObj;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public JSONObject getDataResult() {
		return dataResult;
	}

	public void setDataResult(JSONObject dataResult) {
		this.dataResult = dataResult;
	}

	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		if (StringUtils.isEmpty(msg)) {
			this.msg = "服务器发生未知错误！";
		} else {
			this.msg = msg;
		}
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}



	/**
	 * 初始化所有数据
	 */
	public void init() {

		initResult();
	}

	/**
	 * 获取某一字段
	 * 
	 * @return
	 */
	public String parseByName(String name) {
		try {
			if (!getDataResult().isNull(name)
					&& !TextUtils
							.isEmpty((getDataResult().get(name).toString()))) {
				return (getDataResult().get(name).toString());
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
		return "";
	}

	public JSONObject parseJsonToJSONObject(String json) {
		try {
			return new JSONObject(json);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new JSONObject();
		}
	}

	/**
	 * 获取某一字段
	 * 
	 * @return
	 */
	public String parseByName(JSONObject object, String name) {
		try {
			if (!object.isNull(name)
					&& !TextUtils.isEmpty((object.get(name).toString()))) {
				return (object.get(name).toString());
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
		return "";
	}

	/**
	 * 处理返回的结果，做一些简单的处理
	 */
	private void initResult() {
		if (isStatus()) {
			if (getResult() != null && getResult().length() > 0) {
				String Head = getResult().substring(0, 1);
				// 注：自动快捷解析的情况
				// 1.data直接是对象
				// 2.data直接是数组（不含条数）
				// 3.data直接是对象，对象里面包含“result”数组（条数可选）
				// 其它情况均需返回对象，在你的service中进行处理

				// Data是对象
				if ("{".equals(Head)) {
					JSONObject object;
					try {
						object = new JSONObject(getResult());

						setDataResult(object);
						if (isList) {
							// 带分页
							setResultObj(JsonUtil.convertJsonToList(
									object.get(getEasyName()).toString(),
									getCls()));
							if (!object.isNull(getListCountKey())) {// list
								// 带总条数
								try {
									setTotalCount(Integer.parseInt(object
											.getString(getListCountKey())));
								} catch (Exception e) {
									// TODO: handle exception
									setTotalCount(0);
								}
								// 设置其他分页信息
							}
							return;
						}

						if (isNew()) {
							setResultObj(JsonUtil.convertJsonToObject(object
									.get(getEasyName()).toString(), getCls()));
						} else {
							setResultObj(JsonUtil.convertJsonToObject(
									getResult(), getCls()));
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

				} else if ("[".equals(Head)) {// data是数组
					setResultObj(JsonUtil.convertJsonToList(getResult(),
							getCls()));
				}
			}
		}
	}

    public int getBuyCount() {
        return buyCount;
    }

    public void setBuyCount(int buyCount) {
        this.buyCount = buyCount;
    }

    @Override
    public String toString() {
        return "ResponeModel{" +
                "resultObj=" + resultObj +
                ", dataResult=" + dataResult +
                ", json='" + json + '\'' +
                ", code=" + code +
                ", status=" + status +
                ", msg='" + msg + '\'' +
                ", result='" + result + '\'' +
                ", totalCount=" + totalCount +
                ", cls=" + cls +
                ", easyName='" + easyName + '\'' +
                ", isList=" + isList +
                ", listCountKey='" + listCountKey + '\'' +
                ", isNew=" + isNew +
                ", buyCount=" + buyCount +
                '}';
    }
}
