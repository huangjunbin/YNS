package com.yns.model;

import java.io.Serializable;

public class Benfit implements Serializable {

	private static final long serialVersionUID = 6093547574018817109L;

	private String topupId;// 优惠ID
	private String activityMoney;// 优惠金额
	private String resume;// 消耗的积分 或者金额
	private String insume;// 获得的金额
	private String type;// 充值类型。1积分兑换，2充值
	private String startDate;// 有效起始日期
	private String endDate;// 有效结束日期
	private String benfitDetail;

	public String getTopupId() {
		return topupId;
	}

	public void setTopupId(String topupId) {
		this.topupId = topupId;
	}

	public String getActivityMoney() {
		return activityMoney;
	}

	public void setActivityMoney(String activityMoney) {
		this.activityMoney = activityMoney;
	}

	public String getResume() {
		return resume;
	}

	public void setResume(String resume) {
		this.resume = resume;
	}

	public String getInsume() {
		return insume;
	}

	public void setInsume(String insume) {
		this.insume = insume;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getBenfitDetail() {
		return benfitDetail;
	}

	public void setBenfitDetail(String benfitDetail) {
		this.benfitDetail = benfitDetail;
	}
	
}
