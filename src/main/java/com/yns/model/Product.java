package com.yns.model;


import java.io.Serializable;

public class Product implements Serializable{
    private String appProductInfoId;//Id
    private String name;//名称
    private String pic;//图片
    private double tagPrice;//		吊牌价（标签价、原价）
    private double salePrice;//		销售价格
    private float discount;//	折扣
    private String discountPrice;//	折扣价格
    private String description;//		商品描述
    private String content;//商品详情
    private int ordersSum;//已经购买数量
    private String inventory;//	库存
    private String flag;//产品标记（团购：groupBuying，生日专属：birthday）
    private String createDate;//创建时间

    public String getAppProductInfoId() {
        return appProductInfoId;
    }

    public void setAppProductInfoId(String appProductInfoId) {
        this.appProductInfoId = appProductInfoId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public double getTagPrice() {
        return tagPrice;
    }

    public void setTagPrice(double tagPrice) {
        this.tagPrice = tagPrice;
    }

    public double getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(double salePrice) {
        this.salePrice = salePrice;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public String getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(String discountPrice) {
        this.discountPrice = discountPrice;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getOrdersSum() {
        return ordersSum;
    }

    public void setOrdersSum(int ordersSum) {
        this.ordersSum = ordersSum;
    }

    public String getInventory() {
        return inventory;
    }

    public void setInventory(String inventory) {
        this.inventory = inventory;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    @Override
    public String toString() {
        return "Product{" +
                "appProductInfoId='" + appProductInfoId + '\'' +
                ", name='" + name + '\'' +
                ", pic='" + pic + '\'' +
                ", tagPrice=" + tagPrice +
                ", salePrice=" + salePrice +
                ", discount=" + discount +
                ", discountPrice='" + discountPrice + '\'' +
                ", description='" + description + '\'' +
                ", content='" + content + '\'' +
                ", ordersSum=" + ordersSum +
                ", inventory='" + inventory + '\'' +
                ", flag='" + flag + '\'' +
                ", createDate='" + createDate + '\'' +
                '}';
    }
}
