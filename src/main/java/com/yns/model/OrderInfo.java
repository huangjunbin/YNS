package com.yns.model;

import java.io.Serializable;
import java.util.List;

public class OrderInfo implements Serializable {

   private String appOrderInfoId;//订单Id
   private  String status;//状态0等待支付/1等待发货（已支付）/2已发货（等待确认）/3交易完成 /100交易关闭
   private  String flag;//订单标记(product商品，resumeCount简历阅读卷)
   private  String createDate;//创建日期
    private String paymentDate;//支付日期 如未支付则不存在

   private  double amount;//支付总额

    private List<OrderDetailInfo> orderItems;

    private String  name		;//名称
    private String  isDefault	;//是否默认 1 是 0 否
    private String  mobile		;//手机或者电话
    private String   sheng		;//	省份名称
    private String  shi		;//市名称
    private String  xian		;//县区名称
    private String  xiang		;//镇村或街道名称
    private String  address		;//详细地址

    public String getAppOrderInfoId() {
        return appOrderInfoId;
    }

    public void setAppOrderInfoId(String appOrderInfoId) {
        this.appOrderInfoId = appOrderInfoId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public List<OrderDetailInfo> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderDetailInfo> orderItems) {
        this.orderItems = orderItems;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(String isDefault) {
        this.isDefault = isDefault;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getSheng() {
        return sheng;
    }

    public void setSheng(String sheng) {
        this.sheng = sheng;
    }

    public String getShi() {
        return shi;
    }

    public void setShi(String shi) {
        this.shi = shi;
    }

    public String getXian() {
        return xian;
    }

    public void setXian(String xian) {
        this.xian = xian;
    }

    public String getXiang() {
        return xiang;
    }

    public void setXiang(String xiang) {
        this.xiang = xiang;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "OrderInfo{" +
                "appOrderInfoId='" + appOrderInfoId + '\'' +
                ", status='" + status + '\'' +
                ", flag='" + flag + '\'' +
                ", createDate='" + createDate + '\'' +
                ", paymentDate='" + paymentDate + '\'' +
                ", amount=" + amount +
                ", orderItems=" + orderItems +
                ", name='" + name + '\'' +
                ", isDefault='" + isDefault + '\'' +
                ", mobile='" + mobile + '\'' +
                ", sheng='" + sheng + '\'' +
                ", shi='" + shi + '\'' +
                ", xian='" + xian + '\'' +
                ", xiang='" + xiang + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
