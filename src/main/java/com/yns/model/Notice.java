package com.yns.model;

public class Notice {

	private String noticeId;// Id
	private String userName;// 作者名称
	private String deptName;// 部门名称
	private String title;// 标题
	private String content;// 内容
	private String createDate;// 创建时间
	private String type;// job面试通知/apply申请通知/sys系统通知

	public String getNoticeId() {
		return noticeId;
	}

	public void setNoticeId(String noticeId) {
		this.noticeId = noticeId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Notice [noticeId=" + noticeId + ", userName=" + userName
				+ ", deptName=" + deptName + ", title=" + title + ", content="
				+ content + ", createDate=" + createDate + ", type=" + type
				+ "]";
	}

}
