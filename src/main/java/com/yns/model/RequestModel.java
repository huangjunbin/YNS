package com.yns.model;

import com.yns.net.http.RequestParams;

/**
 * RequestModel.java
 * 
 * @author: allen
 * @Function: 网络请求模型
 * @createDate: 2014-8-29
 * @update:
 */
public class RequestModel {
	private String url;// 请求网络地址
	private RequestParams params;// 请求参数
	private Class<?> cls;// 请求转换数据模型类
	private boolean showDialog = true;// 是否显示网络加载对话框
	private boolean showErrorMessage = false;// 是否显示错误的信息
	private String easyName = "result";// 直接解析的名字
	private boolean isList = false;// 是否是数组,默认是对象
	private String listCountKey = "totalCount";// 获取总条数的key
	private boolean isNew = true;// 是否新的解析
	public boolean isList() {
		return isList;
	}

	public void setList(boolean isList) {
		this.isList = isList;
	}

	public String getListCountKey() {
		return listCountKey;
	}

	public void setListCountKey(String listCountKey) {
		this.listCountKey = listCountKey;
	}

	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}

	public String getEasyName() {
		return easyName;
	}

	public void setEasyName(String easyName) {
		this.easyName = easyName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public RequestParams getParams() {
		return params;
	}

	public void setParams(RequestParams params) {
		this.params = params;
	}

	public Class<?> getCls() {
		return cls;
	}

	public void setCls(Class<?> cls) {
		this.cls = cls;
	}

	public boolean isShowDialog() {
		return showDialog;
	}

	public void setShowDialog(boolean showDialog) {
		this.showDialog = showDialog;
	}

	public boolean isShowErrorMessage() {
		return showErrorMessage;
	}

	public void setShowErrorMessage(boolean showErrorMessage) {
		this.showErrorMessage = showErrorMessage;
	}

    @Override
    public String toString() {
        return "RequestModel{" +
                "url='" + url + '\'' +
                ", params=" + params +
                ", cls=" + cls +
                ", showDialog=" + showDialog +
                ", showErrorMessage=" + showErrorMessage +
                ", easyName='" + easyName + '\'' +
                ", isList=" + isList +
                ", listCountKey='" + listCountKey + '\'' +
                ", isNew=" + isNew +
                '}';
    }
}
