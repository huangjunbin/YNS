package com.yns.model;

import java.io.Serializable;
import java.util.List;

public class JobType implements Serializable {

	private static final long serialVersionUID = 1L;
	private String id;
	private boolean isCheck = false;
	private String name;
	private List<JobType> childs;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isCheck() {
		return isCheck;
	}

	public void setCheck(boolean isCheck) {
		this.isCheck = isCheck;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<JobType> getChilds() {
		return childs;
	}

	public void setChilds(List<JobType> childs) {
		this.childs = childs;
	}

	@Override
	public String toString() {
		return "JobType [id=" + id + ", isCheck=" + isCheck + ", name=" + name
				+ ", childs=" + childs + "]";
	}

}
