package com.yns.model;

/**
 * @className：HomeKind.java
 * @author: lmt
 * @Function: 首页面模型
 * @createDate: 2014-12-2 下午1:50:44
 * @update:
 */
public class HomeKind {
	private int icon_Id;// 图标id
	private int des_Id;// 描述id
	private int type_id;

	public HomeKind(int iconId, int desId) {
		icon_Id = iconId;
		des_Id = desId;
	}

	/**
	 * @return the icon_Id
	 */
	public int getIcon_Id() {
		return icon_Id;
	}

	/**
	 * @param icon_Id
	 *            the icon_Id to set
	 */
	public void setIcon_Id(int icon_Id) {
		this.icon_Id = icon_Id;
	}

	/**
	 * @return the des_Id
	 */
	public int getDes_Id() {
		return des_Id;
	}

	/**
	 * @param des_Id
	 *            the des_Id to set
	 */
	public void setDes_Id(int des_Id) {
		this.des_Id = des_Id;
	}

	@Override
	public String toString() {
		return "HomeKind [icon_Id=" + icon_Id + ", des_Id=" + des_Id
				+ ", type_id=" + type_id + "]";
	}

	public int getType_id() {
		return type_id;
	}

	public void setType_id(int type_id) {
		this.type_id = type_id;
	}

}
