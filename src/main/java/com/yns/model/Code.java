package com.yns.model;

public class Code {
	private String createDate;
	private String flag;
	private String orderCode;
	private String orderCodeId;
	private String orderId;

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public String getOrderCodeId() {
		return orderCodeId;
	}

	public void setOrderCodeId(String orderCodeId) {
		this.orderCodeId = orderCodeId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	@Override
	public String toString() {
		return "Code [createDate=" + createDate + ", flag=" + flag
				+ ", orderCode=" + orderCode + ", orderCodeId=" + orderCodeId
				+ ", orderId=" + orderId + "]";
	}

}
