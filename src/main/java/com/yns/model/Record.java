package com.yns.model;

import java.io.Serializable;

public class Record implements Serializable {
	private static final long serialVersionUID = 2243601742546068538L;
	private String id;// 充值ID
	private String money;// 充值金额
	private String allMoney;// 总金额
	private String createDate;// 日期
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMoney() {
		return money;
	}
	public void setMoney(String money) {
		this.money = money;
	}
	public String getAllMoney() {
		return allMoney;
	}
	public void setAllMoney(String allMoney) {
		this.allMoney = allMoney;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

}
