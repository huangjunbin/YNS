package com.yns.model;

import java.io.Serializable;

public class Advert implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String appBannerInfoId;
	private String title;
	private String pic;
	private String links;
	private String appBannerInfoCreateDate;

	public String getAppBannerInfoId() {
		return appBannerInfoId;
	}

	public void setAppBannerInfoId(String appBannerInfoId) {
		this.appBannerInfoId = appBannerInfoId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public String getLinks() {
		return links;
	}

	public void setLinks(String links) {
		this.links = links;
	}

	public String getAppBannerInfoCreateDate() {
		return appBannerInfoCreateDate;
	}

	public void setAppBannerInfoCreateDate(String appBannerInfoCreateDate) {
		this.appBannerInfoCreateDate = appBannerInfoCreateDate;
	}

	@Override
	public String toString() {
		return "Advert [appBannerInfoId=" + appBannerInfoId + ", title="
				+ title + ", pic=" + pic + ", links=" + links
				+ ", appBannerInfoCreateDate=" + appBannerInfoCreateDate + "]";
	}

}
