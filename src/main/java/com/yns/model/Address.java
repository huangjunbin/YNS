package com.yns.model;

import java.io.Serializable;

public class Address implements Serializable {

    private static final long serialVersionUID = 1L;
    private String consigneeInfoId;//收货地址id
    private String userId;//必填	用户id
    private  boolean isDefault;//必填	是否默认 1 是 0 否
    private String name;//名称
    private  String mobile;//手机
    private String sheng;//省份名称
    private  String shi;//市名称
    private  String xian;//县区名称
    private  String xiang;//镇村或者街道名称
    private  String address;//详细地址

    public String getConsigneeInfoId() {
        return consigneeInfoId;
    }

    public void setConsigneeInfoId(String consigneeInfoId) {
        this.consigneeInfoId = consigneeInfoId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getSheng() {
        return sheng;
    }

    public void setSheng(String sheng) {
        this.sheng = sheng;
    }

    public String getShi() {
        return shi;
    }

    public void setShi(String shi) {
        this.shi = shi;
    }

    public String getXian() {
        return xian;
    }

    public void setXian(String xian) {
        this.xian = xian;
    }

    public String getXiang() {
        return xiang;
    }

    public void setXiang(String xiang) {
        this.xiang = xiang;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public void setIsDefault(boolean isDefault) {
        this.isDefault = isDefault;
    }

    @Override
    public String toString() {
        return "Address{" +
                "consigneeInfoId='" + consigneeInfoId + '\'' +
                ", userId='" + userId + '\'' +
                ", isDefault=" + isDefault +
                ", name='" + name + '\'' +
                ", mobile='" + mobile + '\'' +
                ", sheng='" + sheng + '\'' +
                ", shi='" + shi + '\'' +
                ", xian='" + xian + '\'' +
                ", xiang='" + xiang + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
