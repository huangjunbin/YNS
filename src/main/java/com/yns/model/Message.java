package com.yns.model;

public class Message {
	private String author;
	private String title;
	private String appArticleInfoId;
	private String description;
	private String argeeCount;
	private String createDate;
	private String sortName;
	private String frontCoverPic;
	private String content;
	private String isFavorite; // 1 为已收藏 0为没有收藏
	private String fkId;//
	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAppArticleInfoId() {
		return appArticleInfoId;
	}

	public void setAppArticleInfoId(String appArticleInfoId) {
		this.appArticleInfoId = appArticleInfoId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getArgeeCount() {
		return argeeCount;
	}

	public void setArgeeCount(String argeeCount) {
		this.argeeCount = argeeCount;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getSortName() {
		return sortName;
	}

	public void setSortName(String sortName) {
		this.sortName = sortName;
	}

	public String getFrontCoverPic() {
		return frontCoverPic;
	}

	public void setFrontCoverPic(String frontCoverPic) {
		this.frontCoverPic = frontCoverPic;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getIsFavorite() {
		return isFavorite;
	}

	public void setIsFavorite(String isFavorite) {
		this.isFavorite = isFavorite;
	}

	public String getFkId() {
		return fkId;
	}

	public void setFkId(String fkId) {
		this.fkId = fkId;
	}

	@Override
	public String toString() {
		return "Message [author=" + author + ", title=" + title
				+ ", appArticleInfoId=" + appArticleInfoId + ", description="
				+ description + ", argeeCount=" + argeeCount + ", createDate="
				+ createDate + ", sortName=" + sortName + ", frontCoverPic="
				+ frontCoverPic + ", content=" + content + ", isFavorite="
				+ isFavorite + ", fkId=" + fkId + "]";
	}

 

}
