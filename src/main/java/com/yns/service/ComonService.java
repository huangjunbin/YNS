package com.yns.service;

import android.content.Context;

import com.yns.app.AppContext;
import com.yns.model.Advert;
import com.yns.model.Comment;
import com.yns.model.Job;
import com.yns.model.Market;
import com.yns.model.Message;
import com.yns.model.Notice;
import com.yns.model.RequestModel;
import com.yns.model.Resume;
import com.yns.model.TradeType;
import com.yns.net.Urls;
import com.yns.net.http.CustomAsyncHttpClient;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.net.http.RequestParams;

import java.io.File;
import java.io.FileNotFoundException;

public class ComonService {
    String TAG = "ComonService";
    private static CustomAsyncHttpClient httpClient;
    static ComonService mInstance;

    public static ComonService getInstance(Context context) {
        httpClient = new CustomAsyncHttpClient(context);
        if (mInstance == null) {
            // 只有第一次才彻底执行这里的代码
            synchronized (ComonService.class) {
                // 再检查一次
                if (mInstance == null)
                    mInstance = new ComonService();
            }
        }
        return mInstance;
    }

    /**
     * 上传文件
     *
     * @param file 文件
     */
    public void uploadImg(File file, final CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        RequestParams params = new RequestParams();
        params.put("suffix", "jpg");
        try {
            params.put("fileData", file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        requestModel.setParams(params);
        requestModel.setUrl(Urls.UPLOADIMG);
        httpClient.post(requestModel, handler);
    }

    // 获取新闻列表
    public void getMessageList(String rows, String page, String sortId,
                               String title, CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Message.class);
        RequestParams params = new RequestParams();
        params.put("rows", rows);
        params.put("page", page);
        params.put("title", title);
        params.put("sortId", sortId);
        params.put("userId", AppContext.currentUser == null ? ""
                : AppContext.currentUser.getUserId());
        requestModel.setParams(params);
        requestModel.setUrl(Urls.GETMESSAGELIST);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    public void addCollect(String userId, String fkId, String type,
                           CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Message.class);
        RequestParams params = new RequestParams();
        params.put("userId", userId);
        params.put("fkId", fkId);
        params.put("type", type);
        requestModel.setParams(params);
        requestModel.setShowErrorMessage(true);
        requestModel.setUrl(Urls.ADDCOLLECT);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    public void addEnterpriseFavorite(String userId, String fkId, String type,
                                      CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Message.class);
        RequestParams params = new RequestParams();
        params.put("userId", userId);
        params.put("fkId", fkId);
        params.put("type", type);
        requestModel.setParams(params);
        requestModel.setShowErrorMessage(true);
        requestModel.setUrl(Urls.addEnterpriseFavorite);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    public void removeCollect(String userId, String fkId,
                              CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Message.class);
        RequestParams params = new RequestParams();
        params.put("userId", userId);
        params.put("fkId", fkId);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.REMOVECOLLECT);
        requestModel.setShowErrorMessage(true);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    public <T> void getCollect(String rows, String page, String userId,
                               String type, Class<T> cls, boolean showDialog,
                               CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(cls);
        RequestParams params = new RequestParams();
        params.put("rows", rows);
        params.put("page", page);
        params.put("type", type);
        params.put("userId", userId);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.GETCOLLECT);
        requestModel.setShowDialog(showDialog);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    /**
     * rows Integer 每页条数 page Integer 当前页 keyWordName String 关键字 workType String
     * 职位类型 例如 全职&兼职,全职,兼职,实习 gongZuodidian String 工作地点 gongZuoJingYan Long 工作经验
     * 82 不限, 83 应届生, 84 1-3年, 85 3-5年, 86 5-10年, 87 10年以上, 88 20年以上, 89 30年以上 ,
     * 90 退休人员 jobSort Long 行业类型
     * <p/>
     * userId String 用户id
     */
    public void getJob(String rows, String page, String userId,
                       String keyWordName, String workType, String gongZuodidian,
                       String gongZuoJingYan, String jobSort, int flag, String jobPortSort,
                       CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Job.class);
        RequestParams params = new RequestParams();
        params.put("rows", rows);
        params.put("page", page);
        params.put("keyWordName", keyWordName);
        params.put("userId", userId);
        params.put("keyWordName", keyWordName);
        params.put("workType", workType);
        params.put("gongZuodidian", gongZuodidian);
        params.put("gongZuoJingYan", gongZuoJingYan);
        params.put("jobSort", jobSort);
        params.put("jobPortSort", jobPortSort);
        if (flag == 0) {
            params.put("flag", "zczp");
        }
        if (flag == 1) {
            params.put("flag", "jyzp");
        }
        if (flag == 2) {
            params.put("flag", "normal");
        }
        if (flag == 3) {
            params.put("flag", "qgjx");
        }

        requestModel.setParams(params);
        requestModel.setUrl(Urls.GETJOB);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    public void applyJob(String userId, String jobInfoId,
                         CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        RequestParams params = new RequestParams();
        params.put("userId", userId);
        params.put("jobInfoId", jobInfoId);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.APPLYJOB);
        requestModel.setShowErrorMessage(true);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    public void getApply(String rows, String page, String userId,
                         CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Job.class);
        RequestParams params = new RequestParams();
        params.put("rows", rows);
        params.put("page", page);
        params.put("userId", userId);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.GETAPPLY);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    public void getTradeType(CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(TradeType.class);
        RequestParams params = new RequestParams();
        requestModel.setParams(params);
        requestModel.setUrl(Urls.GETTRADETYPE);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    public void getNotice(String rows, String page, String userId,
                          CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Notice.class);
        RequestParams params = new RequestParams();
        params.put("rows", rows);
        params.put("page", page);
        params.put("userId", userId);
        requestModel.setParams(params);
        if (AppContext.userType == 1) {
            requestModel.setUrl(Urls.GETNOTICE);
        } else {
            requestModel.setUrl(Urls.selectEnterpriseNoticeByCondition);
        }
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    public void getResume(String rows, String page, String userId,
                          CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Resume.class);
        RequestParams params = new RequestParams();
        params.put("rows", rows);
        params.put("page", page);
        params.put("userId", userId);
        requestModel.setParams(params);
        requestModel.setShowDialog(false);
        requestModel.setUrl(Urls.GETRESUME);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    public void getResumeByEnterpriseId(String rows, String page, String userId,
                                        CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Resume.class);
        RequestParams params = new RequestParams();
        params.put("rows", rows);
        params.put("page", page);
        params.put("userId", userId);
        requestModel.setParams(params);
        requestModel.setShowDialog(false);
        requestModel.setUrl(Urls.getResumeByEnterpriseId);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    public void setResumeDefault(String resumeId, String userId,
                                 CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Resume.class);
        RequestParams params = new RequestParams();
        params.put("resumeId", resumeId);
        params.put("userId", userId);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.setUpdefaultFlag);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    public void deleteResume(String resumeId, CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Resume.class);
        RequestParams params = new RequestParams();
        params.put("resumeId", resumeId);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.deleteResume);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    public void deleteMarket(String marketInfoId, CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Market.class);
        RequestParams params = new RequestParams();
        params.put("marketInfoId", marketInfoId);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.deleteMarketInfo);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    public void getMarket(String rows, String page, String userId,String sortId,
                          CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Market.class);
        RequestParams params = new RequestParams();
        params.put("rows", rows);
        params.put("page", page);
        params.put("userId", userId);
        params.put("sortId", sortId);
        requestModel.setParams(params);
        requestModel.setShowDialog(false);
        requestModel.setUrl(Urls.selectMarketInfoByCondition);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    /**
     marketInfoId		String	信息Id
     title		String	标题
     isPublish		String	是否发布 1 是 0 否
     details		String	详细描述
     pic		String	商品图片路径
     price		double	价格
     userId		String	发布者userId
     mobile		String	联系方式
     */
    public void editMarket(String marketInfoId, String title, String isPublish,
                           String details, String pic, String price,
                           String userId, String mobile,String schoolId,String sortId,
                           CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Resume.class);
        RequestParams params = new RequestParams();
        if (marketInfoId != null) {
            params.put("marketInfoId", marketInfoId);
        }
        params.put("title", title);
        params.put("isPublish", isPublish);
        params.put("details", details);
        params.put("pic", pic);
        params.put("price", price);
        params.put("userId", userId);
        params.put("mobile", mobile);
        params.put("schoolId", schoolId);
        params.put("sortId", sortId);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.editMarketInfo);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    public void editResume(String id, String userId, String birthday,
                           String name, String defaults, String gongZuoNainXian,
                           String headerPic, String sex, String hunYinQingKuang,
                           String mingZu, String shenGao, String xueLi, String huJi,
                           String biYeXueXiao, String zhuanYe, String yuYanNengLi,
                           String jiNengZhuanChang, String gongZuoJingYan,
                           String gongZuoLeiXing, String qiDaiXingZi, String daoGangShiJian,
                           String ziWoPingJia, String xiaoYuanChengZhangJingLi,
                           String resumeEducation, String mobile, String qq, String email,
                           CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Resume.class);
        RequestParams params = new RequestParams();
        if (id != null) {
            params.put("id", id);
        }
        params.put("userId", userId);
        params.put("birthday", birthday.trim());
        params.put("name", name.trim());
        params.put("defaults", defaults.trim());
        params.put("gongZuoNainXian", gongZuoNainXian.trim());
        params.put("headerPic", headerPic.trim());
        params.put("sex", sex.trim());
        params.put("hunYinQingKuang", hunYinQingKuang.trim());
        params.put("mingZu", mingZu.trim());
        params.put("shenGao", shenGao.trim());
        params.put("xueLi", xueLi.trim());
        params.put("huJi", huJi.trim());
        params.put("biYeXueXiao", biYeXueXiao.trim());
        params.put("zhuanYe", zhuanYe.trim());
        params.put("yuYanNengLi", yuYanNengLi.trim());
        params.put("jiNengZhuanChang", jiNengZhuanChang.trim());
        params.put("gongZuoJingYan", gongZuoJingYan.trim());
        params.put("gongZuoLeiXing", gongZuoLeiXing.trim());
        params.put("jiNengZhuanChang", jiNengZhuanChang.trim());
        params.put("gongZuoJingYan", gongZuoJingYan.trim());
        params.put("gongZuoLeiXing", gongZuoLeiXing.trim());
        params.put("qiDaiXingZi", qiDaiXingZi.trim());
        params.put("daoGangShiJian", daoGangShiJian.trim());
        params.put("ziWoPingJia", ziWoPingJia.trim());
        params.put("xiaoYuanChengZhangJingLi", xiaoYuanChengZhangJingLi.trim());
        params.put("resumeEducation", resumeEducation.trim());
        params.put("mobile", mobile.trim());
        params.put("qq", qq.trim());
        params.put("email", email.trim());
        requestModel.setParams(params);
        requestModel.setUrl(Urls.editResume);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    public void getBanner(String rows, String page, String type,
                          CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Advert.class);
        RequestParams params = new RequestParams();
        params.put("rows", rows);
        params.put("page", page);
        params.put("type", type);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.selectBannerByCondition);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    /**
     * nickName		String	姓名
     * biYeXueXiao		String	毕业学院
     * xueLi		Integer	学历id
     * gongZuoNainXian		Integer	工作年限id
     * shenGao		String	身高
     * sex		Short	性别
     * mingZu		String	民族
     */
    public void searchResume(String rows, String page, String userId,
                             String keyWordName, String deptId, String nickName, String biYeXueXiao, String xueLi, String gongZuoNainXian, String shenGao, String sex, String mingZu, CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Resume.class);
        RequestParams params = new RequestParams();
        params.put("rows", rows);
        params.put("page", page);
        params.put("userId", userId);
        params.put("keyWordName", keyWordName);
        params.put("deptId", deptId);
        params.put("nickName", nickName);
        params.put("biYeXueXiao", biYeXueXiao);
        params.put("xueLi", xueLi);
        params.put("gongZuoNainXian", gongZuoNainXian);
        params.put("shenGao", shenGao);
        params.put("sex", sex);
        params.put("mingZu", mingZu);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.searchResumeByCondition);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    /**
     * type 必填 String 消息类型(job面试通知/apply申请通知/sys系统通知) title 必填 String 标题 content
     * 必填 String 内容 toUserId 必填 String 发送用户id userId 必填 Long 企业用户id deptId 必填
     * Long 企业部门id
     */
    public void sendJobMsg(String type, String title, String content,
                           String toUserId, String userId, String deptId, String jobInfoId,
                           CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Resume.class);
        RequestParams params = new RequestParams();
        params.put("type", type);
        params.put("title", title);
        params.put("content", content);
        params.put("toUserId", toUserId);
        params.put("userId", userId);
        params.put("deptId", deptId);
        params.put("jobInfoId", jobInfoId);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.sendJobMsg);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    public void selectJobByUserId(String rows, String page, String userId,
                                  int flag, CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Job.class);
        RequestParams params = new RequestParams();
        params.put("rows", rows);
        params.put("page", page);
        params.put("userId", userId);
        if (flag == 0) {
            params.put("flag", "zczp");
        }
        if (flag == 1) {
            params.put("flag", "jyzp");
        }
        if (flag == 2) {
            params.put("flag", "normal");
        }
        if (flag == 3) {
            params.put("flag", "qgjx");
        }
        requestModel.setShowDialog(false);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.selectJobByUserId);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    public void editJob(String id, String userId, String deptId,
                        String zhaoPingRenShu, String description, String title, int flag,
                        String jobSortId, String jobPostId, String gongZuoJingYanSortId,
                        String xinZiDaiYuSortId, String xueLiYaoQiuSortId, String workType,
                        String f, CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Resume.class);
        RequestParams params = new RequestParams();
        if (id != null) {
            params.put("id", id);
        }
        params.put("userId", userId);
        params.put("deptId", deptId);
        params.put("gongZuoJingYanSortId", gongZuoJingYanSortId);
        params.put("xinZiDaiYuSortId", xinZiDaiYuSortId);

        params.put("xueLiYaoQiuSortId", xueLiYaoQiuSortId);
        params.put("zhaoPingRenShu", zhaoPingRenShu);
        params.put("content", description);
        params.put("workType", workType);
        params.put("title", title);
        if (f == null) {
            if (flag == 0) {
                params.put("flag", "zczp");
            }
            if (flag == 1) {
                params.put("flag", "jyzp");
            }
            if (flag == 2) {
                params.put("flag", "normal");
            }
            if (flag == 3) {
                params.put("flag", "qgjx");
            }
        } else {
            params.put("flag", f);
        }
        params.put("jobSortId", jobSortId);
        params.put("jobPostId", jobPostId);

        requestModel.setParams(params);
        requestModel.setUrl(Urls.editJob);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    public void deleteJob(String jobId, CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Resume.class);
        RequestParams params = new RequestParams();
        params.put("jobId", jobId);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.deleteJob);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    public void getJobApplyByJobId(String rows, String page, String jobId,
                                   CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Resume.class);
        RequestParams params = new RequestParams();
        params.put("rows", rows);
        params.put("page", page);
        params.put("jobId", jobId);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.getJobApplyByJobId);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    public void updateEnterprise(String userId, String remark,
                                 CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Resume.class);
        RequestParams params = new RequestParams();
        params.put("userId", userId);
        params.put("remark", remark);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.updateEnterprise);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    public void buyResume(String deptId, String resumeId, String userId,
                          CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Advert.class);
        RequestParams params = new RequestParams();
        params.put("deptId", deptId);
        params.put("resumeId", resumeId);
        params.put("userId", userId);
        requestModel.setParams(params);
        requestModel.setShowErrorMessage(true);
        requestModel.setUrl(Urls.buy_resume);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    public void getResumeById(String resumeId,
                              CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Resume.class);
        requestModel.setShowDialog(false);
        RequestParams params = new RequestParams();
        params.put("resumeId", resumeId);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.getResumeById);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    public void getJobById(String jobId,
                           CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Job.class);
        requestModel.setShowDialog(false);
        RequestParams params = new RequestParams();
        params.put("jobId", jobId);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.selectJobById);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }


    /*
    enterpriseId                                    	必须	Long	企业id
    userId  	必须	String	个人用户id
       content		String                	内容
     */
    public void jobComment(String enterpriseId, String content, String userId,

                           CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Resume.class);
        RequestParams params = new RequestParams();
        params.put("enterpriseId", enterpriseId);
        params.put("content", content);
        params.put("userId", userId);
        requestModel.setShowErrorMessage(true);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.addEnterpriseComment);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    //获取评论列表
    public void getComment(String userId,
                           CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Comment.class);
        requestModel.setShowDialog(false);
        RequestParams params = new RequestParams();
        params.put("userId", userId);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.selectEnterpriseCommentByCondition);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    public void isTopJob(String userId, String jobId,
                         CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Message.class);
        RequestParams params = new RequestParams();
        params.put("userId", userId);
        params.put("jobId", jobId);
        requestModel.setParams(params);
        requestModel.setShowErrorMessage(true);
        requestModel.setUrl(Urls.isTopJob);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }
}
