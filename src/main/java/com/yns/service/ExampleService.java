package com.yns.service;

import android.content.Context;

import com.yns.net.http.CustomAsyncHttpClient;

/**
 * @className：ExampleService.java
 * @author: lmt
 * @Function: 示例，用以copy和参考
 * @createDate: 2014-10-19 上午10:38:10
 * @update:
 */
public class ExampleService {
	private String TAG = "ExampleService";
	private boolean LogSwitch = true;

	private CustomAsyncHttpClient httpClient;
	private static ExampleService mInstance;

	private ExampleService(Context context) {
		httpClient = new CustomAsyncHttpClient(context);
	}

	public static ExampleService getInstance(Context context) {
		// 双重锁定
		if (mInstance == null) {
			synchronized (ExampleService.class) {
				if (mInstance == null) {
					mInstance = new ExampleService(context);
				}
			}
		}
		return mInstance;
	}

}
