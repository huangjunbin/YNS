package com.yns.service;

import android.content.Context;

import com.yns.model.Address;
import com.yns.model.OrderInfo;
import com.yns.model.Product;
import com.yns.model.ProductType;
import com.yns.model.RequestModel;
import com.yns.model.School;
import com.yns.model.ShopCart;
import com.yns.net.Urls;
import com.yns.net.http.CustomAsyncHttpClient;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.net.http.RequestParams;

/**
 * @GoodService.java
 * @author:lmt
 * @function:商品服务
 * @d2014-12-23下午3:32:47
 * @update:
 */
public class GoodService {
    private String TAG = "UserService";
    private boolean LogSwitch = true;

    private static CustomAsyncHttpClient httpClient;
    private static GoodService mInstance;

    public static GoodService getInstance(Context context) {
        httpClient = new CustomAsyncHttpClient(context);
        // 双重锁定
        if (mInstance == null) {
            synchronized (UserService.class) {
                if (mInstance == null) {
                    mInstance = new GoodService();
                }
            }
        }
        return mInstance;
    }

    /**
     * 查询商品类型列表
     *
     * @param handler
     */
    public void getSchoolType(
            CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(School.class);
        RequestParams params = new RequestParams();
        requestModel.setParams(params);
        requestModel.setUrl(Urls.getSchool);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    /**
     * 查询商品类型列表
     *
     * @param handler
     */
    public void getMarketType(
            CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(ProductType.class);
        RequestParams params = new RequestParams();
        requestModel.setParams(params);
        requestModel.setUrl(Urls.selectMarketSortByCondition);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    /**
     * 查询商品类型列表
     *
     * @param handler
     */
    public void getProductType(
            CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(ProductType.class);
        RequestParams params = new RequestParams();
        requestModel.setParams(params);
        requestModel.setUrl(Urls.selectProductSortByCondition);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    /**
     * 查询商品列表
     *
     * @param handler
     */
    public void getProductList(int rows, int page, String sortId,String flag,
                               CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Product.class);
        RequestParams params = new RequestParams();
        params.put("rows", rows + "");
        params.put("page", page + "");
        params.put("sortId", sortId);
        params.put("flag", flag + "");
        requestModel.setParams(params);
        requestModel.setUrl(Urls.selectProductByCondition);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    /**
     * 获得购物车列表
     *
     * @param handler
     */
    public void getShopCartList(String userId,
                                CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(ShopCart.class);
        requestModel.setShowDialog(false);
        RequestParams params = new RequestParams();
        params.put("userId", userId);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.selectShopCarByCondition);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    /**
     * 添加购物车列表
     *
     * @param handler
     */
    public void addShopCart(String appProductInfoId,
                            String userId,
                            int productSum,
                            CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        RequestParams params = new RequestParams();
        params.put("userId", userId);
        params.put("appProductInfoId", appProductInfoId);
        params.put("productSum", productSum + "");
        requestModel.setParams(params);
        requestModel.setUrl(Urls.addShopCar);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    /**
     * 移除购物车列表
     */
    public void removeShopCart(String appProductInfoId,
                               String userId,
                               CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        RequestParams params = new RequestParams();
        params.put("userId", userId);
        params.put("appProductInfoId", appProductInfoId);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.deleteShopCar);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    /**
     * 获得收货地址列表
     *
     * @param handler
     */
    public void getAddressList(String userId,
                               CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Address.class);
        RequestParams params = new RequestParams();
        params.put("userId", userId);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.selectConsigneeByCondition);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    /**
     * 编辑收货地址列表
     *
     * @param handler consigneeInfoId		String	收货地址id
     *                userId	必填	String	用户id
     *                isDefault	必填	Integer	是否默认 1 是 0 否
     *                name		String	名称
     *                mobile		String	手机
     *                sheng		String	省份名称
     *                shi		String	市名称
     *                xian		String	县区名称
     *                xiang		String	镇村或者街道名称
     *                address		String	详细地址
     */
    public void editAddress(Address address,
                            CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(Address.class);
        RequestParams params = new RequestParams();
        params.put("consigneeInfoId", address.getConsigneeInfoId());
        params.put("userId", address.getUserId());
        params.put("isDefault", address.isDefault() ? "1" : "0");
        params.put("name", address.getName());
        params.put("mobile", address.getMobile());
        params.put("sheng", address.getSheng());
        params.put("shi", address.getShi());
        params.put("xiang", address.getXiang());
        params.put("xian", address.getXian());
        params.put("address", address.getAddress());
        requestModel.setParams(params);
        requestModel.setUrl(Urls.editConsignee);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    /**
     * 移除收货地址
     */
    public void removeAddress(String consigneeInfoId,
                              CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        RequestParams params = new RequestParams();
        params.put("consigneeInfoId", consigneeInfoId);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.deleteConsignee);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    /**
     * 获得订单列表
     *
     * @param handler
     */
    public void getOrderList(String userId,
                             CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        requestModel.setCls(OrderInfo.class);
        requestModel.setShowDialog(false);
        RequestParams params = new RequestParams();
        params.put("userId", userId);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.selectOrderByCondition);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    /**
     * 添加订单
     */
    public void addOrder(String consigneeInfoId, String userId, String appProductInfoIds,
                         CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        RequestParams params = new RequestParams();
        params.put("consigneeInfoId", consigneeInfoId);
        params.put("userId", userId);
        params.put("appProductInfoIds", appProductInfoIds);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.settleAccounts);
        requestModel.setCls(OrderInfo.class);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    /**
     * 确认订单
     */
    public void confirmOrder(String appOrderInfoId,
            CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        RequestParams params = new RequestParams();
        params.put("appOrderInfoId", appOrderInfoId);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.confirmOrder);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }

    /**
     * 立即购买
     */
    public void buy(String consigneeInfoId, String userId, String appProductInfoId,String buyCount,
                         CustomAsyncResponehandler handler) {
        RequestModel requestModel = new RequestModel();
        RequestParams params = new RequestParams();
        params.put("consigneeInfoId", consigneeInfoId);
        params.put("userId", userId);
        params.put("appProductInfoId", appProductInfoId);
        params.put("buyCount", buyCount);
        requestModel.setParams(params);
        requestModel.setUrl(Urls.buy);
        requestModel.setCls(OrderInfo.class);
        httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
    }
}
