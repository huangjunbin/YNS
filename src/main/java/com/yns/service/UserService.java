package com.yns.service;

import android.content.Context;

import com.yns.app.AppContext;
import com.yns.db.dao.UserDao;
import com.yns.global.GlobalVariable;
import com.yns.model.Garage;
import com.yns.model.RequestModel;
import com.yns.model.ResponeModel;
import com.yns.model.User;
import com.yns.net.Urls;
import com.yns.net.http.CustomAsyncHttpClient;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.net.http.RequestParams;
import com.yns.util.SPUtil;

public class UserService {
	private String TAG = "UserService";
	private boolean isLog = true;
	private boolean LogSwitch = true;

	private static CustomAsyncHttpClient httpClient;
	private static UserService mInstance;
	public static UserDao userDao;
	private User user;

	public static UserService getInstance(Context context) {
		httpClient = new CustomAsyncHttpClient(context);
		userDao = new UserDao(context);
		// 双重锁定
		if (mInstance == null) {
			synchronized (UserService.class) {
				if (mInstance == null) {
					mInstance = new UserService();
				}
			}
		}
		return mInstance;
	}

	/**
	 * 获取验证码
	 * 
	 * @param cellPhone
	 *            电话号码
	 * @param codeType
	 *            验证码类型
	 */
	public void getCheckCode(String mobileNumber, String codeType,
			CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("mobileNumber", mobileNumber);
		params.put("codeType", codeType);
		requestModel.setParams(params);
		requestModel.setUrl(Urls.getcheckcode_adtion);
		httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
	}

	/**
	 * 注册
	 * 
	 * @param account
	 *            账户
	 * @param mobileNumber
	 *            电话号码
	 * @param code
	 *            验证码
	 * @param myKey
	 *            密码
	 */
	public void register(String userName, String pwd, String email, String pic,
			String nickName, String mobile, CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("userName", userName);
		params.put("pwd", pwd);
		params.put("email", email);
		params.put("nickName", nickName);
		params.put("mobile", mobile);
		requestModel.setNew(false);
		requestModel.setShowErrorMessage(true);
		requestModel.setCls(User.class);
		requestModel.setParams(params);
		requestModel.setUrl(Urls.REGISTERUSER);
		httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
	}

	public void registerCompany(String userName, String pwd, String email,
			String pic, String nickName, String mobile, String name,String headerPic,
			CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("userName", userName);
		params.put("pwd", pwd);
		params.put("email", email);
		params.put("nickName", nickName);
		params.put("mobile", mobile);
		params.put("name", name);// name 必须 String 企业名称
		params.put("pic", pic);//headerPic
		params.put("headerPic", headerPic);
		requestModel.setNew(false);
		requestModel.setShowErrorMessage(true);
		requestModel.setCls(User.class);
		requestModel.setParams(params);
		requestModel.setUrl(Urls.REGISTERCOMPANY);
		httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
	}

	public void updatePassword(String loginType, String userId, String pwd,
			String oldPwd, CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("loginType", loginType);
		params.put("userId", userId);
		params.put("pwd", pwd);
		params.put("oldPwd", oldPwd);
		requestModel.setParams(params);
		requestModel.setUrl(Urls.updatePwd);
		httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
	}

	/**
	 * 登录
	 * 
	 * @param mobileNumber
	 *            手机号码
	 * @param myKey
	 *            密码
	 * @param isRemberPassword
	 *            是否记住密码
	 * @param isAutoLogin
	 *            是否自动登录
	 */
	public void login(final String userName, final String pwd,
			final boolean isRemberPassword, final boolean isAutoLogin,
			boolean isUser, CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("userName", userName);
		params.put("pwd", pwd);
		params.put("loginType", isUser ? "1" : "2");// loginType 必须 String 用户类型
		// 1 个人 // 2 企业
		AppContext.userType = isUser ? 1 : 2;
		requestModel.setParams(params);
		requestModel.setCls(User.class);
		requestModel.setEasyName("user");
		requestModel.setNew(false);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.login_action);
		httpClient.post(requestModel, new CustomAsyncResponehandler(handler) {
			@Override
			public void onSuccess(ResponeModel baseModel) {
				super.onSuccess(baseModel);
				if (baseModel != null && baseModel.isStatus()) {
					GlobalVariable.is_Login_Succed = true;

					AppContext.currentUser = (User) baseModel.getResultObj();
					AppContext.currentUser.setPassword(pwd);
					AppContext.currentUser.setUserName(userName);
					AppContext.currentUser.setType(AppContext.userType);
					if (isRemberPassword) {
						SPUtil.saveString("mobileNumber", userName);
						SPUtil.saveString("myKey", pwd);
						AppContext.currentUser
								.setRemberPassword(User.REMBER_PASSWORD);
					} else {
						AppContext.currentUser.setRemberPassword(-1);
					}

					if (isAutoLogin) {
						AppContext.currentUser.setAutoLogin(User.AUTO_LOGIN);
					} else {
						AppContext.currentUser.setAutoLogin(-1);
					}

					if (userDao != null) {
						userDao.delete();
						userDao.insert(AppContext.currentUser);
					}
				} else {
					GlobalVariable.is_Login_Succed = false;
				}
			}
		});
	}

	/**
	 * 忘记密码
	 * 
	 * @param mobileNumber
	 *            电话号码
	 * @param code
	 *            验证码
	 * @param newMyKey
	 *            新密码
	 */
	public void ForgetPassword(String userName, String email, String userType,
			CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("userName", userName);
		params.put("email", email);
		params.put("userType", userType);
		requestModel.setParams(params);
		requestModel.setShowErrorMessage(true);
		requestModel.setUrl(Urls.forgetPassword_action);
		httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
	}

	/**
	 * 获取用户信息
	 * 
	 * @param userId
	 * @param handler
	 */
	public void GetUserInfo(String userId, CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("userId", userId);
		requestModel.setParams(params);
		requestModel.setEasyName("user");
		requestModel.setCls(User.class);
		requestModel.setUrl(Urls.getuserinfo_action);
		httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
	}

	/**
	 * 
	 * 修改个人信息
	 */
	public void modifUserInfo(String userId, String email, String mobile,
			String pic, String nickName, CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("userId", userId);
		params.put("email", email);
		params.put("mobile", mobile);
		params.put("pic", pic);
		params.put("nickName", nickName);
		requestModel.setEasyName("user");
		requestModel.setNew(false);
		requestModel.setParams(params);
		requestModel.setCls(User.class);
		requestModel.setUrl(Urls.modifUserinfo_action);
		httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
	}

	/**
	 * 
	 * 提交意见
	 * 
	 * @param userId
	 *            用户ID
	 * @param content
	 *            内容
	 * @param mobileNumber
	 *            手机号
	 * @param handler
	 */
	public void commitOpinions(String userId, String content,
			String mobileNumber, CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("userId", userId);
		params.put("content", content);
		params.put("mobileNumber", mobileNumber);
		requestModel.setParams(params);
		requestModel.setUrl(Urls.commitOption_action);
		httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
	}

	/**
	 * 
	 * 获取我的车库
	 * 
	 * @param userId
	 *            用户id
	 * @param pageNum
	 *            from
	 * @param pageSize
	 *            to
	 * @param handler
	 */
	public void getGradeCars(String userId, String pageNum, String pageSize,
			CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("userId", userId);
		params.put("pageNum", pageNum);
		params.put("pageSize", pageSize);
		requestModel.setParams(params);
		requestModel.setUrl(Urls.getMyGradeCars_action);
		requestModel.setCls(Garage.class);
		requestModel.setList(true);
		requestModel.setEasyName("carList");
		httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
	}

	/**
	 * 添加车库
	 * 
	 * @param userId
	 *            用户id
	 * @param carBland
	 *            靓车品牌
	 * @param carNum
	 *            靓车车牌号
	 * @param buyAddress
	 *            购买4s店
	 * @param buyTime
	 *            购买时间
	 * @param handler
	 */
	public void addGaragr(String userId, String carBland, String carNum,
			String buyAddress, String buyTime, CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("userId", userId);
		params.put("carBland", carBland);
		params.put("carNum", carNum);
		params.put("buyAddress", buyAddress);
		params.put("buyTime", buyTime);
		requestModel.setParams(params);
		requestModel.setUrl(Urls.addToMyGrade_action);
		httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
	}

	/**
	 * 获取车库详情
	 * 
	 * @param carId
	 * @param handler
	 */
	public void getGarageDetail(String carId, CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("carId", carId);
		requestModel.setParams(params);
		requestModel.setUrl(Urls.getCarDetail_action);
		requestModel.setList(false);
		requestModel.setNew(false);
		requestModel.setCls(Garage.class);
		httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
	}

	/**
	 * 保存车库详情
	 * 
	 * @param carId
	 *            靓车ID
	 * @param carBland
	 *            靓车品牌
	 * @param carNum
	 *            靓车车牌号
	 * @param buyAddress
	 *            购买4s店
	 * @param buyTime
	 *            购买时间
	 */
	public void modifyGarageDetail(String carId, String carBland,
			String carNum, String buyAddress, String buyTime,
			CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("carId", carId);
		params.put("carBland", carBland);
		params.put("carNum", carNum);
		params.put("buyAddress", buyAddress);
		params.put("buyTime", buyTime);
		requestModel.setParams(params);
		requestModel.setUrl(Urls.modifyCarDetail_action);
		httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
	}

	/**
	 * 获取车库详情
	 * 
	 * @param carId
	 * @param handler
	 */
	public void removeCar(String carId, CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("carId", carId);
		requestModel.setParams(params);
		requestModel.setUrl(Urls.removeCar_action);
		httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
	}

	public User getCacheUser() {
		user = userDao.getUserByDefault();
		return user;
	}

	public void clearCacheUser() {
		userDao.delete();
	}

	public void updateUser(User user) {
		if (userDao != null) {
			userDao.delete();
			userDao.insert(user);
		}
	}
}
