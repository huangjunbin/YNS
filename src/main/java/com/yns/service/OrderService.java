package com.yns.service;

import java.io.File;
import java.io.FileNotFoundException;

import android.content.Context;

import com.yns.model.RequestModel;
import com.yns.net.Urls;
import com.yns.net.http.CustomAsyncHttpClient;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.net.http.RequestParams;

public class OrderService {
	String TAG = "ComonService";
	private static CustomAsyncHttpClient httpClient;
	static OrderService mInstance;

	public static OrderService getInstance(Context context) {
		httpClient = new CustomAsyncHttpClient(context);
		if (mInstance == null) {
			// 只有第一次才彻底执行这里的代码
			synchronized (OrderService.class) {
				// 再检查一次
				if (mInstance == null)
					mInstance = new OrderService();
			}
		}
		return mInstance;
	}

	/**
	 * 提交订单
	 */
	public void insertOrder(String userId, String goodsId, String nowPrice,
			String merchantsId, String industryId, String serviceId,
			String allPrice, String serviceNum, String orderType,
			String getPrice, CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("userId", userId);
		params.put("goodsId", goodsId);
		params.put("nowPrice", nowPrice);
		params.put("merchantsId", merchantsId);
		params.put("industryId", industryId);
		params.put("serviceId", serviceId);
		params.put("allPrice", allPrice);
		params.put("serviceNum", serviceNum);
		params.put("orderType", orderType);
		params.put("getPrice", getPrice);
		requestModel.setParams(params);
		requestModel.setShowDialog(true);
		requestModel.setUrl(Urls.order_insert);
		httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
	}

	/**
	 * order back
	 */
	public void backOrder(String orderId, CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("orderId", orderId);
		requestModel.setParams(params);
		requestModel.setShowDialog(true);
		requestModel.setUrl(Urls.order_back);
		httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
	}

	/**
	 * comment order
	 */
	public void commentOrder(String userId, String goodsId, String star,
			String content, CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("userId", userId);
		params.put("goodsId", goodsId);
		params.put("star", star);
		params.put("content", content);
		requestModel.setParams(params);
		requestModel.setShowDialog(true);
		requestModel.setUrl(Urls.order_comment);
		httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
	}

	/**
	 * 获取签名
	 */
	public void getSign(String orderId, String isDebug,
			CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("orderId", orderId);
		params.put("isDebug", isDebug);
		requestModel.setParams(params);
		requestModel.setUrl(Urls.get_sign);
		httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
	}

	/**
	 * 一 支付
	 */
	public void moneyPay(String orderId, String userId,
			CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("orderId", orderId);
		params.put("userId", userId);
		requestModel.setParams(params);
		requestModel.setUrl(Urls.money_pay);
		httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
	}

	public void scoreMoney(String userId, String score, String money,
			CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("money", money);
		params.put("score", score);
		params.put("userId", userId);
		requestModel.setParams(params);
		requestModel.setUrl(Urls.score_money);
		httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
	}

	public void getCode(String orderId, CustomAsyncResponehandler handler) {
		RequestModel requestModel = new RequestModel();
		RequestParams params = new RequestParams();
		params.put("orderId", orderId);

		requestModel.setParams(params);
		requestModel.setUrl(Urls.get_code);
		httpClient.post(requestModel, new CustomAsyncResponehandler(handler));
	}

}
