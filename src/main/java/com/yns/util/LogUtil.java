package com.yns.util;

import android.util.Log;

public class LogUtil {
	private static boolean canLog = false;

	public static void d(String TAG, String msg, boolean isLog) {
		if (isLog && canLog)
			Log.d(TAG, msg);
	}

	public static void e(String TAG, String msg, boolean isLog) {
		if (isLog && canLog)
			Log.e(TAG, msg);
	}

	public static void setLogStart(boolean start) {
		canLog = start;
	}
}
