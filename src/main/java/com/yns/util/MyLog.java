package com.yns.util;

import android.util.Log;

import com.yns.BuildConfig;

import java.text.SimpleDateFormat;
import java.util.Locale;


/**
 * @ClassName: MyLog
 * @Description: 日志工具类，负责App运行过程中的日志打印工作
 * @author BaoHang baohang2011@gmail.com
 * @date 2014�?�?0�?下午7:39:16
 */
public class MyLog {
	/** Verbose级别日志�?�� */
	private static boolean isVerboseEnable = true;

	/** Debug级别日志�?�� */
	private static boolean isDebugEnable = true;

	/** Info级别日志�?�� */
	private static boolean isInfoEnable = true;

	/** Warn级别日志�?�� */
	private static boolean isWarnEnable = true;

	/** Error级别日志�?�� */
	private static boolean isErrorEnable = true;

	/** 日志工具总开�? */
	public static boolean isLogEnable = BuildConfig.DEBUG;

	/** 写入文件日志�?�� */
	private static boolean isFileEnable = true;

	/** 日志文件名称 */
	private static final String MYLOGFILEName = "Log.txt";

	/** 日志的输出格�? */
	private static final SimpleDateFormat myLogSdf = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss", Locale.CHINA);// 日志的输出格�?

	/** 日志文件格式 */
	private static final SimpleDateFormat logfile = new SimpleDateFormat(
			"yyyy-MM-dd", Locale.CHINA);// 日志文件格式

	/**
	 * 打印 VERBOSE 级别的日志信�?
	 * 
	 * @param tag
	 *            标签，用以区别信息来�?
	 * @param message
	 *            日志信息内容
	 */
	public static void v(String tag, String message) {
		v(tag, message, null);
	}

	/**
	 * 打印 VERBOSE 级别的日志信息，并且记录异常信息
	 * 
	 * @param tag
	 *            标签，用以区别信息来�?
	 * @param message
	 *            日志信息内容
	 * @param tr
	 *            异常信息
	 */
	public static void v(String tag, String message, Throwable tr) {
		if (isVerboseEnable()) {
			if (null != tr) {
				Log.v(tag, message, tr);
			} else {
				Log.v(tag, message);
			}
			if (isFileEnable) {
				writeLogtoFile("v", tag, message);
			}
		}
	}

	/**
	 * 打印 DEBUG 级别的日志信�?
	 * 
	 * @param tag
	 *            标签，用以区别信息来�?
	 * @param message
	 *            日志信息内容
	 */
	public static void d(String tag, String message) {
		d(tag, message, null);
	}

	/**
	 * 打印 DEBUG 级别的日志信息，并且记录异常信息
	 * 
	 * @param tag
	 *            标签，用以区别信息来�?
	 * @param message
	 *            日志信息内容
	 * @param tr
	 *            异常信息
	 */
	public static void d(String tag, String message, Throwable tr) {
		if (isDebugEnable()) {
			if (null != tr) {
				Log.d(tag, message, tr);
			} else {
				Log.d(tag, message);
			}
			if (isFileEnable) {
				writeLogtoFile("d", tag, message);
			}
		}
	}

	/**
	 * 打印 INFO 级别的日志信�?
	 * 
	 * @param tag
	 *            标签，用以区别信息来�?
	 * @param message
	 *            日志信息内容
	 */
	public static void i(String tag, String message) {
		i(tag, message, null);
	}

	/**
	 * 打印 INFO 级别的日志信息，并且记录异常信息
	 * 
	 * @param tag
	 *            标签，用以区别信息来�?
	 * @param message
	 *            日志信息内容
	 * @param tr
	 *            异常信息
	 */
	public static void i(String tag, String message, Throwable tr) {
		if (isInfoEnable()) {
			if (null != tr) {
				Log.i(tag, message, tr);
			} else {
				Log.i(tag, message);
			}
			if (isFileEnable) {
				writeLogtoFile("i", tag, message);
			}
		}
	}

	/**
	 * 打印 WARN 级别的日志信�?
	 * 
	 * @param tag
	 *            标签，用以区别信息来�?
	 * @param message
	 *            日志信息内容
	 */
	public static void w(String tag, String message) {
		w(tag, message, null);
	}

	/**
	 * 打印 WARN 级别的日志信息，并且记录异常信息
	 * 
	 * @param tag
	 *            标签，用以区别信息来�?
	 * @param message
	 *            日志信息内容
	 * @param tr
	 *            异常信息
	 */
	public static void w(String tag, String message, Throwable tr) {
		if (isWarnEnable()) {
			if (null != tr) {
				Log.w(tag, message, tr);
			} else {
				Log.w(tag, message);
			}
			if (isFileEnable) {
				writeLogtoFile("w", tag, message);
			}
		}
	}

	/**
	 * 打印 ERROR 级别的日志信�?
	 * 
	 * @param tag
	 *            标签，用以区别信息来�?
	 * @param message
	 *            日志信息内容
	 */
	public static void e(String tag, String message) {
		e(tag, message, null);
	}

	/**
	 * 打印 ERROR 级别的日志信息，并且记录异常信息
	 * 
	 * @param tag
	 *            标签，用以区别信息来�?
	 * @param message
	 *            日志信息内容
	 * @param tr
	 *            异常信息
	 */
	public static void e(String tag, String message, Throwable tr) {
		if (isErrorEnable()) {
			if (null != tr) {
				Log.e(tag, message, tr);
			} else {
				Log.e(tag, message);
			}
			if (isFileEnable) {
				writeLogtoFile("e", tag, message);
			}
		}
	}

	/**
	 * 判断Verbose级别的日志是否可打印
	 * 
	 * @return 可打印返回true，否则返回false
	 */
	private static boolean isVerboseEnable() {
		return isLogEnable && isVerboseEnable;
	}

	/**
	 * 判断Debug级别的日志是否可打印
	 * 
	 * @return 可打印返回true，否则返回false
	 */
	private static boolean isDebugEnable() {
		return isLogEnable && isDebugEnable;
	}

	/**
	 * 判断Info级别的日志是否可打印
	 * 
	 * @return 可打印返回true，否则返回false
	 */
	private static boolean isInfoEnable() {
		return isLogEnable && isInfoEnable;
	}

	/**
	 * 判断Warn级别的日志是否可打印
	 * 
	 * @return 可打印返回true，否则返回false
	 */
	private static boolean isWarnEnable() {
		return isLogEnable && isWarnEnable;
	}

	/**
	 * 判断Error级别的日志是否可打印
	 * 
	 * @return 可打印返回true，否则返回false
	 */
	private static boolean isErrorEnable() {
		return isLogEnable && isErrorEnable;
	}

	/**
	 * 打开日志文件并写入日�?
	 * 
	 * @return
	 * **/
	private static void writeLogtoFile(String mylogtype, String tag, String text) {// 新建或打�?��志文�?

	}
}
