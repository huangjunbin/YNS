package com.yns.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 工具类
 */
@SuppressLint("SimpleDateFormat")
public class Utils {



	@SuppressWarnings("deprecation")
	public static int getScreenWidth(Context context) {
		WindowManager manager = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);
		Display display = manager.getDefaultDisplay();
		return display.getWidth();
	}

	@SuppressWarnings("deprecation")
	public static int getScreenHeight(Context context) {
		WindowManager manager = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);
		Display display = manager.getDefaultDisplay();
		return display.getHeight();
	}

	public static float getScreenDensity(Context context) {
		try {
			DisplayMetrics dm = new DisplayMetrics();
			WindowManager manager = (WindowManager) context
					.getSystemService(Context.WINDOW_SERVICE);
			manager.getDefaultDisplay().getMetrics(dm);
			return dm.density;
		} catch (Exception ex) {

		}
		return 1.0f;
	}

	public static String getIMEI(Context context) {
		return ((TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();

	}

	public static boolean getWeekOfDate(String str, String str2) {
		if (str2 == null) {
			return false;
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Date d = null;
		try {
			d = sdf.parse(str);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		long time1 = d.getTime();
		// 20140118===2014-01-17 14:17:37==1389974400000==0==-1389974400000
		System.out.println(str + "===" + str2 + "==");
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date d2 = null;
		long time2 = 0;
		try {
			d2 = sdf2.parse(str2);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		try {
			time2 = d2.getTime();
		} catch (Exception e) {
			e.printStackTrace();
		}
		long timeInterval = time2 - time1;
		// 20140118===2013-12-30 13:30:00==1389974400000==0==-1389974400000

		if (timeInterval > 0 && timeInterval < (1000 * 60 * 60 * 25)) {
			return true;
		} else {
			return false;
		}

	}

	@SuppressWarnings("deprecation")
	public static boolean isAm(String str2) {

		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date d2 = null;
		if (str2 == null) {
			return true;
		}
		try {
			System.out.println("谁是NULL=" + sdf2 + "==" + d2);
			d2 = sdf2.parse(str2);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		if (d2.getHours() < 12) {
			return true;
		} else {
			return false;
		}
	}

	public static long getTime(String str2) {

		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		Date d2 = null;
		try {
			d2 = sdf2.parse(str2);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return d2.getTime();

	}

	@SuppressWarnings("unused")
	public static String getDate(long time) {

		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date d2 = null;
		Date curDate = new Date(time);

		return sdf2.format(curDate);

	}



	/** 保存方法 */
	public static void saveBitmap(String filePath, Bitmap bm) {
		File f = new File(filePath);
		if (f.exists()) {
			f.delete();
		}
		try {
			FileOutputStream out = new FileOutputStream(f);
			bm.compress(Bitmap.CompressFormat.JPEG, 90, out);
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@SuppressWarnings("unused")
	public static Bitmap copressImage(String imgPath, int imagew, int imageh) {
		File picture = new File(imgPath);
		Options bitmapFactoryOptions = new Options();
		// 下面这个设置是将图片边界不可调节变为可调节
		bitmapFactoryOptions.inJustDecodeBounds = true;
		bitmapFactoryOptions.inSampleSize = 2;
		int outWidth = bitmapFactoryOptions.outWidth;
		int outHeight = bitmapFactoryOptions.outHeight;
		Bitmap bmap = BitmapFactory.decodeFile(picture.getAbsolutePath(),
				bitmapFactoryOptions);
		int yRatio = (int) Math.ceil(bitmapFactoryOptions.outHeight / imageh);
		int xRatio = (int) Math.ceil(bitmapFactoryOptions.outWidth / imagew);
		if (yRatio > 1 || xRatio > 1) {
			if (yRatio > xRatio) {
				bitmapFactoryOptions.inSampleSize = yRatio;
			} else {
				bitmapFactoryOptions.inSampleSize = xRatio;
			}
		}
		bitmapFactoryOptions.inJustDecodeBounds = false;
		bmap = BitmapFactory.decodeFile(picture.getAbsolutePath(),
				bitmapFactoryOptions);
		if (bmap != null) {
			return bmap;
		}
		return null;
	}
}
