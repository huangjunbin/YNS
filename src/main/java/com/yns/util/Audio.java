package com.yns.util;

import java.util.HashMap;

import android.annotation.SuppressLint;
import android.media.AudioManager;
import android.media.SoundPool;

import com.yns.R;
import com.yns.app.AppContext;

@SuppressLint("UseSparseArrays")
@SuppressWarnings("deprecation")
public class Audio {
	private static SoundPool soundPool;
	private static HashMap<Integer, Integer> soundPoolMap;

	public static final int AUDIO_DENGDENGDENG = 0;
	
	static {
		soundPool = new SoundPool(3, AudioManager.STREAM_MUSIC, 0);
		soundPoolMap = new HashMap<Integer, Integer>();
		soundPoolMap.put(AUDIO_DENGDENGDENG, soundPool.load(
				AppContext.getApplication(), R.raw.dengdengdeng, 1));
	}
	
	/**
	 * 音效播放函数
	 * 
	 * @param sound
	 */
	public static void playAudio(int audioName) {
		// 实例化AudioManager对象，控制声音
		AudioManager am = (AudioManager) AppContext.getApplication()
				.getSystemService(AppContext.getApplication().AUDIO_SERVICE);
		// 最大音量
		float audioMaxVolumn = am.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		// 当前音量
		float audioCurrentVolumn = am
				.getStreamVolume(AudioManager.STREAM_MUSIC);
		float volumnRatio = audioCurrentVolumn / audioMaxVolumn;
		// 播放
		soundPool.play(soundPoolMap.get(audioName), // 声音资源
				volumnRatio, // 左声道
				volumnRatio, // 右声道
				1, // 优先级，0最低
				0, // 循环次数，0是不循环，-1是永远循环
				1.2f); // 回放速度，0.5-2.0之间。1为正常速度
	}
}
