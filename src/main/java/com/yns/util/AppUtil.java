package com.yns.util;

public class AppUtil {

	/**
	 * 解析城市名称
	 * 
	 * @param cName
	 * @return
	 */
	public static String parceCityName(String cName) {
		String resCName = cName;
		if (resCName != null) {
			if (cName.contains("市")) {// 如果为空就去掉市字再试试
				String subStr[] = cName.split("市");
				resCName = subStr[0];
			} else if (cName.contains("县")) {// 或者去掉县字再试试
				String subStr[] = cName.split("县");
				resCName = subStr[0];
			}
		}
		return resCName;
	}
}
