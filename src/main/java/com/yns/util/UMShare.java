/**
 * 友盟分享
 */
package com.yns.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;

import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.controller.UMServiceFactory;
import com.umeng.socialize.controller.UMSocialService;
import com.umeng.socialize.media.QQShareContent;
import com.umeng.socialize.media.QZoneShareContent;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.sso.QZoneSsoHandler;
import com.umeng.socialize.sso.SinaSsoHandler;
import com.umeng.socialize.sso.UMQQSsoHandler;
import com.umeng.socialize.weixin.controller.UMWXHandler;
import com.umeng.socialize.weixin.media.CircleShareContent;
import com.umeng.socialize.weixin.media.WeiXinShareContent;
import com.yns.R;

public class UMShare {
	public static final String APP_ID = "wx1cfad37385f13d7a";
	public static final String APP_SECRET = "79741e4615330449533cc13af43ef65e";
	public static final String QQ_APP_ID = "1104075980";
	public static final String QQ_APP_SECRET = "XCaeLt6S2H0fOJiu";
	private static String title = "云南大学生";
	private static String content = "最专业的大学生招聘平台";

	/**
	 * 初始化友盟分享
	 */
	public static UMSocialService ShareUrl(Context context, String url,
			String text, int image) {
		return Share(context, url, text, image, null);
	}

	public static UMSocialService Share(Context context, String url,
			String text, int image, Bitmap bmp) {
		// 添加微信平台
		UMWXHandler wxHandler = new UMWXHandler(context, APP_ID, APP_SECRET);
		wxHandler.addToSocialSDK();
		// 支持微信朋友圈
		UMWXHandler wxCircleHandler = new UMWXHandler(context, APP_ID,
				APP_SECRET);
		wxCircleHandler.setToCircle(true);
		wxCircleHandler.addToSocialSDK();
		UMSocialService mController = UMServiceFactory
				.getUMSocialService("com.umeng.share");
		if (bmp == null) {
			if (text == null) {
				// 设置分享文字
				mController.setShareContent(content + url);
			} else {
				mController.setShareContent(text + url);
			}
		}
		// mController.setShareMedia(object);
		// 设置分享图片, 参数2为图片的url地址
		// mController.setShareMedia(new UMImage(getActivity(),
		// "http://www.umeng.com/images/pic/banner_module_social.png"));
		// mController.setShareMedia(object);
		mController.getConfig().removePlatform(SHARE_MEDIA.RENREN,
				SHARE_MEDIA.DOUBAN, SHARE_MEDIA.WEIXIN,
				SHARE_MEDIA.WEIXIN_CIRCLE);

		UMImage localImage;
		if (bmp == null) {
			localImage = new UMImage(context,
					ImageUtils.drawableToBitmap(context.getResources()
							.getDrawable(image)));
		} else {
			localImage = new UMImage(context, bmp);
		}
		mController.setShareImage(localImage);
		// 设置微信好友分享内容
		WeiXinShareContent weixinContent = new WeiXinShareContent();
		if (bmp == null) {

			if (text == null) {
				// 设置分享文字
				weixinContent.setShareContent(content);
			} else {
				weixinContent.setShareContent(text);
			}

			// 设置title
			weixinContent.setTitle(title);
		}
		// 设置分享内容跳转URL
		weixinContent.setTargetUrl(url);
		// 设置分享图片
		weixinContent.setShareImage(localImage);
		mController.setShareMedia(weixinContent);
		// 设置微信朋友圈分享内容
		CircleShareContent circleMedia = new CircleShareContent();

		// 设置朋友圈title
		if (bmp == null) {
			if (text == null) {
				// 设置分享文字
				circleMedia.setShareContent(content);
			} else {
				circleMedia.setShareContent(text);
			}
			circleMedia.setTitle(title);
		}
		circleMedia.setShareImage(localImage);
		circleMedia.setTargetUrl(url);
		mController.setShareMedia(circleMedia);

		// 参数1为当前Activity， 参数2为开发者在QQ互联申请的APP ID，参数3为开发者在QQ互联申请的APP kEY.
		UMQQSsoHandler qqSsoHandler = new UMQQSsoHandler((Activity) context,
				QQ_APP_ID, QQ_APP_SECRET);
		qqSsoHandler.addToSocialSDK();
		QQShareContent qqShareContent = new QQShareContent();
		if (bmp == null) {
			// 设置分享文字
			if (text == null) {
				// 设置分享文字
				qqShareContent.setShareContent(content);
			} else {
				qqShareContent.setShareContent(text);
			}
			// 设置分享title
			qqShareContent.setTitle(title);
		}
		// 设置分享图片
		qqShareContent.setShareImage(localImage);
		// 设置点击分享内容的跳转链接
		qqShareContent.setTargetUrl(url);
		mController.setShareMedia(qqShareContent);
		QZoneSsoHandler qZoneSsoHandler = new QZoneSsoHandler(
				(Activity) context, QQ_APP_ID, QQ_APP_SECRET);
		qZoneSsoHandler.addToSocialSDK();

		QZoneShareContent qzShareContent = new QZoneShareContent();

		if (bmp == null) {
			// 设置分享文字
			if (text == null) {
				// 设置分享文字
				qzShareContent.setShareContent(content);
			} else {
				qzShareContent.setShareContent(text);
			}
			// 设置分享title
			qzShareContent.setTitle(title);
		}
		// 设置分享图片
		qzShareContent.setShareImage(localImage);
		// 设置点击分享内容的跳转链接
		qzShareContent.setTargetUrl(url);
		mController.setShareMedia(qzShareContent);

		SinaSsoHandler so = new SinaSsoHandler();

		// 设置新浪SSO handler
		mController.getConfig().setSsoHandler(so);

		// 设置分享图片，参数2为本地图片的资源引用
		// mController.setShareMedia(new UMImage(getActivity(),
		// R.drawable.icon));
		// 设置分享图片，参数2为本地图片的路径(绝对路径)
		// mController.setShareMedia(new UMImage(getActivity(),
		// BitmapFactory.decodeFile("/mnt/sdcard/icon.png")));

		// 设置分享音乐
		// UMusic uMusic = new
		// UMusic("http://sns.whalecloud.com/test_music.mp3");
		// uMusic.setAuthor("GuGu");
		// uMusic.setTitle("天籁之音");
		// 设置音乐缩略图
		// uMusic.setThumb("http://www.umeng.com/images/pic/banner_module_social.png");
		// mController.setShareMedia(uMusic);

		// 设置分享视频
		// UMVideo umVideo = new UMVideo(
		// "http://v.youku.com/v_show/id_XNTE5ODAwMDM2.html?f=19001023");
		// 设置视频缩略图
		// umVideo.setThumb("http://www.umeng.com/images/pic/banner_module_social.png");
		// umVideo.setTitle("友盟社会化分享!");
		// mController.setShareMedia(umVideo);
		return mController;
	}

	/**
	 * 初始化友盟分享
	 */
	public static UMSocialService ShareText(Context context, String text) {
		String url = "www.dxsapp.com";
		return Share(context, url, text, R.drawable.ic_launcher, null);
	}

	public static UMSocialService ShareImage(Context context, String text,
			Bitmap bitmap) {
		String url = "www.dxsapp.com";
		return Share(context, url, text, R.drawable.ic_launcher, bitmap);
	}
}
