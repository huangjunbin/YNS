package com.yns.global;

import java.util.ArrayList;
import java.util.List;

import com.yns.model.Agent;

public class GlobalVariable {
	public static boolean get_LatLong_Succed = false;// 获取经纬度是否成功
	public static double currentLatitude;
	public static double currentLongitude;

	public static boolean loc_City_Succed = false;// 定位成功
	public static boolean is_Login_Succed = false;// 是否登录成功

	public static String currentServiceKindKey;// 当前服务的种类
	public static List<Agent> listCurrentArea;// 当前区
	public static List<Agent> listCurrentServiceKind;// 当前服务类型
	public static List<Agent> listCurrentTarget;// 当前搜索条件

	/**
	 * 初始化当前服务
	 */
	public static void initCurrentService() {
		listCurrentServiceKind = GlobalConstant.SERVICEKINDMAP
				.get(currentServiceKindKey);
		listCurrentTarget = GlobalConstant.LIST_TARGET;
		listCurrentArea = new ArrayList<Agent>();
		listCurrentArea.add(new Agent("洪山区", 1 + ""));
		listCurrentArea.add(new Agent("武昌区", 2 + ""));
		listCurrentArea.add(new Agent("江夏区", 3 + ""));
		listCurrentArea.add(new Agent("汉口区", 4 + ""));
		listCurrentArea.add(new Agent("汉阳区", 5 + ""));
		listCurrentArea.add(new Agent("东西湖区", 6 + ""));
	}
}
