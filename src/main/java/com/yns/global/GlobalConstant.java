package com.yns.global;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;

import com.yns.model.Agent;

@SuppressLint("UseSparseArrays")
public class GlobalConstant {
	public static final int DEFAULT_PAGE_SIZE = 2;// 默认条数

	public static final String NET_CHANGE_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";
	// 验证码类型，0：注册,1、绑定手机,2、忘记密码
	public static final int CODE_TYPE_REGISTER = 0;
	public static final int CODE_TYPE_BINDTEL = 1;
	public static final int CODE_TYPE_FORGET = 2;

	public static final int CODE_EFFECT_TIME = 60;// 验证码的有效时间
	public static final int PASSWORD_MINLEN = 6;// 密码的最小单位
	/** 订单详情 */
	public static final int OLDERINFO_NOPAY = 1;
	public static final int OLDERINFO_NOUSE = 2;
	public static final int OLDERINFO_HAVEUSE = 3;
	public static final int OLDERINFO_REMOVE = 4;
	public static final int OLDERINFO_IN = 5;
	/** 服务类型key */
	public static final String SERVICEKEY[] = new String[] { "1", "2", "3",
			"4", "5", "6", "7" };

	/** 服务名称 */
	public static final String SERVIVE_NAME[] = new String[] { "快捷洗车", "维修保养",
			"新车试驾", "餐饮", "休闲中心", "丽人", "购物中心" };

	/** 热门城市 */
	public static final String HOTCITYS[] = new String[] { "北京", "广州", "上海",
			"天津", "深圳", "武汉", "长沙", "东莞", "西安", "重庆", "佛山", "南京", "郑州", "惠州",
			"杭州", "济南", "汕头", "福州", "合肥", "江门", "南宁", "南昌", "厦门", "成都", "昆明",
			"青岛", "沈阳", "太原", "大连", "哈尔滨", "长春", "苏州" };

	/** 服务类型 */
	// 1、快捷洗车：洗车、护理、空气净化、喷漆、其他；
	// 2、维修保养：维修、保养、装潢、其他；
	// 3、新车试驾：无；
	// 4、餐饮：甜点、主食、饮品、其他；
	// 5、休闲中心：KTV、足疗按摩、棋牌、电影、健身运动、其他；
	// 6、丽人：美容、美发、美甲、瘦身、其他；
	// 7、购物中心：商场百货、儿童天地、专业市场、其他；
	public static final String[][] SERVIVEKINSTR = new String[][] {
			{ "洗车", "护理", "空气净化", "喷漆", "其他" }, { "维修", "保养", "装潢", "其他" }, {},
			{ " 甜点", "主食", "饮品", "其他" },
			{ "KTV", "足疗按摩", "棋牌", "电影", "健身运动", "其他" },
			{ "美容", "美发", "美甲", "瘦身", "其他" }, { "商场百货", "儿童天地", "专业市场", "其他" } };

	public static final Map<String, List<Agent>> SERVICEKINDMAP = new HashMap<String, List<Agent>>();

	/** 搜索条件 */
	public static final String[] STR_TARGET = new String[] { "离我最近", "评价最高",
			"最新发布" };
	public static final List<Agent> LIST_TARGET = new ArrayList<Agent>();

	static {
		int serviceKindCode = 0;
		// 服务类型
		for (int i = 0; i < SERVIVEKINSTR.length; i++) {
			String key = SERVICEKEY[i];
			List<Agent> agents = new ArrayList<Agent>();
			for (int j = 0; j < SERVIVEKINSTR[i].length; j++) {
				agents.add(new Agent(SERVIVEKINSTR[i][j], serviceKindCode + ""));
				serviceKindCode++;
			}
			SERVICEKINDMAP.put(key, agents);
		}

		// 搜索条件
		int targetKindKey = 0;
		for (String target : STR_TARGET) {
			LIST_TARGET.add(new Agent(target, targetKindKey++ + ""));
		}

	}

}
