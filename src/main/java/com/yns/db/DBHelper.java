package com.yns.db;

import android.content.Context;

import com.simple.util.db.operation.SimpleDbHelper;
import com.yns.model.User;

public class DBHelper extends SimpleDbHelper {

	private static final String DBNAME = "yns.db";
	private static final int DBVERSION = 13;
	private static final Class<?>[] clazz = { User.class };

	public DBHelper(Context context) {
		super(context, DBNAME, null, DBVERSION, clazz);
	}
}