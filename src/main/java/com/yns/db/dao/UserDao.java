package com.yns.db.dao;

import java.util.List;

import android.content.Context;

import com.simple.util.db.operation.TemplateDAO;
import com.yns.db.DBHelper;
import com.yns.model.User;

public class UserDao extends TemplateDAO<User> {
	private List<User> users;

	public UserDao(Context context) {
		super(new DBHelper(context));
	}

	public User getUserByDefault() {
		users = find();
		if (users != null && users.size() > 0) {
			return users.get(0);
		} else {
			return null;
		}
	}
	
	
}
