package com.yns.app;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;

import com.yns.R;
import com.yns.app.ui.MainActivity2;
import com.yns.app.ui.SplashActivity;
import com.yns.model.User;
import com.yns.service.UserService;

/**
 * 应用程序启动类：显示欢迎界面并跳转到主界面
 * 
 * @author allen
 * @version 1.0.0
 * @created 2014-4-16
 */
@SuppressLint("Registered")
public class AppStart extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		final View view = View.inflate(this, R.layout.activity_start, null);
		setContentView(view);
		// 渐变展示启动屏
		AlphaAnimation aa = new AlphaAnimation(0.3f, 1.0f);
		aa.setDuration(2000);
		view.startAnimation(aa);
		aa.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationEnd(Animation arg0) {
				login();
			}

			@Override
			public void onAnimationRepeat(Animation animation) {

			}

			@Override
			public void onAnimationStart(Animation animation) {

			}
		});
	}

	@Override
	public void onDestroy() {

		super.onDestroy();
	}

	@Override
	public void onPause() {
		super.onPause();

	}

	@Override
	public void onResume() {

		super.onResume();

	}

	/**
	 * 登录
	 */
	private void login() {
		User cacheUser = UserService.getInstance(this).getCacheUser();
		if (cacheUser != null
				&& (int) cacheUser.getAutoLogin() == (int) User.AUTO_LOGIN) {
			AppContext.currentUser=cacheUser;
			AppContext.userType=cacheUser.getType();
			startActivity(new Intent(AppStart.this, SplashActivity.class));

			finish();

		} else {
			startActivity(new Intent(AppStart.this, SplashActivity.class));
			finish();
		}
	}

}
