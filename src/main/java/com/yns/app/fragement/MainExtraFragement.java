package com.yns.app.fragement;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.yns.R;
import com.yns.app.adapter.HomeKindAdapter;
import com.yns.model.HomeKind;
import com.yns.widget.HeaderBar;

public class MainExtraFragement extends BaseFragment {

	private HeaderBar headerBar;

	@SuppressLint("InflateParams")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mView = inflater.inflate(R.layout.fragment_extra, null);
		return mView;
	}

	@Override
	protected void initData() {
		initHomeKind();
	}

	@Override
	protected void initView() {

		headerBar = (HeaderBar) findViewWithId(R.id.title);
		headerBar.setTitle("生活服务");
	}

	@Override
	public void bindView() {
		headerBar.back.setVisibility(View.GONE);

	}

	/**
	 * 初始化首页服务种类
	 */
	private void initHomeKind() {
		List<HomeKind> homeKinds = new ArrayList<HomeKind>();
		HomeKind ks1 = new HomeKind(R.drawable.extra_verification,
				R.string.extra_1);
		HomeKind ks2 = new HomeKind(R.drawable.extra_illegal, R.string.extra_2);

		homeKinds.add(ks1);
		homeKinds.add(ks2);

		GridView home = (GridView) findViewWithId(R.id.grid_kind);
		home.setAdapter(new HomeKindAdapter(baseActivity, homeKinds, false));
	}

}
