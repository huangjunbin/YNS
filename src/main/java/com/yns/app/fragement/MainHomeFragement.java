package com.yns.app.fragement;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;

import com.yns.R;
import com.yns.app.adapter.HomeKindAdapter;
import com.yns.app.ui.BaseActivity;
import com.yns.app.ui.JobActivity;
import com.yns.app.ui.MessageActivity;
import com.yns.app.ui.MessageDetailActivity;
import com.yns.app.ui.MyMarketActivity;
import com.yns.app.ui.WebViewActivity;
import com.yns.model.Advert;
import com.yns.model.HomeKind;
import com.yns.model.ResponeModel;
import com.yns.net.Urls;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.ComonService;
import com.yns.widget.HeaderBar;
import com.yns.widget.SlideShowView;
import com.yns.widget.SlideShowView.OnBranchClickListener;

import java.util.ArrayList;
import java.util.List;

public class MainHomeFragement extends BaseFragment implements
        OnBranchClickListener {

    private HeaderBar headerBar;
    private SlideShowView slideShowView;
    private ImageView iv1, iv2, iv3, iv4, iv5, iv6, iv7, iv8, iv9, iv10, iv11,
            iv12;

    @SuppressLint("InflateParams")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.activity_home, null);
        return mView;
    }

    @Override
    protected void initData() {
        initHomeKind();
    }

    @Override
    protected void initView() {

        headerBar = (HeaderBar) findViewWithId(R.id.title);
        headerBar.setTitle("云南大学生");
        headerBar.back.setVisibility(View.GONE);
        slideShowView = (SlideShowView) findViewWithId(R.id.slideshowview);
        iv1 = findViewWithId(R.id.iv1);
        iv2 = findViewWithId(R.id.iv2);
        iv3 = findViewWithId(R.id.iv3);
        iv4 = findViewWithId(R.id.iv4);
        iv5 = findViewWithId(R.id.iv5);
        iv6 = findViewWithId(R.id.iv6);
        iv7 = findViewWithId(R.id.iv7);
        iv8 = findViewWithId(R.id.iv8);
        iv9 = findViewWithId(R.id.iv9);
        iv10 = findViewWithId(R.id.iv10);
        iv11 = findViewWithId(R.id.iv11);
        iv12 = findViewWithId(R.id.iv12);
    }

    @Override
    public void onResume() {

        super.onResume();
    }

    @Override
    public void lastLoad() {
        super.lastLoad();

    }

    @Override
    public void bindView() {

    }

    private void initHomeKind() {
        List<HomeKind> homeKinds = new ArrayList<HomeKind>();
        HomeKind ks1 = new HomeKind(R.drawable.h1, R.string.no);
        HomeKind ks2 = new HomeKind(R.drawable.h2, R.string.no);
        HomeKind ks3 = new HomeKind(R.drawable.h3, R.string.no);
        HomeKind ks4 = new HomeKind(R.drawable.h4, R.string.no);
        HomeKind ks5 = new HomeKind(R.drawable.h5, R.string.no);
        HomeKind ks6 = new HomeKind(R.drawable.h6, R.string.no);
        HomeKind ks7 = new HomeKind(R.drawable.h7, R.string.no);
        HomeKind ks8 = new HomeKind(R.drawable.h8, R.string.no);
        HomeKind ks9 = new HomeKind(R.drawable.h9, R.string.no);
        HomeKind ks10 = new HomeKind(R.drawable.h10, R.string.no);
        HomeKind ks11 = new HomeKind(R.drawable.h11, R.string.no);
        HomeKind ks12 = new HomeKind(R.drawable.h12, R.string.no);
        homeKinds.add(ks1);
        homeKinds.add(ks2);
        homeKinds.add(ks3);
        homeKinds.add(ks4);
        homeKinds.add(ks5);
        homeKinds.add(ks6);
        homeKinds.add(ks7);
        homeKinds.add(ks8);
        homeKinds.add(ks9);
        homeKinds.add(ks10);
        homeKinds.add(ks11);
        homeKinds.add(ks12);
        GridView home = (GridView) findViewWithId(R.id.grid_kind);
        home.setAdapter(new HomeKindAdapter((BaseActivity) getActivity(),
                homeKinds));

        List<Integer> strList = new ArrayList<Integer>();
        strList.add(R.drawable.h0);
        slideShowView.setImagesById(strList);
        getBanner();
        initListener(iv1, 1);
        initListener(iv2, 2);
        initListener(iv3, 3);
        initListener(iv4, 4);
        initListener(iv5, 5);
        initListener(iv6, 6);
        initListener(iv7, 7);
        initListener(iv8, 8);
        initListener(iv9, 9);
        initListener(iv10, 10);
        initListener(iv11, 11);
        initListener(iv12, 12);
    }

    @Override
    public void OnBranchClick(int position) {

    }

    private void initListener(View view, final int position) {
        view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                int p = 0;
                switch (position) {
                    case 1:
                        p = 31;
                        break;
                    case 2:
                        p = 14;
                        break;
                    case 3:
                        p = 1;
                        baseActivity.startActivity(new Intent(baseActivity,
                                JobActivity.class));
                        return;
                    case 4:
                        p = 116;
                        break;
                    case 5:
                        p = 19;
                        break;
                    case 6:
                        p = 26;
                        break;
                    case 7:
                        p = 35;
                        baseActivity.startActivity(new Intent(getActivity(),
                                MyMarketActivity.class));
                        return;

                    case 8:
                        p = 5;
                        break;
                    case 9:
                        p = 36;
                        break;
                    case 10:
                        p = 22;
                        break;
                    case 11:
                        p = 32;
                        break;
                    case 12:
                        break;
                    default:
                        break;
                }
                Intent intent;
                if (p == 14 || p == 19 || p == 26 || p == 22) {
                    intent = new Intent();
                    intent.setClass(baseActivity, MessageDetailActivity.class);
                    if (p == 14) {
                        intent.putExtra("title", "创业中心");
                    }
                    if (p == 19) {
                        intent.putExtra("title", "技能培训");
                    }
                    if (p == 26) {
                        intent.putExtra("title", "就业指导");
                    }
                    if (p == 22) {
                        intent.putExtra("title", "校园风云");
                    }
                    intent.putExtra("position", p);
                    baseActivity.startActivity(intent);
                } else if (position == 12) {
                    intent = new Intent();
                    intent.setClass(baseActivity, WebViewActivity.class);
                    intent.putExtra("url",
                            "http://www.dxsapp.com/front/app_contact.html");
                    intent.putExtra("title", "关于我们");
                    baseActivity.startActivity(intent);
                } else {
                    intent = new Intent();
                    intent.setClass(baseActivity, MessageActivity.class);
                    intent.putExtra("position", p);
                    baseActivity.startActivity(intent);
                }
            }

        });
    }

    private void getBanner() {
        ComonService.getInstance(context).getBanner(10 + "", 1 + "", "index",
                new CustomAsyncResponehandler() {
                    public void onFinish() {

                    }

                    ;

                    @SuppressWarnings("unchecked")
                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel != null && baseModel.isStatus()) {
                            List<Advert> lists = (List<Advert>) baseModel
                                    .getResultObj();
                            if (lists != null && lists.size() > 0) {
                                List<String> strList = new ArrayList<String>();
                                for (Advert item : lists) {
                                    strList.add(Urls.IMAGE_URL + item.getPic());
                                }
                                slideShowView.setImageUris(strList);
                            }
                        }
                    }
                });
    }
}
