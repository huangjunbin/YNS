package com.yns.app.fragement;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.location.LocationClientOption.LocationMode;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.model.LatLng;
import com.yns.R;
import com.yns.util.LogUtil;

/**
 * @BaseBaidMapFragement.java
 * @author:lmt
 * @function:百度基础地图
 * @d2014-12-16下午4:29:57
 * @update:
 */
@SuppressLint("InflateParams")
public class BaseBaidMapFragement extends BaseFragment {
	private String TAG = "BaseBaidMapFragement";
	private boolean isLog = true;
	// -------
	public MapView mMapView = null;
	public BaiduMap mBaiduMap = null;
	// 定位相关
	public LocationClient mLocClient;
	public MyLocationListenner myListener = new MyLocationListenner();
	public boolean isFirstLoc = true;// 是否首次定位

	public OnLocEndListener mcallback;

	public interface OnLocEndListener {
		public void onLocEnd(LatLng latLng);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		if (activity instanceof OnLocEndListener) {
			mcallback = (OnLocEndListener) activity;
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		mView = inflater.inflate(R.layout.activity_basemap, null);
		return mView;
	}

	@Override
	protected void initData() {
		// TODO Auto-generated method stub
		// 地图类型:普通地图
		mBaiduMap.setMapType(BaiduMap.MAP_TYPE_NORMAL);
		// 开启交通图
		mBaiduMap.setTrafficEnabled(true);
		// 开启定位图层
		mBaiduMap.setMyLocationEnabled(true);
		// // // 设置地图默认缩放级别
		// MapStatusUpdate msu = MapStatusUpdateFactory.zoomTo(15.0f);
		// mBaiduMap.setMapStatus(msu);
		// 定位初始化
		mLocClient = new LocationClient(getActivity());
		mLocClient.registerLocationListener(myListener);
		LocationClientOption option = new LocationClientOption();
		option.setLocationMode(LocationMode.Hight_Accuracy);
		option.setOpenGps(true);// 打开gps
		option.setCoorType("bd09ll"); // 设置坐标类型
		option.setScanSpan(1000);// 设置发起定位请求的间隔时间为1000ms
		option.setIsNeedAddress(true);// 返回的定位结果包含地址信息
		mLocClient.setLocOption(option);
		mLocClient.start();
	}

	@Override
	protected void initView() {
		// TODO Auto-generated method stub
		// 获取地图控件引用
		mMapView = (MapView) mView.findViewById(R.id.bmapView);
		mBaiduMap = mMapView.getMap();
		System.out.println("here:"+mBaiduMap);
	}

	@Override
	protected void bindView() {
		// TODO Auto-generated method stub

	}

	/**
	 * 定位SDK监听函数
	 */
	public class MyLocationListenner implements BDLocationListener {

		@Override
		public void onReceiveLocation(BDLocation location) {
			// map view 销毁后不在处理新接收的位置
			if (location == null || mMapView == null)
				return;
			MyLocationData locData = new MyLocationData.Builder()
					.accuracy(location.getRadius())
					// 此处设置开发者获取到的方向信息，顺时针0-360
					.direction(100).latitude(location.getLatitude())
					.longitude(location.getLongitude()).build();
			mBaiduMap.setMyLocationData(locData);
			LogUtil.d(TAG, "---------", isLog);
			if (isFirstLoc) {
				isFirstLoc = false;
				// LogUtil.d(TAG, "hasAddr:---------" + location.hasAddr(),
				// isLog);
				// LogUtil.d(TAG, "getAddrStr:---------" +
				// location.getAddrStr(),
				// isLog);
				// LogUtil.d(TAG,
				// "getProvince:---------" + location.getProvince(), isLog);
				// LogUtil.d(TAG, "getCity:---------" + location.getCity(),
				// isLog);
				// LogUtil.d(TAG, "getStreet:---------" + location.getStreet(),
				// isLog);
				// LogUtil.d(
				// TAG,
				// "getStreetNumber:---------"
				// + location.getStreetNumber(), isLog);
				// LogUtil.d(TAG,
				// "getDistrict:---------" + location.getDistrict(), isLog);
				// LogUtil.d(TAG, "getFloor:---------" + location.getFloor(),
				// isLog);
				// LogUtil.d(
				// TAG,
				// "getNetworkLocationType:---------"
				// + location.getNetworkLocationType(), isLog);
				//
				LatLng ll = new LatLng(location.getLatitude(),
						location.getLongitude());
				MapStatusUpdate u = MapStatusUpdateFactory.newLatLng(ll);
				mBaiduMap.animateMapStatus(u);
				if (mcallback != null) {
					mcallback.onLocEnd(ll);
				}
			}
		}

		public void onReceivePoi(BDLocation poiLocation) {
			System.out.println(poiLocation);

		}
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		// 在activity执行onDestroy时执行mMapView.onDestroy()，实现地图生命周期管理
		// 退出时销毁定位
		mLocClient.stop();
		// 关闭定位图层
		mBaiduMap.setMyLocationEnabled(false);
		mMapView.onDestroy();
		mMapView = null;
		super.onDestroy();
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		// 在activity执行onPause时执行mMapView. onPause ()，实现地图生命周期管理
		mMapView.onPause();

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// 在activity执行onResume时执行mMapView. onResume ()，实现地图生命周期管理
		mMapView.onResume();
	}

}
