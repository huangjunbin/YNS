package com.yns.app.fragement;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yns.R;
import com.yns.app.AppContext;
import com.yns.app.ui.CompanyRemarkActivity;
import com.yns.app.ui.JobCompanyListActivity;
import com.yns.app.ui.LoginActivity;
import com.yns.app.ui.MainActivity2;
import com.yns.app.ui.NoticeActivity;
import com.yns.app.ui.ResumeActivity;
import com.yns.app.ui.SetActivity;
import com.yns.app.ui.UpdatePwdActivity;
import com.yns.model.User;
import com.yns.service.UserService;
import com.yns.widget.CircleImageView;
import com.yns.widget.HeaderBar;

public class MainCompanyPersonFragement extends BaseFragment implements
		OnClickListener {

	private HeaderBar headerBar;
	private User tempUser = AppContext.currentUser;

	private CircleImageView circleImageViewPhoto;
	private TextView textViewNickName;
	private RelativeLayout rlResume,rlResume2;
	private RelativeLayout rlZ1, rlZ2, rlMessage;
	private LinearLayout rlZ3, rlZ4, llUpdate, llRemark;
	private Intent intent;

	@SuppressLint("InflateParams")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mView = inflater.inflate(R.layout.activity_company_person, null);
		return mView;
	}

	@Override
	protected void initData() {
		updateView();
	}

	@Override
	protected void initView() {
		headerBar = (HeaderBar) findViewWithId(R.id.title);

		headerBar.setTitle("企业中心");
		circleImageViewPhoto = (CircleImageView) findViewWithId(R.id.person_photo);
		textViewNickName = (TextView) findViewWithId(R.id.person_nickname);
		rlResume = findViewWithId(R.id.rl_resume);
		rlResume2= findViewWithId(R.id.rl_resume2);
		rlZ1 = findViewWithId(R.id.rl_z1);
		rlZ2 = findViewWithId(R.id.rl_z2);
		rlZ3 = findViewWithId(R.id.rl_z3);
		rlMessage = findViewWithId(R.id.rl_message);
		rlZ4 = findViewWithId(R.id.rl_z4);
		llUpdate = findViewWithId(R.id.ll_update);
		llRemark = findViewWithId(R.id.ll_remark);
	}

	@Override
	public void lastLoad() {

		super.lastLoad();

	}

	private void updateView() {
		if (tempUser == null) {
			circleImageViewPhoto.setImageDrawable(getResources().getDrawable(
					R.drawable.def_head));
			baseActivity.setTextView(textViewNickName, "", "", false);
		} else {

			AppContext.setImage(tempUser.getHeaderPic(), circleImageViewPhoto,
					AppContext.memberPhotoOption);
			baseActivity.setTextView(textViewNickName, tempUser.getNickName(),
					"佚名", false);
			headerBar.back.setVisibility(View.GONE);
		}
	}

	@Override
	public void bindView() {
		((View) findViewWithId(R.id.person_set)).setOnClickListener(this);
		((View) findViewWithId(R.id.person_logout)).setOnClickListener(this);
		circleImageViewPhoto.setOnClickListener(this);
		rlMessage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(getActivity(), NoticeActivity.class));
			}
		});
		rlResume.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(getActivity(), ResumeActivity.class);
				i.putExtra("type", 1);
				startActivity(i);
			}
		});
		rlResume2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(getActivity(), ResumeActivity.class);
				i.putExtra("type", 3);
				startActivity(i);
			}
		});
		rlZ1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				intent = new Intent();
				intent.setClass(getActivity(), JobCompanyListActivity.class);
				intent.putExtra("flag", 0);
				startActivity(intent);
			}
		});
		rlZ2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				intent = new Intent();
				intent.setClass(getActivity(), JobCompanyListActivity.class);
				intent.putExtra("flag", 1);
				startActivity(intent);
			}
		});
		rlZ3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				intent = new Intent();
				intent.setClass(getActivity(), JobCompanyListActivity.class);
				intent.putExtra("flag", 2);
				startActivity(intent);
			}
		});
		rlZ4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				intent = new Intent();
				intent.setClass(getActivity(), JobCompanyListActivity.class);
				intent.putExtra("flag", 3);
				startActivity(intent);
			}
		});
		llUpdate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(getActivity(), UpdatePwdActivity.class));
			}
		});
		llRemark.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(getActivity(),
						CompanyRemarkActivity.class));
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.person_set:
			baseActivity.JumpToActivity(SetActivity.class, false);
			break;
		case R.id.person_logout:
			logout();
			break;
		}
	}

	private void logout() {
		AppContext.currentUser = null;
		UserService.getInstance(baseActivity).clearCacheUser();

		Intent intent = new Intent(getActivity(), LoginActivity.class);
		intent.putExtra("from", LoginActivity.FROM_PERSON);
		startActivity(intent);

		((MainActivity2) getActivity()).toMain();
	}

	@Override
	public void onResume() {
		lastLoad();
		tempUser = AppContext.currentUser;
		updateView();
		super.onResume();
	}

}
