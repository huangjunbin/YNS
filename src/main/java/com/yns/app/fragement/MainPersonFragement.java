package com.yns.app.fragement;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.yns.R;
import com.yns.app.AppContext;
import com.yns.app.ui.AddressActivity;
import com.yns.app.ui.ApplyActivity;
import com.yns.app.ui.LoginActivity;
import com.yns.app.ui.MainActivity2;
import com.yns.app.ui.MarketActivity;
import com.yns.app.ui.MyCollectActivity;
import com.yns.app.ui.NoticeActivity;
import com.yns.app.ui.OrderListActivity;
import com.yns.app.ui.PersonInfoActivity;
import com.yns.app.ui.ResumeActivity;
import com.yns.app.ui.SetActivity;
import com.yns.app.ui.ShopCartActivity;
import com.yns.app.ui.UpdatePwdActivity;
import com.yns.model.User;
import com.yns.service.UserService;
import com.yns.widget.CircleImageView;
import com.yns.widget.HeaderBar;

public class MainPersonFragement extends BaseFragment implements
		OnClickListener {

	private HeaderBar headerBar;
	private User tempUser = AppContext.currentUser;

	private CircleImageView circleImageViewPhoto;
	private TextView textViewNickName;
	private RelativeLayout rlApply, rlMessage, rlCollect, rlResume,rlShopCart,rlOrder,rlAddress,rlClear,rlMarket;
	private LinearLayout llUpdate;

	@SuppressLint("InflateParams")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mView = inflater.inflate(R.layout.activity_person, null);
		return mView;
	}

	@Override
	protected void initData() {
		updateView();
	}

	@Override
	protected void initView() {
		headerBar = (HeaderBar) findViewWithId(R.id.title);

		headerBar.setTitle(baseActivity.getResString(R.string.perosn_title));
		circleImageViewPhoto = (CircleImageView) findViewWithId(R.id.person_photo);
		textViewNickName = (TextView) findViewWithId(R.id.person_nickname);
		rlApply = findViewWithId(R.id.rl_apply);
		rlMessage = findViewWithId(R.id.rl_message);
		rlCollect = findViewWithId(R.id.rl_collect);
		rlResume = findViewWithId(R.id.rl_resume);
		llUpdate = findViewWithId(R.id.ll_update);
		rlShopCart=findViewWithId(R.id.rl_shopcart);
		rlOrder=findViewWithId(R.id.rl_order);
		rlAddress=findViewWithId(R.id.rl_address);
		rlClear=findViewWithId(R.id.rl_clear);
		rlMarket=findViewWithId(R.id.rl_market);
	}

	@Override
	public void lastLoad() {

		super.lastLoad();

	}

	private void updateView() {
		if (tempUser == null) {
			circleImageViewPhoto.setImageDrawable(getResources().getDrawable(
					R.drawable.def_head));
			baseActivity.setTextView(textViewNickName, "", "", false);
		} else {

			AppContext.setImage(tempUser.getHeaderPic(), circleImageViewPhoto,
					AppContext.memberPhotoOption);
			baseActivity.setTextView(textViewNickName, tempUser.getNickName(),
					"佚名", false);
			headerBar.back.setVisibility(View.GONE);
		}
	}

	@Override
	public void bindView() {
		((View) findViewWithId(R.id.person_set)).setOnClickListener(this);
		((View) findViewWithId(R.id.person_logout)).setOnClickListener(this);
		circleImageViewPhoto.setOnClickListener(this);
		rlMarket.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(getActivity(), MarketActivity.class));
			}
		});
		rlMessage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(getActivity(), NoticeActivity.class));
			}
		});
		rlCollect.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(getActivity(), MyCollectActivity.class));
			}
		});
		rlApply.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(getActivity(), ApplyActivity.class));
			}
		});
		rlResume.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(getActivity(), ResumeActivity.class));
			}
		});
		llUpdate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(getActivity(), UpdatePwdActivity.class));
			}
		});
		textViewNickName.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(getActivity(),
						PersonInfoActivity.class));
			}
		});
		circleImageViewPhoto.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				startActivity(new Intent(getActivity(),
						PersonInfoActivity.class));
			}
		});
		rlShopCart.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(getActivity(), ShopCartActivity.class));
			}
		});
		rlAddress.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(getActivity(), AddressActivity.class));
			}
		});
		rlOrder.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(getActivity(), OrderListActivity.class));
			}
		});
		rlClear.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ImageLoader.getInstance().clearMemoryCache();
				ImageLoader.getInstance().clearDiscCache();
				baseActivity.showToast("清除成功");
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.person_set:
			baseActivity.JumpToActivity(SetActivity.class, false);
			break;
		case R.id.person_logout:
			logout();
			break;
		}
	}

	private void logout() {
		AppContext.currentUser = null;
		UserService.getInstance(baseActivity).clearCacheUser();

		Intent intent = new Intent(getActivity(), LoginActivity.class);
		intent.putExtra("from", LoginActivity.FROM_PERSON);
		startActivity(intent);

		((MainActivity2) getActivity()).toMain();

	}

	@Override
	public void onResume() {
		lastLoad();
		tempUser = AppContext.currentUser;
		updateView();
		super.onResume();
	}

}
