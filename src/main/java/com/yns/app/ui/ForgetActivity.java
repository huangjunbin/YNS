package com.yns.app.ui;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.yns.R;
import com.yns.app.AppContext;
import com.yns.model.ResponeModel;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.UserService;
import com.yns.util.StringUtils;
import com.yns.util.UIHelper;
import com.yns.widget.HeaderBar;

public class ForgetActivity extends BaseActivity implements OnClickListener {

	private HeaderBar headerBar;
	private EditText editTextTel, etEmail;

	public ForgetActivity() {
		super(R.layout.activity_forgetpassword);
	}

	@Override
	public void initViews() {
		headerBar = (HeaderBar) findViewById(R.id.title);
		editTextTel = (EditText) findViewById(R.id.forget_tel);
		etEmail = (EditText) findViewById(R.id.et_email);

	}

	@Override
	public void initData() {

	}

	@Override
	public void bindViews() {
		headerBar.setTitle("找回密码");

		findViewById(R.id.forget).setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.forget:
			if (checkForget()) {
				UserService.getInstance(context).ForgetPassword(
						editTextTel.getText().toString(),
						etEmail.getText().toString(), AppContext.userType + "",
						new CustomAsyncResponehandler() {

							@Override
							public void onSuccess(ResponeModel baseModel) {
								super.onSuccess(baseModel);

							}
						});
			}
			break;
		}
	}

	private boolean checkForget() {
		if (StringUtils.isEmpty(editTextTel.getText().toString())) {
			UIHelper.ShowMessage(context, "用户名不能为空");
			return false;
		}

		 
		if (!StringUtils.isEmail(etEmail.getText().toString())) {
			UIHelper.ShowMessage(context, "邮箱格式不正确");
			return false;
		}
		return true;
	}

}
