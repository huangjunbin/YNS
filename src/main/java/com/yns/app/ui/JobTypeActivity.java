package com.yns.app.ui;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;

import com.yns.R;
import com.yns.app.adapter.JobTypeAdapter;
import com.yns.model.JobType;
import com.yns.widget.HeaderBar;
import com.yns.widget.XListView;

public class JobTypeActivity extends BaseActivity {

	private HeaderBar headerBar;
	private XListView xlvData;

	private JobTypeAdapter jobTypeAdapter;
	public static List<JobType> jobTypes;

	private String title = "";

	public JobTypeActivity() {
		super(R.layout.activity_jobtype);

	}

	@Override
	public void initViews() {

		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.top_right_btn.setVisibility(View.VISIBLE);
		headerBar.top_right_btn.setText("完成");
		headerBar.top_right_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if ("工作地点".equals(title)) {
					Intent data=new Intent();
					data.putExtra("data", jobTypeAdapter.getCurrentName());
					setResult(JobSearchActivity.code1, data);
				}
				if ("行业类别".equals(title)) {
					Intent data=new Intent();
					data.putExtra("data", jobTypeAdapter.getCurrentName());
					setResult(JobSearchActivity.code2, data);
				}
				if ("工作经验".equals(title)) {
					Intent data=new Intent();
					data.putExtra("data", jobTypeAdapter.getCurrentName());
					setResult(JobSearchActivity.code3, data);
				}
				if ("职位类型".equals(title)) {
					Intent data=new Intent();
					data.putExtra("data", jobTypeAdapter.getCurrentName());
					setResult(JobSearchActivity.code4, data);
				}
				JobTypeActivity.this.finish();
			}
		});
		xlvData = (XListView) findViewById(R.id.xlv_data);
		xlvData.setPullRefreshEnable(true);
		xlvData.setPullLoadEnable(true);

	}

	@SuppressWarnings("unchecked")
	@Override
	public void initData() {
		jobTypes = new ArrayList<JobType>();
		title = getIntent().getStringExtra("title");
		jobTypes = (List<JobType>) getIntent().getSerializableExtra("data");
		headerBar.setTitle(title);
		jobTypeAdapter = new JobTypeAdapter(this, jobTypes);
		xlvData.setAdapter(jobTypeAdapter);
		xlvData.setPullRefreshEnable(false);
		xlvData.setPullLoadEnable(false);
		xlvData.setFooterDividersEnabled(false);
	}

	@Override
	public void bindViews() {

	}

}
