package com.yns.app.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.TimePicker;

import com.yns.R;
import com.yns.app.AppConfig;
import com.yns.app.AppContext;
import com.yns.model.ResponeModel;
import com.yns.model.Resume;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.ComonService;
import com.yns.util.MediaUtil;
import com.yns.util.StringUtils;
import com.yns.util.UIHelper;
import com.yns.widget.CameraView;
import com.yns.widget.HeaderBar;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Calendar;

public class ResumeAddActivity extends BaseActivity {

	private HeaderBar headerBar;

	private String pic = "";
	private String personImageName = "resume.jpg";
	private EditText etName, etYuanxiao, etZhuanye, etMingzu, etHuji,
			etShengao,  ettechang, etPingjia, etGongzuojinyan,
			etXiaoyuan;
	private RadioButton cbGril, cbBoy, cbYes, cbNo, cbthree,cbY,cbN;

	private TextView etDate, etXueli, etJinyan, etXinzi, tvSelect;
	private EditText etMobile, etQQ, etEmail;
	int xueli = -1;
	int xinzhi = -1;
	int nianxian = -1;
	private ImageView ivPhoto;
	private Resume resume;

	public ResumeAddActivity() {
		super(R.layout.activity_resumeadd);
	}

	@Override
	public void initViews() {

		headerBar = (HeaderBar) findViewById(R.id.title);
		etName = (EditText) findViewById(R.id.et_name);
		etDate = (TextView) findViewById(R.id.et_date);
		etYuanxiao = (EditText) findViewById(R.id.et_yuanxiao);
		etZhuanye = (EditText) findViewById(R.id.et_zhuanye);
		etMingzu = (EditText) findViewById(R.id.et_mingzu);
		etHuji = (EditText) findViewById(R.id.et_huji);
		etShengao = (EditText) findViewById(R.id.et_shengao);
		etXueli = (TextView) findViewById(R.id.et_xueli);
		etJinyan = (TextView) findViewById(R.id.et_jinyan);
		cbY= (RadioButton) findViewById(R.id.cb_y);
		cbN= (RadioButton) findViewById(R.id.cb_n);
		etXinzi = (TextView) findViewById(R.id.et_xinzi);
		ettechang = (EditText) findViewById(R.id.et_techang);
		etPingjia = (EditText) findViewById(R.id.et_pingjia);
		etGongzuojinyan = (EditText) findViewById(R.id.et_gongzuojinyan);
		etXiaoyuan = (EditText) findViewById(R.id.et_xiaoyuan);
		cbGril = (RadioButton) findViewById(R.id.cb_gril);
		cbBoy = (RadioButton) findViewById(R.id.cb_boy);
		cbYes = (RadioButton) findViewById(R.id.cb_yes);
		cbNo = (RadioButton) findViewById(R.id.cb_no);
		cbthree = (RadioButton) findViewById(R.id.cb_three);
		tvSelect = (TextView) findViewById(R.id.tv_select);
		ivPhoto = (ImageView) findViewById(R.id.iv_photo);
		etMobile = (EditText) findViewById(R.id.et_mobile);
		etQQ = (EditText) findViewById(R.id.et_qq);
		etEmail = (EditText) findViewById(R.id.et_email);
	}

	@Override
	public void initData() {
		resume = (Resume) getIntent().getSerializableExtra("resume");
	}

	@Override
	public void bindViews() {
		tvSelect.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				CameraView.showCameraView(ResumeAddActivity.this,
						personImageName);
			}
		});
		if (resume == null) {
			headerBar.setTitle("添加简历");
		} else {
			headerBar.setTitle("编辑简历");
			etName.setText(resume.getName());
			etDate.setText(resume.getChuShengNianYue());
			etYuanxiao.setText(resume.getBiYeXueXiao());
			etZhuanye.setText(resume.getZhuanYe());
			etMingzu.setText(resume.getMingZu());
			etHuji.setText(resume.getHuJi());
			etShengao.setText(resume.getShenGao());
			etXueli.setText(resume.getXueLiName());
			etJinyan.setText(resume.getGongZuoNainXianName());

			if("全职".equals(resume.getGongZuoLeiXing())){
				cbY.setChecked(true);
			}else{
				cbN.setChecked(true);
			}
			etXinzi.setText(resume.getQiDaiXingZiName());
			ettechang.setText(resume.getJiNengZhuanChang());
			etPingjia.setText(resume.getZiWoPingJia());
			etGongzuojinyan.setText(resume.getGongZuoJingYan());
			etXiaoyuan.setText(resume.getXiaoYuanChengZhangJingLi());
			etEmail.setText(resume.getEmail());
			etQQ.setText(resume.getQq());
			etMobile.setText(resume.getMobile());
			if ("0".equals(resume.getSex())) {
				cbGril.setChecked(true);
			} else {
				cbBoy.setChecked(true);
			}
			if ("1".equals(resume.getHunYinQingKuang())) {
				cbYes.setChecked(true);
			} else if ("0".equals(resume.getHunYinQingKuang())) {
				cbNo.setChecked(true);
			} else {
				cbthree.setChecked(true);
			}
			if (resume.getHeaderPic() != null) {
				AppContext.setImage(resume.getHeaderPic(), ivPhoto,
						AppContext.imageOption);
			}
		}
		headerBar.top_right_btn.setText("保存");
		headerBar.top_right_btn.setVisibility(View.VISIBLE);
		headerBar.top_right_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (StringUtils.isEmpty(etDate.getText().toString())) {
					showToast("请选择出生日期");
					return;
				}
				if (StringUtils.isEmpty(etName.getText().toString())) {
					showToast("请输入姓名");
					return;
				}
				if (nianxian == -1 && "".equals(etJinyan.getText().toString())) {
					showToast("请选择工作经验");
					return;
				}
				int sex = 0;
				if (cbBoy.isChecked()) {
					sex = 1;
				}
				if (cbGril.isChecked()) {
					sex = 0;
				}
				if (!cbGril.isChecked() && !cbBoy.isChecked()) {
					showToast("请选择性别");
					return;
				}

				int hunYinQingKuang = 0;
				if (cbYes.isChecked()) {
					hunYinQingKuang = 1;
				}
				if (cbNo.isChecked()) {
					hunYinQingKuang = 0;
				}
				if (cbthree.isChecked()) {
					hunYinQingKuang = 2;
				}
				if (!cbYes.isChecked() && !cbNo.isChecked()
						&& !cbthree.isChecked()) {
					showToast("请选择婚姻情况");
					return;
				}
				cbYes.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						if (isChecked) {
							cbNo.setChecked(false);
							cbthree.setChecked(false);
						}
					}
				});
				cbNo.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						if (isChecked) {
							cbYes.setChecked(false);
							cbthree.setChecked(false);
						}
					}
				});
				cbthree.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						if (isChecked) {
							cbYes.setChecked(false);
							cbNo.setChecked(false);
						}
					}
				});
				if (StringUtils.isEmpty(etMingzu.getText().toString())) {
					showToast("请输入名族信息");
					return;
				}
				if (StringUtils.isEmpty(etShengao.getText().toString())) {
					showToast("请输入身高信息");
					return;
				}
				if (xueli == -1 && "".equals(etXueli.getText().toString())) {
					showToast("请选择学历");
					return;
				}
				if (StringUtils.isEmpty(etHuji.getText().toString())) {
					showToast("请输入户籍信息");
					return;
				}
				if (StringUtils.isEmpty(etYuanxiao.getText().toString())) {
					showToast("请输入院校信息");
					return;
				}
				if (StringUtils.isEmpty(etZhuanye.getText().toString())) {
					showToast("请输入专业信息");
					return;
				}
				if (StringUtils.isEmpty(ettechang.getText().toString())) {
					showToast("请输入技能特长信息");
					return;
				}
				if (StringUtils.isEmpty(etGongzuojinyan.getText().toString())) {
					showToast("请输入工作经验");
					return;
				}

				if (xinzhi == -1 && "".equals(etXinzi.getText().toString())) {
					showToast("请选择期望薪资");
					return;
				}
				if (StringUtils.isEmpty(etPingjia.getText().toString())) {
					showToast("请输入自我评价");
					return;
				}
				if (StringUtils.isEmpty(etXiaoyuan.getText().toString())) {
					showToast("请输入校园成长经历");
					return;
				}
				ComonService.getInstance(context).editResume(
						resume == null ? null : resume
								.getAppMemberResumeInfoId(),
						AppContext.currentUser == null ? ""
								: AppContext.currentUser.getUserId(),
						etDate.getText().toString(),
						etName.getText().toString(), "0", nianxian + "", pic,
						sex + "", hunYinQingKuang + "",
						etMingzu.getText().toString(),
						etShengao.getText().toString(), xueli + "",
						etHuji.getText().toString(),
						etYuanxiao.getText().toString(),
						etZhuanye.getText().toString(), "",
						ettechang.getText().toString(),
						etGongzuojinyan.getText().toString(),
						cbY.isChecked()?"全职":"兼职", xinzhi + "", "",
						etPingjia.getText().toString(),
						etXiaoyuan.getText().toString(), "",
						etMobile.getText().toString(),
						etQQ.getText().toString(),
						etEmail.getText().toString(),
						new CustomAsyncResponehandler() {
							public void onFinish() {

							};

							@Override
							public void onSuccess(ResponeModel baseModel) {
								super.onSuccess(baseModel);
								if (baseModel != null && baseModel.isStatus()) {
									UIHelper.ShowMessage(
											ResumeAddActivity.this, "保存成功");
									ResumeAddActivity.this.finish();
								}
							}
						});

			}
		});
		etDate.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(
						ResumeAddActivity.this);
				View view = View.inflate(ResumeAddActivity.this,
						R.layout.date_time_dialog, null);
				final DatePicker datePicker = (DatePicker) view
						.findViewById(R.id.date_picker);
				final TimePicker timePicker = (android.widget.TimePicker) view
						.findViewById(R.id.time_picker);
				builder.setView(view);

				Calendar cal = Calendar.getInstance();
				cal.setTimeInMillis(System.currentTimeMillis());
				datePicker.init(cal.get(Calendar.YEAR),
						cal.get(Calendar.MONTH),
						cal.get(Calendar.DAY_OF_MONTH), null);

				timePicker.setIs24HourView(true);
				timePicker.setCurrentHour(cal.get(Calendar.HOUR_OF_DAY));
				timePicker.setCurrentMinute(Calendar.MINUTE);

				final int inType = etDate.getInputType();
				etDate.setInputType(InputType.TYPE_NULL);

				etDate.setInputType(inType);

				builder.setTitle("选取起始时间");
				builder.setPositiveButton("确  定",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {

								StringBuffer sb = new StringBuffer();
								sb.append(String.format("%d-%02d-%02d",
										datePicker.getYear(),
										datePicker.getMonth() + 1,
										datePicker.getDayOfMonth()));
								sb.append("  ");

								etDate.setText(sb);

								dialog.cancel();
							}
						});
				Dialog dialog = builder.create();
				dialog.show();
			}
		});

		etXueli.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				final CharSequence[] item = { "初中", "高中", "中技", "中专", "大专",
						"本科", "硕士", "博士", "博后" };
				new AlertDialog.Builder(ResumeAddActivity.this)
						.setTitle("选择学历")
						.setItems(item, new DialogInterface.OnClickListener() { // content
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										etXueli.setText(item[which]);
										xueli = 72 + which;
									}

								})
						.setNegativeButton("取消",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {

										dialog.dismiss(); // 关闭alertDialog
									}
								}).show();

			}
		});
		etXinzi.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				final CharSequence[] item = { "面议", "1000-1500元/月",
						"1500-2000元/月", "2000-3000元/月", "3000-5000元/月",
						"5000-10000元/月" };
				new AlertDialog.Builder(ResumeAddActivity.this)
						.setTitle("选择薪资")
						.setItems(item, new DialogInterface.OnClickListener() { // content
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										etXinzi.setText(item[which]);
										xinzhi = 92 + which;
									}

								})
						.setNegativeButton("取消",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {

										dialog.dismiss(); // 关闭alertDialog
									}
								}).show();
			}
		});
		etJinyan.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				final CharSequence[] item = { "不限", "应届生", "1-3年", "3-5年",
						"5-10年", "10年以上", "20年以上", "30年以上", "退休人员" };
				new AlertDialog.Builder(ResumeAddActivity.this)
						.setTitle("选择薪资")
						.setItems(item, new DialogInterface.OnClickListener() { // content
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										etJinyan.setText(item[which]);
										nianxian = 82 + which;
									}

								})
						.setNegativeButton("取消",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {

										dialog.dismiss(); // 关闭alertDialog
									}
								}).show();
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case MediaUtil.ALBUM:
			if (data != null || resultCode == Activity.RESULT_OK) {
				startPhotoZoom(data.getData());
			}
			break;
		case MediaUtil.PHOTO:
			if (resultCode == Activity.RESULT_OK) {
				File temp = new File(MediaUtil.ROOTPATH + File.separator
						+ AppConfig.APP_PATH + File.separator + personImageName);
				startPhotoZoom(Uri.fromFile(temp));
			}
			break;
		case MediaUtil.ZOOMPHOTO:
			if (data != null || resultCode == Activity.RESULT_OK) {
				setPicToView(data);
			}
			break;
		}

	}

	/**
	 * 裁剪图片方法实现
	 * 
	 * @param uri
	 */
	public void startPhotoZoom(Uri uri) {
		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setDataAndType(uri, "image/*");
		// 下面这个crop=true是设置在开启的Intent中设置显示的VIEW可裁剪
		intent.putExtra("crop", "true");
		// aspectX aspectY 是宽高的比例
		// outputX outputY 是裁剪图片宽高
		intent.putExtra("outputX", 150);
		intent.putExtra("outputY", 150);
		intent.putExtra("return-data", true);
		File dir = new File((MediaUtil.ROOTPATH + File.separator
				+ AppConfig.APP_PATH + File.separator));
		if (!dir.exists())
			dir.mkdirs();

		File saveFile = new File(MediaUtil.ROOTPATH + File.separator
				+ AppConfig.APP_PATH + File.separator + personImageName);

		intent.putExtra("output", Uri.fromFile(saveFile)); // 传入目标文件
		intent.putExtra("outputFormat", "JPEG"); // 输入文件格式
		Intent it = Intent.createChooser(intent, "剪裁图片");
		startActivityForResult(it, MediaUtil.ZOOMPHOTO);
	}

	private void setPicToView(Intent data) {
		Bundle extras = data.getExtras();
		if (extras != null) {
			final Bitmap img = extras.getParcelable("data");
			ComonService.getInstance(context).uploadImg(

					new File(MediaUtil.ROOTPATH + File.separator
							+ AppConfig.APP_PATH + File.separator
							+ personImageName),
					new CustomAsyncResponehandler() {
						@Override
						public void onSuccess(ResponeModel baseModel) {
							super.onSuccess(baseModel);
							if (baseModel != null && baseModel.isStatus()) {

								try {
									ivPhoto.setImageBitmap(img);
									pic = new JSONObject(baseModel.getResult())
											.getString("url");
								} catch (JSONException e) {
									e.printStackTrace();
								}
								UIHelper.ShowMessage(context, "上传成功");
							}
						}
					});
		}
	}

}
