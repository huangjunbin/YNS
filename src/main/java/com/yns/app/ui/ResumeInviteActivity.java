package com.yns.app.ui;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

import com.yns.R;
import com.yns.app.AppContext;
import com.yns.model.ResponeModel;
import com.yns.model.Resume;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.ComonService;
import com.yns.util.StringUtils;
import com.yns.util.UIHelper;
import com.yns.widget.HeaderBar;

public class ResumeInviteActivity extends BaseActivity {
	private HeaderBar headerBar;
	private EditText editTextContent, editTextTel;
	private Resume resume;
	private String title = "job";
	private String jobInfoId="";

	public ResumeInviteActivity() {
		super(R.layout.activity_resumeinvite);
	}

	@Override
	public void initViews() {
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle("面试邀请");
		headerBar.top_right_btn.setText("发送");

		editTextContent = (EditText) findViewById(R.id.feedback_content);
		editTextTel = (EditText) findViewById(R.id.feedback_tel);
	}

	@Override
	public void initData() {
		jobInfoId=getIntent().getStringExtra("jobInfoId");
		resume = (Resume) getIntent().getSerializableExtra("resume");
		editTextContent.setText("面试邀请");
		editTextTel
				.setText(resume.getName()
						+ "先生\n"
						+ "你好!我公司已查看你的简历资料,经初步审核履历后,恭喜你符合本公司之职缺招募条件,行政人事部已着手安排面试,并邀请你于        年    月    日9:00-12:00、 14:00-16:00 (两个时间段均可)至公司参加面试.");
	}

	@Override
	public void bindViews() {
		headerBar.top_right_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (checkInput()) {
					ComonService.getInstance(context).sendJobMsg(title,
							editTextContent.getText().toString(),
							editTextTel.getText().toString(),
							resume.getToUserId(),
							AppContext.currentUser.getUserId(),
							AppContext.currentUser.getDeptId() + "",jobInfoId,
							new CustomAsyncResponehandler() {
								@Override
								public void onSuccess(ResponeModel baseModel) {
									super.onSuccess(baseModel);
									if (baseModel != null
											&& baseModel.isStatus()) {
										UIHelper.ShowMessage(context, "发送成功！");
										finish();
									}
								}
							});
				}
			}
		});
	}

	private boolean checkInput() {
		if (StringUtils.isEmpty(editTextContent.getText().toString())) {
			UIHelper.ShowMessage(context, "请先输入标题");
			return false;
		}
		if (StringUtils.isEmpty(editTextTel.getText().toString())) {
			UIHelper.ShowMessage(context, "请输入内容");
			return false;
		}

		return true;
	}
}
