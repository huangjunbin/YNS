package com.yns.app.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.yns.R;
import com.yns.app.AppConfig;
import com.yns.app.AppContext;
import com.yns.model.Market;
import com.yns.model.ProductType;
import com.yns.model.ResponeModel;
import com.yns.model.School;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.ComonService;
import com.yns.service.GoodService;
import com.yns.util.StringUtils;
import com.yns.util.UIHelper;
import com.yns.util.Utils;
import com.yns.widget.HeaderBar;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MarketAddActivity extends BaseActivity {

    private HeaderBar headerBar;
    private TextView tvSelect;
    private EditText etPrice, etTitle, etPhone, etDetail;
    private ImageView ivPhoto;
    private Market market;
    private TextView tvSchool, tvType;
    protected static final int PICTURE = 0;
    protected static final int ADD = 3;
    private LinearLayout container;
    private List<String> photoUrls;
    private int type;
    private String[] items = new String[]{"从相册选择", "拍照"};
    private String filePath = "";
    private boolean isPhoto;
    private boolean isAlready;
    LayoutInflater inflater;
    public List<String> strList = new ArrayList<String>();
    private String schoolId="",sortId="";
    public MarketAddActivity() {
        super(R.layout.activity_marketadd);
    }

    @Override
    public void initViews() {

        headerBar = (HeaderBar) findViewById(R.id.title);
        etPrice = (EditText) findViewById(R.id.et_price);
        etTitle = (EditText) findViewById(R.id.et_title);
        etPhone = (EditText) findViewById(R.id.et_phone);
        etDetail = (EditText) findViewById(R.id.et_detail);
        tvSelect = (TextView) findViewById(R.id.tv_select);
        ivPhoto = (ImageView) findViewById(R.id.iv_photo);
        tvSchool = (TextView) findViewById(R.id.tv_school);
        tvType = (TextView) findViewById(R.id.tv_type);
        container = (LinearLayout) findViewById(R.id.homeAdd_container);
        inflater = getLayoutInflater();
        FrameLayout fl = (FrameLayout) inflater.inflate(
                R.layout.layout_checklist, null);

        fl.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (container.getChildCount() - 1 < 9) {
                    showDialog();
                } else {
                    showToast("选择图片不能超过5张");
                }
            }
        });
        container.addView(fl);
    }

    @Override
    public void initData() {
        market = (Market) getIntent().getSerializableExtra("market");
    }

    @Override
    public void bindViews() {
        tvSchool.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                GoodService.getInstance(context).getSchoolType(new CustomAsyncResponehandler() {
                    public void onFinish() {
                    }

                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel != null && baseModel.isStatus()) {
                            final List<School> typeList = (List<School>) baseModel
                                    .getResultObj();
                            final CharSequence[] item;
                            if (typeList != null && typeList.size() > 0) {
                                item = new CharSequence[typeList.size()];
                                for (int i = 0; i < typeList.size(); i++) {
                                    item[i] = typeList.get(i).getName();
                                }
                                new AlertDialog.Builder(MarketAddActivity.this)
                                        .setTitle("选择学校")
                                        .setItems(item, new DialogInterface.OnClickListener() { // content
                                            @Override
                                            public void onClick(DialogInterface dialog,
                                                                int which) {
                                                tvSchool.setText(item[which]);
                                                tvSchool.setTag(typeList.get(which));
                                            }

                                        })
                                        .setNegativeButton("取消",
                                                new DialogInterface.OnClickListener() {

                                                    @Override
                                                    public void onClick(DialogInterface dialog,
                                                                        int which) {

                                                        dialog.dismiss(); // 关闭alertDialog
                                                    }
                                                }).show();
                            }
                        }
                    }
                });
            }
        });

        tvType.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                GoodService.getInstance(context).getMarketType(new CustomAsyncResponehandler() {
                    public void onFinish() {
                    }

                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel != null && baseModel.isStatus()) {
                            final List<ProductType> typeList = (List<ProductType>) baseModel
                                    .getResultObj();
                            final CharSequence[] item;
                            if (typeList != null && typeList.size() > 0) {
                                item = new CharSequence[typeList.size()];
                                for (int i = 0; i < typeList.size(); i++) {
                                    item[i] = typeList.get(i).getName();
                                }
                                new AlertDialog.Builder(MarketAddActivity.this)
                                        .setTitle("选择分类")
                                        .setItems(item, new DialogInterface.OnClickListener() { // content
                                            @Override
                                            public void onClick(DialogInterface dialog,
                                                                int which) {
                                                tvType.setText(item[which]);
                                                tvType.setTag(typeList.get(which));
                                            }

                                        })
                                        .setNegativeButton("取消",
                                                new DialogInterface.OnClickListener() {

                                                    @Override
                                                    public void onClick(DialogInterface dialog,
                                                                        int which) {

                                                        dialog.dismiss(); // 关闭alertDialog
                                                    }
                                                }).show();
                            }
                        }
                    }
                });

            }
        });
        tvSelect.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

            }
        });
        if (market == null) {
            headerBar.setTitle("添加二手市场信息");
        } else {
            headerBar.setTitle("编辑二手市场信息");
            etPhone.setText(market.getMobile());
            etPrice.setText(market.getPrice() + "");
            etTitle.setText(market.getTitle());
            etDetail.setText(market.getDetails());
            tvSchool.setText(market.getSchoolName());
            tvType.setText(market.getTypeSortName());
            schoolId=market.getSchoolId();
            sortId=market.getTypeSortId();
            ProductType type=new ProductType();
            type.setSortId(market.getTypeSortId());
            type.setName(market.getTypeSortName());
            tvType.setTag(type);
            School sc=new School();
            sc.setId(market.getSchoolId());
            sc.setName(market.getSchoolName());
            tvSchool.setTag(sc);
            if (market.getPic() != null) {
                AppContext.setImage(market.getPic(), ivPhoto,
                        AppContext.imageOption);
            }
        }
        headerBar.top_right_btn.setText("保存");
        headerBar.top_right_btn.setVisibility(View.VISIBLE);
        headerBar.top_right_btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                ProductType type= (ProductType) tvType.getTag();
                if(type==null){
                    showToast("请选择分类");
                    return;
                }else{
                    sortId=type.getSortId();
                }

                School school= (School) tvSchool.getTag();
                if(school==null){
                    showToast("请选择学校");
                    return;
                }else{
                    schoolId=school.getId();
                }
                if (StringUtils.isEmpty(etTitle.getText().toString())) {
                    showToast("请输入标题");
                    return;
                }
                if (StringUtils.isEmpty(etPrice.getText().toString())) {
                    showToast("请输入价格");
                    return;
                }
                if (StringUtils.isEmpty(etDetail.getText().toString())) {
                    showToast("请输描述");
                    return;
                }
                if (StringUtils.isEmpty(etPhone.getText().toString())) {
                    showToast("请输入联系方式");
                    return;
                }
                String pic="";
                for (String str:strList){
                    pic+=str+",";
                }
                ComonService.getInstance(context).editMarket(
                        market == null ? null : market
                                .getMarketInfoId(),

                        etTitle.getText().toString(),

                        "1",
                        etDetail.getText().toString(),
                        pic,
                        etPrice.getText().toString(),
                        AppContext.currentUser.getUserId(),
                        etPhone.getText().toString(),schoolId,sortId,
                        new CustomAsyncResponehandler() {
                            public void onFinish() {

                            }

                            ;

                            @Override
                            public void onSuccess(ResponeModel baseModel) {
                                super.onSuccess(baseModel);
                                if (baseModel != null && baseModel.isStatus()) {
                                    UIHelper.ShowMessage(
                                            MarketAddActivity.this, "保存成功");
                                    MarketAddActivity.this.finish();
                                }
                            }
                        });

            }
        });

    }
    /**
     * 显示选择对话框
     */
    private void showDialog() {
        new AlertDialog.Builder(this).setTitle("选择图片")
                .setItems(items, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                Intent intent = new Intent();
                                intent.setType("image/*");
                                intent.setAction(Intent.ACTION_GET_CONTENT);
                                startActivityForResult(intent, ADD);
                                break;
                            case 1:
                                openCamera();
                                break;
                        }
                    }
                })
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    /**
     * 根据日历获取文件时间戳名称
     */
    @SuppressLint("DefaultLocale")
    private String getFileStamp() {

        Calendar calendar = Calendar.getInstance();
        int Y = calendar.get(Calendar.YEAR);
        int M = calendar.get(Calendar.MONTH) + 1;
        int D = calendar.get(Calendar.DAY_OF_MONTH);
        int h = calendar.get(Calendar.HOUR_OF_DAY);
        int m = calendar.get(Calendar.MINUTE);
        int s = calendar.get(Calendar.SECOND);
        Object[] args = new Object[]{Y, M, D, h, m, s};
        String stamp = String.format("%04d%02d%02d%02d%02d%02d", args);
        return stamp;
    }

    /**
     * 打开摄像头
     */
    private void openCamera() {
        Intent localIntent = new Intent();
        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            Toast.makeText(this, "SD卡当前不可用，无法拍照！", Toast.LENGTH_SHORT).show();
            return;
        }
        filePath = AppConfig.DEFAULT_SAVE_PATH + getFileStamp() + ".jpg";
        localIntent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
        localIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(filePath)));
        startActivityForResult(localIntent, PICTURE);


    }

    @SuppressWarnings({"unused", "deprecation"})
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        if (requestCode == PICTURE) {
            if (filePath != null && filePath.length() > 0) {
                int width = (int) getResources().getDimension(R.dimen.size_100);
                final Bitmap bitmap = Utils.copressImage(filePath, width, width);
                filePath = AppConfig.DEFAULT_SAVE_PATH + getFileStamp() + ".jpg";
                Utils.saveBitmap(filePath, bitmap);

                //TODO 上传图片
                ComonService.getInstance(context).uploadImg(
                        new File(filePath),
                        new CustomAsyncResponehandler() {
                            @Override
                            public void onSuccess(ResponeModel baseModel) {
                                super.onSuccess(baseModel);
                                if (baseModel.isStatus()) {
                                    String url = "";
                                    try {
                                        JSONObject object = new JSONObject(baseModel.getResult());
                                        url = object.getString("url");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    strList.add(url);
                                    AddView(bitmap);
                                }
                            }
                        });

            }
        } else if (requestCode == ADD) {
            Uri uri = data.getData();
            try {
                String[] proj = {MediaStore.Images.Media.DATA};
                Uri contentUri = data.getData();
                Cursor cursor = managedQuery(contentUri, proj, null, null, null);
                int column_index = cursor
                        .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                filePath = cursor.getString(column_index);
                int width = (int) getResources().getDimension(R.dimen.size_100);
                final Bitmap bitmap = Utils.copressImage(filePath, width, width);
                filePath = AppConfig.DEFAULT_SAVE_PATH + getFileStamp() + ".jpg";
                Utils.saveBitmap(filePath, bitmap);
                //TODO 上传图片
                ComonService.getInstance(context).uploadImg(
                        new File(filePath),
                        new CustomAsyncResponehandler() {
                            @Override
                            public void onSuccess(ResponeModel baseModel) {
                                super.onSuccess(baseModel);
                                if (baseModel.isStatus()) {
                                    String url = "";
                                    try {
                                        JSONObject object = new JSONObject(baseModel.getResult());
                                        url = object.getString("url");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    strList.add(url);
                                    AddView(bitmap);
                                }
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressWarnings("deprecation")
    public void AddView(View v) {
        container.addView(v, container.getChildCount() - 1);
        ImageButton delPic = (ImageButton) (container.getChildAt(container
                .getChildCount() - 2).findViewById(R.id.del));
        delPic.setTag(container.getChildCount() - 2);
        delPic.setVisibility(View.VISIBLE);
        container.postInvalidate(); // 提示UI重绘界面

    }

    @SuppressWarnings("deprecation")
    public void AddView(Bitmap bitmap) {

        FrameLayout con = (FrameLayout) inflater.inflate(
                R.layout.layout_checklist, null);
        ImageView pic = (ImageView) con.findViewById(R.id.pic);
        pic.setBackgroundDrawable(new BitmapDrawable(bitmap));
        container.addView(con, container.getChildCount() - 1);
        ImageButton delPic = (ImageButton) (container.getChildAt(container
                .getChildCount() - 2).findViewById(R.id.del));
        delPic.setTag(container.getChildCount() - 2);
        delPic.setVisibility(View.VISIBLE);
        container.postInvalidate(); // 提示UI重绘界面

    }

    public void deleteView(View view) {
        int index = (Integer) view.getTag();
        container.removeView((View) view.getParent());
        container.invalidate();
        strList.remove(index);
    }

}
