package com.yns.app.ui;

import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yns.R;
import com.yns.app.AppContext;
import com.yns.app.adapter.MarketAdapter;
import com.yns.model.Market;
import com.yns.model.ProductType;
import com.yns.model.ResponeModel;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.ComonService;
import com.yns.service.GoodService;
import com.yns.widget.HeaderBar;
import com.yns.widget.multicolumn.MultiColumnListView;
import com.yns.widget.multicolumn.MulticolumPullToRefreshView;
import com.yns.widget.multicolumn.PLA_AdapterView;

import java.util.ArrayList;
import java.util.List;

public class MarketActivity extends BaseActivity {

    private HeaderBar headerBar;
    private MulticolumPullToRefreshView integral_refresh_view;
    private MultiColumnListView listview_integral;
    private LinearLayout llMore;
    private MarketAdapter adapter;
    public static List<Market> dataList;
    private int page = 1;
    private int rows = 5;
    private boolean isRefresh = true;
    private int type = 0;//0 我的市场  1表示所有市场
    private String sortId = "";

    public MarketActivity() {
        super(R.layout.activity_market);
    }

    @Override
    public void initViews() {
        type = getIntent().getIntExtra("type", 0);

        headerBar = (HeaderBar) findViewById(R.id.title);
        integral_refresh_view = (MulticolumPullToRefreshView) findViewById(R.id.pull_integral_view);
        listview_integral = (MultiColumnListView) findViewById(R.id.listview_integral);
        llMore = (LinearLayout) findViewById(R.id.ll_more);
        if (type == 0) {
            headerBar.setTitle("我的二手市场");
            headerBar.top_right_btn.setVisibility(View.VISIBLE);
            headerBar.top_right_btn.setText("新增");
            headerBar.top_right_btn.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setClass(MarketActivity.this,
                            MarketAddActivity.class);
                    startActivity(intent);
                }
            });
        } else if (type == 1) {
            headerBar.setTitle("二手市场");
        }

    }

    @Override
    public void initData() {
        dataList = new ArrayList<Market>();
        adapter = new MarketAdapter(this, dataList, type);
        listview_integral.setAdapter(adapter);
        isRefresh = true;
        page = 1;
        getTypeList();
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void bindViews() {
        listview_integral
                .setOnItemClickListener(new PLA_AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(PLA_AdapterView<?> parent,
                                    View view, int position, long id) {
                Intent intent = new Intent(MarketActivity.this, MarketDetailActivity.class);
                intent.putExtra("data", dataList.get(position));
                startActivity(intent);
            }
        });
        integral_refresh_view
                .setOnHeaderRefreshListener(new MulticolumPullToRefreshView.OnHeaderRefreshListener() {

                    @Override
                    public void onHeaderRefresh(MulticolumPullToRefreshView view) {
                        isRefresh = true;
                        page = 1;
                        getMarket();
                        integral_refresh_view.onHeaderRefreshComplete();
                        ;
                    }
                });
        integral_refresh_view
                .setOnFooterRefreshListener(new MulticolumPullToRefreshView.OnFooterRefreshListener() {

                    @Override
                    public void onFooterRefresh(MulticolumPullToRefreshView view) {
                        isRefresh = false;
                        page++;
                        getMarket();
                        integral_refresh_view.onFooterRefreshComplete();
                    }
                });


    }


    private void getMarket() {
        ComonService.getInstance(context).getMarket(
                rows + "",
                page + "",
                type == 0 ? AppContext.currentUser
                        .getUserId() : "", sortId, new CustomAsyncResponehandler() {
                    public void onFinish() {
                    }

                    ;

                    @SuppressWarnings("unchecked")
                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel != null && baseModel.isStatus()) {
                            List<Market> lists = (List<Market>) baseModel
                                    .getResultObj();
                            if (lists != null && lists.size() > 0) {
                                integral_refresh_view.setVisibility(View.VISIBLE);
                                if (isRefresh) {
                                    dataList.clear();
                                    dataList.addAll(0, lists);
                                } else {
                                    dataList.addAll(lists);
                                }
                                adapter.notifyDataSetChanged();
                            }
                        }
                    }
                });
    }

    private void getTypeList() {
        GoodService.getInstance(context).getMarketType(new CustomAsyncResponehandler() {
            public void onFinish() {
            }

            @Override
            public void onSuccess(ResponeModel baseModel) {
                super.onSuccess(baseModel);
                if (baseModel != null && baseModel.isStatus()) {
                    final List<ProductType> typeList = (List<ProductType>) baseModel
                            .getResultObj();
                    if (typeList != null && typeList.size() > 0) {
                        llMore.removeAllViews();
                        for (int i = 0; i < typeList.size(); i++) {
                            TextView t = new TextView(context);
                            if (i == 0) {
                                t.setTextColor(getResources().getColor(
                                        R.color.main_title_bg));
                            } else {
                                t.setTextColor(Color.BLACK);
                            }

                            t.setText(typeList.get(i).getName());

                            t.setTextSize(17);
                            LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(
                                    LinearLayout.LayoutParams.WRAP_CONTENT,
                                    LinearLayout.LayoutParams.WRAP_CONTENT);
                            t.setPadding(30, 10, 30, 10);
                            t.setLayoutParams(p);
                            typeList.get(i).setIndex(i);
                            t.setTag(typeList.get(i));
                            t.setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    ((TextView) v).setTextColor(getResources()
                                            .getColor(R.color.main_title_bg));
                                    ProductType type = (ProductType) v.getTag();
                                    for (int i = 0; i < llMore
                                            .getChildCount(); i++) {
                                        page = 1;
                                        isRefresh = true;
                                        sortId = type.getSortId();
                                        getMarket();
                                        if (i != type.getIndex()) {
                                            ((TextView) llMore
                                                    .getChildAt(i))
                                                    .setTextColor(getResources()
                                                            .getColor(
                                                                    R.color.black));
                                        }
                                    }
                                }
                            });
                            if (i == 0) {
                                if (i == 0) {
                                    sortId = typeList.get(0).getSortId();
                                    getMarket();
                                }
                            }
                            llMore.addView(t);
                        }
                    }
                }
            }
        });
    }
}
