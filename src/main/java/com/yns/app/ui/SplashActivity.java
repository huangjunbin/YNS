package com.yns.app.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.ant.liao.GifView;
import com.umeng.analytics.MobclickAgent;
import com.yns.R;
import com.yns.app.adapter.SplashPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Splash界面，继承于{@link BaseActivity}
 *
 * @author jalen
 * @version 1.0
 * @date 2015-1-6下午5:07:28
 * @since 1.0
 */
public class SplashActivity extends BaseActivity {
    private static final String TAG = "SplashActivity";

    private List<View> dots;
    private List<View> mList;
    private ViewPager mViewPager;
    private int oldPosition;


    public SplashActivity() {
        super(R.layout.activity_splash);
    }


    @Override
    public void initViews() {
        WindowManager wm = this.getWindowManager();

        int width = wm.getDefaultDisplay().getWidth();
        int height = wm.getDefaultDisplay().getHeight();
        mViewPager = ((ViewPager) findViewById(R.id.viewpager));
        mList = new ArrayList<View>();
        LayoutInflater layoutInflater = getLayoutInflater();
        View splash_1 = layoutInflater.inflate(R.layout.layout_splash_1, null);
        GifView  myGifView = (GifView) splash_1.findViewById(R.id.img_gif);
        myGifView.setGifImage(R.drawable.s1);

        myGifView.setShowDimension(width,height);
        myGifView.setGifImageType(GifView.GifImageType.COVER);
        mList.add(splash_1);
        View splash_2 = layoutInflater.inflate(R.layout.layout_splash_2, null);
        GifView  myGifView2 = (GifView) splash_2.findViewById(R.id.img_gif);
        myGifView2.setGifImage(R.drawable.s2);
        myGifView2.setShowDimension(width, height);
        myGifView2.setGifImageType(GifView.GifImageType.COVER);
        mList.add(splash_2);
        Button btnGo= (Button) splash_2.findViewById(R.id.button);
        btnGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_main = new Intent(SplashActivity.this,
                        MainActivity2.class);
                SplashActivity.this.startActivity(intent_main);
                SplashActivity.this.finish();
            }
        });
        // btnSplashEI = ((Button) splash_5.findViewById(R.id.btn_splash_ei));
        dots = new ArrayList<View>();
        dots.add(findViewById(R.id.dot_0));
        dots.add(findViewById(R.id.dot_1));
    }

    @Override
    public void initData() {

    }

    @Override
    public void bindViews() {


        mViewPager.setAdapter(new SplashPagerAdapter(mViewPager, mList));
        mViewPager.setCurrentItem(0);
        mViewPager.setOnPageChangeListener(new SplashPageChangeListener());

    }


    private int count = 0;

    /**
     * viewpager监听器
     *
     * @author jalen
     * @version 1.0
     * @date 2015-1-6下午5:09:35
     * @since 1.0
     */
    private class SplashPageChangeListener implements
            ViewPager.OnPageChangeListener {
        private SplashPageChangeListener() {
        }

        public void onPageScrollStateChanged(int paramInt) {

        }

        public void onPageScrolled(int paramInt1, float paramFloat,
                                   int paramInt2) {

            if (paramInt1 == 3 && paramFloat == 0.0 && paramInt2 == 0) {
                count++;

                if (count == 1) {


                    return;
                }

            } else {
                count = 0;
            }


        }

        public void onPageSelected(int position) {
            ((View) SplashActivity.this.dots.get(position
                    % SplashActivity.this.dots.size()))
                    .setBackgroundResource(R.drawable.pitch);
            ((View) SplashActivity.this.dots
                    .get(SplashActivity.this.oldPosition
                            % SplashActivity.this.dots.size()))
                    .setBackgroundResource(R.drawable.no_pitch);
            SplashActivity.this.oldPosition = position;

        }
    }

}
