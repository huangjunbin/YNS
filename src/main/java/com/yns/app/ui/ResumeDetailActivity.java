package com.yns.app.ui;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alipay.Result;
import com.alipay.SignUtils;
import com.alipay.sdk.app.PayTask;
import com.yns.R;
import com.yns.app.AppContext;
import com.yns.model.Job;
import com.yns.model.ResponeModel;
import com.yns.model.Resume;
import com.yns.model.Resume.Education;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.ComonService;
import com.yns.util.JsonUtil;
import com.yns.util.UIHelper;
import com.yns.widget.HeaderBar;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

public class ResumeDetailActivity extends BaseActivity {
    private HeaderBar headerBar;
    private TextView tvTitle, tvContent, tvZhuanye, tvZhiye, tvXinzi,
            tvTechang, tvJinyan, tvPingjia, tvXiaoyuan;
    private LinearLayout llEducation;
    private ImageView ivPhoto;
    private Resume resume;
    private TextView tvMobile, tvQq, tvEmail;
    private LinearLayout lyContact;
    private Button btnGet;
    public static final String PARTNER = "2088611516608282";
    public static final String SELLER = "kunmingxuanzhi@163.com";
    public static final String RSA_PRIVATE = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAKR4R30Dy0TRmCWLgWXtWOXzo/7w2TunMbHiqQPjhYaJjxBvsdEhuMmtqPUoy//2QDO1eS69hAYsKnO0+DXktNiGOnne0sTrFab8bBRditV2ga4uytEw84S5EkmX8g55EdOjJqHJNR6DVWfq6jzwHqsWrB1eSbrLp3flUWzvtBPVAgMBAAECgYBlZZINTM2AvAck/oDkKhPokPDGOA51bHqCxM6WiyrC6wO8imACItwd2maT3ncGcvbZ6kOvwT8n3wq9ExEDiODEr1d+udPW0n5PZFhgwXZbMjV7IZm5JK/joAln3UIo967U+JspTMkzZC5qSYunp4+GDYGlvIevWNVOqTJPudYm8QJBANqg0Sz6vuyxPHfTNp7if1c8eckUopOKQ1gyfTyl94WcYp+Q871b1lp979WPQ5/yaCVEr/w3W8mFrVHrwAGp8LcCQQDAlX1MHzxq+/YfEgdsjG2wQluPRTtSg1UYtpbqH5JJ6JNB4SRttidoA0xw9jLMY6DCkfWX6zcjjn67B3DJm7vTAkBP/iVq6rfuzI6OXTaP/dmP5q28uBMgHqezXo3aIfOl7GldbuDvuOl+JLQbFJcur7gRYsdtZifsXT/kSHjxYueTAkEAhqH7TlQn8MLkdSQtfH1P0YEScT59ElouC3DaYCJiKrkk+VFkHi7Rra1gqzfym2Cq6lsPznzptznO7wFsoAT3cQJBAKZPIVplxsjYUYpdWbhYHsc0H5a6Ef4UOsZh39zzNa2dLful6MotC88ns25Bodr0D7ei3oequOfvnEsAGGTtnc4=";
    public static final String RSA_PUBLIC = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCnxj/9qwVfgoUh/y2W89L6BkRAFljhNhgPdyPuBV64bfQNN1PjbCzkIM6qRdKBoLPXmKKMiFYnkd6rAoprih3/PrQEB/VsW8OoM8fxn67UDYuyBTqA23MML9q1+ilIZwBC2AQ2UBVOrFXfFl75p6/B5KsiNG9zpgmLCUYuLkxpLQIDAQAB";
    private static final int SDK_PAY_FLAG = 1;
    private static final int SDK_CHECK_FLAG = 2;
    private int type = 0;  //1 显示 收藏 2 3 显示收藏和查看职位
    private Button btnCollect, btnJob;

    public ResumeDetailActivity() {
        super(R.layout.activity_resumedetail);
    }

    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SDK_PAY_FLAG: {
                    Result resultObj = new Result((String) msg.obj);
                    String resultStatus = resultObj.resultStatus;
                    // 判断resultStatus 为“9000”则代表支付成功，具体状态码代表含义可参考接口文档
                    if (TextUtils.equals(resultStatus, "9000")) {
                        Toast.makeText(ResumeDetailActivity.this, "支付成功",
                                Toast.LENGTH_SHORT).show();
                        lyContact.setVisibility(View.VISIBLE);
                        btnGet.setVisibility(View.GONE);
                    } else {
                        // 判断resultStatus 为非“9000”则代表可能支付失败
                        // “8000”
                        // 代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）
                        if (TextUtils.equals(resultStatus, "8000")) {
                            Toast.makeText(ResumeDetailActivity.this, "支付结果确认中",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ResumeDetailActivity.this, "支付失败",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                    break;
                }
                case SDK_CHECK_FLAG: {
                    Toast.makeText(ResumeDetailActivity.this, "检查结果为：" + msg.obj,
                            Toast.LENGTH_SHORT).show();
                    break;
                }
                default:
                    break;
            }
        }

        ;
    };

    @Override
    public void initViews() {
        headerBar = (HeaderBar) findViewById(R.id.title);
        headerBar.setTitle("简历预览");
        tvTitle = (TextView) findViewById(R.id.tv_title);
        tvContent = (TextView) findViewById(R.id.tv_content);
        tvZhuanye = (TextView) findViewById(R.id.tv_zhuanye);
        tvZhiye = (TextView) findViewById(R.id.tv_zhiye);
        tvXinzi = (TextView) findViewById(R.id.tv_xinzhi);
        tvTechang = (TextView) findViewById(R.id.tv_techang);
        tvJinyan = (TextView) findViewById(R.id.tv_jingyan);
        tvPingjia = (TextView) findViewById(R.id.tv_pingjia);
        tvXiaoyuan = (TextView) findViewById(R.id.tv_xiaoyuan);
        ivPhoto = (ImageView) findViewById(R.id.iv_photo);
        llEducation = (LinearLayout) findViewById(R.id.ll_education);
        tvMobile = (TextView) findViewById(R.id.tv_mobile);
        tvQq = (TextView) findViewById(R.id.tv_qq);
        tvEmail = (TextView) findViewById(R.id.tv_email);
        lyContact = (LinearLayout) findViewById(R.id.ll_contact);
        btnGet = (Button) findViewById(R.id.btn_get);
        btnCollect = (Button) findViewById(R.id.btn_collect);
        btnJob = (Button) findViewById(R.id.btn_job);
    }

    @Override
    public void initData() {
        resume = (Resume) getIntent().getSerializableExtra("resume");
        type =   getIntent().getIntExtra("type",0);
        if (AppContext.userType == 2) {
            headerBar.top_right_btn.setVisibility(View.VISIBLE);
            headerBar.top_right_btn.setText("面试邀请");
            headerBar.top_right_btn.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setClass(ResumeDetailActivity.this,
                            JobCompanyListActivity.class);
                    intent.putExtra("resume", resume);
                    startActivity(intent);
                }
            });
        }
        if (type == 1) {
            btnCollect.setVisibility(View.VISIBLE);
        } else if (type == 2 || type == 3) {
            btnCollect.setVisibility(View.VISIBLE);
            btnJob.setVisibility(View.VISIBLE);
        }
        if (type == 3) {
            headerBar.top_right_btn.setText("");
        }
        btnCollect.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                addEnterpriseFavorite(resume.getAppMemberResumeInfoId(),"resume");
            }
        });

        btnJob.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Job job = JsonUtil.convertJsonToObject(JsonUtil.convertObjectToJson(resume),Job.class);
                Intent intent = new Intent();
                intent.setClass(
                        ResumeDetailActivity.this,
                        JobDetailActivity.class);
                job.setTopflag(true);
                intent.putExtra("job",
                        job);
                startActivity(intent);
            }
        });
    }
    private void addEnterpriseFavorite(String id, final String type) {
        ComonService.getInstance(context).addEnterpriseFavorite(
                AppContext.currentUser.getUserId() + "", id, type,
                new CustomAsyncResponehandler() {
                    public void onFinish() {

                    }

                    ;

                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel != null && baseModel.isStatus()) {

                        }
                    }
                });
    }
    @Override
    public void bindViews() {
        tvTitle.setText(resume.getName());
        tvContent.setText(("1".equals(resume.getSex()) ? "男" : "女") + "|"
                + ("1".equals(resume.getHunYinQingKuang()) ? "已婚" : "未婚") + "|"
                + resume.getChuShengNianYue() + "|" + resume.getHuJi() + "|"
                + "\n" + resume.getMingZu() + "|" + resume.getBiYeXueXiao()
                + "|" + resume.getXueLiName() + "|" + resume.getXueLi());
        tvZhuanye.setText("所学专业:" + resume.getZhuanYe());
        tvZhiye.setText("期望职业:" + resume.getGongZuoLeiXing());
        tvXinzi.setText("期望薪资:" + resume.getQiDaiXingZiName());
        tvTechang.setText(resume.getJiNengZhuanChang());
        tvJinyan.setText(resume.getGongZuoJingYan());
        tvPingjia.setText(resume.getZiWoPingJia());
        tvXiaoyuan.setText(resume.getXiaoYuanChengZhangJingLi());
        if (resume.getEmail() == null || "null".equals(resume.getEmail())) {
            tvEmail.setText("电子邮箱:");
        } else {
            tvEmail.setText("电子邮箱:" + resume.getEmail());
        }
        if (resume.getQq() == null || "null".equals(resume.getQq())) {
            tvQq.setText("QQ号码:");
        } else {
            tvQq.setText("QQ号码:" + resume.getQq());
        }
        if (resume.getMobile() == null || "null".equals(resume.getMobile())) {
            tvMobile.setText("手机号码:");
        } else {
            tvMobile.setText("手机号码:" + resume.getMobile());
        }
        if (resume.getHeaderPic() == null || "".equals(resume.getEducations())) {

        } else {
            AppContext.setImage(resume.getHeaderPic(), ivPhoto,
                    AppContext.imageOption);
        }

        if (resume.getEducations() != null && resume.getEducations().size() > 0) {
            for (Education item : resume.getEducations()) {
                TextView t1 = new TextView(this);
                t1.setText("起始时间:" + item.getBeginDate());
                t1.setPadding(0, 10, 0, 0);
                llEducation.addView(t1);
                TextView t2 = new TextView(this);
                t2.setText("院校名称:" + item.getName());
                llEducation.addView(t2);
                TextView t3 = new TextView(this);
                t3.setText("获得学历:" + item.getXueLi());
                llEducation.addView(t3);
            }
        }
        if (AppContext.userType == 2) {
            if (!resume.isButFlag()) {
                lyContact.setVisibility(View.GONE);
                btnGet.setVisibility(View.VISIBLE);
            } else {
                lyContact.setVisibility(View.VISIBLE);
                btnGet.setVisibility(View.GONE);
            }

        } else {
            btnGet.setVisibility(View.GONE);
        }
        btnGet.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                ComonService.getInstance(context).buyResume(AppContext.currentUser.getDeptId() + "", resume.getAppMemberResumeInfoId(),
                        AppContext.currentUser.getUserId(),
                        new CustomAsyncResponehandler() {
                            @Override
                            public void onSuccess(ResponeModel baseModel) {
                                super.onSuccess(baseModel);
                                if (baseModel != null
                                        && baseModel.isStatus()) {
                                    int buyCount = baseModel.getBuyCount();
                                    if (buyCount == 0) {
                                        UIHelper.showSysDialog(ResumeDetailActivity.this, "购买会员",
                                                "您现在还不是会员,需要交纳800元会员费，即可随意查看简历",
                                                new UIHelper.OnSurePress() {

                                                    @Override
                                                    public void onClick(View view) {
                                                        pay(btnGet);
                                                    }
                                                }, false);
                                    } else if (buyCount == 1) {
                                        lyContact.setVisibility(View.VISIBLE);
                                        btnGet.setVisibility(View.GONE);
                                    }

                                }
                            }
                        });


            }
        });
    }

    /**
     * call alipay sdk pay. 调用SDK支付
     */
    public void pay(View v) {
        String orderInfo = getOrderInfo("开通会员", "开通会员", "800");
        String sign = sign(orderInfo);
        try {
            System.out.println("s::" + sign);
            // 仅需对sign 做URL编码
            sign = URLEncoder.encode(sign, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        final String payInfo = orderInfo + "&sign=\"" + sign + "\"&"
                + getSignType();

        Runnable payRunnable = new Runnable() {

            @Override
            public void run() {
                // 构造PayTask 对象
                PayTask alipay = new PayTask(ResumeDetailActivity.this);
                // 调用支付接口
                String result = alipay.pay(payInfo);

                Message msg = new Message();
                msg.what = SDK_PAY_FLAG;
                msg.obj = result;
                mHandler.sendMessage(msg);
            }
        };

        Thread payThread = new Thread(payRunnable);
        payThread.start();
    }

    /**
     * check whether the device has authentication alipay account.
     * 查询终端设备是否存在支付宝认证账户
     */
    public void check(View v) {
        Runnable checkRunnable = new Runnable() {

            @Override
            public void run() {
                PayTask payTask = new PayTask(ResumeDetailActivity.this);
                boolean isExist = payTask.checkAccountIfExist();

                Message msg = new Message();
                msg.what = SDK_CHECK_FLAG;
                msg.obj = isExist;
                mHandler.sendMessage(msg);
            }
        };

        Thread checkThread = new Thread(checkRunnable);
        checkThread.start();

    }

    /**
     * get the sdk version. 获取SDK版本号
     */
    public void getSDKVersion() {
        PayTask payTask = new PayTask(this);
        String version = payTask.getVersion();
        Toast.makeText(this, version, Toast.LENGTH_SHORT).show();
    }

    /**
     * create the order info. 创建订单信息
     */
    public String getOrderInfo(String subject, String body, String price) {
        // 合作者身份ID
        String orderInfo = "partner=" + "\"" + PARTNER + "\"";

        // 卖家支付宝账号
        orderInfo += "&seller_id=" + "\"" + SELLER + "\"";

        // 商户网站唯一订单号
        orderInfo += "&out_trade_no=" + "\"" + getOutTradeNo() + "\"";

        // 商品名称
        orderInfo += "&subject=" + "\"" + subject + "\"";

        // 商品详情
        orderInfo += "&body=" + "\"" + body + "\"";

        // 商品金额
        orderInfo += "&total_fee=" + "\"" + price + "\"";

        // 服务器异步通知页面路径
        orderInfo += "&notify_url=" + "\"" + "http://notify.msp.hk/notify.htm"
                + "\"";

        // 接口名称， 固定值
        orderInfo += "&service=\"mobile.securitypay.pay\"";

        // 支付类型， 固定值
        orderInfo += "&payment_type=\"1\"";

        // 参数编码， 固定值
        orderInfo += "&_input_charset=\"utf-8\"";

        // 设置未付款交易的超时时间
        // 默认30分钟，一旦超时，该笔交易就会自动被关闭。
        // 取值范围：1m～15d。
        // m-分钟，h-小时，d-天，1c-当天（无论交易何时创建，都在0点关闭）。
        // 该参数数值不接受小数点，如1.5h，可转换为90m。
        orderInfo += "&it_b_pay=\"30m\"";

        // 支付宝处理完请求后，当前页面跳转到商户指定页面的路径，可空
        orderInfo += "&return_url=\"m.alipay.com\"";

        // 调用银行卡支付，需配置此参数，参与签名， 固定值
        // orderInfo += "&paymethod=\"expressGateway\"";

        return orderInfo;
    }

    /**
     * get the out_trade_no for an order. 获取外部订单号
     */
    public String getOutTradeNo() {
        SimpleDateFormat format = new SimpleDateFormat("MMddHHmmss",
                Locale.getDefault());
        Date date = new Date();
        String key = format.format(date);

        Random r = new Random();
        key = key + r.nextInt();
        key = key.substring(0, 15);
        return key;
    }

    /**
     * sign the order info. 对订单信息进行签名
     *
     * @param content 待签名订单信息
     */
    public String sign(String content) {
        return SignUtils.sign(content, RSA_PRIVATE);
    }

    /**
     * get the sign type we use. 获取签名方式
     */
    public String getSignType() {
        return "sign_type=\"RSA\"";
    }
}
