package com.yns.app.ui;

import java.util.Timer;
import java.util.TimerTask;

import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.yns.R;
import com.yns.app.AppContext;
import com.yns.global.GlobalConstant;
import com.yns.model.ResponeModel;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.ComonService;
import com.yns.service.UserService;
import com.yns.util.StringUtils;
import com.yns.util.UIHelper;
import com.yns.widget.HeaderBar;

/**
 * @className：BindActivity.java
 * @author: lmt
 * @Function: 更换绑定
 * @createDate: 2014-12-11 下午5:26:13
 * @update:
 */
public class BindActivity extends BaseActivity {
	private HeaderBar headerBar;
	private EditText etPhone, etCode;
	private Button btnGetCode, btnCheck;
	private Timer timer;
	private int timeCount;
	private static final int HAND_TIME = 1;

	public BindActivity() {
		super(R.layout.activity_bind);
	}

	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			switch (msg.what) {
			case HAND_TIME:
				timeCount--;
				btnGetCode.setText(timeCount + "S");
				if (timeCount <= 0) {
					btnGetCode.setEnabled(true);
					timer.cancel();
					timer = null;
					timeCount = 0;
					btnGetCode.setText("获取验证码");
				}
				break;
			}
		}
	};

	@Override
	public void initViews() {
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle(getResString(R.string.bind_title));
		etPhone = (EditText) findViewById(R.id.et_moblie);
		etCode = (EditText) findViewById(R.id.et_code);
		btnGetCode = (Button) findViewById(R.id.btn_getcode);
		btnCheck = (Button) findViewById(R.id.btn_check);
	}

	@Override
	public void initData() {

	}

	@Override
	public void bindViews() {
		btnGetCode.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (!StringUtils.isPhoneNumberValid2(etPhone.getText()
						.toString())) {
					UIHelper.ShowMessage(context,
							getResString(R.string.register_telnoavail));
					return;
				}

				UserService.getInstance(context).getCheckCode(
						etPhone.getText().toString(),
						GlobalConstant.CODE_TYPE_BINDTEL + "",
						new CustomAsyncResponehandler() {
							@Override
							public void onSuccess(ResponeModel baseModel) {
								super.onSuccess(baseModel);
								if (baseModel != null && baseModel.isStatus()) {
									timer = new Timer();
									btnGetCode.setEnabled(false);
									timeCount = GlobalConstant.CODE_EFFECT_TIME + 1;
									timer.schedule(new TimerTask() {
										@Override
										public void run() {
											Message msg = new Message();
											msg.what = HAND_TIME;
											handler.sendMessage(msg);
										}
									}, 0, 1000);
								}
							}
						});
			}
		});
		btnCheck.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!StringUtils.isPhoneNumberValid2(etPhone.getText()
						.toString())) {
					showToast("手机号码不合法");
					return;
				}

				if (StringUtils.isEmpty(etCode.getText()
						.toString())) {
					showToast("验证码不能为空");
					return;
				}
				 
			}
		});
	}
}
