package com.yns.app.ui;

import java.util.List;

import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.mapapi.map.BaiduMap.OnMarkerClickListener;
import com.baidu.mapapi.map.BaiduMap.OnMarkerDragListener;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.InfoWindow;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.model.LatLng;
import com.yns.R;
import com.yns.app.fragement.BaseBaidMapFragement;
import com.yns.app.fragement.BaseBaidMapFragement.OnLocEndListener;
import com.yns.model.MerchantsGood;
import com.yns.widget.HeaderBar;

/**
 * @className：LocActivity.java
 * @author: lmt
 * @Function: 商家地址
 * @createDate: 2014-12-11 下午3:42:00
 * @update:
 */
public class LocActivity extends BaseActivity {
	// ---------------
	private HeaderBar headerBar;
	private BaseBaidMapFragement baseBaidMapFragement;
	private List<MerchantsGood> datas;
	private InfoWindow mInfoWindow;
	private Marker mMarkerA;
	private TextView tvLocation;
	private Button btnGo;
	// 初始化全局 bitmap 信息，不用时及时 recycle
	BitmapDescriptor bdA = BitmapDescriptorFactory
			.fromResource(R.drawable.icon_gcoding);

	public LocActivity() {
		super(R.layout.activity_loc);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void initViews() {
		// TODO Auto-generated method stub
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle(getResString(R.string.loc_title));
		tvLocation = (TextView) findViewById(R.id.tv_location);
		btnGo = (Button) findViewById(R.id.btn_go);
		datas = (List<MerchantsGood>) getIntent().getSerializableExtra("data");
	}

	@Override
	public void initData() {
		// TODO Auto-generated method stub
		baseBaidMapFragement = new BaseBaidMapFragement();
		getSupportFragmentManager().beginTransaction()
				.add(R.id.fragment_container, baseBaidMapFragement)
				.show(baseBaidMapFragement).commit();
	}

	@Override
	public void bindViews() {
		tvLocation.setText("当前位置距离商家" + datas.get(0).getDistance());
		btnGo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					Intent intent = LocActivity.this.getPackageManager()
							.getLaunchIntentForPackage("com.baidu.BaiduMap");
					startActivity(intent);
				} catch (Exception e) {
					Toast.makeText(LocActivity.this, "没有安装百度地图", Toast.LENGTH_LONG).show();
				}
			}
		});
		baseBaidMapFragement.mcallback = new OnLocEndListener() {

			@Override
			public void onLocEnd(LatLng latLng) {
				baseBaidMapFragement.mBaiduMap
						.setOnMarkerClickListener(new OnMarkerClickListener() {
							public boolean onMarkerClick(final Marker marker) {
								Button button = new Button(
										getApplicationContext());
								button.setBackgroundResource(R.drawable.popup);
								button.setText(marker.getTitle());
								button.setTextColor(Color.BLACK);
								button.setOnClickListener(new OnClickListener() {
									public void onClick(View v) {

										baseBaidMapFragement.mBaiduMap
												.hideInfoWindow();
									}
								});
								LatLng ll = marker.getPosition();
								mInfoWindow = new InfoWindow(button, ll, -47);
								baseBaidMapFragement.mBaiduMap
										.showInfoWindow(mInfoWindow);

								return true;
							}
						});
				initOverlay();
			}
		};

	}

	public void initOverlay() {
		if (datas != null) {
			for (int i = 0; i < datas.size(); i++) {
				LatLng llA = new LatLng(Double.parseDouble(datas.get(i)
						.getLatitude()), Double.parseDouble(datas.get(i)
						.getLongitude()));
				if (datas.size() == 1) {
					LatLng ll = new LatLng(Double.parseDouble(datas.get(i)
							.getLatitude()), Double.parseDouble(datas.get(i)
							.getLongitude()));
					MapStatusUpdate u = MapStatusUpdateFactory.newLatLng(ll);
					baseBaidMapFragement.mBaiduMap.animateMapStatus(u);
				}
				OverlayOptions ooA = new MarkerOptions().position(llA)
						.title(datas.get(i).getMerchantsName()).icon(bdA).zIndex(i)
						.draggable(true);

				baseBaidMapFragement.mBaiduMap.addOverlay(ooA);
			}
		}

		baseBaidMapFragement.mBaiduMap
				.setOnMarkerDragListener(new OnMarkerDragListener() {
					public void onMarkerDrag(Marker marker) {
					}

					public void onMarkerDragEnd(Marker marker) {
						Toast.makeText(
								LocActivity.this,
								"拖拽结束，新位置：" + marker.getPosition().latitude
										+ ", " + marker.getPosition().longitude,
								Toast.LENGTH_LONG).show();
					}

					public void onMarkerDragStart(Marker marker) {
					}
				});
	}

	/**
	 * 清除所有Overlay
	 * 
	 * @param view
	 */
	public void clearOverlay(View view) {
		baseBaidMapFragement.mBaiduMap.clear();
	}

	/**
	 * 重新添加Overlay
	 * 
	 * @param view
	 */
	public void resetOverlay(View view) {
		clearOverlay(null);
		initOverlay();
	}
}
