package com.yns.app.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.yns.R;
import com.yns.app.AppContext;
import com.yns.app.adapter.NoticeAdapter;
import com.yns.model.Message;
import com.yns.model.Notice;
import com.yns.model.ResponeModel;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.ComonService;
import com.yns.util.DateUtil;
import com.yns.widget.HeaderBar;
import com.yns.widget.XListView;
import com.yns.widget.XListView.IXListViewListener;

public class NoticeActivity extends BaseActivity implements IXListViewListener {

	private HeaderBar headerBar;
	private XListView xlvData;

	private NoticeAdapter adapter;
	public static List<Notice> lists;
	private int page = 1;
	private int rows = 5;
	private boolean isRefresh = true;

	public NoticeActivity() {
		super(R.layout.activity_apply);

	}

	@Override
	public void initViews() {

		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle("通知中心");
		xlvData = (XListView) findViewById(R.id.xlv_data);
		xlvData.setPullRefreshEnable(true);
		xlvData.setPullLoadEnable(true);

	}

	@Override
	public void initData() {
		lists = new ArrayList<Notice>();
		adapter = new NoticeAdapter(this, lists);
		xlvData.setAdapter(adapter);

	}

	@Override
	public void bindViews() {
		xlvData.setXListViewListener(this);
		xlvData.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				final Notice item = lists.get(position - 1);
				Intent intent = new Intent();
				intent.setClass(NoticeActivity.this, WebViewActivity.class);

				intent.putExtra("url",
						"http://www.dxsapp.com/front/app_qyzx_msg_info.html?noticeId="
								+ item.getNoticeId());

				intent.putExtra("title", item.getTitle());

				startActivity(intent);
			}
		});
		onRefresh();
	}

	@Override
	public void onRefresh() {
		xlvData.setRefreshTime(DateUtil.getDateTime(new Date(System
				.currentTimeMillis())));
		isRefresh = true;
		page = 1;
		getLists();
	}

	@Override
	public void onLoadMore() {
		isRefresh = false;
		page++;
		getLists();
	}

	private void getLists() {
		ComonService.getInstance(context).getNotice(
				rows + "",
				page + "",
				AppContext.currentUser == null ? "" : AppContext.currentUser
						.getUserId(), new CustomAsyncResponehandler() {
					public void onFinish() {
						xlvData.stopLoadMore();
						xlvData.stopRefresh();
						xlvData.setRefreshTime(DateUtil.getDateTime(new Date(
								System.currentTimeMillis())));
					};

					@SuppressWarnings("unchecked")
					@Override
					public void onSuccess(ResponeModel baseModel) {
						super.onSuccess(baseModel);
						if (baseModel != null && baseModel.isStatus()) {
							List<Notice> list = (List<Notice>) baseModel
									.getResultObj();
							if (list != null && list.size() > 0) {

								if (isRefresh) {
									lists.clear();
									lists.addAll(0, list);
								} else {
									lists.addAll(list);
								}
								adapter.notifyDataSetChanged();
							}
						}
					}
				});
	}
}
