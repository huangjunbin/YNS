package com.yns.app.ui;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

import com.yns.R;
import com.yns.app.AppContext;
import com.yns.db.dao.UserDao;
import com.yns.model.ResponeModel;
import com.yns.model.User;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.ComonService;
import com.yns.util.StringUtils;
import com.yns.util.UIHelper;
import com.yns.widget.HeaderBar;

public class CompanyRemarkActivity extends BaseActivity {
	private HeaderBar headerBar;
	private EditText editTextTel;

	public CompanyRemarkActivity() {
		super(R.layout.activity_companyremark);
	}

	@Override
	public void initViews() {
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle("企业简介");
		headerBar.top_right_btn.setText("保存");

		editTextTel = (EditText) findViewById(R.id.feedback_tel);
	}

	@Override
	public void initData() {

		editTextTel.setText(AppContext.currentUser.getRemark());
	}

	@Override
	public void bindViews() {
		headerBar.top_right_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (checkInput()) {
					ComonService.getInstance(context).updateEnterprise(
							AppContext.currentUser.getUserId(),
							editTextTel.getText().toString(),
							new CustomAsyncResponehandler() {
								@Override
								public void onSuccess(ResponeModel baseModel) {
									super.onSuccess(baseModel);
									if (baseModel != null
											&& baseModel.isStatus()) {
										AppContext.currentUser
												.setRemark(editTextTel
														.getText().toString());
										UserDao userDao = new UserDao(
												CompanyRemarkActivity.this);
										User user = userDao.getUserByDefault();
										user.setRemark(editTextTel.getText()
												.toString());
										userDao.update(user);
										UIHelper.ShowMessage(context, "修改成功！");
										finish();
									}
								}
							});
				}
			}
		});
	}

	private boolean checkInput() {

		if (StringUtils.isEmpty(editTextTel.getText().toString())) {
			UIHelper.ShowMessage(context, "请输入内容");
			return false;
		}

		return true;
	}
}
