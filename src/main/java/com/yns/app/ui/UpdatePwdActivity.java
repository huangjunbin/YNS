package com.yns.app.ui;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.yns.R;
import com.yns.app.AppContext;
import com.yns.model.ResponeModel;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.UserService;
import com.yns.util.StringUtils;
import com.yns.widget.HeaderBar;

public class UpdatePwdActivity extends BaseActivity {

	private HeaderBar headerBar;
	private EditText etOldPwd, etpwd, etRePwd;
	private Button btnUpdate;

	public UpdatePwdActivity() {
		super(R.layout.activity_updatepwd);

	}

	@Override
	public void initViews() {

		headerBar = (HeaderBar) findViewById(R.id.header);
		etOldPwd = (EditText) findViewById(R.id.et_old_pwd);
		etpwd = (EditText) findViewById(R.id.et_pwd);
		etRePwd = (EditText) findViewById(R.id.et_repwd);
		btnUpdate = (Button) findViewById(R.id.btn_forget);
	}

	@Override
	public void initData() {

	}

	@Override
	public void bindViews() {
		headerBar.setTitle("修改密码");

		btnUpdate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (StringUtils.isEmpty(etOldPwd.getText().toString())) {
					showToast("旧密码不能为空");
					return;
				}
				if (StringUtils.isEmpty(etpwd.getText().toString())) {
					showToast("密码不能为空");
					return;
				}
				if (!etRePwd.getText().toString()
						.equals((etpwd.getText().toString()))) {
					showToast("两次密码不一致");
					return;
				}
				UserService.getInstance(context).updatePassword(
						AppContext.userType + "",
						AppContext.currentUser.getUserId(),
						etpwd.getText().toString(),
						etOldPwd.getText().toString(),
						new CustomAsyncResponehandler() {
							@Override
							public void onSuccess(ResponeModel baseModel) {
								super.onSuccess(baseModel);
								if (baseModel != null && baseModel.isStatus()) {
									showToast("修改成功");
									UpdatePwdActivity.this.finish();
								}
							}
						});
			}
		});

	}

}
