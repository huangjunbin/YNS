package com.yns.app.ui;

import android.content.Intent;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.TextView;

import com.yns.R;
import com.yns.app.adapter.MessageAdapter;
import com.yns.model.Message;
import com.yns.model.ResponeModel;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.ComonService;
import com.yns.util.DateUtil;
import com.yns.widget.HeaderBar;
import com.yns.widget.XListView;
import com.yns.widget.XListView.IXListViewListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @className：MessageActivity.java
 * @author: allen
 * @Function: 消息列表
 * @createDate: 2015-2-15
 * @update:
 */
public class MessageActivity extends BaseActivity implements IXListViewListener {
	private HeaderBar headerBar;
	private XListView xListView;
	private MessageAdapter messageAdapter;
	public static List<Message> messages;
	private int page = 1;
	private int rows = 5;
	private boolean isRefresh = true;
	private int p = 0;
	private EditText editText;
	private String searchKey;

	public MessageActivity() {
		super(R.layout.activity_message);
	}

	@Override
	public void initViews() {
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle("新闻列表");
		xListView = (XListView) findViewById(R.id.messagelist);
		editText = (EditText) findViewById(R.id.search_edit);
		editText.setBackgroundResource(R.drawable.edit_city_search_bg);
		editText.setCompoundDrawablesWithIntrinsicBounds(
				R.drawable.city_edit_search, 0, 0, 0);
		editText.setHint("输入关键字搜索");
		editText.setHintTextColor(getResColor(R.color.gray));
		editText.setImeOptions(EditorInfo.IME_ACTION_SEARCH);

		xListView.setXListViewListener(this);
	}

	@Override
	public void initData() {
		p = getIntent().getIntExtra("position", 0);

		messages = new ArrayList<Message>();
		messageAdapter = new MessageAdapter(MessageActivity.this, messages);
		xListView.setAdapter(messageAdapter);
		onRefresh();

	}

	@Override
	public void bindViews() {

		editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				searchKey = editText.getText().toString();
				if (actionId == EditorInfo.IME_ACTION_SEARCH) {
					getMessageList();
					return true;
				}
				return false;
			}
		});
		xListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if (position <= messages.size()) {
					final Message msg = messages.get(position - 1);
					Intent intent = new Intent();
					intent.setClass(MessageActivity.this, WebViewActivity.class);
					if (p == 5) {
						intent.putExtra("url",
								"http://www.dxsapp.com/front/app_dxfc_info.html?id="
										+ msg.getAppArticleInfoId());
					} else {
						intent.putExtra("url",
								"http://www.dxsapp.com/front/app_article_info.html?id="
										+ msg.getAppArticleInfoId());
					}
					intent.putExtra("title", "详细内容");
					intent.putExtra("fkId", msg.getAppArticleInfoId());
					intent.putExtra("type", "article");
					intent.putExtra("isFavorite", msg.getIsFavorite());
					startActivity(intent);
				}

			}
		});
	}

	@Override
	public void onRefresh() {
		isRefresh = true;
		page = 1;
		getMessageList();

	}

	@Override
	public void onLoadMore() {
		isRefresh = false;
		page++;
		getMessageList();
	}

	private void getMessageList() {
		ComonService.getInstance(context).getMessageList(rows + "", page + "",
				p + "", searchKey, new CustomAsyncResponehandler() {
					public void onFinish() {
						xListView.stopLoadMore();
						xListView.stopRefresh();
						xListView.setRefreshTime(DateUtil.getDateTime(new Date(
								System.currentTimeMillis())));
					};

					@SuppressWarnings("unchecked")
					@Override
					public void onSuccess(ResponeModel baseModel) {
						super.onSuccess(baseModel);
						if (baseModel != null && baseModel.isStatus()) {
							List<Message> messageList = (List<Message>) baseModel
									.getResultObj();
							if (messageList != null && messageList.size() > 0) {
								if (isRefresh) {
									messages.clear();
									messages.addAll(0, messageList);
								} else {
									messages.addAll(messageList);
								}
								messageAdapter.notifyDataSetChanged();
							}
						}
					}
				});
	}
}
