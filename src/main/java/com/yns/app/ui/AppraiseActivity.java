package com.yns.app.ui;

import com.yns.R;
import com.yns.app.adapter.AppraiseAdapter;
import com.yns.global.GlobalConstant;
import com.yns.model.Appraise;
import com.yns.model.MerchantsGood;
import com.yns.util.DateUtil;
import com.yns.widget.HeaderBar;
import com.yns.widget.XListView;
import com.yns.widget.XListView.IXListViewListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @className：AppraiseActivity.java
 * @author: lmt
 * @Function: 更多评论
 * @createDate: 2014-12-11 下午5:56:25
 * @update:
 */
public class AppraiseActivity extends BaseActivity implements
		IXListViewListener {
	private String TAG = "AppraiseActivity";
	private boolean isLog = true;
	// --------------
	private int pageNum = 0, pageSize = GlobalConstant.DEFAULT_PAGE_SIZE;
	private boolean isRefresh;
	// -------------
	private HeaderBar headerBar;
	private XListView xListViewAppraise;
	private List<Appraise> listAppraises;
	private AppraiseAdapter appraiseAdapter;
	private MerchantsGood inMerchantsGood;

	public AppraiseActivity() {
		super(R.layout.activity_apprase);
		// TODO Auto-generated constructor stub

	}

	@Override
	public void initViews() {
		// TODO Auto-generated method stub
		inMerchantsGood = (MerchantsGood) getIntent().getSerializableExtra(
				"data");
		// ----------
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle(getResString(R.string.appraise_title));
		xListViewAppraise = (XListView) findViewById(R.id.appraise_list);
		xListViewAppraise.setPullLoadEnable(false);
		xListViewAppraise.setPullRefreshEnable(true);
	}

	@Override
	public void initData() {
		// TODO Auto-generated method stub
		listAppraises = new ArrayList<Appraise>();
		// Appraise appraise;
		// for (int i = 0; i < 10; i++) {
		// appraise = new Appraise();
		// appraise.setAppraiserName("齐天大圣");
		// appraise.setAppraiseTime("12月6日");
		// appraise.setAppraiseStar(6);
		// appraise.setAppraiseContent("洗车效果好！服务态度好！");
		// appraises.add(appraise);
		// }

		appraiseAdapter = new AppraiseAdapter(AppraiseActivity.this,
				listAppraises);
		xListViewAppraise.setAdapter(appraiseAdapter);
	}

	@Override
	public void bindViews() {
		// TODO Auto-generated method stub
		xListViewAppraise.setXListViewListener(this);
		
		onRefresh();
	}

	@Override
	public void onRefresh() {
		isRefresh = true;
		// TODO Auto-generated method stub
		xListViewAppraise.setRefreshTime(DateUtil.getDateTime(new Date(System
				.currentTimeMillis())));
		pageNum = 0;
		pageSize = GlobalConstant.DEFAULT_PAGE_SIZE;

		smartQueryApprases();
	}

	@Override
	public void onLoadMore() {
		// TODO Auto-generated method stub
		isRefresh = false;
		pageNum = pageSize;
		pageSize = GlobalConstant.DEFAULT_PAGE_SIZE;

		smartQueryApprases();
	}

	private void smartQueryApprases() {

	}
}
