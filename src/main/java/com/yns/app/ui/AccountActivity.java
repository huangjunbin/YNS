package com.yns.app.ui;

import android.widget.TextView;

import com.yns.R;
import com.yns.app.AppContext;
import com.yns.widget.HeaderBar;

/**
 * @AccountActivity.java
 * @author:lmt
 * @function:账户管理
 * @d2014-12-12上午10:35:59
 * @update:
 */
public class AccountActivity extends BaseActivity {
	private HeaderBar headerBar;
	private TextView tvNow, tvUse, tvMoney;

	public AccountActivity() {
		super(R.layout.activity_account);
	}

	@Override
	public void initViews() {
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle(getResString(R.string.account_title));
		tvNow = (TextView) findViewById(R.id.tv_now);
		tvUse = (TextView) findViewById(R.id.tv_use);
		tvMoney = (TextView) findViewById(R.id.tv_money);
	}

	@Override
	public void initData() {

	}

	@Override
	public void bindViews() {
	 
	}

}
