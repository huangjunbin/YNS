package com.yns.app.ui;

import com.yns.R;
import com.yns.app.AppContext;
import com.yns.app.adapter.OrderAdapter;
import com.yns.model.OrderInfo;
import com.yns.model.ResponeModel;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.GoodService;
import com.yns.util.DateUtil;
import com.yns.widget.HeaderBar;
import com.yns.widget.XListView;
import com.yns.widget.XListView.IXListViewListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OrderListActivity extends BaseActivity implements IXListViewListener {
    private HeaderBar headerBar;
    private XListView xListView;
    private OrderAdapter adapter;
    public static List<OrderInfo> dataList;
    private int page = 1;
    private int rows = 5;
    private boolean isRefresh = true;
    private int position = 0;


    public OrderListActivity() {
        super(R.layout.activity_orderinfo);
    }

    @Override
    public void initViews() {
        headerBar = (HeaderBar) findViewById(R.id.title);
        headerBar.setTitle("订单列表");
        xListView = (XListView) findViewById(R.id.messagelist);
        xListView.setXListViewListener(this);
        xListView.setPullLoadEnable(false);
    }

    public void setAllPrice(String price) {

    }

    @Override
    public void initData() {
        position = getIntent().getIntExtra("position", 0);

        dataList = new ArrayList<OrderInfo>();
        adapter = new OrderAdapter(this, dataList);
        xListView.setAdapter(adapter);


    }

    @Override
    public void bindViews() {
        xListView.setFooterDividersEnabled(false);
    }

    @Override
    public void onRefresh() {
        isRefresh = true;
        page = 1;
        getMessageList();

    }

    @Override
    public void onLoadMore() {
        isRefresh = false;
        page++;
        getMessageList();
    }

    private void getMessageList() {
        GoodService.getInstance(context).getOrderList(
                AppContext.currentUser.getUserId()+"", new CustomAsyncResponehandler() {
                    public void onFinish() {
                        xListView.stopLoadMore();
                        xListView.stopRefresh();
                        xListView.setRefreshTime(DateUtil.getDateTime(new Date(
                                System.currentTimeMillis())));
                    }

                    ;

                    @SuppressWarnings("unchecked")
                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel != null && baseModel.isStatus()) {
                            List<OrderInfo> OrderInfo = (List<OrderInfo>) baseModel
                                    .getResultObj();
                            if (OrderInfo != null && OrderInfo.size() > 0) {
                                if (isRefresh) {
                                    dataList.clear();
                                    dataList.addAll(0, OrderInfo);
                                } else {
                                    dataList.addAll(OrderInfo);
                                }
                                adapter.notifyDataSetChanged();
                            }
                        }
                    }
                });
    }

    @Override
    protected void onResume() {
        onRefresh();
        super.onResume();
    }
}
