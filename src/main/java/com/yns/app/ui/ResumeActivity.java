package com.yns.app.ui;

import android.content.Intent;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.TextView;

import com.yns.R;
import com.yns.app.AppContext;
import com.yns.app.adapter.ResumeAdapter;
import com.yns.model.ResponeModel;
import com.yns.model.Resume;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.ComonService;
import com.yns.util.DateUtil;
import com.yns.widget.HeaderBar;
import com.yns.widget.XListView;
import com.yns.widget.XListView.IXListViewListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ResumeActivity extends BaseActivity implements IXListViewListener {

    private HeaderBar headerBar;
    private XListView xlvData;

    private ResumeAdapter adapter;
    public static List<Resume> resumeList;
    private int page = 1;
    private int rows = 5;
    private boolean isRefresh = true;
    private EditText editText;
    private String searchKey;

    private int type = 0;//0 我的简历  1表示推荐简历 2表示应聘简历 3企业简历
    private String jobId = "";
    String nickName = "", biYeXueXiao = "", xueLi = "", gongZuoNainXian = "", shenGao = "", sex = "", mingZu = "";

    public ResumeActivity() {
        super(R.layout.activity_resume);

    }

    @Override
    public void initViews() {
        type = getIntent().getIntExtra("type", 0);
        jobId = getIntent().getStringExtra("jobId");
        nickName = getIntent().getStringExtra("nickName");
        biYeXueXiao = getIntent().getStringExtra("biYeXueXiao");
        xueLi = getIntent().getStringExtra("xueLi");
        gongZuoNainXian = getIntent().getStringExtra("gongZuoNainXian");
        shenGao = getIntent().getStringExtra("shenGao");
        sex = getIntent().getStringExtra("sex");
        mingZu = getIntent().getStringExtra("mingZu");

        headerBar = (HeaderBar) findViewById(R.id.title);

        xlvData = (XListView) findViewById(R.id.xlv_data);
        xlvData.setPullRefreshEnable(true);
        xlvData.setPullLoadEnable(true);
        editText = (EditText) findViewById(R.id.search_edit);
        editText.setBackgroundResource(R.drawable.edit_city_search_bg);
        editText.setCompoundDrawablesWithIntrinsicBounds(
                R.drawable.city_edit_search, 0, 0, 0);
        editText.setHint("输入关键字搜索");
        editText.setHintTextColor(getResColor(R.color.gray));
        editText.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
        if (type == 0) {
            findViewById(R.id.search).setVisibility(View.GONE);
            headerBar.setTitle("我的简历");
            headerBar.top_right_btn.setVisibility(View.VISIBLE);
            headerBar.top_right_btn.setText("新增");
            headerBar.top_right_btn.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setClass(ResumeActivity.this,
                            ResumeAddActivity.class);
                    startActivity(intent);
                }
            });
        } else if (type == 1) {
            headerBar.setTitle("推荐简历");
            headerBar.top_right_btn.setVisibility(View.GONE);
            headerBar.top_right_img2.setVisibility(View.VISIBLE);
            headerBar.top_right_img2.setImageDrawable(getResources().getDrawable(
                    R.drawable.search));
            headerBar.top_right_img2.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setClass(ResumeActivity.this,
                            ResumeSearchActivity.class);
                    startActivity(intent);
                }
            });
        } else if (type == 2) {
            headerBar.setTitle("应聘简历");
            findViewById(R.id.search).setVisibility(View.GONE);
            headerBar.top_right_btn.setVisibility(View.GONE);
        } else if (type == 3) {
            headerBar.setTitle("应聘简历");
            findViewById(R.id.search).setVisibility(View.GONE);
            headerBar.top_right_btn.setVisibility(View.GONE);
        }
    }

    @Override
    public void initData() {
        resumeList = new ArrayList<Resume>();
        adapter = new ResumeAdapter(this, resumeList);
        xlvData.setAdapter(adapter);

    }


    @Override
    protected void onResume() {
        onRefresh();
        super.onResume();
    }

    @Override
    public void bindViews() {
        xlvData.setXListViewListener(this);
        xlvData.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                Resume resume = resumeList.get(position - 1);
                Intent intent = new Intent();
                intent.setClass(ResumeActivity.this, ResumeDetailActivity.class);
                intent.putExtra("resume", resume);
                intent.putExtra("type", type);
                startActivity(intent);

            }
        });

        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                searchKey = editText.getText().toString();
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    page = 1;
                    getApplyList(searchKey);
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onRefresh() {
        xlvData.setRefreshTime(DateUtil.getDateTime(new Date(System
                .currentTimeMillis())));
        isRefresh = true;
        page = 1;
        if (type == 1) {
            getApplyList("");
        } else if (type == 2) {
            getApplyList2();
        }  else if (type == 3) {
            getApplyList3();
        } else {
            getApplyList();
        }
    }

    @Override
    public void onLoadMore() {
        isRefresh = false;
        page++;
        if (type == 1) {
            getApplyList("");
        } else if (type == 2) {
            getApplyList2();
        } else if (type == 3) {
            getApplyList3();
        } else {
            getApplyList();
        }
    }
    private void getApplyList3() {
        ComonService.getInstance(context).getResumeByEnterpriseId(
                rows + "",
                page + "",
                AppContext.currentUser == null ? "" : AppContext.currentUser
                        .getUserId()+"",new CustomAsyncResponehandler() {
                    public void onFinish() {
                        xlvData.stopLoadMore();
                        xlvData.stopRefresh();
                        xlvData.setRefreshTime(DateUtil.getDateTime(new Date(
                                System.currentTimeMillis())));
                    }

                    ;

                    @SuppressWarnings("unchecked")
                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel != null && baseModel.isStatus()) {
                            List<Resume> lists = (List<Resume>) baseModel
                                    .getResultObj();
                            if (lists != null && lists.size() > 0) {

                                if (isRefresh) {
                                    resumeList.clear();
                                    resumeList.addAll(0, lists);
                                } else {
                                    resumeList.addAll(lists);
                                }
                                adapter.notifyDataSetChanged();
                            }
                        }
                    }
                });
    }
    private void getApplyList() {
        ComonService.getInstance(context).getResume(
                rows + "",
                page + "",
                AppContext.currentUser == null ? "" : AppContext.currentUser
                        .getUserId(), new CustomAsyncResponehandler() {
                    public void onFinish() {
                        xlvData.stopLoadMore();
                        xlvData.stopRefresh();
                        xlvData.setRefreshTime(DateUtil.getDateTime(new Date(
                                System.currentTimeMillis())));
                    }

                    ;

                    @SuppressWarnings("unchecked")
                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel != null && baseModel.isStatus()) {
                            List<Resume> lists = (List<Resume>) baseModel
                                    .getResultObj();
                            if (lists != null && lists.size() > 0) {

                                if (isRefresh) {
                                    resumeList.clear();
                                    resumeList.addAll(0, lists);
                                } else {
                                    resumeList.addAll(lists);
                                }
                                adapter.notifyDataSetChanged();
                            }
                        }
                    }
                });
    }

    private void getApplyList2() {
        ComonService.getInstance(context).getJobApplyByJobId(rows + "",
                page + "", jobId, new CustomAsyncResponehandler() {
                    public void onFinish() {
                        xlvData.stopLoadMore();
                        xlvData.stopRefresh();
                        xlvData.setRefreshTime(DateUtil.getDateTime(new Date(
                                System.currentTimeMillis())));
                    }

                    ;

                    @SuppressWarnings("unchecked")
                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel != null && baseModel.isStatus()) {
                            List<Resume> lists = (List<Resume>) baseModel
                                    .getResultObj();
                            if (lists != null && lists.size() > 0) {

                                if (isRefresh) {
                                    resumeList.clear();
                                    resumeList.addAll(0, lists);
                                } else {
                                    resumeList.addAll(resumeList.size(), lists);
                                }
                                adapter.notifyDataSetChanged();
                            }
                        }
                    }
                });
    }

    private void getApplyList(String key) {
        ComonService.getInstance(context).searchResume(
                rows + "",
                page + "",
                AppContext.currentUser == null ? "" : AppContext.currentUser
                        .getUserId(), key, "" + AppContext.currentUser.getDeptId(), nickName, biYeXueXiao, xueLi, gongZuoNainXian, shenGao, sex, mingZu, new CustomAsyncResponehandler() {
                    public void onFinish() {
                        xlvData.stopLoadMore();
                        xlvData.stopRefresh();
                        xlvData.setRefreshTime(DateUtil.getDateTime(new Date(
                                System.currentTimeMillis())));
                    }

                    ;

                    @SuppressWarnings("unchecked")
                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel != null && baseModel.isStatus()) {
                            List<Resume> lists = (List<Resume>) baseModel
                                    .getResultObj();
                            if (lists != null && lists.size() > 0) {

                                if (isRefresh) {
                                    resumeList.clear();
                                    resumeList.addAll(0, lists);
                                } else {
                                    resumeList.addAll(resumeList.size(), lists);
                                }
                                adapter.notifyDataSetChanged();
                            }
                        }
                    }
                });
    }
}
