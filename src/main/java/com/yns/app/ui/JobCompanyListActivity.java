package com.yns.app.ui;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.yns.R;
import com.yns.app.AppContext;
import com.yns.app.adapter.JobCompanyAdapter;
import com.yns.model.Job;
import com.yns.model.ResponeModel;
import com.yns.model.Resume;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.ComonService;
import com.yns.util.DateUtil;
import com.yns.widget.HeaderBar;
import com.yns.widget.XListView;
import com.yns.widget.XListView.IXListViewListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class JobCompanyListActivity extends BaseActivity implements
        IXListViewListener {

    private HeaderBar headerBar;
    private XListView xlvData;

    private JobCompanyAdapter jobAdapter;
    public static List<Job> jobs;
    private int page = 1;
    private int rows = 5;
    private boolean isRefresh = true;

    private int flag;
    private Resume resume;

    public JobCompanyListActivity() {
        super(R.layout.activity_jobcompany);

    }

    @Override
    public void initViews() {

        headerBar = (HeaderBar) findViewById(R.id.title);
        headerBar.setTitle("职位列表");
        headerBar.top_right_btn.setText("添加职位");
        headerBar.top_right_btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(JobCompanyListActivity.this,
                        JobAddActivity.class);
                intent.putExtra("flag", flag);
                startActivity(intent);
            }
        });
        xlvData = (XListView) findViewById(R.id.xlv_data);
        xlvData.setPullRefreshEnable(true);
        xlvData.setPullLoadEnable(true);

    }

    @Override
    public void initData() {
        jobs = new ArrayList<Job>();

        flag = getIntent().getIntExtra("flag", -1);

        jobAdapter = new JobCompanyAdapter(this, jobs);
        xlvData.setAdapter(jobAdapter);
        resume = (Resume) getIntent().getSerializableExtra("resume");
        if (resume != null) {
            headerBar.top_right_btn.setVisibility(View.GONE);
            jobAdapter.isEdit=false;
        }
    }

    @Override
    public void bindViews() {

        xlvData.setXListViewListener(this);
        xlvData.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Job job = jobs.get(position - 1);
                if (resume == null) {

                    Intent intent = new Intent();
                    intent.setClass(JobCompanyListActivity.this,
                            JobDetailActivity.class);
                    intent.putExtra("job", job);
                    intent.putExtra("tag", 1);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent();
                    intent.setClass(JobCompanyListActivity.this,
                            ResumeInviteActivity.class);
                    intent.putExtra("resume", resume);
                    intent.putExtra("jobInfoId", job.getAppJobInfoId());
                    startActivity(intent);
                }
            }
        });

    }

    @Override
    protected void onResume() {
        onRefresh();
        super.onResume();
    }

    @Override
    public void onRefresh() {
        xlvData.setRefreshTime(DateUtil.getDateTime(new Date(System
                .currentTimeMillis())));
        isRefresh = true;
        page = 1;
        getJobList();
    }

    @Override
    public void onLoadMore() {
        isRefresh = false;
        page++;
        getJobList();
    }

    private void getJobList() {
        ComonService.getInstance(context).selectJobByUserId(
                rows + "",
                page + "",
                AppContext.currentUser == null ? "" : AppContext.currentUser
                        .getUserId(), flag, new CustomAsyncResponehandler() {
                    public void onFinish() {
                        xlvData.stopLoadMore();
                        xlvData.stopRefresh();
                        xlvData.setRefreshTime(DateUtil.getDateTime(new Date(
                                System.currentTimeMillis())));
                    }

                    ;

                    @SuppressWarnings("unchecked")
                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel != null && baseModel.isStatus()) {
                            List<Job> jobList = (List<Job>) baseModel
                                    .getResultObj();
                            if (jobList != null && jobList.size() > 0) {
                                if (isRefresh) {
                                    jobs.clear();
                                    jobs.addAll(0, jobList);
                                } else {
                                    jobs.addAll(jobList.size(), jobList);
                                }
                                jobAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                });
    }

}
