package com.yns.app.ui;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.yns.R;
import com.yns.app.AppConfig;
import com.yns.app.AppContext;
import com.yns.db.dao.UserDao;
import com.yns.global.GlobalConstant;
import com.yns.global.GlobalVariable;
import com.yns.model.ResponeModel;
import com.yns.model.User;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.ComonService;
import com.yns.service.UserService;
import com.yns.util.MediaUtil;
import com.yns.util.StringUtils;
import com.yns.util.UIHelper;
import com.yns.widget.CameraView;
import com.yns.widget.HeaderBar;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

public class RegisterActivity extends BaseActivity implements OnClickListener {

    private HeaderBar headerBar;

    private EditText editTextName, editTextEmail, etMoblie, editTextAccount,
            editTextPasworrd, editTextSurePassword;

    private CheckBox checkUser, checkCompany;
    private LinearLayout lyCompany;
    private EditText etCompanyName, etCompanyTel;
    private Button btnSelect, btnSelect2;
    private int flag = 0;
    private String companyPIC = "";
    private String personImageName = "company.jpg";
    private String companyHeaderPic = "";
    private String companyImageName = "companyImage.jpg";
    private int tag = 0;

    public RegisterActivity() {
        super(R.layout.activity_register);
    }

    @Override
    public void initViews() {

        headerBar = (HeaderBar) findViewById(R.id.title);
        etMoblie = (EditText) findViewById(R.id.register_mobile);
        editTextName = (EditText) findViewById(R.id.register_name);
        editTextEmail = (EditText) findViewById(R.id.register_email);
        editTextPasworrd = (EditText) findViewById(R.id.register_password);
        editTextAccount = (EditText) findViewById(R.id.register_account);
        editTextSurePassword = (EditText) findViewById(R.id.register_surepassword);
        lyCompany = (LinearLayout) findViewById(R.id.ly_company);
        checkUser = (CheckBox) findViewById(R.id.login_user);
        checkCompany = (CheckBox) findViewById(R.id.login_company);
        etCompanyName = (EditText) findViewById(R.id.register_company_name);
        etCompanyTel = (EditText) findViewById(R.id.register_company_tel);
        btnSelect = (Button) findViewById(R.id.btn_select);
        btnSelect2 = (Button) findViewById(R.id.btn_select2);
    }

    @Override
    public void initData() {
    }

    @Override
    public void bindViews() {
        headerBar.setTitle(getResString(R.string.register_title));
        btnSelect.setOnClickListener(this);
        btnSelect2.setOnClickListener(this);
        findViewById(R.id.register).setOnClickListener(this);
        checkUser.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                if (isChecked) {
                    flag = 0;
                    checkCompany.setChecked(false);
                } else {
                    flag = 1;
                }

            }
        });

        checkCompany.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {
                if (isChecked) {
                    lyCompany.setVisibility(View.VISIBLE);
                    checkUser.setChecked(false);
                    flag = 1;
                } else {
                    lyCompany.setVisibility(View.GONE);
                    flag = 0;
                }

            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_select:
                tag = 0;
                CameraView.showCameraView(RegisterActivity.this, personImageName);
                break;
            case R.id.btn_select2:
                tag = 1;
                CameraView.showCameraView(RegisterActivity.this, companyImageName);
                break;
            case R.id.register:
                if (checkRegister()) {
                    if (flag == 0) {
                        UserService.getInstance(context).register(
                                editTextAccount.getText().toString(),
                                editTextPasworrd.getText().toString(),
                                editTextEmail.getText().toString(), "",
                                editTextName.getText().toString(), etMoblie.getText().toString(),
                                new CustomAsyncResponehandler() {
                                    @Override
                                    public void onSuccess(ResponeModel baseModel) {
                                        super.onSuccess(baseModel);
                                        if (baseModel != null
                                                && baseModel.isStatus()) {
                                            AppContext.currentUser = (User) baseModel
                                                    .getResultObj();
                                            GlobalVariable.is_Login_Succed = true;
                                            UserDao userDao = new UserDao(
                                                    RegisterActivity.this);
                                            if (userDao != null) {
                                                userDao.delete();
                                                userDao.insert(AppContext.currentUser);
                                            }
                                            startActivity(new Intent(
                                                    RegisterActivity.this,
                                                    MainActivity2.class));
                                            RegisterActivity.this.finish();
                                        }
                                    }
                                });
                    } else {
                        if (!StringUtils.isMobileNum(etMoblie.getText()
                                .toString())) {
                            UIHelper.ShowMessage(context, "手机号不能为空");
                            return;
                        }
                        if (StringUtils.isEmpty(etCompanyName.getText().toString())) {
                            UIHelper.ShowMessage(context, "企业姓名不能为空");
                            return;
                        }
                        if (StringUtils.isEmpty(companyPIC)) {
                            UIHelper.ShowMessage(context, "营业执照不能为空");
                            return;
                        }
                        UserService.getInstance(context).registerCompany(
                                editTextAccount.getText().toString(),
                                editTextPasworrd.getText().toString(),
                                editTextEmail.getText().toString(), companyPIC,
                                editTextName.getText().toString(),
                                etMoblie.getText().toString(),
                                etCompanyName.getText().toString(), companyHeaderPic,
                                new CustomAsyncResponehandler() {
                                    @Override
                                    public void onSuccess(ResponeModel baseModel) {
                                        super.onSuccess(baseModel);
                                        if (baseModel != null
                                                && baseModel.isStatus()) {
//                                            AppContext.currentUser = (User) baseModel
//                                                    .getResultObj();
//                                            GlobalVariable.is_Login_Succed = true;
//                                            UserDao userDao = new UserDao(
//                                                    RegisterActivity.this);
//                                            if (userDao != null) {
//                                                userDao.delete();
//                                                userDao.insert(AppContext.currentUser);
//                                            }
//                                            startActivity(new Intent(
//                                                    RegisterActivity.this,
//                                                    MainActivity2.class));
                                            UIHelper.ShowMessage(RegisterActivity.this, "请等待审核");
                                            RegisterActivity.this.finish();

                                        }
                                    }
                                });
                    }
                }
                break;
        }
    }

    /**
     * 检查注册输入的有效性
     *
     * @return
     */
    private boolean checkRegister() {
        if (StringUtils.isEmpty(editTextName.getText().toString())) {
            UIHelper.ShowMessage(context,
                    getResString(R.string.register_nameempt));
            return false;
        }
        if (!StringUtils.isEmail(editTextEmail.getText().toString())) {
            UIHelper.ShowMessage(context,
                    getResString(R.string.register_telempt));
            return false;
        }

        if (StringUtils.isEmpty(editTextPasworrd.getText().toString())) {
            UIHelper.ShowMessage(context,
                    getResString(R.string.register_passwordempt));
            return false;
        }

        if (StringUtils.isEmpty(editTextSurePassword.getText().toString())) {
            UIHelper.ShowMessage(context,
                    getResString(R.string.register_surepasswordempt));
            return false;
        }

        if (!editTextPasworrd.getText().toString()
                .equals(editTextSurePassword.getText().toString())) {
            UIHelper.ShowMessage(context,
                    getResString(R.string.register_passworddiffer));
            return false;
        }

        if (editTextPasworrd.getText().toString().length() < GlobalConstant.PASSWORD_MINLEN) {
            UIHelper.ShowMessage(context,
                    getResString(R.string.register_passwordlen));
            return false;
        }
        if (!checkUser.isChecked() && !checkCompany.isChecked()) {
            showToast("请选择账户类型");
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case MediaUtil.ALBUM:
                if (data != null || resultCode == Activity.RESULT_OK) {
                    startPhotoZoom(data.getData());
                }
                break;
            case MediaUtil.PHOTO:
                if (resultCode == Activity.RESULT_OK) {
                    if (tag == 0) {
                        File temp = new File(MediaUtil.ROOTPATH + File.separator
                                + AppConfig.APP_PATH + File.separator + personImageName);
                        startPhotoZoom(Uri.fromFile(temp));
                    } else {
                        File temp = new File(MediaUtil.ROOTPATH + File.separator
                                + AppConfig.APP_PATH + File.separator + companyImageName);
                        startPhotoZoom(Uri.fromFile(temp));
                    }
                }
                break;
            case MediaUtil.ZOOMPHOTO:
                if (data != null || resultCode == Activity.RESULT_OK) {
                    setPicToView(data);
                }
                break;
        }

    }

    /**
     * 裁剪图片方法实现
     *
     * @param uri
     */
    public void startPhotoZoom(Uri uri) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        // 下面这个crop=true是设置在开启的Intent中设置显示的VIEW可裁剪
        intent.putExtra("crop", "true");
        // aspectX aspectY 是宽高的比例
        // outputX outputY 是裁剪图片宽高
        intent.putExtra("outputX", 150);
        intent.putExtra("outputY", 150);
        intent.putExtra("return-data", true);
        File dir = new File((MediaUtil.ROOTPATH + File.separator
                + AppConfig.APP_PATH + File.separator));
        if (!dir.exists())
            dir.mkdirs();

        File saveFile = null;
        if (tag == 0) {
            saveFile = new File(MediaUtil.ROOTPATH + File.separator
                    + AppConfig.APP_PATH + File.separator + personImageName);
        }

        if (tag == 1) {
            saveFile = new File(MediaUtil.ROOTPATH + File.separator
                    + AppConfig.APP_PATH + File.separator + companyImageName);
        }
        intent.putExtra("output", Uri.fromFile(saveFile)); // 传入目标文件
        intent.putExtra("outputFormat", "JPEG"); // 输入文件格式
        Intent it = Intent.createChooser(intent, "剪裁图片");
        startActivityForResult(it, MediaUtil.ZOOMPHOTO);
    }

    private void setPicToView(Intent data) {
        Bundle extras = data.getExtras();
        if (extras != null) {
            final Bitmap img = extras.getParcelable("data");
            ComonService.getInstance(context).uploadImg(
                    tag == 0 ?
                            new File(MediaUtil.ROOTPATH + File.separator
                                    + AppConfig.APP_PATH + File.separator
                                    + personImageName) : new File(MediaUtil.ROOTPATH + File.separator
                            + AppConfig.APP_PATH + File.separator
                            + companyImageName),
                    new CustomAsyncResponehandler() {
                        @Override
                        public void onSuccess(ResponeModel baseModel) {
                            super.onSuccess(baseModel);
                            if (baseModel != null && baseModel.isStatus()) {

                                try {
                                    if (tag == 0) {
                                        companyPIC = new JSONObject(baseModel
                                                .getResult()).getString("url");
                                    } else {
                                        companyHeaderPic = new JSONObject(baseModel
                                                .getResult()).getString("url");
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                UIHelper.ShowMessage(context, "上传成功");
                            }
                        }
                    });
        }
    }
}
