package com.yns.app.ui;

import java.util.ArrayList;
import java.util.List;

import android.widget.Toast;

import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.overlayutil.OverlayManager;
import com.baidu.mapapi.search.core.CityInfo;
import com.baidu.mapapi.search.core.PoiInfo;
import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.poi.OnGetPoiSearchResultListener;
import com.baidu.mapapi.search.poi.PoiDetailResult;
import com.baidu.mapapi.search.poi.PoiNearbySearchOption;
import com.baidu.mapapi.search.poi.PoiResult;
import com.baidu.mapapi.search.poi.PoiSearch;
import com.yns.R;
import com.yns.app.fragement.BaseBaidMapFragement;
import com.yns.app.fragement.BaseBaidMapFragement.OnLocEndListener;
import com.yns.util.UIHelper;
import com.yns.widget.HeaderBar;

/**
 * @FullActivity.java
 * @author:lmt
 * @function:加油站
 * @d2014-12-16下午4:39:45
 * @update:
 */
public class FullActivity extends BaseActivity implements
		OnGetPoiSearchResultListener, OnLocEndListener {
	private BaseBaidMapFragement baseBaidMapFragement;
	private HeaderBar headerBar;

	private PoiSearch mPoiSearch = null;

	public FullActivity() {
		super(R.layout.activity_full);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void initViews() {
		// TODO Auto-generated method stub
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle(getResString(R.string.full_title));
	}

	@Override
	public void initData() {
		// TODO Auto-generated method stub
		baseBaidMapFragement = new BaseBaidMapFragement();
		getSupportFragmentManager().beginTransaction()
				.add(R.id.fragment_container, baseBaidMapFragement)
				.show(baseBaidMapFragement).commit();
		mPoiSearch = PoiSearch.newInstance();
		mPoiSearch.setOnGetPoiSearchResultListener(this);
	}

	@Override
	public void bindViews() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onGetPoiDetailResult(PoiDetailResult result) {
		// TODO Auto-generated method stub
		if (result.error != SearchResult.ERRORNO.NO_ERROR) {
			Toast.makeText(context, "抱歉，未找到结果", Toast.LENGTH_SHORT).show();
		} else {
			Toast.makeText(context,
					result.getName() + ": " + result.getAddress(),
					Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onGetPoiResult(PoiResult result) {
		// TODO Auto-generated method stub
		if (result == null
				|| result.error == SearchResult.ERRORNO.RESULT_NOT_FOUND) {
			UIHelper.ShowMessage(context, getResString(R.string.baidu_poino));
			return;
		}
		if (result.error == SearchResult.ERRORNO.NO_ERROR) {
			baseBaidMapFragement.mBaiduMap.clear();
			MyPoiOverlay overlay = new MyPoiOverlay(
					baseBaidMapFragement.mBaiduMap);
			overlay.setResult(result);
			baseBaidMapFragement.mBaiduMap.setOnMarkerClickListener(overlay);
			overlay.addToMap();
			overlay.zoomToSpan();
			return;
		}

		if (result.error == SearchResult.ERRORNO.AMBIGUOUS_KEYWORD) {

			// 当输入关键字在本市没有找到，但在其他城市找到时，返回包含该关键字信息的城市列表
			String strInfo = "在";
			for (CityInfo cityInfo : result.getSuggestCityList()) {
				strInfo += cityInfo.city;
				strInfo += ",";
			}
			strInfo += "找到结果";
			UIHelper.ShowMessage(context, strInfo);
		}
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		mPoiSearch.destroy();
	}

	@Override
	public void onLocEnd(LatLng latLng) {
		// TODO Auto-generated method stub
		mPoiSearch.searchNearby((new PoiNearbySearchOption()).location(latLng)
				.keyword("加油站").radius(3000).pageCapacity(10).pageNum(0));
	}

	// private class MyPoiOverlay extends PoiOverlay {
	//
	// public MyPoiOverlay(BaiduMap baiduMap) {
	// super(baiduMap);
	// }
	//
	// @Override
	// public boolean onPoiClick(int index) {
	// super.onPoiClick(index);
	// PoiInfo poi = getPoiResult().getAllPoi().get(index);
	// // if (poi.hasCaterDetails) {
	// // mPoiSearch.searchPoiDetail((new PoiDetailSearchOption())
	// // .poiUid(poi.uid));
	// // }
	// UIHelper.ShowMessage(context, poi.name + ": " + poi.address);
	// return true;
	// }
	//
	// }

	private class MyPoiOverlay extends OverlayManager {
		private PoiResult result;
		private BaiduMap bdmap;

		public void setResult(PoiResult result) {
			this.result = result;
		}

		public MyPoiOverlay(BaiduMap bdmap) {
			super(bdmap);
			this.bdmap = bdmap;
		}

		@Override
		public boolean onMarkerClick(Marker marker) {
			onClick(marker.getZIndex());
			return true;
		}

		public boolean onClick(int index) {
			PoiInfo poi = result.getAllPoi().get(index);
			UIHelper.ShowMessage(context, poi.name + ": " + poi.address);
			return true;
		}

		@Override
		public List<OverlayOptions> getOverlayOptions() {
			List<OverlayOptions> ops = new ArrayList<OverlayOptions>();
			List<PoiInfo> pois = result.getAllPoi();
			BitmapDescriptor bitmap = BitmapDescriptorFactory
					.fromResource(R.drawable.iocn_full_loc);
			for (int i = 0; i < pois.size(); i++) {
				OverlayOptions op = new MarkerOptions().position(
						pois.get(i).location).icon(bitmap);
				ops.add(op);
				bdmap.addOverlay(op).setZIndex(i);
			}
			return ops;
		}
	}
}
