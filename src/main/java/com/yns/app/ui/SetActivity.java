package com.yns.app.ui;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;

import com.yns.R;
import com.yns.model.Text;
import com.yns.util.UpdateManager;
import com.yns.widget.HeaderBar;

/**
 * @className：SetActivity.java
 * @author: lmt
 * @Function: 设置
 * @createDate: 2014-12-9 下午3:41:55
 * @update:
 */
public class SetActivity extends BaseActivity implements OnClickListener {
	private HeaderBar headerBar;

	public SetActivity() {
		super(R.layout.activity_set);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void initViews() {
		// TODO Auto-generated method stub
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle(getResString(R.string.set_title));
	}

	@Override
	public void initData() {
		// TODO Auto-generated method stub

	}

	@Override
	public void bindViews() {
		// TODO Auto-generated method stub
		findViewById(R.id.set_aboutus_bar).setOnClickListener(this);
		findViewById(R.id.set_servicepay_bar).setOnClickListener(this);
		findViewById(R.id.set_ydprotocol_bar).setOnClickListener(this);
		findViewById(R.id.set_feedback_bar).setOnClickListener(this);
		findViewById(R.id.set_share).setOnClickListener(this);
		findViewById(R.id.set_update).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Text text = new Text();
		switch (v.getId()) {
		case R.id.set_update:
			UpdateManager.getUpdateManager(SetActivity.this).checkAppUpdate(true);
			break;
		case R.id.set_aboutus_bar:
			text = new Text();
			text.setTitle(getResString(R.string.set_about_title));
			text.setContent(getResString(R.string.set_aboutus_content));
			JumpToActivity(TextActivity.class, text, false);
			break;
		case R.id.set_feedback_bar:
			JumpToActivity(FeedBackActivity.class, false);
			break;
		case R.id.set_servicepay_bar:
			text = new Text();
			text.setTitle(getResString(R.string.set_servicepay_title));
			text.setContent(getResString(R.string.set_servicepay_content));
			JumpToActivity(TextActivity.class, text, false);
			break;
		case R.id.set_ydprotocol_bar:
			text = new Text();
			text.setTitle(getResString(R.string.set_ydprotocol_title));
			text.setContent(getResString(R.string.set_ydprotocol_content));
			JumpToActivity(TextActivity.class, text, false);
			break;
		case R.id.set_share:
			String str = "靓车汇平台APP，洗车省钱不只是一丁点。我发现一款实用的洗车软件，你也来试试看";
			Intent intent_share = new Intent("android.intent.action.SEND");
			intent_share.setType("text/plain");
			intent_share.putExtra("android.intent.extra.SUBJECT", "分享");
			intent_share.putExtra("android.intent.extra.TEXT", str);
			intent_share.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(Intent.createChooser(intent_share, this.getTitle()));
			break;
		}
	}
}
