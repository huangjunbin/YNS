package com.yns.app.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yns.R;
import com.yns.app.AppContext;
import com.yns.model.Product;
import com.yns.model.ResponeModel;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.ComonService;
import com.yns.service.GoodService;
import com.yns.widget.HeaderBar;

@SuppressLint("InflateParams")

public class ProductDetailActivity extends BaseActivity {

    private TextView textViewMessageContent, tvTitle, textViewMessageTime, tvSalePrice, tvTagPrice;
    private ImageView ivPhoto;
    private Button btnAdd;
    private Product data;
    private HeaderBar bar;
    private WebView wvDetail;
    private Button btnBuy;
    private int type = 0;

    public ProductDetailActivity() {
        super(R.layout.activity_product_detail);
    }

    @Override
    public void initViews() {
        textViewMessageContent = (TextView) findViewById(R.id.message_content);
        textViewMessageTime = (TextView) findViewById(R.id.message_time);
        tvTitle = (TextView) findViewById(R.id.message_title);
        ivPhoto = (ImageView) findViewById(R.id.iv_photo);
        tvSalePrice = (TextView) findViewById(R.id.tv_saleprice);
        tvTagPrice = (TextView) findViewById(R.id.tv_tagprice);
        btnAdd = (Button) findViewById(R.id.btn_add);
        bar = (HeaderBar) findViewById(R.id.title);
        wvDetail = (WebView) findViewById(R.id.wv_detail);
        btnBuy = (Button) findViewById(R.id.btn_buy);
    }

    @Override
    public void initData() {
        data = (Product) getIntent().getSerializableExtra("data");
        type = getIntent().getIntExtra("type", 0);
        WindowManager wm = (WindowManager) this.getApplicationContext()
                .getSystemService(Context.WINDOW_SERVICE);
        int width = wm.getDefaultDisplay().getWidth();
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, width);
        ivPhoto.setLayoutParams(params);
    }

    @Override
    public void bindViews() {
        bar.setTitle("商品详情");
        tvTitle.setText(data.getName());
        textViewMessageContent.setText(data.getDescription());
        textViewMessageTime.setText(data.getCreateDate());
        tvSalePrice.setText("￥" + data.getSalePrice() + "");
        tvTagPrice.setText("￥" + data.getTagPrice() + "");
        tvTagPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        AppContext.setImage(data.getPic(), ivPhoto,
                AppContext.imageOption);
        ivPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProductDetailActivity.this, BigImageActivity.class);
                intent.putExtra("url", data.getPic());
                startActivity(intent);
            }
        });
        String str = data.getContent();
        try {

            str = str.replaceAll("&amp;", "");
            str = str.replaceAll("quot;", "\"");
            str = str.replaceAll("lt;", "<");
            str = str.replaceAll("gt;", ">");
            wvDetail.loadDataWithBaseURL(null, str, "text/html", "utf-8",
                    null);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppContext.currentUser == null) {
                    JumpToActivity(LoginActivity.class, false);
                    return;
                }
                GoodService.getInstance(ProductDetailActivity.this).addShopCart(data.getAppProductInfoId(),
                        AppContext.currentUser.getUserId(), 1,
                        new CustomAsyncResponehandler() {
                            public void onFinish() {

                            }

                            @Override
                            public void onSuccess(ResponeModel baseModel) {
                                super.onSuccess(baseModel);
                                if (baseModel != null && baseModel.isStatus()) {
                                    showToast("加入购物车成功");
                                }
                            }
                        });
            }
        });
        //appProductInfoId	必填	String	产品id  buyCount	必填	Integer	数量
        btnBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppContext.currentUser == null) {
                    JumpToActivity(LoginActivity.class, false);
                    return;
                }
                Intent intent = new Intent(ProductDetailActivity.this, AddressActivity.class);
                intent.putExtra("appProductInfoId", data.getAppProductInfoId());
                intent.putExtra("buyCount", "1");
                startActivity(intent);
            }
        });
        bar.top_right_img2.setVisibility(View.VISIBLE);
        if (type == 0) {
            bar.top_right_img2.setImageResource(R.drawable.star);
        } else {
            bar.top_right_img2.setImageResource(R.drawable.select_star);
        }
        bar.top_right_img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AppContext.currentUser == null) {
                    JumpToActivity(LoginActivity.class, false);
                    return;
                } else {
                    if (type == 0) {
                        addColelct();
                    } else {
                        removeColelct();
                    }
                }
            }
        });
    }

    private void addColelct() {
        ComonService.getInstance(context).addCollect(
                AppContext.currentUser.getUserId(), data.getAppProductInfoId(), "product ",
                new CustomAsyncResponehandler() {
                    public void onFinish() {

                    }

                    ;

                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel != null && baseModel.isStatus()) {
                        }
                    }
                });
    }

    private void removeColelct() {
        ComonService.getInstance(context).removeCollect(
                AppContext.currentUser.getUserId(), data.getAppProductInfoId(),
                new CustomAsyncResponehandler() {
                    public void onFinish() {

                    }

                    ;

                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel != null && baseModel.isStatus()) {

                            if (CollectActivity.products != null) {
                                CollectActivity.products.remove(data);
                            }

                        }
                    }
                });
    }

    @Override
    public void lastLoad() {
        super.lastLoad();

    }

}
