package com.yns.app.ui;

import java.util.Calendar;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.text.InputType;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import com.yns.R;
import com.yns.app.AppContext;
import com.yns.model.ResponeModel;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.UserService;
import com.yns.util.DateUtil;
import com.yns.util.StringUtils;
import com.yns.util.UIHelper;
import com.yns.widget.HeaderBar;

/**
 * @className：GarageAddActivity.java
 * @author: lmt
 * @Function:添加我的车库
 * @createDate: 2014-12-9 下午2:49:30
 * @update:
 */
public class GarageAddActivity extends BaseActivity implements OnClickListener {
	private HeaderBar headerBar;
	private EditText editTextCarNum, editTextCar4SShop, editTextCarBrand,
			editTextCarBuyTime;

	public GarageAddActivity() {
		super(R.layout.activity_garageadd);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void initViews() {
		// TODO Auto-generated method stub
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle(getResString(R.string.garageadd_title));
		editTextCarNum = (EditText) findViewById(R.id.car_num);
		editTextCar4SShop = (EditText) findViewById(R.id.car_4Sshop);
		editTextCarBrand = (EditText) findViewById(R.id.car_brand);
		editTextCarBuyTime = (EditText) findViewById(R.id.car_buytime);
	}

	@Override
	public void initData() {
		// TODO Auto-generated method stub

	}

	@Override
	public void bindViews() {
		// TODO Auto-generated method stub
		findViewById(R.id.btn_addcar).setOnClickListener(this);
		editTextCarBuyTime.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					AlertDialog.Builder builder = new AlertDialog.Builder(
							GarageAddActivity.this);
					View view = View.inflate(GarageAddActivity.this,
							R.layout.date_time_dialog, null);
					final DatePicker datePicker = (DatePicker) view
							.findViewById(R.id.date_picker);
					final TimePicker timePicker = (android.widget.TimePicker) view
							.findViewById(R.id.time_picker);
					builder.setView(view);

					Calendar cal = Calendar.getInstance();
					cal.setTimeInMillis(System.currentTimeMillis());
					datePicker.init(cal.get(Calendar.YEAR),
							cal.get(Calendar.MONTH),
							cal.get(Calendar.DAY_OF_MONTH), null);

					timePicker.setIs24HourView(true);
					timePicker.setCurrentHour(cal.get(Calendar.HOUR_OF_DAY));
					timePicker.setCurrentMinute(Calendar.MINUTE);

					final int inType = editTextCarBuyTime.getInputType();
					editTextCarBuyTime.setInputType(InputType.TYPE_NULL);

					editTextCarBuyTime.setInputType(inType);
					editTextCarBuyTime.setSelection(editTextCarBuyTime
							.getText().length());

					builder.setTitle("选取起始时间");
					builder.setPositiveButton("确  定",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {

									StringBuffer sb = new StringBuffer();
									sb.append(String.format("%d-%02d-%02d",
											datePicker.getYear(),
											datePicker.getMonth() + 1,
											datePicker.getDayOfMonth()));
									sb.append("  ");
									/*sb.append(timePicker.getCurrentHour())
											.append(":")
											.append(timePicker
													.getCurrentMinute());*/

									editTextCarBuyTime.setText(sb);

									dialog.cancel();
								}
							});
					Dialog dialog = builder.create(); 
		            dialog.show();
				}
			
				return false;
			}
		});
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_addcar:
			if (checkInfo()) {
				UserService.getInstance(context).addGaragr(
						AppContext.currentUser.getUserId(),
						editTextCarBrand.getText().toString(),
						editTextCarNum.getText().toString(),
						editTextCar4SShop.getText().toString(),
						DateUtil.exchangeDate(editTextCarBuyTime.getText()
								.toString(), "yyyy-MM-dd", "yyyyMMddHHmmss"),
						new CustomAsyncResponehandler() {
							@Override
							public void onSuccess(ResponeModel baseModel) {
								// TODO Auto-generated method stub
								super.onSuccess(baseModel);
								if (baseModel != null && baseModel.isStatus()) {
									UIHelper.ShowMessage(context, "添加成功！");
									finish();
								}
							}
						});
			}
			break;
		}
	}

	private boolean checkInfo() {
		// TODO Auto-generated method stub
		if (StringUtils.isEmpty(editTextCarNum.getText().toString())) {
			UIHelper.ShowMessage(context, "请输入您的车牌");
			return false;
		}

		if (StringUtils.isEmpty(editTextCar4SShop.getText().toString())) {
			UIHelper.ShowMessage(context, "请输入您购买的4S店");
			return false;
		}

		if (StringUtils.isEmpty(editTextCarBrand.getText().toString())) {
			UIHelper.ShowMessage(context, "请输入您的汽车品牌");
			return false;
		}

		if (StringUtils.isEmpty(editTextCarBuyTime.getText().toString())) {
			UIHelper.ShowMessage(context, "请输入您的购买时间");
			return false;
		}

		return true;
	}

}
