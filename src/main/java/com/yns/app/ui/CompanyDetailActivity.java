package com.yns.app.ui;

import android.annotation.SuppressLint;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import com.yns.R;
import com.yns.app.AppContext;
import com.yns.model.Company;
import com.yns.model.ResponeModel;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.ComonService;
import com.yns.widget.HeaderBar;

@SuppressLint("ResourceAsColor")
public class CompanyDetailActivity extends BaseActivity {

	private HeaderBar bar;

	private Company company;
	private TextView tvCName, tvCType, tvCwtype;

	private WebView tvCDetail;
	private Button btnApply;

	public CompanyDetailActivity() {
		super(R.layout.activity_companydetail);

	}

	@Override
	public void initViews() {

		bar = (HeaderBar) findViewById(R.id.title);
		bar.setTitle("公司详情");
		tvCName = (TextView) findViewById(R.id.tv_company_name);

		tvCType = (TextView) findViewById(R.id.tv_company_type);
		tvCwtype = (TextView) findViewById(R.id.tv_company_wtype);

		tvCDetail = (WebView) findViewById(R.id.tv_company_detail);

		btnApply = (Button) findViewById(R.id.btn_apply);
	}

	@Override
	public void initData() {
		company = (Company) getIntent().getSerializableExtra("company");

	}

	@Override
	public void bindViews() {

		tvCName.setText(company.getDeptName());

		tvCType.setText(company.getCompanyType());
		tvCwtype.setText(company.getIndustry());

		tvCDetail.loadDataWithBaseURL(null, company.getDescribe(), "text/html",
				"utf-8", null);
		btnApply.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				removeColelct(company.getDeptId());
			}
		});
	}

	@Override
	public void lastLoad() {
		super.lastLoad();

	}

	private void removeColelct(String id) {
		ComonService.getInstance(context).removeCollect(
				AppContext.currentUser.getUserId(), id,
				new CustomAsyncResponehandler() {
					public void onFinish() {

					};

					@Override
					public void onSuccess(ResponeModel baseModel) {
						super.onSuccess(baseModel);
						if (baseModel != null && baseModel.isStatus()) {
							CompanyDetailActivity.this.finish();
						}
					}
				});
	}

}
