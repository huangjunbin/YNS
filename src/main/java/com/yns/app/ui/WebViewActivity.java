package com.yns.app.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.Settings;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebSettings.RenderPriority;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.yns.R;
import com.yns.app.AppContext;
import com.yns.model.Message;
import com.yns.model.ResponeModel;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.ComonService;
import com.yns.util.UMShare;
import com.yns.widget.HeaderBar;

/**
 * 应用程序主界面
 * 
 * @author 黃俊彬
 * @version 1.0
 * @created 2013-8-7
 */
@SuppressLint("SetJavaScriptEnabled")
public class WebViewActivity extends BaseActivity {

	private HeaderBar bar;

	public WebViewActivity() {
		super(R.layout.activity_webview);
	}

	/**
	 * 控件
	 * **/
	public static WebView webview; // 主网页加载控件

	/***
	 * 数据
	 */
	public boolean isRefresh = false; // 判断webview当前是否属于刷新

	private String url = "";

	private String str = "";
	private String title = "";
	private String fkId = "";
	/**
	 * article 为新闻 job 为工作 company 为公司
	 */
	private String type = "article";
	private String isFavorite = "0";

	/**
	 * @Name: InitControl
	 * @Description: 初始化控件
	 * @Author: 黄俊彬
	 * @Version: V1.00
	 * @Create 2013-8-7
	 * @Parameters: 无
	 * @Return: 无
	 */
	private void InitControl() {

		webview = (WebView) findViewById(R.id.webView);
		bar = (HeaderBar) findViewById(R.id.title);

	}

	/**
	 * @Name: InitData
	 * @Description: 初始化数据
	 * @Author: 黄俊彬
	 * @Version: V1.00
	 * @Create 2013-8-7
	 * @Parameters: 无
	 * @Return: 无
	 */
	private void InitData() {
		initWebView();

	}

	/**
	 * @Name: 綁定控件
	 * @Description: 初始化数据
	 * @Author: 黄俊彬
	 * @Version: V1.00
	 * @Create 2013-8-7
	 * @Parameters: 无
	 * @Return: 无
	 */
	private void BindControl() {
		if (fkId != null && !"".equals(fkId)) {
			if ("1".equals(isFavorite)) {
				bar.top_right_img.setImageResource(R.drawable.select_star);
			} else {
				bar.top_right_img.setImageResource(R.drawable.star);
			}
			bar.top_right_img.setVisibility(View.VISIBLE);
			bar.top_right_img2.setVisibility(View.VISIBLE);
			bar.top_right_img2.setImageResource(R.drawable.share);
		}
		bar.top_right_img.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (AppContext.currentUser == null) {
					JumpToActivity(LoginActivity.class, false);
					return;
				}
				if ("0".equals(isFavorite)) {
					addColelct();
				} else {
					removeColelct();
				}
			}
		});
		bar.top_right_img2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				UMShare.ShareUrl(WebViewActivity.this, url, null,
						R.drawable.ic_launcher).openShare(WebViewActivity.this,
						false);
			}
		});
	}

	/**
	 * @Name: initWebView
	 * @Description: 初始化webview控件属性
	 * @Author: 黄俊彬
	 * @Version: V1.00
	 * @Create 2013-8-7
	 * @Parameters: 无
	 * @Return: 无
	 */
	private void initWebView() {
		WebSettings settings = webview.getSettings();
		// 设置WebView属性，能够执行Javascript脚本
		settings.setJavaScriptEnabled(true);
		/*
		 * // 设置WebView不支持缩放 settings.setBuiltInZoomControls(false); //
		 * 设置WebView允许缓存 settings.setAppCacheEnabled(true); String cache_dir =
		 * this.getApplicationContext() .getDir("cache",
		 * Context.MODE_PRIVATE).getPath();
		 * settings.setAppCachePath(cache_dir);// 设置应用缓存的路径 // 设置缓存的模式
		 * 如果内容已经存在cache 则使用cache，即使是过去的历史记录。如果cache中不存在，从网络中获取
		 * settings.setCacheMode(WebSettings.LOAD_DEFAULT);
		 * settings.setAppCacheMaxSize(1024 * 1024 * 8);// 设置应用缓存的最大尺寸
		 * settings.setAllowFileAccess(true);// 可以读取文件缓存(manifest生效)
		 * settings.setRenderPriority(RenderPriority.HIGH);
		 * settings.setBlockNetworkLoads(true);
		 * settings.setJavaScriptCanOpenWindowsAutomatically(true);
		 */
		settings.setBuiltInZoomControls(true);
		settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
		settings.setUseWideViewPort(true);
		settings.setLoadWithOverviewMode(true);
		settings.setSavePassword(true);
		settings.setSaveFormData(true);
		settings.setJavaScriptEnabled(true);
		settings.setGeolocationEnabled(true);
		settings.setGeolocationDatabasePath("/data/data/com.gmall.app/databases/");
		settings.setDomStorageEnabled(true);
		webview.requestFocus();
		webview.setScrollBarStyle(0);
		// set.setUserAgentString(SmartConfig.getWebUserAgent(mContext));

		/**
		 * 使用户跳转到系统网络连接界面
		 * */

		webview.setWebViewClient(new WebViewClientDemo());
		if (url != null && url.length() > 0) {
			webview.loadUrl(url);
		}
		if (str != null && str.length() > 0) {
			try {

				str = str.replaceAll("&amp;", "");
				str = str.replaceAll("quot;", "\"");
				str = str.replaceAll("lt;", "<");
				str = str.replaceAll("gt;", ">");
				webview.loadDataWithBaseURL(null, str, "text/html", "utf-8",
						null);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		bar.setTitle(title);
	}

	/**
	 * WebViewClientDemo类 主要帮助WebView处理各种通知、请求事件
	 * 
	 * @author 黃俊彬
	 * @version 1.0
	 * @created 2013-8-7
	 */
	private class WebViewClientDemo extends WebViewClient {
		@Override
		// 在WebView中而不是默认浏览器中显示页面
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);
			isRefresh = false;
			view.getSettings().setBlockNetworkImage(false);
		}

		@Override
		public void onReceivedError(WebView view, int errorCode,
				String description, String failingUrl) {
			view.stopLoading();
			view.clearView();

		}

		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			super.onPageStarted(view, url, favicon);
			isRefresh = true;
			view.getSettings().setBlockNetworkImage(true);
		}

	}

	public void initViews() {
		InitControl();
	}

	public void initData() {
		url = getIntent().getStringExtra("url");
		str = getIntent().getStringExtra("str");
		title = getIntent().getStringExtra("title");
		fkId = getIntent().getStringExtra("fkId");
		type = getIntent().getStringExtra("type");
		isFavorite = getIntent().getStringExtra("isFavorite");
		InitData();
	}

	public void bindViews() {
		BindControl();
	}

	private void addColelct() {
		ComonService.getInstance(context).addCollect(
				AppContext.currentUser.getUserId(), fkId, type,
				new CustomAsyncResponehandler() {
					public void onFinish() {

					};

					@Override
					public void onSuccess(ResponeModel baseModel) {
						super.onSuccess(baseModel);
						if (baseModel != null && baseModel.isStatus()) {
							bar.top_right_img
									.setImageResource(R.drawable.select_star);
							if (MessageActivity.messages != null) {
								for (Message m : MessageActivity.messages) {
									if (m.getAppArticleInfoId().equals(fkId)) {
										m.setIsFavorite("1");
									}
								}
							}
							isFavorite = "1";
						}
					}
				});
	}

	private void removeColelct() {
		ComonService.getInstance(context).removeCollect(
				AppContext.currentUser.getUserId(), fkId,
				new CustomAsyncResponehandler() {
					public void onFinish() {

					};

					@Override
					public void onSuccess(ResponeModel baseModel) {
						super.onSuccess(baseModel);
						if (baseModel != null && baseModel.isStatus()) {
							bar.top_right_img.setImageResource(R.drawable.star);
							if (MessageActivity.messages != null) {
								for (Message m : MessageActivity.messages) {
									if (m.getAppArticleInfoId().equals(fkId)) {
										m.setIsFavorite("0");
									}
								}
							}
							isFavorite = "0";
						}
					}
				});
	}
}
