package com.yns.app.ui;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yns.R;
import com.yns.app.AppContext;
import com.yns.app.adapter.ServiceKindAdapter;
import com.yns.model.Advert;
import com.yns.model.HomeKind;
import com.yns.model.Message;
import com.yns.model.ResponeModel;
import com.yns.net.Urls;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.ComonService;
import com.yns.util.DateUtil;
import com.yns.widget.HeaderBar;
import com.yns.widget.SlideShowView;
import com.yns.widget.SlideShowView.OnBranchClickListener;
import com.yns.widget.XListView;
import com.yns.widget.XListView.IXListViewListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MessageDetailActivity extends BaseActivity implements
        IXListViewListener, OnBranchClickListener {

    public MessageDetailActivity() {
        super(R.layout.activity_service_list);
    }

    private String title = "";
    private int position = 0;
    private HeaderBar headerBar;
    private SlideShowView slideShowViewService;
    private XListView xListViewService;
    private ServiceKindAdapter serviceAdapter;
    private List<HomeKind> homeKinds;
    private LinearLayout lyFoot;
    private GridView xListViewService2;
    @Override
    public void initViews() {
        headerBar = (HeaderBar) findViewById(R.id.title);
        slideShowViewService = (SlideShowView) findViewById(R.id.slideshowview_service);
        xListViewService = (XListView) findViewById(R.id.service_list);
        xListViewService2= (GridView) findViewById(R.id.service_list2);
        xListViewService.setPullRefreshEnable(true);
        xListViewService.setPullLoadEnable(false);
        lyFoot = (LinearLayout) getLayoutInflater().inflate(
                R.layout.layout_foot, null);

        xListViewService.addFooterView(lyFoot);
    }

    private void initDiff() {
        title = getIntent().getStringExtra("title");
        position = getIntent().getIntExtra("position", 0);
        headerBar.setTitle(title);

    }

    @Override
    public void initData() {

        initDiff();

        homeKinds = new ArrayList<HomeKind>();

        if (position == 14) {
            HomeKind ks1 = new HomeKind(R.drawable.icon_a1, R.string.new_15);
            ks1.setType_id(15);
            HomeKind ks2 = new HomeKind(R.drawable.icon_a2, R.string.new_16);
            ks2.setType_id(16);
            HomeKind ks3 = new HomeKind(R.drawable.icon_a3, R.string.new_17);
            ks3.setType_id(17);
            HomeKind ks4 = new HomeKind(R.drawable.icon_a4, R.string.new_10);
            ks4.setType_id(10);
            homeKinds.add(ks1);
            homeKinds.add(ks2);
            homeKinds.add(ks3);
            homeKinds.add(ks4);
        }
        if (position == 19) {
            HomeKind ks1 = new HomeKind(R.drawable.icon_a5, R.string.new_20);
            ks1.setType_id(20);
            HomeKind ks2 = new HomeKind(R.drawable.icon_a6, R.string.new_21);
            ks2.setType_id(21);
            HomeKind ks3 = new HomeKind(R.drawable.icon_a7, R.string.new_120);
            ks3.setType_id(120);
            homeKinds.add(ks1);
            homeKinds.add(ks2);
            homeKinds.add(ks3);

        }
        if (position == 26) {
            HomeKind ks1 = new HomeKind(R.drawable.icon_a8, R.string.new_27);
            ks1.setType_id(27);
            HomeKind ks2 = new HomeKind(R.drawable.icon_a9, R.string.new_28);
            ks2.setType_id(28);
            HomeKind ks3 = new HomeKind(R.drawable.icon_a10, R.string.new_29);
            ks3.setType_id(29);
            homeKinds.add(ks1);
            homeKinds.add(ks2);
            homeKinds.add(ks3);

        }
        if (position == 22) {
            HomeKind ks1 = new HomeKind(R.drawable.icon_a14, R.string.new_23);
            ks1.setType_id(23);
            HomeKind ks2 = new HomeKind(R.drawable.icon_a15, R.string.new_24);
            ks2.setType_id(24);
            HomeKind ks3 = new HomeKind(R.drawable.icon_a16, R.string.new_25);
            ks3.setType_id(25);
            homeKinds.add(ks1);
            homeKinds.add(ks2);
            homeKinds.add(ks3);

        }
        xListViewService2.setNumColumns(homeKinds.size());
        serviceAdapter = new ServiceKindAdapter(this, homeKinds);
        xListViewService2.setAdapter(serviceAdapter);
        xListViewService.setAdapter( new ServiceKindAdapter(this, new  ArrayList<HomeKind>()));
        xListViewService.setPullRefreshEnable(false);
        xListViewService.setPullLoadEnable(false);
        getBanner();
    }

    @Override
    public void bindViews() {
        xListViewService.setXListViewListener(this);
        xListViewService2.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                if (position <= homeKinds.size()) {
                    final HomeKind msg = homeKinds.get(position);
                    Intent intent = new Intent();
                    intent.setClass(MessageDetailActivity.this,
                            MessageActivity.class);
                    intent.putExtra("position", msg.getType_id());
                    startActivity(intent);
                }

            }
        });
        onRefresh();
    }

    @Override
    public void onRefresh() {
        getMessageList();
        xListViewService.setRefreshTime(DateUtil.getDateTime(new Date(System
                .currentTimeMillis())));

    }

    @Override
    public void onLoadMore() {

    }

    @Override
    public void OnBranchClick(int position) {

    }

    private void getMessageList() {
        ComonService.getInstance(context).getMessageList(5 + "", 1 + "",
                position + "", "", new CustomAsyncResponehandler() {
                    public void onFinish() {

                    }

                    ;

                    @SuppressWarnings("unchecked")
                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel != null && baseModel.isStatus()) {
                            List<Message> messageList = (List<Message>) baseModel
                                    .getResultObj();
                            if (messageList != null && messageList.size() > 0) {
                                for (final Message message : messageList) {
                                    View v = getLayoutInflater().inflate(
                                            R.layout.item_message, null);
                                    TextView tvContent = (TextView) v
                                            .findViewById(R.id.message_content);
                                    TextView tvTime = (TextView) v
                                            .findViewById(R.id.message_time);
                                    ImageView ivPhoto = (ImageView) v
                                            .findViewById(R.id.iv_photo);
                                    TextView tvTitle = (TextView) v
                                            .findViewById(R.id.message_title);
                                    tvTitle.setText(message.getTitle());
                                    tvContent.setText(message.getDescription());
                                    tvTime.setText(message.getCreateDate());
                                    AppContext.setImage(
                                            message.getFrontCoverPic(),
                                            ivPhoto, AppContext.imageOption);
                                    v.setOnClickListener(new OnClickListener() {

                                        @Override
                                        public void onClick(View v) {
                                            Intent intent = new Intent();
                                            intent.setClass(
                                                    MessageDetailActivity.this,
                                                    WebViewActivity.class);
                                            intent.putExtra(
                                                    "url",
                                                    "http://www.dxsapp.com/front/app_article_info.html?id="
                                                            + message
                                                            .getAppArticleInfoId());
                                            intent.putExtra("title", "详细内容");
                                            intent.putExtra("fkId", message
                                                    .getAppArticleInfoId());
                                            intent.putExtra("type", "article");
                                            intent.putExtra("isFavorite",
                                                    message.getIsFavorite());
                                            startActivity(intent);
                                        }
                                    });
                                    lyFoot.addView(v);
                                }

                            }
                        }
                    }
                });
    }

    private void getBanner() {
        ComonService.getInstance(context).getBanner(10 + "", 1 + "", "index",
                new CustomAsyncResponehandler() {
                    public void onFinish() {

                    }

                    ;

                    @SuppressWarnings("unchecked")
                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel != null && baseModel.isStatus()) {
                            List<Advert> lists = (List<Advert>) baseModel
                                    .getResultObj();
                            if (lists != null && lists.size() > 0) {
                                List<String> strList = new ArrayList<String>();
                                for (Advert item : lists) {
                                    strList.add(Urls.IMAGE_URL + item.getPic());
                                }
                                slideShowViewService.setImageUris(strList);
                            }
                        }
                    }
                });
    }
}
