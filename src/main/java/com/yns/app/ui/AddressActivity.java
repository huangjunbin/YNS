package com.yns.app.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;

import com.yns.R;
import com.yns.app.AppContext;
import com.yns.app.adapter.AddressAdapter;
import com.yns.model.Address;
import com.yns.model.OrderInfo;
import com.yns.model.ResponeModel;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.GoodService;
import com.yns.util.JsonUtil;
import com.yns.widget.HeaderBar;

import java.util.ArrayList;
import java.util.List;

public class AddressActivity extends BaseActivity {
    public AddressActivity() {
        super(R.layout.activity_address);
    }

    ListView lvAddress;
    AddressAdapter adapter;
    private List<Address> addressList;
    private HeaderBar bar;
    private String title = "";
    private String str;
    private String appProductInfoId = "";
    private String buyCount = "";

    @Override
    public void initViews() {
        lvAddress = (ListView) findViewById(R.id.address_lv);
        bar = (HeaderBar) findViewById(R.id.header);
    }



    @SuppressLint("InflateParams")
    @Override
    public void initData() {
        title = getIntent().getStringExtra("title");
        str = getIntent().getStringExtra("data");
        appProductInfoId = getIntent().getStringExtra("appProductInfoId");
        buyCount = getIntent().getStringExtra("buyCount");
        addressList = new ArrayList<Address>();
        adapter = new AddressAdapter(this, addressList);
        adapter.isEdit = true;
        View v = findViewById(R.id.ly_add);
        v.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AddressActivity.this,
                        AddAddressActivity.class));
            }
        });
        lvAddress.setAdapter(adapter);
    }

    @Override
    public void bindViews() {
        if (str == null && appProductInfoId == null) {
            bar.setTitle("地址管理");
        } else {
            bar.setTitle("选择收货地址");
            adapter.isEdit = false;
            adapter.notifyDataSetChanged();
            ;
            bar.top_right_btn.setText("确认");
            bar.top_right_btn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (adapter.getSelectAddress() == null) {
                        showToast("请选择收货地址");
                        return;
                    }
                    if (str != null) {
                        GoodService.getInstance(context).addOrder(adapter.getSelectAddress().getConsigneeInfoId(),
                                AppContext.currentUser.getUserId(), str, new CustomAsyncResponehandler() {
                                    public void onFinish() {
                                    }

                                    @SuppressWarnings("unchecked")
                                    @Override
                                    public void onSuccess(ResponeModel baseModel) {
                                        super.onSuccess(baseModel);
                                        if (baseModel != null && baseModel.isStatus()) {
                                            OrderInfo orderInfo = (OrderInfo) JsonUtil.convertJsonToObject(baseModel.getResult(), OrderInfo.class);
                                            Intent i = new Intent();
                                            i.putExtra("order", orderInfo);
                                            i.putExtra("type", "1");
                                            i.setClass(AddressActivity.this, OrderDetailActivity.class);
                                            startActivity(i);
                                        }
                                    }
                                });
                    } else {
                        GoodService.getInstance(context).buy(adapter.getSelectAddress().getConsigneeInfoId(),
                                AppContext.currentUser.getUserId(), appProductInfoId, buyCount, new CustomAsyncResponehandler() {
                                    public void onFinish() {
                                    }

                                    @SuppressWarnings("unchecked")
                                    @Override
                                    public void onSuccess(ResponeModel baseModel) {
                                        super.onSuccess(baseModel);
                                        if (baseModel != null && baseModel.isStatus()) {
                                            OrderInfo orderInfo = (OrderInfo) JsonUtil.convertJsonToObject(baseModel.getResult(), OrderInfo.class);
                                            Intent i = new Intent();
                                            i.putExtra("order", orderInfo);
                                            i.putExtra("type", "1");
                                            i.setClass(AddressActivity.this, OrderDetailActivity.class);
                                            startActivity(i);
                                        }
                                    }
                                });
                    }
                }
            });
        }
    }

    @Override
    protected void onResume() {
        getDataList();
        super.onResume();
    }

    public void onPause() {
        super.onPause();
    }

    private void getDataList() {
        GoodService.getInstance(context).getAddressList(
                AppContext.currentUser.getUserId(), new CustomAsyncResponehandler() {
                    public void onFinish() {
                    }

                    @SuppressWarnings("unchecked")
                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel != null && baseModel.isStatus()) {
                            List<Address> dataList = (List<Address>) baseModel
                                    .getResultObj();
                            if (dataList != null && dataList.size() > 0) {
                                addressList.clear();
                                addressList.addAll(0, dataList);
                                adapter.notifyDataSetChanged();
                            }
                        }
                    }
                });
    }

}