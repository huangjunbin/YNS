package com.yns.app.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.view.View;
import android.view.View.OnClickListener;

import com.yns.R;
import com.yns.app.AppContext;
import com.yns.app.adapter.GaragesAdapter;
import com.yns.global.GlobalConstant;
import com.yns.model.Garage;
import com.yns.model.ResponeModel;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.UserService;
import com.yns.util.DateUtil;
import com.yns.util.LogUtil;
import com.yns.widget.HeaderBar;
import com.yns.widget.XListView;
import com.yns.widget.XListView.IXListViewListener;

/**
 * @className：GarageActivity.java
 * @author: lmt
 * @Function: 我的车库
 * @createDate: 2014-12-9 下午2:00:42
 * @update:
 */
public class GarageActivity extends BaseActivity implements IXListViewListener {
	private String TAG = "GarageActivity";
	private boolean isLog = true;
	// ----
	private boolean isRefresh;
	private int pageNum = 0, pageSize = GlobalConstant.DEFAULT_PAGE_SIZE;
	// -----------
	private HeaderBar headerBar;
	private XListView xListView;
	private GaragesAdapter garagesAdapter;
	private List<Garage> listGarages;

	public GarageActivity() {
		super(R.layout.activity_garage);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void initViews() {
		// TODO Auto-generated method stub
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle(getResString(R.string.garage_title));
		headerBar.top_right_btn.setText(getResString(R.string.add));
		xListView = (XListView) findViewById(R.id.carlist);
		xListView.setPullLoadEnable(false);
		xListView.setPullRefreshEnable(true);
	}

	@Override
	public void initData() {
		// TODO Auto-generated method stub
		listGarages = new ArrayList<Garage>();

		garagesAdapter = new GaragesAdapter(GarageActivity.this, listGarages);
		xListView.setAdapter(garagesAdapter);
	}

	@Override
	public void bindViews() {
		// TODO Auto-generated method stub
		headerBar.top_right_btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				JumpToActivity(GarageAddActivity.class, false);
			}
		});

		xListView.setXListViewListener(this);
	}

	@Override
	public void lastLoad() {
		// TODO Auto-generated method stub
		super.lastLoad();
		onRefresh();
	}

	private void smartGetGradeCars() {
		// TODO Auto-generated method stub
		UserService.getInstance(context).getGradeCars(
				AppContext.currentUser.getUserId(), pageNum + "",
				pageSize + "", new CustomAsyncResponehandler() {

					@Override
					public void onFinish() {
						// TODO Auto-generated method stub
						super.onFinish();
						xListView.setEmptyView(findViewById(R.id.empty));
						xListView.stopLoadMore();
						xListView.stopRefresh();

					}

					@SuppressWarnings("unchecked")
					@Override
					public void onSuccess(ResponeModel baseModel) {
						// TODO Auto-generated method stub
						super.onSuccess(baseModel);
						if (baseModel != null && baseModel.isStatus()) {

							List<Garage> merchantsGoods = (List<Garage>) baseModel
									.getResultObj();
							if (merchantsGoods != null
									&& merchantsGoods.size() > 0) {
								if (isRefresh) {
									listGarages.clear();
									//Audio.playAudio(Audio.AUDIO_DENGDENGDENG);
								}

								listGarages.addAll(merchantsGoods);

								pageSize = listGarages.size();

								if (listGarages.size() >= baseModel
										.getTotalCount()) {
									xListView.setPullLoadEnable(false);
								} else {
									xListView.setPullLoadEnable(true);
								}

								LogUtil.d(TAG, baseModel.getTotalCount()
										+ "=======", isLog);

								garagesAdapter.notifyDataSetChanged();

							} else {
								listGarages = new ArrayList<Garage>();

								garagesAdapter = new GaragesAdapter(GarageActivity.this, listGarages);
								xListView.setAdapter(garagesAdapter);
							}
						}

					}
				});
	}

	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		xListView.setRefreshTime(DateUtil.getDateTime(new Date(System
				.currentTimeMillis())));
		isRefresh = true;
		pageNum = 0;
		pageSize = GlobalConstant.DEFAULT_PAGE_SIZE;
		smartGetGradeCars();
	}

	@Override
	public void onLoadMore() {
		// TODO Auto-generated method stub
		isRefresh = false;
		pageNum = pageSize;
		pageSize += GlobalConstant.DEFAULT_PAGE_SIZE;
		smartGetGradeCars();
	}

}
