package com.yns.app.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.yns.R;
import com.yns.model.Market;
import com.yns.net.Urls;
import com.yns.widget.HeaderBar;
import com.yns.widget.SlideShowView;

import java.util.ArrayList;
import java.util.List;

@SuppressLint("InflateParams")

public class MarketDetailActivity extends BaseActivity {

    private TextView textViewMessageContent, tvTitle, textViewMessageTime, tvSalePrice;
    private SlideShowView ivPhoto;
    private Button btnAdd;
    private Market data;
    private HeaderBar bar;
    private TextView wvDetail;

    public MarketDetailActivity() {
        super(R.layout.activity_market_detail);
    }

    @Override
    public void initViews() {
        textViewMessageContent = (TextView) findViewById(R.id.message_content);
        textViewMessageTime = (TextView) findViewById(R.id.message_time);
        tvTitle = (TextView) findViewById(R.id.message_title);
        ivPhoto = (SlideShowView) findViewById(R.id.slideshowview);
        tvSalePrice = (TextView) findViewById(R.id.tv_saleprice);
        btnAdd = (Button) findViewById(R.id.btn_add);
        bar = (HeaderBar) findViewById(R.id.title);
        wvDetail = (TextView) findViewById(R.id.wv_detail);
    }

    @Override
    public void initData() {
        data = (Market) getIntent().getSerializableExtra("data");
    }

    @Override
    public void bindViews() {
        bar.setTitle("物品详情");
        tvTitle.setText(data.getTitle());
        textViewMessageContent.setText("");
        textViewMessageTime.setText(data.getCreateDate());
        tvSalePrice.setText("￥" + data.getPrice() + "");
        textViewMessageContent.setText(data.getSchoolName());
        List<String> dataList = new ArrayList<String>();
        String[] strList = data.getPic().split(",");
        if (strList != null) {
            for (int i = 0; i < strList.length; i++) {
                dataList.add(Urls.ONLINE_IMAGE_URL+strList[i]);
            }
        }
        ivPhoto.setImageUris(dataList);

        wvDetail.setText(data.getDetails());


        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String number = data.getMobile();
                //用intent启动拨打电话
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + number));
                startActivity(intent);
            }
        });
        ivPhoto.myCallBack= new SlideShowView.OnBranchClickListener() {
            @Override
            public void OnBranchClick(int position) {
                Intent intent=new Intent(MarketDetailActivity.this,BigImageActivity.class);
                intent.putExtra("url",data.getPic());
                startActivity(intent);
            }
        };
    }

    @Override
    public void lastLoad() {
        super.lastLoad();

    }

}
