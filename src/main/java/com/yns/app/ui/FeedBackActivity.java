package com.yns.app.ui;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

import com.yns.R;
import com.yns.app.AppContext;
import com.yns.model.ResponeModel;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.UserService;
import com.yns.util.StringUtils;
import com.yns.util.UIHelper;
import com.yns.widget.HeaderBar;

/**
 * @className：FeedBackActivity.java
 * @author: lmt
 * @Function: 意见反馈
 * @createDate: 2014-12-9 下午6:13:45
 * @update:
 */
public class FeedBackActivity extends BaseActivity {
	private HeaderBar headerBar;
	private EditText editTextContent, editTextTel;

	public FeedBackActivity() {
		super(R.layout.activity_feedback);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void initViews() {
		// TODO Auto-generated method stub
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle(getResString(R.string.feedback_title));
		headerBar.top_right_btn.setText(getResString(R.string.commit));

		editTextContent = (EditText) findViewById(R.id.feedback_content);
		editTextTel = (EditText) findViewById(R.id.feedback_tel);
	}

	@Override
	public void initData() {
		// TODO Auto-generated method stub

	}

	@Override
	public void bindViews() {
		// TODO Auto-generated method stub
		headerBar.top_right_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (checkInput()) {
					UserService.getInstance(context).commitOpinions(
							AppContext.currentUser.getUserId(),
							editTextContent.getText().toString(),
							editTextTel.getText().toString(),
							new CustomAsyncResponehandler() {
								@Override
								public void onSuccess(ResponeModel baseModel) {
									// TODO Auto-generated method stub
									super.onSuccess(baseModel);
									if (baseModel != null
											&& baseModel.isStatus()) {
										UIHelper.ShowMessage(context, "提交成功！");
										finish();
									}
								}
							});
				}
			}
		});
	}

	private boolean checkInput() {
		if (StringUtils.isEmpty(editTextContent.getText().toString())) {
			UIHelper.ShowMessage(context, "请先输入您的意见！");
			return false;
		}
		if (StringUtils.isEmpty(editTextTel.getText().toString())) {
			UIHelper.ShowMessage(context, "请输入您的电话号码！");
			return false;
		}

		if (!StringUtils.isPhoneNumberValid2(editTextTel.getText().toString())) {
			UIHelper.ShowMessage(context, "手机号码不合法！");
			return false;
		}

		return true;
	}
}
