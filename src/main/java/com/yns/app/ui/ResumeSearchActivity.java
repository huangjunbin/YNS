package com.yns.app.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.yns.R;
import com.yns.app.AppManager;
import com.yns.model.Resume;
import com.yns.widget.HeaderBar;

public class ResumeSearchActivity extends BaseActivity {

    private HeaderBar headerBar;
    private EditText etName, etYuanxiao, etMingzu,
            etShengao;
    private RadioButton cbGril, cbBoy, cbNo;
    int xueli = -1;
    int nianxian = -1;
    private Resume resume;
    private Button btnSearch;
    private TextView etXueli, etJinyan;

    public ResumeSearchActivity() {
        super(R.layout.activity_resumesearch);
    }

    @Override
    public void initViews() {

        headerBar = (HeaderBar) findViewById(R.id.title);
        etName = (EditText) findViewById(R.id.et_name);
        etYuanxiao = (EditText) findViewById(R.id.et_yuanxiao);
        etMingzu = (EditText) findViewById(R.id.et_mingzu);
        etShengao = (EditText) findViewById(R.id.et_shengao);
        cbGril = (RadioButton) findViewById(R.id.cb_gril);
        cbBoy = (RadioButton) findViewById(R.id.cb_boy);
        cbNo = (RadioButton) findViewById(R.id.cb_no);
        etXueli = (TextView) findViewById(R.id.et_xueli);
        etJinyan = (TextView) findViewById(R.id.et_jinyan);
        btnSearch = (Button) findViewById(R.id.btn_search);
    }

    @Override
    public void initData() {

    }

    @Override
    public void bindViews() {
        headerBar.setTitle("简历搜索");
        btnSearch.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                //nickName, biYeXueXiao, xueLi, gongZuoNainXian, shenGao, sex, mingZu
                Intent intent=new Intent(ResumeSearchActivity.this,ResumeActivity.class);
                intent.putExtra("nickName",etName.getText().toString());
                intent.putExtra("biYeXueXiao",etYuanxiao.getText().toString());
                intent.putExtra("xueLi",xueli+"");
                intent.putExtra("gongZuoNainXian",nianxian+"");
                intent.putExtra("shenGao",etShengao.getText().toString());
                if (cbBoy.isChecked()) {
                    intent.putExtra("sex","1");
                } else if (cbGril.isChecked()) {
                    intent.putExtra("sex","0");
                }
                intent.putExtra("mingZu",etMingzu.getText().toString());
                intent.putExtra("type",1);
                startActivity(intent);
                AppManager.getAppManager().finishActivity(ResumeActivity.class);
                ResumeSearchActivity.this.finish();
            }
        });

        etXueli.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                final CharSequence[] item = {"初中", "高中", "中技", "中专", "大专",
                        "本科", "硕士", "博士", "博后"};
                new AlertDialog.Builder(ResumeSearchActivity.this)
                        .setTitle("选择学历")
                        .setItems(item, new DialogInterface.OnClickListener() { // content
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                etXueli.setText(item[which]);
                                xueli = 72 + which;
                            }

                        })
                        .setNegativeButton("取消",
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which) {

                                        dialog.dismiss(); // 关闭alertDialog
                                    }
                                }).show();

            }
        });

        etJinyan.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                final CharSequence[] item = {"不限", "应届生", "1-3年", "3-5年",
                        "5-10年", "10年以上", "20年以上", "30年以上", "退休人员"};
                new AlertDialog.Builder(ResumeSearchActivity.this)
                        .setTitle("选择薪资")
                        .setItems(item, new DialogInterface.OnClickListener() { // content
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                etJinyan.setText(item[which]);
                                nianxian = 82 + which;
                            }

                        })
                        .setNegativeButton("取消",
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which) {

                                        dialog.dismiss(); // 关闭alertDialog
                                    }
                                }).show();
            }
        });
    }
}
