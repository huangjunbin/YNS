package com.yns.app.ui;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.yns.R;
import com.yns.app.AppContext;
import com.yns.model.Job;
import com.yns.model.ResponeModel;
import com.yns.model.TradeType;
import com.yns.model.TradeType.ChildType;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.ComonService;
import com.yns.util.StringUtils;
import com.yns.util.UIHelper;
import com.yns.widget.HeaderBar;

import java.io.Serializable;
import java.util.List;

public class JobAddActivity extends BaseActivity {

	private HeaderBar headerBar;

	private EditText etName, etNum, etRemark;
	private TextView tvType;
	private int flag = 0;
	private Job job;
	String jobSortId;
	String jobPostId;
	private String f = null;
	String gongZuoJingYanSortId = "82";
	String xinZiDaiYuSortId = "92";
	String xueLiYaoQiuSortId = "336";
	String workType = "全职";
	private RadioButton rbY,rbN;

	public JobAddActivity() {
		super(R.layout.activity_jobadd);
	}

	@Override
	public void initViews() {
		headerBar = (HeaderBar) findViewById(R.id.title);
		etName = (EditText) findViewById(R.id.et_name);
		etNum = (EditText) findViewById(R.id.et_num);
		etRemark = (EditText) findViewById(R.id.et_remark);
		tvType = (TextView) findViewById(R.id.tv_type);
		rbY= (RadioButton) findViewById(R.id.cb_y);
		rbN= (RadioButton) findViewById(R.id.cb_n);
	}

	@Override
	public void initData() {
		flag = getIntent().getIntExtra("flag", 0);
		job = (Job) getIntent().getSerializableExtra("job");
	}

	@Override
	public void bindViews() {
		if (job == null) {
			headerBar.setTitle("添加职位");
			headerBar.top_right_btn.setText("发布");
		} else {
			headerBar.setTitle("修改职位");
			headerBar.top_right_btn.setText("修改");
			etName.setText(job.getTitle());
			etNum.setText(job.getZhaoPingRenShu());
			etRemark.setText(job.getDescription());
			jobSortId = job.getJobSortId();
			jobPostId = job.getJobPostId();
			f = job.getFlag();
			gongZuoJingYanSortId = job.getGongZuoJingYanSortId();
			xinZiDaiYuSortId = job.getXinZiDaiYuSortId();
			xueLiYaoQiuSortId = job.getXinZiDaiYuSortId();
			workType = job.getWorktype();
			tvType.setText(job.getJobSort());
		}
		headerBar.top_right_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (StringUtils.isEmpty(etName.getText().toString())) {
					showToast("职位名称不能为空");
					return;
				}
				if (StringUtils.isEmpty(etNum.getText().toString())) {
					showToast("招聘人数不能为空");
					return;
				}
				if (StringUtils.isEmpty(etRemark.getText().toString())) {
					showToast("职位描述不能为空");
					return;
				}
				if (jobSortId == null || jobPostId == null) {
					showToast("请选择职位类别");
					return;
				}
				if(rbY.isChecked()){
					workType="全职";
				}else{
					workType="兼职";
				}
				ComonService.getInstance(context).editJob(
						job == null ? null : job.getAppJobInfoId(),
						AppContext.currentUser == null ? ""
								: AppContext.currentUser.getUserId(),
						AppContext.currentUser.getDeptId() + "",
						etNum.getText().toString(),
						etRemark.getText().toString(),
						etName.getText().toString(), flag, jobSortId,
						jobPostId, gongZuoJingYanSortId, xinZiDaiYuSortId,
						xueLiYaoQiuSortId, workType, f,

						new CustomAsyncResponehandler() {
							public void onFinish() {

							};

							@Override
							public void onSuccess(ResponeModel baseModel) {
								super.onSuccess(baseModel);
								if (baseModel != null && baseModel.isStatus()) {
									UIHelper.ShowMessage(JobAddActivity.this,
											"保存成功");
									JobAddActivity.this.finish();
								}
							}
						});
			}
		});
		tvType.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ComonService.getInstance(context).getTradeType(
						new CustomAsyncResponehandler() {
							public void onFinish() {

							};

							@SuppressWarnings("unchecked")
							@Override
							public void onSuccess(ResponeModel baseModel) {
								super.onSuccess(baseModel);
								if (baseModel != null && baseModel.isStatus()) {
									List<TradeType> tradeTypeList = (List<TradeType>) baseModel
											.getResultObj();
									if (tradeTypeList != null
											&& tradeTypeList.size() > 0) {

										Intent intent = new Intent();
										intent.putExtra("title", "行业类别");
										intent.putExtra("data",
												(Serializable) tradeTypeList);
										intent.setClass(JobAddActivity.this,
												JobType2Activity.class);
										startActivityForResult(intent, 2000);
									}
								}
							}
						});
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == 2000) {
			if (data != null) {
				ChildType t2 = (ChildType) data.getSerializableExtra("data");
				tvType.setText(t2.getName());
				jobPostId = t2.getSonSortId();
				TradeType tt = (TradeType) data.getSerializableExtra("data2");
				jobSortId = tt.getSortId();
			} else {
				tvType.setText("");
			}
		}

		super.onActivityResult(requestCode, resultCode, data);
	}
}
