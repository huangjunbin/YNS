package com.yns.app.ui;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.TextView;

import com.yns.R;
import com.yns.model.TradeType;
import com.yns.widget.HeaderBar;
import com.yns.widget.lib.PullToRefreshBase.OnRefreshListener;
import com.yns.widget.lib.PullToRefreshExpandableListView;

import java.util.List;

public class JobType2Activity extends BaseActivity {
    public JobType2Activity() {
        super(R.layout.activity_jobtype2);
    }

    private PullToRefreshExpandableListView expandableListView;
    private ExpandableListView mView;
    private List<TradeType> groupInfoList;
    private MyExpListAdapter adapter;
    private HeaderBar bar;
    private int tag = 0;
    private int flag=-1;

    public void initViews() {
        expandableListView = (PullToRefreshExpandableListView) findViewById(R.id.expandableListView);

        bar = (HeaderBar) findViewById(R.id.header);

    }

    @SuppressWarnings("unchecked")
    public void initData() {

        groupInfoList = (List<TradeType>) getIntent().getSerializableExtra(
                "data");
        tag = getIntent().getIntExtra("tag", 0);
        flag=getIntent().getIntExtra("flag",0);
    }

    @SuppressWarnings("deprecation")
    public void bindViews() {

        mView = expandableListView.getAdapterView();

        expandableListView.setPullToRefreshEnabled(false);
        expandableListView.setOnUpPullRefreshListener(new OnRefreshListener() {

            @Override
            public void onRefresh() {

            }
        });

        mView.setOnGroupExpandListener(new OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {

            }
        });
        mView.setOnChildClickListener(new OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {

                if (tag == 0) {
                    Intent data = new Intent();
                    data.putExtra("data", groupInfoList.get(groupPosition)
                            .getSort().get(childPosition));
                    data.putExtra("data2", groupInfoList.get(groupPosition));
                    setResult(JobSearchActivity.code2, data);
                    JobType2Activity.this.finish();
                } else {
                    Intent i = new Intent();
                    i.setClass(JobType2Activity.this, JobListActivity.class);
                    i.putExtra("jobPortSort",groupInfoList.get(groupPosition)
                            .getSort().get(childPosition).getSonSortId());
                    i.putExtra("flag",flag);
                    startActivity(i);
                }
                return false;
            }
        });

        bar.setTitle("职位类别");
        adapter = new MyExpListAdapter(JobType2Activity.this);
        mView.setAdapter(adapter);
    }

    public class MyExpListAdapter extends BaseExpandableListAdapter {
        private Context context;
        ViewHolder holder;

        public MyExpListAdapter(Context context) {
            this.context = context;
        }

        @Override
        public int getGroupCount() {

            return groupInfoList == null ? 0 : groupInfoList.size();
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return groupInfoList.get(groupPosition) == null ? 0 : groupInfoList
                    .get(groupPosition).getSort() == null ? 0 : groupInfoList
                    .get(groupPosition).getSort().size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return groupInfoList.get(groupPosition);
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return groupInfoList.get(groupPosition).getSort()
                    .get(childPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
        }

        @Override
        public View getGroupView(final int groupPosition,
                                 final boolean isExpanded, View convertView, ViewGroup parent) {
            View v = convertView;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = inflater.inflate(R.layout.group_item, null);
            }
            TextView groupText = (TextView) v.findViewById(R.id.groupText);

            final String gname = groupInfoList.get(groupPosition).getName();
            groupText.setText(gname);

            return v;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition,
                                 boolean isLastChild, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.child_item, null);
                holder = new ViewHolder();
                holder.cText = (TextView) convertView
                        .findViewById(R.id.childText);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.cText.setText(groupInfoList.get(groupPosition).getSort()
                    .get(childPosition).getName());

            return convertView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

    }

    private static class ViewHolder {
        TextView cText;

    }

}