package com.yns.app.ui;

import android.graphics.Color;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yns.R;
import com.yns.app.adapter.ProductListAdapter;
import com.yns.model.Product;
import com.yns.model.ProductType;
import com.yns.model.ResponeModel;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.GoodService;
import com.yns.widget.HeaderBar;
import com.yns.widget.multicolumn.MultiColumnListView;
import com.yns.widget.multicolumn.MulticolumPullToRefreshView;
import com.yns.widget.multicolumn.PLA_AdapterView;

import java.util.ArrayList;
import java.util.List;

public class ProductListActivity extends BaseActivity   {
	private HeaderBar headerBar;
	private MulticolumPullToRefreshView integral_refresh_view;
	private MultiColumnListView listview_integral;
	private ProductListAdapter adapter;
	public static List<Product> dataList;
	private int page = 1;
	private int rows = 5;
	private boolean isRefresh = true;
	private int position = 0;
	private LinearLayout llMore;
	private String sortId="";
	private String flag="groupBuying";
	private String title="";
	public ProductListActivity() {
		super(R.layout.activity_productlist);
	}

	@Override
	public void initViews() {
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle("商品列表");
		integral_refresh_view = (MulticolumPullToRefreshView) findViewById(R.id.pull_integral_view);
		listview_integral = (MultiColumnListView) findViewById(R.id.listview_integral);
		llMore= (LinearLayout) findViewById(R.id.ll_more);
	}

	@Override
	public void initData() {
		position = getIntent().getIntExtra("position", 0);
		flag=getIntent().getStringExtra("flag");
		title=getIntent().getStringExtra("title");
		headerBar.setTitle(title);
		dataList = new ArrayList<Product>();
		adapter = new ProductListAdapter(ProductListActivity.this, dataList);
		listview_integral.setAdapter(adapter);
		isRefresh = true;
		page = 1;
		getTypeList();
	}

	@Override
	public void bindViews() {
		integral_refresh_view.onFooterRefreshComplete();
		listview_integral
				.setOnItemClickListener(new PLA_AdapterView.OnItemClickListener() {

					@Override
					public void onItemClick(PLA_AdapterView<?> parent,
											View view, int position, long id) {

					}
				});
		integral_refresh_view
				.setOnHeaderRefreshListener(new MulticolumPullToRefreshView.OnHeaderRefreshListener() {

					@Override
					public void onHeaderRefresh(MulticolumPullToRefreshView view) {
						isRefresh = true;
						page = 1;
						getTypeList();
						getMessageList();
						integral_refresh_view.onHeaderRefreshComplete();
					}
				});
		integral_refresh_view
				.setOnFooterRefreshListener(new MulticolumPullToRefreshView.OnFooterRefreshListener() {

					@Override
					public void onFooterRefresh(MulticolumPullToRefreshView view) {
						isRefresh = false;
						page++;
						getMessageList();
						integral_refresh_view.onFooterRefreshComplete();
					}
				});
	}
	private void getMessageList() {
		GoodService.getInstance(context).getProductList(rows, page,
				sortId,flag, new CustomAsyncResponehandler() {
					public void onFinish() {

					}

					;

					@SuppressWarnings("unchecked")
					@Override
					public void onSuccess(ResponeModel baseModel) {
						super.onSuccess(baseModel);
						if (baseModel != null && baseModel.isStatus()) {
							List<Product> productList = (List<Product>) baseModel
									.getResultObj();
							if (productList != null && productList.size() >= 0) {
								if (isRefresh) {
									dataList.clear();
									dataList.addAll(0, productList);
								} else {
									dataList.addAll(
											productList.size(),
											productList);
								}
								adapter.notifyDataSetChanged();
							}

						}
					}
				});
	}

	private void getTypeList() {
		GoodService.getInstance(context).getProductType(new CustomAsyncResponehandler() {
			public void onFinish() {
			}
			@Override
			public void onSuccess(ResponeModel baseModel) {
				super.onSuccess(baseModel);
				if (baseModel != null && baseModel.isStatus()) {
					final List<ProductType> typeList = (List<ProductType>) baseModel
							.getResultObj();
					if (typeList != null && typeList.size() > 0) {
						llMore.removeAllViews();
						for (int i = 0; i < typeList.size(); i++) {
							TextView t = new TextView(context);
							if (i == 0) {
								t.setTextColor(getResources().getColor(
										R.color.main_title_bg));
							} else {
								t.setTextColor(Color.BLACK);
							}

							t.setText(typeList.get(i).getName());

							t.setTextSize(17);
							LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(
									LinearLayout.LayoutParams.WRAP_CONTENT,
									LinearLayout.LayoutParams.WRAP_CONTENT);
							t.setPadding(30, 10, 30, 10);
							t.setLayoutParams(p);
							typeList.get(i).setIndex(i);
							t.setTag(typeList.get(i));
							t.setOnClickListener(new View.OnClickListener() {

								@Override
								public void onClick(View v) {
									((TextView) v).setTextColor(getResources()
											.getColor(R.color.main_title_bg));
									ProductType type = (ProductType) v.getTag();
									for (int i = 0; i < llMore
											.getChildCount(); i++) {
										page = 1;
										isRefresh=true;
										 sortId = type.getSortId();
										getMessageList();
										if (i != type.getIndex()) {
											((TextView) llMore
													.getChildAt(i))
													.setTextColor(getResources()
															.getColor(
																	R.color.black));
										}
									}
								}
							});
							if (i == 0) {
								sortId = typeList.get(0).getSortId();
								getMessageList();
							}
							llMore.addView(t);
						}
					}
				}
			}
		});
	}
}
