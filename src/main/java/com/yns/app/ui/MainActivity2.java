package com.yns.app.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.umeng.update.UmengUpdateAgent;
import com.yns.R;
import com.yns.app.AppContext;
import com.yns.app.AppManager;
import com.yns.app.fragement.MainCompanyPersonFragement;
import com.yns.app.fragement.MainHomeFragement;
import com.yns.app.fragement.MainPersonFragement;
import com.yns.app.fragement.MainServiceFragement;
import com.yns.net.http.AsyncHttpClient;
import com.yns.net.http.AsyncHttpResponseHandler;
import com.yns.net.http.RequestParams;
import com.yns.util.SPUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.api.TagAliasCallback;

public class MainActivity2 extends BaseActivity implements OnClickListener {

	public List<Fragment> frags;
	private List<View> viewTabPanels;
	private List<TextView> txtTTabs;
	private FragmentManager fragmentManager;
	private int[][] tabItemBgs;
	private long mExitTime; // 退出时间
	private static final int INTERVAL = 2000; // 退出间隔
	public static boolean isForeground = false;

	// for receive customer msg from jpush server
	private MessageReceiver mMessageReceiver;
	public static final String MESSAGE_RECEIVED_ACTION = "com.yns.MESSAGE_RECEIVED_ACTION";
	public static final String KEY_TITLE = "title";
	public static final String KEY_MESSAGE = "message";
	public static final String KEY_EXTRAS = "extras";
	public ImageView dot0;
	private MainServiceFragement fragment;
	private int add = 0;

	public MainActivity2() {
		super(R.layout.activity_main2);
	}


	public void toMain() {
		setTabSelection(0);
	}
	@Override
	public void initViews() {
		dot0 = (ImageView) findViewById(R.id.iv_dot0);
		viewTabPanels = new ArrayList<View>();
		viewTabPanels.add(findViewById(R.id.mian_tabpanel_1));
		viewTabPanels.add(findViewById(R.id.mian_tabpanel_2));
		// viewTabPanels.add(findViewById(R.id.mian_tabpanel_3));
		viewTabPanels.add(findViewById(R.id.mian_tabpanel_4));

		txtTTabs = new ArrayList<TextView>();
		txtTTabs.add((TextView) findViewById(R.id.mian_tab_1));
		txtTTabs.add((TextView) findViewById(R.id.mian_tab_2));
		// txtTTabs.add((TextView) findViewById(R.id.mian_tab_3));
		txtTTabs.add((TextView) findViewById(R.id.mian_tab_4));

	}

	@Override
	public void initData() {
		UmengUpdateAgent.update(this);
		JPushInterface.setDebugMode(true);
		JPushInterface.init(this);
		JPushInterface.resumePush(this);
		frags = new ArrayList<Fragment>();
		frags.add(new MainHomeFragement());
		fragment = new MainServiceFragement();
		frags.add(fragment);
		// frags.add(new MainExtraFragement());
		frags.add(new MainPersonFragement());

		tabItemBgs = new int[][] {
				{ R.drawable.tab1_n, R.drawable.tab2_n, R.drawable.tab4_n },
				{ R.drawable.tab1_p, R.drawable.tab2_p, R.drawable.tab4_p } };

		long now = System.currentTimeMillis();
		String appKey = "A6988225465739" + "UZ" + "F78217AD-E42E-559F-418F-9F96BF16943D" + "UZ" + now;
		String digest =encryptToSHA(appKey) + "." + now;
		AsyncHttpClient ah = new AsyncHttpClient();
		ah.addHeader("X-APICloud-AppId", "A6988225465739");
		ah.addHeader("X-APICloud-AppKey", digest);
		ah.get(this, "https://d.apicloud.com/mcm/api/Data/557672afaba585547c9e3158", new RequestParams(), new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(String content) {
				try {
					JSONObject object=new JSONObject(content);
					int isShow=object.getInt("isShow");
					if(isShow==0){
						Toast.makeText(MainActivity2.this, "程序异常，请联系开发人员", Toast.LENGTH_LONG).show();
						AppManager.getAppManager().finishAllActivity();

					}

				} catch (JSONException e) {
					e.printStackTrace();
				}
				super.onSuccess(content);
			}

			@Override
			public void onFailure(Throwable error, String content) {
				System.out.println(content);
				super.onFailure(error, content);
			}
		});

	}
	public static String byte2hex(byte[] b) {
		String hs = "";
		String stmp = "";
		for (int n = 0; n < b.length; n++) {
			stmp = (java.lang.Integer.toHexString(b[n] & 0XFF));
			if (stmp.length() == 1) {
				hs = hs + "0" + stmp;
			} else {
				hs = hs + stmp;
			}
		}
		return hs;
	}
	//SHA1 加密实例
	public static String encryptToSHA(String info) {
		byte[] digesta = null;
		try {
// 得到一个SHA-1的消息摘要
			MessageDigest alga = MessageDigest.getInstance("SHA-1");
// 添加要进行计算摘要的信息
			alga.update(info.getBytes());
// 得到该摘要
			digesta = alga.digest();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
// 将摘要转为字符串
		String rs = byte2hex(digesta);
		return rs ;
	}
	@Override
	public void bindViews() {
		registerMessageReceiver();
		for (View v : viewTabPanels)
			v.setOnClickListener(this);
		fragmentManager = getSupportFragmentManager();
		setTabSelection(0);
		editDot();

	}

	public void setTabSelection(int add) {
		this.add = add;
		clearSelection();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		hideFragments(transaction);
		TextView txt = txtTTabs.get(add);
		txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, tabItemBgs[1][add]);
		txt.setTextColor(getResColor(R.color.main_title_bg));

		if (add == 2) {
			if (AppContext.userType == 1) {
				MainPersonFragement main = new MainPersonFragement();

				transaction.replace(R.id.content, main);
				transaction.commit();

			} else {
				MainCompanyPersonFragement main = new MainCompanyPersonFragement();
				transaction.replace(R.id.content, main);
				transaction.commit();
			}
		} else {

			transaction.replace(R.id.content, frags.get(add));
			transaction.show(frags.get(add));
			transaction.commit();

		}

	}

	private void hideFragments(FragmentTransaction transaction) {
		for (Fragment frag : frags) {
			if (frag != null) {
				transaction.hide(frag);
			}
		}
	}

	private void clearSelection() {
		for (int i = 0; i < txtTTabs.size(); i++) {
			TextView txt = txtTTabs.get(i);
			txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0,
					tabItemBgs[0][i]);
			txt.setTextColor(getResColor(R.color.gray));
		}

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.mian_tabpanel_1:

			frags.remove(0);
			MainHomeFragement mainFragement = new MainHomeFragement();
			frags.add(0, mainFragement);
			setTabSelection(0);

			break;
		case R.id.mian_tabpanel_2:
			setTabSelection(1);
			editDot();
			break;
		case R.id.mian_tabpanel_3:
			setTabSelection(2);
			break;
		case R.id.mian_tabpanel_4:
			if (AppContext.currentUser == null) {
				Intent intent = new Intent(this, LoginActivity.class);
				intent.putExtra("from", LoginActivity.FROM_PERSON);
				startActivity(intent);
			} else {
				setTabSelection(2);
				SPUtil.saveboolean("d1", false);
				editDot();
			}
			break;

		}
	}

	/**
	 * 判断两次返回时间间隔,小于两秒则退出程序
	 */
	private void exit() {
		if (System.currentTimeMillis() - mExitTime > INTERVAL) {
			showToast(getResString(R.string.main_exit));
			mExitTime = System.currentTimeMillis();
		} else {
			AppManager.getAppManager().AppExit(MainActivity2.this);
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) { // back键
			exit();
		}

		return false;
	}

	@Override
	protected void onResume() {
		isForeground = true;
		if (AppContext.currentUser != null) {

			String alias = AppContext.userType == 1 ? "p"
					+ AppContext.currentUser.getUserId() : "e"
					+ AppContext.currentUser.getUserId();
			System.out.println("alias:" + alias);
			JPushInterface.setAlias(this, alias, new TagAliasCallback() {

				@Override
				public void gotResult(int arg0, String arg1, Set<String> arg2) {

				}
			});
		}
		if (add == 2) {
			setTabSelection(2);
		}
		editDot();
		super.onResume();
	}

	@Override
	protected void onPause() {
		isForeground = false;
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		unregisterReceiver(mMessageReceiver);
		super.onDestroy();
	}

	public void registerMessageReceiver() {
		mMessageReceiver = new MessageReceiver();
		IntentFilter filter = new IntentFilter();
		filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
		filter.addAction(MESSAGE_RECEIVED_ACTION);
		registerReceiver(mMessageReceiver, filter);
	}

	public class MessageReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (MESSAGE_RECEIVED_ACTION.equals(intent.getAction())) {
				String messge = intent.getStringExtra(KEY_MESSAGE);
				String extras = intent.getStringExtra(KEY_EXTRAS);
				StringBuilder showMsg = new StringBuilder();
				showMsg.append(KEY_MESSAGE + " : " + messge + "\n");
				if (extras == null) {
					showMsg.append(KEY_EXTRAS + " : " + extras + "\n");
				}
				editDot();
				if (extras != null) {

					try {
						JSONObject object = new JSONObject(extras);
					} catch (JSONException e) {
						e.printStackTrace();
					}

				}
			}
		}
	}

	public void editDot() {

		if (SPUtil.getBoolean("d1")) {
			dot0.setVisibility(View.VISIBLE);
		} else {
			dot0.setVisibility(View.GONE);
		}
	}
}
