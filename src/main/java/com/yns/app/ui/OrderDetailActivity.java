package com.yns.app.ui;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alipay.Result;
import com.alipay.SignUtils;
import com.alipay.sdk.app.PayTask;
import com.umeng.analytics.MobclickAgent;
import com.yns.R;
import com.yns.app.AppContext;
import com.yns.model.OrderDetailInfo;
import com.yns.model.OrderInfo;
import com.yns.model.ResponeModel;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.GoodService;
import com.yns.widget.HeaderBar;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

public class OrderDetailActivity extends BaseActivity {

    private TextView textOrderStatus, textOrderPay, textOrderTime,
            textOrderNumber;
    private TextView textAddressName, textAddressPhone, textAddressAddress,
            textAddressTimeType;
    private TextView textPriceCount;
    private OrderInfo order;
    private HeaderBar bar;
    private String type = "1";
    private Button btnOK, btnNo;
    public static final int ACTION = 1;// 评价成功跳转时候的值
    private LinearLayout lyShop;

    public OrderDetailActivity() {
        super(R.layout.activity_order_detail);
    }

    public static final String PARTNER = "2088611516608282";
    public static final String SELLER = "kunmingxuanzhi@163.com";
    public static final String RSA_PRIVATE = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAKR4R30Dy0TRmCWLgWXtWOXzo/7w2TunMbHiqQPjhYaJjxBvsdEhuMmtqPUoy//2QDO1eS69hAYsKnO0+DXktNiGOnne0sTrFab8bBRditV2ga4uytEw84S5EkmX8g55EdOjJqHJNR6DVWfq6jzwHqsWrB1eSbrLp3flUWzvtBPVAgMBAAECgYBlZZINTM2AvAck/oDkKhPokPDGOA51bHqCxM6WiyrC6wO8imACItwd2maT3ncGcvbZ6kOvwT8n3wq9ExEDiODEr1d+udPW0n5PZFhgwXZbMjV7IZm5JK/joAln3UIo967U+JspTMkzZC5qSYunp4+GDYGlvIevWNVOqTJPudYm8QJBANqg0Sz6vuyxPHfTNp7if1c8eckUopOKQ1gyfTyl94WcYp+Q871b1lp979WPQ5/yaCVEr/w3W8mFrVHrwAGp8LcCQQDAlX1MHzxq+/YfEgdsjG2wQluPRTtSg1UYtpbqH5JJ6JNB4SRttidoA0xw9jLMY6DCkfWX6zcjjn67B3DJm7vTAkBP/iVq6rfuzI6OXTaP/dmP5q28uBMgHqezXo3aIfOl7GldbuDvuOl+JLQbFJcur7gRYsdtZifsXT/kSHjxYueTAkEAhqH7TlQn8MLkdSQtfH1P0YEScT59ElouC3DaYCJiKrkk+VFkHi7Rra1gqzfym2Cq6lsPznzptznO7wFsoAT3cQJBAKZPIVplxsjYUYpdWbhYHsc0H5a6Ef4UOsZh39zzNa2dLful6MotC88ns25Bodr0D7ei3oequOfvnEsAGGTtnc4=";
    public static final String RSA_PUBLIC = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCnxj/9qwVfgoUh/y2W89L6BkRAFljhNhgPdyPuBV64bfQNN1PjbCzkIM6qRdKBoLPXmKKMiFYnkd6rAoprih3/PrQEB/VsW8OoM8fxn67UDYuyBTqA23MML9q1+ilIZwBC2AQ2UBVOrFXfFl75p6/B5KsiNG9zpgmLCUYuLkxpLQIDAQAB";
    private static final int SDK_PAY_FLAG = 1;

    private static final int SDK_CHECK_FLAG = 2;

    @SuppressLint("InflateParams")
    @Override
    public void initViews() {

        textOrderStatus = (TextView) findViewById(R.id.order_status);
        textOrderPay = (TextView) findViewById(R.id.order_pay);
        textOrderTime = (TextView) findViewById(R.id.order_time);
        textOrderNumber = (TextView) findViewById(R.id.order_number);
        textAddressName = (TextView) findViewById(R.id.order_address_name);
        textAddressPhone = (TextView) findViewById(R.id.order_address_phone);
        textAddressAddress = (TextView) findViewById(R.id.order_address_address);
        textAddressTimeType = (TextView) findViewById(R.id.order_address_time_type);
        textPriceCount = (TextView) findViewById(R.id.order_price_money);
        btnOK = (Button) findViewById(R.id.order_btnOK);
        btnNo = (Button) findViewById(R.id.order_btnNO);
        bar = (HeaderBar) findViewById(R.id.title);
        lyShop = (LinearLayout) findViewById(R.id.layout_shop);
    }

    @Override
    public void initData() {
        order = (OrderInfo) getIntent().getSerializableExtra("order");
        type = getIntent().getStringExtra("type");
    }

    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SDK_PAY_FLAG: {
                    Result resultObj = new Result((String) msg.obj);
                    String resultStatus = resultObj.resultStatus;
                    // 判断resultStatus 为“9000”则代表支付成功，具体状态码代表含义可参考接口文档
                    if (TextUtils.equals(resultStatus, "9000")) {
                        Toast.makeText(OrderDetailActivity.this, "支付成功",
                                Toast.LENGTH_SHORT).show();
                        order.setStatus("1");
                        bindViews();
                    } else {
                        // 判断resultStatus 为非“9000”则代表可能支付失败
                        // “8000”
                        // 代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）
                        if (TextUtils.equals(resultStatus, "8000")) {
                            Toast.makeText(OrderDetailActivity.this, "支付结果确认中",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(OrderDetailActivity.this, "支付失败",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                    break;
                }
                case SDK_CHECK_FLAG: {
                    Toast.makeText(OrderDetailActivity.this, "检查结果为：" + msg.obj,
                            Toast.LENGTH_SHORT).show();
                    break;
                }
                default:
                    break;
            }
        }

        ;
    };

    @Override
    public void bindViews() {
        bar.setTitle("订单详情");
        textOrderTime.setText("下单时间:" + order.getCreateDate());
        textOrderNumber.setText("订单编号:" + order.getAppOrderInfoId());
        textAddressName.setText("收货人:" + order.getAddress());
        textAddressPhone.setText("手机号码:" + order.getMobile());
        textAddressAddress.setText("收货地址:" + order.getAddress());
        textAddressTimeType.setText("支付时间:" + order.getPaymentDate()==null?"":order.getPaymentDate());


        for (OrderDetailInfo item : order.getOrderItems()) {
            View v = getLayoutInflater().inflate(R.layout.layout_order_product, null);
            TextView textName = (TextView) v.findViewById(R.id.order_detail_name);
            TextView textPrice = (TextView) v.findViewById(R.id.order_textPrice);
            TextView textSummary = (TextView) v.findViewById(R.id.order_detail_summary);
            ImageView photo = (ImageView) v.findViewById(R.id.order_detail_header_im);
            textName.setText(item.getName());
            textSummary.setText(item.getDescription());
            textPrice.setText("￥" + item.getSalePrice() + "x" + item.getBuyCount());
            AppContext.setImage(item.getPic(), photo, AppContext.imageOption);
            lyShop.addView(v);
        }

        textPriceCount.setText("￥" + order.getAmount());
        if ("1".equals(order.getStatus())) {
            textOrderStatus.setText("订单状态:" + "等待发货（已支付）");
            btnNo.setVisibility(View.GONE);
            btnOK.setVisibility(View.GONE);
        } else if ("2".equals(order.getStatus())) {
            textOrderStatus.setText("订单状态:" + "已发货（等待确认）");
            btnNo.setVisibility(View.GONE);
            btnOK.setVisibility(View.VISIBLE);
            btnOK.setText("确认收货");
            btnOK.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GoodService.getInstance(OrderDetailActivity.this).confirmOrder(order.getAppOrderInfoId(), new CustomAsyncResponehandler() {
                        public void onFinish() {
                        }

                        @Override
                        public void onSuccess(ResponeModel baseModel) {
                            super.onSuccess(baseModel);
                            if (baseModel != null && baseModel.isStatus()) {
                                order.setStatus("2");
                                bindViews();
                            }
                        }
                    });
                }
            });
        } else if ("3".equals(order.getStatus())) {
            textOrderStatus.setText("订单状态:" + "交易完成");
            btnNo.setVisibility(View.GONE);
            btnOK.setVisibility(View.GONE);
        } else if ("100".equals(order.getStatus())) {
            textOrderStatus.setText("订单状态:" + "交易关闭");
            btnNo.setVisibility(View.GONE);
            btnOK.setVisibility(View.GONE);
        } else if ("0".equals(order.getStatus())) {
            textOrderStatus.setText("订单状态:" + "未支付");
            btnNo.setVisibility(View.GONE);
            btnOK.setVisibility(View.VISIBLE);
            btnOK.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pay(v);
                }
            });
        }
    }

    /**
     * call alipay sdk pay. 调用SDK支付
     */
    public void pay(View v) {
        String orderInfo = getOrderInfo("购买商品", "购买商品", order.getAmount()+"");
        String sign = sign(orderInfo);
        try {
            System.out.println("s::" + sign);
            // 仅需对sign 做URL编码
            sign = URLEncoder.encode(sign, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        final String payInfo = orderInfo + "&sign=\"" + sign + "\"&"
                + getSignType();

        Runnable payRunnable = new Runnable() {

            @Override
            public void run() {
                // 构造PayTask 对象
                PayTask alipay = new PayTask(OrderDetailActivity.this);
                // 调用支付接口
                String result = alipay.pay(payInfo);

                Message msg = new Message();
                msg.what = SDK_PAY_FLAG;
                msg.obj = result;
                mHandler.sendMessage(msg);
            }
        };

        Thread payThread = new Thread(payRunnable);
        payThread.start();
    }

    /**
     * check whether the device has authentication alipay account.
     * 查询终端设备是否存在支付宝认证账户
     */
    public void check(View v) {
        Runnable checkRunnable = new Runnable() {

            @Override
            public void run() {
                PayTask payTask = new PayTask(OrderDetailActivity.this);
                boolean isExist = payTask.checkAccountIfExist();

                Message msg = new Message();
                msg.what = SDK_CHECK_FLAG;
                msg.obj = isExist;
                mHandler.sendMessage(msg);
            }
        };

        Thread checkThread = new Thread(checkRunnable);
        checkThread.start();

    }

    /**
     * get the sdk version. 获取SDK版本号
     */
    public void getSDKVersion() {
        PayTask payTask = new PayTask(this);
        String version = payTask.getVersion();
        Toast.makeText(this, version, Toast.LENGTH_SHORT).show();
    }

    /**
     * create the order info. 创建订单信息
     */
    public String getOrderInfo(String subject, String body, String price) {
        // 合作者身份ID
        String orderInfo = "partner=" + "\"" + PARTNER + "\"";

        // 卖家支付宝账号
        orderInfo += "&seller_id=" + "\"" + SELLER + "\"";

        // 商户网站唯一订单号
        orderInfo += "&out_trade_no=" + "\"" + getOutTradeNo() + "\"";

        // 商品名称
        orderInfo += "&subject=" + "\"" + subject + "\"";

        // 商品详情
        orderInfo += "&body=" + "\"" + body + "\"";

        // 商品金额
        orderInfo += "&total_fee=" + "\"" + price + "\"";

        // 服务器异步通知页面路径
        orderInfo += "&notify_url=" + "\"" + "http://notify.msp.hk/notify.htm"
                + "\"";

        // 接口名称， 固定值
        orderInfo += "&service=\"mobile.securitypay.pay\"";

        // 支付类型， 固定值
        orderInfo += "&payment_type=\"1\"";

        // 参数编码， 固定值
        orderInfo += "&_input_charset=\"utf-8\"";

        // 设置未付款交易的超时时间
        // 默认30分钟，一旦超时，该笔交易就会自动被关闭。
        // 取值范围：1m～15d。
        // m-分钟，h-小时，d-天，1c-当天（无论交易何时创建，都在0点关闭）。
        // 该参数数值不接受小数点，如1.5h，可转换为90m。
        orderInfo += "&it_b_pay=\"30m\"";

        // 支付宝处理完请求后，当前页面跳转到商户指定页面的路径，可空
        orderInfo += "&return_url=\"m.alipay.com\"";

        // 调用银行卡支付，需配置此参数，参与签名， 固定值
        // orderInfo += "&paymethod=\"expressGateway\"";

        return orderInfo;
    }

    /**
     * get the out_trade_no for an order. 获取外部订单号
     */
    public String getOutTradeNo() {
        SimpleDateFormat format = new SimpleDateFormat("MMddHHmmss",
                Locale.getDefault());
        Date date = new Date();
        String key = format.format(date);

        Random r = new Random();
        key = key + r.nextInt();
        key = key.substring(0, 15);
        return key;
    }

    /**
     * sign the order info. 对订单信息进行签名
     *
     * @param content 待签名订单信息
     */
    public String sign(String content) {
        return SignUtils.sign(content, RSA_PRIVATE);
    }

    /**
     * get the sign type we use. 获取签名方式
     */
    public String getSignType() {
        return "sign_type=\"RSA\"";
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }
}
