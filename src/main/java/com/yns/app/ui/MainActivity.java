package com.yns.app.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.umeng.update.UmengUpdateAgent;
import com.yns.R;
import com.yns.app.AppContext;
import com.yns.app.AppManager;
import com.yns.app.fragement.MainCompanyPersonFragement;
import com.yns.app.fragement.MainHomeFragement;
import com.yns.app.fragement.MainPersonFragement;
import com.yns.app.fragement.MainServiceFragement;
import com.yns.service.UserService;
import com.yns.widget.CircleImageView;
import com.yns.widget.SlidingMenu;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.api.TagAliasCallback;

/**
 * 主界面
 * 
 * @author Administrator
 * @time 2014-10-28
 */
public class MainActivity extends BaseActivity {

	public MainActivity() {
		super(R.layout.activity_main);

	}

	public SlidingMenu slideMenu;
	private View mLeft, mCenter;
	private FragmentManager fragmentManager;
	public TextView menuMain, menuService, menuUser, menuClear, menuAbout,
			menuUpdate, menuLoginOut;
	private long mExitTime; // 退出时间
	private static final int INTERVAL = 2000; // 退出间隔
	// 管理fragment
	public static List<Fragment> list = new ArrayList<Fragment>();
	private CircleImageView ivPhoto;
	private TextView tvName, tvTime, tvPower;

	@SuppressLint("InflateParams")
	@Override
	public void initViews() {

		slideMenu = (SlidingMenu) findViewById(R.id.slide_menu);
		mLeft = getLayoutInflater().inflate(R.layout.layout_menu, null);
		mCenter = getLayoutInflater().inflate(R.layout.activity_center, null);

		menuMain = (TextView) mLeft.findViewById(R.id.menu_shouye);
		menuService = (TextView) mLeft.findViewById(R.id.menu_service);
		menuUser = (TextView) mLeft.findViewById(R.id.menu_user);
		menuClear = (TextView) mLeft.findViewById(R.id.menu_clear);
		menuAbout = (TextView) mLeft.findViewById(R.id.menu_about);
		menuUpdate = (TextView) mLeft.findViewById(R.id.menu_update);
		menuLoginOut = (TextView) mLeft.findViewById(R.id.menu_loginout);
		ivPhoto = (CircleImageView) mLeft.findViewById(R.id.iv_photo);
		tvName = (TextView) mLeft.findViewById(R.id.tv_name);
		tvTime = (TextView) mLeft.findViewById(R.id.tv_lastlogintime);
		tvPower = (TextView) mLeft.findViewById(R.id.tv_power);
	}

	@Override
	public void initData() {
		JPushInterface.setDebugMode(true);
		JPushInterface.init(this);
		JPushInterface.resumePush(this);
		UmengUpdateAgent.update(this);
	}

	public void toMain() {
		list.clear();
		list = new ArrayList<Fragment>();
		AppContext.imageLoader.clearMemoryCache();
		AppContext.imageLoader.clearDiscCache();
		slideMenu.showLeftView();

		fragmentManager = getSupportFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		MainHomeFragement main = new MainHomeFragement();

		transaction.replace(R.id.content, main);
		transaction.commit();
		list.add(main);
	}

	@Override
	public void bindViews() {

		mCenter.findViewById(R.id.content).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {

					}
				});
		slideMenu.setLeftView(mLeft);

		slideMenu.setCenterView(mCenter);
		slideMenu.setRightView(new View(this));
		slideMenu.setCanSliding(true, false);
		fragmentManager = getSupportFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		MainHomeFragement main = new MainHomeFragement();
		transaction.replace(R.id.content, main);
		transaction.commit();
		list.add(main);

		menuMain.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				list.clear();
				list = new ArrayList<Fragment>();
				AppContext.imageLoader.clearMemoryCache();
				AppContext.imageLoader.clearDiscCache();
				slideMenu.showLeftView();

				fragmentManager = getSupportFragmentManager();
				FragmentTransaction transaction = fragmentManager
						.beginTransaction();
				MainHomeFragement main = new MainHomeFragement();

				transaction.replace(R.id.content, main);
				transaction.commit();
				list.add(main);
			}
		});

		menuService.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				AppContext.imageLoader.clearMemoryCache();
				AppContext.imageLoader.clearDiscCache();
				list.clear();
				list = new ArrayList<Fragment>();
				slideMenu.showLeftView();
				fragmentManager = getSupportFragmentManager();
				FragmentTransaction transaction = fragmentManager
						.beginTransaction();
				MainServiceFragement service = new MainServiceFragement();

				transaction.replace(R.id.content, service);
				transaction.commit();
				list.add(service);
			}
		});
		menuUser.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (AppContext.currentUser != null) {

					AppContext.imageLoader.clearMemoryCache();
					AppContext.imageLoader.clearDiscCache();
					list.clear();
					list = new ArrayList<Fragment>();
					slideMenu.showLeftView();
					fragmentManager = getSupportFragmentManager();
					FragmentTransaction transaction = fragmentManager
							.beginTransaction();

					if (AppContext.userType == 1) {
						MainPersonFragement main = new MainPersonFragement();

						transaction.replace(R.id.content, main);
						transaction.commit();
						list.add(main);
					} else {
						MainCompanyPersonFragement main = new MainCompanyPersonFragement();
						transaction.replace(R.id.content, main);
						transaction.commit();
						list.add(main);
					}
				} else {
					JumpToActivity(LoginActivity.class, false);
				}
			}
		});

		menuLoginOut.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				list.clear();
				list = new ArrayList<Fragment>();
				logout();
			}
		});

	}

	private void logout() {
		AppContext.currentUser = null;
		UserService.getInstance(context).clearCacheUser();
		Intent intent = new Intent(this, LoginActivity.class);
		intent.putExtra("from", LoginActivity.FROM_PERSON);
		startActivity(intent);
	}

	/**
	 * 判断两次返回时间间隔,小于两秒则退出程序
	 */
	private void exit() {
		if (System.currentTimeMillis() - mExitTime > INTERVAL) {
			showToast(getResources().getString(R.string.main_exit));
			mExitTime = System.currentTimeMillis();
		} else {
			JPushInterface.stopPush(this);
			JPushInterface.clearAllNotifications(this);
			AppManager.getAppManager().AppExit(MainActivity.this);
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (list.size() <= 1) {
			if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) { // back键
				exit();
			}
		} else {
			finishFragment();
		}
		return false;
	}

	/**
	 * 进入下一页面
	 * 
	 * @param fragment
	 *            要被隐藏的当前页面
	 * @param nextFragment
	 *            将要显示的下一页面
	 * @param bundle
	 *            传递参数 ，如果不需要传值，直接赋null
	 */
	public void nextFragment(Fragment fragment, Fragment nextFragment,
			Bundle bundle) {
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		if (null != bundle) {
			nextFragment.setArguments(bundle);
		}
		// 先判断是否被add过
		if (!nextFragment.isAdded()) {
			// 隐藏当前的fragment，add下一个到Activity中
			transaction.hide(fragment).add(R.id.content, nextFragment)
					.commitAllowingStateLoss();
		} else {
			// 隐藏当前的fragment，显示下一个
			transaction.hide(fragment).show(nextFragment)
					.commitAllowingStateLoss();
		}
		// 向列表中添加一个fragment
		list.add(nextFragment);
	}

	/**
	 * 销毁当前fragment，并显示上一页面
	 * 
	 * @param fragment
	 *            要被销毁的fragment
	 */
	public void finishFragment() {
		if (list.size() >= 1) {
			FragmentTransaction transaction = fragmentManager
					.beginTransaction();
			Fragment fragment = list.get(list.size() - 1);// 获取当前的fragment
			Fragment oldFragment = list.get(list.size() - 2);// 获取上一级的fragment
			oldFragment.onResume();
			// 移除当前的fragment，显示上一级
			transaction.remove(fragment).show(oldFragment).commit();
			// 从列表中移除一个fragment
			list.remove(list.size() - 1);
		}
	}

	public void onResume() {

		if (AppContext.currentUser != null) {
			tvName.setText(AppContext.currentUser.getUserName());
			tvTime.setText(AppContext.currentUser.getCreateDate());
			tvTime.setVisibility(View.VISIBLE);
			tvPower.setVisibility(View.VISIBLE);
			AppContext.setImage(AppContext.currentUser.getHeaderPic(), ivPhoto,
					AppContext.imageOption);
			String alias = AppContext.userType == 1 ? "p"
					+ AppContext.currentUser.getUserId() : "e"
					+ AppContext.currentUser.getUserId();
			JPushInterface.setAlias(this, alias, new TagAliasCallback() {

				@Override
				public void gotResult(int arg0, String arg1, Set<String> arg2) {

				}
			});
		} else {
			ivPhoto.setImageDrawable(getResources().getDrawable(
					R.drawable.def_head));
			tvName.setText("点击登录");
			tvTime.setVisibility(View.GONE);
			tvPower.setVisibility(View.GONE);
			tvName.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					JumpToActivity(LoginActivity.class, false);
				}
			});
			ivPhoto.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					if (AppContext.currentUser == null) {
						JumpToActivity(LoginActivity.class, false);
					}
				}
			});
		}
		super.onResume();

	}

	public void onPause() {
		super.onPause();

	}
}
