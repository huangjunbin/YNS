package com.yns.app.ui;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

import com.yns.R;
import com.yns.app.AppContext;
import com.yns.model.ResponeModel;
import com.yns.model.Resume;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.ComonService;
import com.yns.util.StringUtils;
import com.yns.util.UIHelper;
import com.yns.widget.HeaderBar;

public class JobCommentActivity extends BaseActivity {
    private HeaderBar headerBar;
    private EditText editTextTel;
    private Resume resume;
    private String title = "job";
    private String enterpriseId = "";

    public JobCommentActivity() {
        super(R.layout.activity_jobcomment);
    }

    @Override
    public void initViews() {
        headerBar = (HeaderBar) findViewById(R.id.title);
        headerBar.setTitle("添加评价");
        headerBar.top_right_btn.setText("发送");
        editTextTel = (EditText) findViewById(R.id.feedback_tel);
    }

    @Override
    public void initData() {
        enterpriseId = getIntent().getStringExtra("enterpriseId");
        ;
    }

    @Override
    public void bindViews() {
        headerBar.top_right_btn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (checkInput()) {
                    ComonService.getInstance(context).jobComment(enterpriseId,
                            editTextTel.getText().toString(),
                            AppContext.currentUser.getUserId(),
                            new CustomAsyncResponehandler() {
                                @Override
                                public void onSuccess(ResponeModel baseModel) {
                                    super.onSuccess(baseModel);
                                    if (baseModel != null
                                            && baseModel.isStatus()) {
                                        UIHelper.ShowMessage(context, "发送成功！");
                                        finish();
                                    }
                                }
                            });
                }
            }
        });
    }

    private boolean checkInput() {
        if (StringUtils.isEmpty(editTextTel.getText().toString())) {
            UIHelper.ShowMessage(context, "请输入内容");
            return false;
        }

        return true;
    }
}
