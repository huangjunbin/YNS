package com.yns.app.ui;

import java.util.Calendar;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import com.yns.R;
import com.yns.model.Garage;
import com.yns.model.ResponeModel;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.UserService;
import com.yns.util.DateUtil;
import com.yns.util.LogUtil;
import com.yns.util.StringUtils;
import com.yns.util.UIHelper;
import com.yns.widget.HeaderBar;

/**
 * @className：GarageDetailActivity.java
 * @author: lmt
 * @Function: 车库详情
 * @createDate: 2014-12-9 下午2:39:36
 * @update:
 */
public class GarageDetailActivity extends BaseActivity {
	private String TAG = "GarageDetailActivity";
	private boolean isLog = true;
	// ---
	private Garage inGarage, currentGarage;
	private boolean isChanged = false;
	// ---
	private HeaderBar headerBar;
	private EditText editTextCarNum, editTextCar4SShop, editTextCarBrand,
			editTextCarBuyTime;

	public GarageDetailActivity() {
		super(R.layout.activity_garagedetail);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void initViews() {
		// TODO Auto-generated method stub
		inGarage = (Garage) getIntent().getSerializableExtra("data");
		// ----
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle(getResString(R.string.garagedetail_title));
		headerBar.top_right_btn.setText(getResString(R.string.save));

		editTextCarNum = (EditText) findViewById(R.id.detail_carnum);
		editTextCar4SShop = (EditText) findViewById(R.id.detail_car4s);
		editTextCarBrand = (EditText) findViewById(R.id.detail_carbrand);
		editTextCarBuyTime = (EditText) findViewById(R.id.detail_cartime);
	}

	@Override
	public void initData() {
		// TODO Auto-generated method stub

	}

	@Override
	public void bindViews() {
		// TODO Auto-generated method stub
		editTextCarNum.addTextChangedListener(textWatcher);
		editTextCar4SShop.addTextChangedListener(textWatcher);
		editTextCarBrand.addTextChangedListener(textWatcher);
		editTextCarBuyTime.addTextChangedListener(textWatcher);

		headerBar.top_right_btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (isChanged) {
					if (checkInput()) {

						String time = "";
						time = DateUtil.exchangeDate(editTextCarBuyTime
								.getText().toString(), "yyyy-MM-dd",
								"yyyyMMddHHmmss");

						UserService.getInstance(context).modifyGarageDetail(
								inGarage.getCarId(),
								editTextCarBrand.getText().toString(),
								editTextCarNum.getText().toString(),
								editTextCar4SShop.getText().toString(), time,
								new CustomAsyncResponehandler() {
									public void onSuccess(ResponeModel baseModel) {
										if (baseModel != null
												&& baseModel.isStatus()) {
											UIHelper.ShowMessage(context,
													"修改成功！");
											finish();
										}
									};
								});
					}
				} else {
					UIHelper.ShowMessage(context, "额，您没有修改任何信息！");
				}
			}
		});

		findViewById(R.id.btn_removecar).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						UserService.getInstance(context).removeCar(
								inGarage.getCarId(),
								new CustomAsyncResponehandler() {
									@Override
									public void onSuccess(ResponeModel baseModel) {
										// TODO Auto-generated method stub
										super.onSuccess(baseModel);
										if (baseModel != null
												&& baseModel.isStatus()) {
											UIHelper.ShowMessage(context,
													"删除成功！");
											finish();
										}
									}
								});
					}
				});
		editTextCarBuyTime.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					AlertDialog.Builder builder = new AlertDialog.Builder(
							GarageDetailActivity.this);
					View view = View.inflate(GarageDetailActivity.this,
							R.layout.date_time_dialog, null);
					final DatePicker datePicker = (DatePicker) view
							.findViewById(R.id.date_picker);
					final TimePicker timePicker = (android.widget.TimePicker) view
							.findViewById(R.id.time_picker);
					builder.setView(view);

					Calendar cal = Calendar.getInstance();
					cal.setTimeInMillis(System.currentTimeMillis());
					datePicker.init(cal.get(Calendar.YEAR),
							cal.get(Calendar.MONTH),
							cal.get(Calendar.DAY_OF_MONTH), null);

					timePicker.setIs24HourView(true);
					timePicker.setCurrentHour(cal.get(Calendar.HOUR_OF_DAY));
					timePicker.setCurrentMinute(Calendar.MINUTE);

					final int inType = editTextCarBuyTime.getInputType();
					editTextCarBuyTime.setInputType(InputType.TYPE_NULL);

					editTextCarBuyTime.setInputType(inType);
					editTextCarBuyTime.setSelection(editTextCarBuyTime
							.getText().length());

					builder.setTitle("选取起始时间");
					builder.setPositiveButton("确  定",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {

									StringBuffer sb = new StringBuffer();
									sb.append(String.format("%d-%02d-%02d",
											datePicker.getYear(),
											datePicker.getMonth() + 1,
											datePicker.getDayOfMonth()));
									sb.append("  ");
								/*	sb.append(timePicker.getCurrentHour())
											.append(":")
											.append(timePicker
													.getCurrentMinute());*/

									editTextCarBuyTime.setText(sb);

									dialog.cancel();
								}
							});
					Dialog dialog = builder.create();
					dialog.show();
				}
				return false;
			}
		});

	}

	private boolean checkInput() {
		if (StringUtils.isEmpty(editTextCarNum.getText().toString())) {
			UIHelper.ShowMessage(context, "请输入您的车牌");
			return false;
		}
		if (StringUtils.isEmpty(editTextCar4SShop.getText().toString())) {
			UIHelper.ShowMessage(context, "请输入您的4S店");
			return false;
		}
		if (StringUtils.isEmpty(editTextCarBrand.getText().toString())) {
			UIHelper.ShowMessage(context, "请输入您的汽车品牌");
			return false;
		}
		if (StringUtils.isEmpty(editTextCarBuyTime.getText().toString())) {
			UIHelper.ShowMessage(context, "请输入您的购买时间");
			return false;
		}

		return true;
	}

	private TextWatcher textWatcher = new TextWatcher() {

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub

		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub

		}

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub
			isChanged = true;
		}
	};

	/**
	 * 更新车库详情
	 */
	public void updateCarDetailView() {
		// TODO Auto-generated method stub
		isChanged = false;
		if (currentGarage == null)
			return;
		setEditText(editTextCarNum, currentGarage.getCarNum());
		setEditText(editTextCarBrand, currentGarage.getCarBland());
		setEditText(editTextCar4SShop, currentGarage.getBuyAddress());

		String time = "时间异常";
		time = DateUtil.exchangeDate(currentGarage.getBuyTime(),
				"yyyyMMddHHmmss", "yyyy-MM-dd");
		setEditText(editTextCarBuyTime, time);

		isChanged = false;
	}

	@Override
	public void lastLoad() {
		// TODO Auto-generated method stub
		super.lastLoad();
		UserService.getInstance(context).getGarageDetail(inGarage.getCarId(),
				new CustomAsyncResponehandler() {
					@Override
					public void onSuccess(ResponeModel baseModel) {
						// TODO Auto-generated method stub
						super.onSuccess(baseModel);
						if (baseModel != null && baseModel.isStatus()) {
							Garage garage = (Garage) baseModel.getResultObj();
							if (garage != null) {
								currentGarage = garage;
								updateCarDetailView();
								LogUtil.d(TAG, currentGarage.toString(), isLog);
							}
						}
					}
				});
	}
}
