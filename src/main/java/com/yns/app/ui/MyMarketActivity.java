package com.yns.app.ui;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.yns.R;
import com.yns.widget.HeaderBar;

/**
 * @className：BindActivity.java
 * @author: lmt
 * @Function: 更换绑定
 * @createDate: 2014-12-11 下午5:26:13
 * @update:
 */
public class MyMarketActivity extends BaseActivity {
	private HeaderBar headerBar;

	private ImageView btnNew, btnJob, btnCompany,btnOther;

	public MyMarketActivity() {
		super(R.layout.activity_mymarket);
	}

	@Override
	public void initViews() {
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle("生活服务");
		btnNew = (ImageView) findViewById(R.id.btn_new);
		btnJob = (ImageView) findViewById(R.id.btn_job);
		btnOther = (ImageView) findViewById(R.id.btn_other);
		btnCompany = (ImageView) findViewById(R.id.btn_company);
	}

	@Override
	public void initData() {

	}

	@Override
	public void bindViews() {
		btnOther.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				showToast("即将推出，敬请期待");
			}
		});
		btnNew.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent();
				i.putExtra("flag", "groupBuying");
				i.putExtra("title", "团购");
				i.setClass(MyMarketActivity.this, ProductListActivity.class);
				startActivity(i);
			}
		});
		btnJob.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent();
				i.putExtra("flag", "birthday");
				i.putExtra("title", "生日专属");
				i.setClass(MyMarketActivity.this, ProductListActivity.class);
				startActivity(i);
			}
		});
		btnCompany.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent=new Intent(MyMarketActivity.this, MarketActivity.class);
				intent.putExtra("type", 1);
				startActivity(intent);
			}
		});
	}
}
