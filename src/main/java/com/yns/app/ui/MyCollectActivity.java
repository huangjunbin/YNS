package com.yns.app.ui;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.yns.R;
import com.yns.widget.HeaderBar;

/**
 * @className：BindActivity.java
 * @author: lmt
 * @Function: 更换绑定
 * @createDate: 2014-12-11 下午5:26:13
 * @update:
 */
public class MyCollectActivity extends BaseActivity {
	private HeaderBar headerBar;

	private ImageView btnNew, btnJob, btnCompany,btnShop;

	public MyCollectActivity() {
		super(R.layout.activity_mycollect);
	}

	@Override
	public void initViews() {
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle("我的收藏");
		btnNew = (ImageView) findViewById(R.id.btn_new);
		btnJob = (ImageView) findViewById(R.id.btn_job);
		btnCompany = (ImageView) findViewById(R.id.btn_company);
		btnShop= (ImageView) findViewById(R.id.btn_shop);
	}

	@Override
	public void initData() {

	}

	@Override
	public void bindViews() {
		btnNew.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent();
				i.putExtra("type", "article");
				i.setClass(MyCollectActivity.this, CollectActivity.class);
				startActivity(i);
			}
		});
		btnJob.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent();
				i.putExtra("type", "job");
				i.setClass(MyCollectActivity.this, CollectActivity.class);
				startActivity(i);
			}
		});
		btnCompany.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent();
				i.putExtra("type", "company");
				i.setClass(MyCollectActivity.this, CollectActivity.class);
				startActivity(i);
			}
		});
		btnShop.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent();
				i.putExtra("type", "product");
				i.setClass(MyCollectActivity.this, CollectActivity.class);
				startActivity(i);
			}
		});
	}
}
