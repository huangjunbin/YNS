package com.yns.app.ui;

import android.content.Intent;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.TextView;

import com.yns.R;
import com.yns.app.AppContext;
import com.yns.app.adapter.JobAdapter;
import com.yns.model.Job;
import com.yns.model.ResponeModel;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.ComonService;
import com.yns.util.DateUtil;
import com.yns.widget.HeaderBar;
import com.yns.widget.XListView;
import com.yns.widget.XListView.IXListViewListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class JobListActivity extends BaseActivity implements IXListViewListener {

	private HeaderBar headerBar;
	private XListView xlvData;

	private JobAdapter jobAdapter;
	public static List<Job> jobs;
	private int page = 1;
	private int rows = 5;
	private boolean isRefresh = true;
	private String key = "";
	private EditText editText;
	private String searchKey;

	private String gongZuodidian = "";
	private String jobSort = "";
	private String gongZuoJingYan = "";
	private String workType = "全职";
	private int flag;
	private String jobPortSort="";

	public JobListActivity() {
		super(R.layout.activity_job);

	}

	@Override
	public void initViews() {

		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle("职位列表");
		xlvData = (XListView) findViewById(R.id.xlv_data);
		xlvData.setPullRefreshEnable(true);
		xlvData.setPullLoadEnable(true);
		editText = (EditText) findViewById(R.id.search_edit);
		editText.setBackgroundResource(R.drawable.edit_city_search_bg);
		editText.setCompoundDrawablesWithIntrinsicBounds(
				R.drawable.city_edit_search, 0, 0, 0);
		editText.setHint("输入关键字/公司/职位/地点搜索");
		editText.setHintTextColor(getResColor(R.color.gray));
		editText.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
	}

	@Override
	public void initData() {
		jobs = new ArrayList<Job>();
		key = getIntent().getStringExtra("key");
		flag = getIntent().getIntExtra("flag", -1);
		gongZuodidian = getIntent().getStringExtra("gongZuodidian");
		jobSort = getIntent().getStringExtra("jobSort");
		jobPortSort= getIntent().getStringExtra("jobPortSort");
		workType = getIntent().getStringExtra("workType");
		gongZuoJingYan = getIntent().getStringExtra("gongZuoJingYan");
		jobAdapter = new JobAdapter(this, jobs);
		xlvData.setAdapter(jobAdapter);

	}

	@Override
	public void bindViews() {
		editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				searchKey = editText.getText().toString();
				key = searchKey;
				if (actionId == EditorInfo.IME_ACTION_SEARCH) {
					getJobList(searchKey);
					return true;
				}
				return false;
			}
		});
		xlvData.setXListViewListener(this);
		xlvData.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Job job = jobs.get(position - 1);
				Intent intent = new Intent();
				intent.setClass(JobListActivity.this, JobDetailActivity.class);
				intent.putExtra("job", job);
				startActivity(intent);
			}
		});
		onRefresh();
	}

	@Override
	public void onRefresh() {
		xlvData.setRefreshTime(DateUtil.getDateTime(new Date(System
				.currentTimeMillis())));
		isRefresh = true;
		page = 1;
		getJobList(key);
	}

	@Override
	public void onLoadMore() {
		isRefresh = false;
		page++;
		getJobList(key);
	}

	private void getJobList(String key) {
		ComonService.getInstance(context).getJob(
				rows + "",
				page + "",
				AppContext.currentUser == null ? "" : AppContext.currentUser
						.getUserId(), key, workType, gongZuodidian,
				gongZuoJingYan, jobSort, flag,jobPortSort, new CustomAsyncResponehandler() {
					public void onFinish() {
						xlvData.stopLoadMore();
						xlvData.stopRefresh();
						xlvData.setRefreshTime(DateUtil.getDateTime(new Date(
								System.currentTimeMillis())));
					};

					@SuppressWarnings("unchecked")
					@Override
					public void onSuccess(ResponeModel baseModel) {
						super.onSuccess(baseModel);
						if (baseModel != null && baseModel.isStatus()) {
							List<Job> jobList = (List<Job>) baseModel
									.getResultObj();

							if (jobList != null && jobList.size() > 0) {
								if (isRefresh) {
									jobs.clear();
									jobs.addAll(0, jobList);
								} else {
									jobs.addAll(jobList);
								}
								jobAdapter.notifyDataSetChanged();
							} else {
								if (isRefresh) {
									showToast("未查找到相关职位信息");
								}
							}
						}
					}
				});
	}

}
