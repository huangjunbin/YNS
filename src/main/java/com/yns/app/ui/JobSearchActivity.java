package com.yns.app.ui;

import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.content.Intent;
import android.content.res.AssetManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.yns.R;
import com.yns.app.AppContext;
import com.yns.model.City;
import com.yns.model.District;
import com.yns.model.JobType;
import com.yns.model.Province;
import com.yns.model.ResponeModel;
import com.yns.model.TradeType;
import com.yns.model.TradeType.ChildType;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.ComonService;
import com.yns.util.StringUtils;
import com.yns.widget.HeaderBar;

public class JobSearchActivity extends BaseActivity {
	private HeaderBar headerBar;
	private EditText editText;
	private String searchKey;
	private Button btnSearch;
	private TextView tvCollect, tvResume, tvApply;
	private List<JobType> jobTypeList;
	private Intent intent;
	private RelativeLayout rlwType, rlEx, rlLocation, rlType;
	private TextView tvwType, tvEx, tvLocation, tvType;
	/**
	 * 所有省
	 */
	protected String[] mProvinceDatas;
	/**
	 * key - 省 value - 市
	 */
	protected Map<String, String[]> mCitisDatasMap = new HashMap<String, String[]>();
	/**
	 * key - 市 values - 区
	 */
	protected Map<String, String[]> mDistrictDatasMap = new HashMap<String, String[]>();
	/**
	 * key - 区 values - 邮编
	 */
	protected Map<String, String> mZipcodeDatasMap = new HashMap<String, String>();
	/**
	 * 当前省的名称
	 */
	protected String mCurrentProviceName;
	/**
	 * 当前市的名称
	 */
	protected String mCurrentCityName;
	/**
	 * 当前区的名称
	 */
	protected String mCurrentDistrictName = "";

	/**
	 * 当前区的邮政编码
	 */
	protected String mCurrentZipCode = "";

	public static int code1 = 1000;
	public static int code2 = 2000;
	public static int code3 = 3000;
	public static int code4 = 4000;
	public JobType t1, t4, t3;
	public ChildType t2;

	public JobSearchActivity() {
		super(R.layout.activity_search);
	}

	@Override
	public void initViews() {

		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle(getResString(R.string.search_title));
		rlwType = (RelativeLayout) findViewById(R.id.rl_wtype);
		rlEx = (RelativeLayout) findViewById(R.id.rl_ex);
		rlType = (RelativeLayout) findViewById(R.id.rl_type);
		rlLocation = (RelativeLayout) findViewById(R.id.rl_location);
		tvwType = (TextView) findViewById(R.id.tv_wtype);
		tvEx = (TextView) findViewById(R.id.tv_ex);
		tvType = (TextView) findViewById(R.id.tv_type);
		tvLocation = (TextView) findViewById(R.id.tv_location);
		editText = (EditText) findViewById(R.id.search_edit);
		editText.setBackgroundResource(R.drawable.edit_city_search_bg);
		editText.setCompoundDrawablesWithIntrinsicBounds(
				R.drawable.city_edit_search, 0, 0, 0);
		editText.setHint("输入关键字/公司/职位/地点搜索");
		editText.setHintTextColor(getResColor(R.color.gray));
		editText.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
		btnSearch = (Button) findViewById(R.id.btn_search);
		tvCollect = (TextView) findViewById(R.id.tv_collect);
		tvResume = (TextView) findViewById(R.id.tv_resume);
		tvApply = (TextView) findViewById(R.id.tv_apply);
	}

	@Override
	public void initData() {
		initProvinceDatas();
	}

	@Override
	public void bindViews() {
		rlType.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ComonService.getInstance(context).getTradeType(
						new CustomAsyncResponehandler() {
							public void onFinish() {

							};

							@SuppressWarnings("unchecked")
							@Override
							public void onSuccess(ResponeModel baseModel) {
								super.onSuccess(baseModel);
								if (baseModel != null && baseModel.isStatus()) {
									List<TradeType> tradeTypeList = (List<TradeType>) baseModel
											.getResultObj();
									if (tradeTypeList != null
											&& tradeTypeList.size() > 0) {

										intent = new Intent();
										intent.putExtra("title", "行业类别");
										intent.putExtra("data",
												(Serializable) tradeTypeList);
										intent.setClass(JobSearchActivity.this,
												JobType2Activity.class);
										startActivityForResult(intent, code2);
									}
								}
							}
						});
			}
		});
		rlLocation.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				int pCurrent = 28;
				mCurrentProviceName = mProvinceDatas[pCurrent];
				String[] cities = mCitisDatasMap.get(mCurrentProviceName);
				jobTypeList = new ArrayList<JobType>();
				for (int i = 0; i < cities.length; i++) {

					// 全职&兼职,全职,兼职,实习
					JobType j = new JobType();
					j.setId(i + "");
					j.setName(cities[i]);
					jobTypeList.add(j);
				}
				intent = new Intent();
				intent.putExtra("title", "工作地点");
				intent.putExtra("data", (Serializable) jobTypeList);
				intent.setClass(JobSearchActivity.this, JobTypeActivity.class);
				startActivityForResult(intent, code1);
			}
		});
		rlEx.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// 82 不限, 83 应届生, 84 1-3年, 85 3-5年,
				// 86 5-10年, 87 10年以上, 88 20年以上,
				// 89 30年以上 , 90 退休人员
				jobTypeList = new ArrayList<JobType>();
				// 全职&兼职,全职,兼职,实习
				JobType j1 = new JobType();
				j1.setId("82");
				j1.setName("不限");
				jobTypeList.add(j1);
				JobType j2 = new JobType();
				j2.setId("83 ");
				j2.setName("应届生");
				jobTypeList.add(j2);
				JobType j3 = new JobType();
				j3.setId("84");
				j3.setName("1-3年");
				jobTypeList.add(j3);
				JobType j4 = new JobType();
				j4.setId("85");
				j4.setName("3-5年");
				jobTypeList.add(j4);
				JobType j5 = new JobType();
				j5.setId("86");
				j5.setName("5-10年");
				jobTypeList.add(j5);
				JobType j6 = new JobType();
				j6.setId("87");
				j6.setName("10年以上");
				jobTypeList.add(j6);
				JobType j7 = new JobType();
				j7.setId("88");
				j7.setName("20年以上");
				jobTypeList.add(j7);
				JobType j8 = new JobType();
				j8.setId("89");
				j8.setName("30年以上");
				jobTypeList.add(j8);
				JobType j9 = new JobType();
				j9.setId("90");
				j9.setName("退休人员");
				jobTypeList.add(j9);
				intent = new Intent();
				intent.putExtra("title", "工作经验");
				intent.putExtra("data", (Serializable) jobTypeList);
				intent.setClass(JobSearchActivity.this, JobTypeActivity.class);
				startActivityForResult(intent, code3);

			}
		});
		rlwType.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				jobTypeList = new ArrayList<JobType>();
				// 全职&兼职,全职,兼职,实习
				JobType j1 = new JobType();
				j1.setId("1");
				j1.setName("全职&兼职");
				jobTypeList.add(j1);
				JobType j2 = new JobType();
				j2.setId("2");
				j2.setName("全职");
				jobTypeList.add(j2);
				JobType j3 = new JobType();
				j3.setId("3");
				j3.setName("兼职");
				jobTypeList.add(j3);
				JobType j4 = new JobType();
				j4.setId("4");
				j4.setName("实习");
				jobTypeList.add(j4);
				intent = new Intent();
				intent.putExtra("title", "职位类型");
				intent.putExtra("data", (Serializable) jobTypeList);
				intent.setClass(JobSearchActivity.this, JobTypeActivity.class);
				startActivityForResult(intent, code4);
			}
		});
		editText.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {

				searchKey = editText.getText().toString();

				return false;
			}
		});

		if (!StringUtils.isEmpty(searchKey)) {
			editText.setText(searchKey);

		}
		btnSearch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent();
				i.putExtra("key", editText.getText().toString());
				i.putExtra("gongZuodidian", tvLocation.getText().toString());
				i.putExtra("jobSort", tvType.getText().toString());

				i.putExtra("gongZuoJingYan", t3 == null ? "" : t3.getId());
				i.putExtra("workType",
						tvwType.getText().toString() == "" ? "全职" : tvwType
								.getText().toString());
				i.setClass(JobSearchActivity.this, JobListActivity.class);
				startActivity(i);
			}
		});
		tvCollect.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (AppContext.currentUser == null) {
					JumpToActivity(LoginActivity.class, false);
					return;
				}
				Intent i = new Intent();
				i.putExtra("type", "job");
				i.setClass(JobSearchActivity.this, CollectActivity.class);
				startActivity(i);
			}
		});
		tvApply.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (AppContext.currentUser == null) {
					JumpToActivity(LoginActivity.class, false);
					return;
				}
				Intent i = new Intent();
				i.setClass(JobSearchActivity.this, ApplyActivity.class);
				startActivity(i);
			}
		});
		tvResume.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (AppContext.currentUser == null) {
					JumpToActivity(LoginActivity.class, false);
					return;
				}
			}
		});
	}

	/**
	 * 解析省市区的XML数据
	 */

	protected void initProvinceDatas() {
		List<Province> provinceList = null;
		AssetManager asset = getAssets();
		try {
			InputStream input = asset.open("province_data.xml");
			// 创建一个解析xml的工厂对象
			SAXParserFactory spf = SAXParserFactory.newInstance();
			// 解析xml
			SAXParser parser = spf.newSAXParser();
			XmlParserHandler handler = new XmlParserHandler();
			parser.parse(input, handler);
			input.close();
			// 获取解析出来的数据
			provinceList = handler.getDataList();
			// */ 初始化默认选中的省、市、区
			if (provinceList != null && !provinceList.isEmpty()) {
				mCurrentProviceName = provinceList.get(0).getName();
				List<City> cityList = provinceList.get(0).getCityList();
				if (cityList != null && !cityList.isEmpty()) {
					mCurrentCityName = cityList.get(0).getName();
					List<District> districtList = cityList.get(0)
							.getDistrictList();
					mCurrentDistrictName = districtList.get(0).getName();
					mCurrentZipCode = districtList.get(0).getZipcode();
				}
			}
			// */
			mProvinceDatas = new String[provinceList.size()];
			for (int i = 0; i < provinceList.size(); i++) {
				// 遍历所有省的数据
				mProvinceDatas[i] = provinceList.get(i).getName();
				List<City> cityList = provinceList.get(i).getCityList();
				String[] cityNames = new String[cityList.size()];
				for (int j = 0; j < cityList.size(); j++) {
					// 遍历省下面的所有市的数据
					cityNames[j] = cityList.get(j).getName();
					List<District> districtList = cityList.get(j)
							.getDistrictList();
					String[] distrinctNameArray = new String[districtList
							.size()];
					District[] distrinctArray = new District[districtList
							.size()];
					for (int k = 0; k < districtList.size(); k++) {
						// 遍历市下面所有区/县的数据
						District District = new District(districtList.get(k)
								.getName(), districtList.get(k).getZipcode());
						// 区/县对于的邮编，保存到mZipcodeDatasMap
						mZipcodeDatasMap.put(districtList.get(k).getName(),
								districtList.get(k).getZipcode());
						distrinctArray[k] = District;
						distrinctNameArray[k] = District.getName();
					}
					// 市-区/县的数据，保存到mDistrictDatasMap
					mDistrictDatasMap.put(cityNames[j], distrinctNameArray);
				}
				// 省-市的数据，保存到mCitisDatasMap
				mCitisDatasMap.put(provinceList.get(i).getName(), cityNames);
			}
		} catch (Throwable e) {
			e.printStackTrace();
		} finally {

		}
	}

	public class XmlParserHandler extends DefaultHandler {

		/**
		 * 存储所有的解析对象
		 */
		private List<Province> provinceList = new ArrayList<Province>();

		public XmlParserHandler() {

		}

		public List<Province> getDataList() {
			return provinceList;
		}

		@Override
		public void startDocument() throws SAXException {
			// 当读到第一个开始标签的时候，会触发这个方法
		}

		Province Province = new Province();
		City City = new City();
		District District = new District();

		@Override
		public void startElement(String uri, String localName, String qName,
				Attributes attributes) throws SAXException {
			// 当遇到开始标记的时候，调用这个方法
			if (qName.equals("province")) {
				Province = new Province();
				Province.setName(attributes.getValue(0));
				Province.setCityList(new ArrayList<City>());
			} else if (qName.equals("city")) {
				City = new City();
				City.setName(attributes.getValue(0));
				City.setDistrictList(new ArrayList<District>());
			} else if (qName.equals("district")) {
				District = new District();
				District.setName(attributes.getValue(0));
				District.setZipcode(attributes.getValue(1));
			}
		}

		@Override
		public void endElement(String uri, String localName, String qName)
				throws SAXException {
			// 遇到结束标记的时候，会调用这个方法
			if (qName.equals("district")) {
				City.getDistrictList().add(District);
			} else if (qName.equals("city")) {
				Province.getCityList().add(City);
			} else if (qName.equals("province")) {
				provinceList.add(Province);
			}
		}

		@Override
		public void characters(char[] ch, int start, int length)
				throws SAXException {
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == code1) {
			if (data != null) {
				t1 = (JobType) data.getSerializableExtra("data");
				tvLocation.setText(t1.getName());
			} else {
				tvLocation.setText("");
			}
		}
		if (requestCode == code2) {
			if (data != null) {
				t2 = (ChildType) data.getSerializableExtra("data");
				tvType.setText(t2.getName());
			} else {
				tvType.setText("");
			}
		}
		if (requestCode == code3) {
			if (data != null) {
				t3 = (JobType) data.getSerializableExtra("data");
				tvEx.setText(t3.getName());
			} else {
				tvEx.setText("");
			}
		}
		if (requestCode == code4) {
			if (data != null) {
				t4 = (JobType) data.getSerializableExtra("data");
				tvwType.setText(t4.getName());
			} else {
				tvwType.setText("");
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
}
