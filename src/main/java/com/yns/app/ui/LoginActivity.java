package com.yns.app.ui;

import android.app.Activity;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;

import com.yns.R;
import com.yns.app.AppContext;
import com.yns.app.AppManager;
import com.yns.model.ResponeModel;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.UserService;
import com.yns.util.SPUtil;
import com.yns.util.StringUtils;
import com.yns.util.UIHelper;
import com.yns.widget.HeaderBar;

/**
 * @className：LoginActivity.java
 * @author: lmt
 * @Function: 登陆
 * @createDate: 2014-12-1 下午3:35:48
 * @update:
 */
public class LoginActivity extends BaseActivity implements OnClickListener {
	public static final int FROM_PERSON = 2;
	public static final int FROM_OTHER = 1;
	private int from;

	private static final int REQUEST_REGISTER = 1;
	// ------
	private HeaderBar headerBar;
	private CheckBox checkUser, checkCompany;
	public EditText editTextAccount, editTextPassword;
 
	private long mExitTime; // 退出时间
	private static final int INTERVAL = 2000; // 退出间隔

	public LoginActivity() {
		super(R.layout.activity_login);
	}

	// 18664652825 123456
	@Override
	public void initViews() {
		// TODO Auto-generated method stub
		from = getIntent().getIntExtra("form", FROM_OTHER);
		headerBar = (HeaderBar) findViewById(R.id.title);
		checkUser = (CheckBox) findViewById(R.id.login_user);
		checkCompany = (CheckBox) findViewById(R.id.login_company);
		editTextAccount = (EditText) findViewById(R.id.login_account);
		editTextPassword = (EditText) findViewById(R.id.login_password);
	}

	@Override
	public void initData() {
		SPUtil.saveboolean("isRemberPassword", false);
		// 是否记住密码
		if (AppContext.currentUser != null) {
			if (StringUtils.isEmail(AppContext.currentUser.getUserName())) {
				editTextAccount.setText(AppContext.currentUser.getUserName());
			}

		}

		if (SPUtil.getBoolean("isRemberPassword")) {
			editTextAccount.setText(SPUtil.getString("mobileNumber"));
			editTextPassword.setText(SPUtil.getString("myKey"));

		}
	}

	@Override
	public void bindViews() {
		// TODO Auto-generated method stub
		headerBar.setTitle(getResString(R.string.login_title));
		((TextView) findViewById(R.id.text_register)).setOnClickListener(this);
		((TextView) findViewById(R.id.text_forget)).setOnClickListener(this);
		((Button) findViewById(R.id.btn_login)).setOnClickListener(this);
		checkUser.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {

				if (isChecked) {
					AppContext.userType=1;
					checkCompany.setChecked(false);
				}

			}
		});

		checkCompany.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (isChecked) {
					AppContext.userType=2;
					checkUser.setChecked(false);
				}

			}
		});
		headerBar.back.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btn_top_back:
			if (from == FROM_PERSON) {
				exit();
			} else {
				finish();
			}
			break;
		case R.id.text_register:
			JumpToActivityForResult(RegisterActivity.class, REQUEST_REGISTER,
					false);
			break;
		case R.id.text_forget:
			JumpToActivity(ForgetActivity.class, false);
			break;
		case R.id.btn_login:

			if (checkLogin()) {
				UserService.getInstance(context).login(
						editTextAccount.getText().toString(),
						editTextPassword.getText().toString(), true, true,
						checkUser.isChecked(), new CustomAsyncResponehandler() {
							@Override
							public void onSuccess(ResponeModel baseModel) {
								super.onSuccess(baseModel);
								if (baseModel != null && baseModel.isStatus()) {
									// JumpToActivity(MainActivity.class,
									// false);
									finish();
								}
							}
						});
			}
			break;
		}
	}

	private boolean checkLogin() {
		if (StringUtils.isEmpty(editTextAccount.getText().toString())) {
			UIHelper.ShowMessage(context, getResString(R.string.login_nameempt));
			return false;
		}

		if (StringUtils.isEmpty(editTextPassword.getText().toString())) {
			UIHelper.ShowMessage(context,
					getResString(R.string.login_passwordempt));
			return false;
		}
		if (!checkUser.isChecked() && !checkCompany.isChecked()) {
			showToast("请选择账户类型");
			return false;
		}
		return true;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) { // back键
			if (from == FROM_PERSON) {
				exit();
			} else {
				finish();
			}
		}
		return false;
	}

	/**
	 * 判断两次返回时间间隔,小于两秒则退出程序
	 */
	private void exit() {
		if (System.currentTimeMillis() - mExitTime > INTERVAL) {
			showToast(getResString(R.string.main_exit));
			mExitTime = System.currentTimeMillis();
		} else {
			AppManager.getAppManager().AppExit(LoginActivity.this);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			switch (requestCode) {
			case REQUEST_REGISTER:
				editTextAccount.setText(data.getStringExtra("mobileNumber"));
				editTextPassword.setText(data.getStringExtra("password"));
				break;
			}
		}
	}
}
