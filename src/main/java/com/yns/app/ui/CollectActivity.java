package com.yns.app.ui;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.yns.R;
import com.yns.app.AppContext;
import com.yns.app.adapter.CompanyAdapter;
import com.yns.app.adapter.JobAdapter;
import com.yns.app.adapter.MessageAdapter;
import com.yns.app.adapter.ProductCollectListAdapter;
import com.yns.model.Company;
import com.yns.model.Job;
import com.yns.model.Message;
import com.yns.model.Product;
import com.yns.model.ResponeModel;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.ComonService;
import com.yns.util.DateUtil;
import com.yns.widget.HeaderBar;
import com.yns.widget.XListView;
import com.yns.widget.XListView.IXListViewListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CollectActivity extends BaseActivity implements IXListViewListener {

	private HeaderBar headerBar;
	private XListView xlvData;

	private JobAdapter jobAdapter;
	public static List<Job> jobs;
	private int page = 1;
	private int rows = 5;
	private boolean isRefresh = true;
	private String type = "job";
	public static List<Message> messages;
	public MessageAdapter messageAdapter;
	public static List<Company> companys;
	private CompanyAdapter companyAdapter;
	public static List<Product> products;
	private ProductCollectListAdapter productListAdapter;
	public CollectActivity() {
		super(R.layout.activity_collect);

	}

	@Override
	public void initViews() {

		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle("收藏列表");
		xlvData = (XListView) findViewById(R.id.xlv_data);
		xlvData.setPullRefreshEnable(true);
		xlvData.setPullLoadEnable(true);

	}

	@Override
	public void initData() {
		jobs = new ArrayList<Job>();
		messages = new ArrayList<Message>();
		companys = new ArrayList<Company>();
		products=new ArrayList<Product>();
		type = getIntent().getStringExtra("type");
		if ("job".equals(type)) {
			jobAdapter = new JobAdapter(this, jobs);
			xlvData.setAdapter(jobAdapter);
		}
		if ("article".equals(type)) {
			messageAdapter = new MessageAdapter(this, messages);
			xlvData.setAdapter(messageAdapter);
		}
		if ("company".equals(type)) {
			companyAdapter = new CompanyAdapter(this, companys);
			xlvData.setAdapter(companyAdapter);
		}
		if ("product".equals(type)) {
			productListAdapter = new ProductCollectListAdapter(this, products);
			xlvData.setAdapter(productListAdapter);
		}
	}

	@Override
	public void bindViews() {
		xlvData.setXListViewListener(this);
		xlvData.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if ("job".equals(type)) {
					Job job = jobs.get(position - 1);
					Intent intent = new Intent();
					intent.setClass(CollectActivity.this,
							JobDetailActivity.class);
					intent.putExtra("job", job);
					startActivity(intent);
				}
				if ("article".equals(type)) {
					if (position <= messages.size()) {
						final Message msg = messages.get(position - 1);
						Intent intent = new Intent();
						intent.setClass(CollectActivity.this,
								WebViewActivity.class);

						intent.putExtra("url",
								"http://www.dxsapp.com/front/app_article_info.html?id="
										+ msg.getFkId());

						intent.putExtra("title", msg.getTitle());
						intent.putExtra("fkId", msg.getFkId());
						intent.putExtra("type", "article");
						intent.putExtra("isFavorite", msg.getIsFavorite());
						startActivity(intent);
					}
				}
				if ("company".equals(type)) {
					if (position <= companys.size()) {
						Company company = companys.get(position - 1);
						Intent intent = new Intent();
						intent.setClass(CollectActivity.this,
								CompanyDetailActivity.class);
						intent.putExtra("company", company);
						startActivity(intent);
					}
				}
				if ("product".equals(type)) {
					if (position <= products.size()) {
						Product product = products.get(position - 1);
						Intent intent = new Intent();
						intent.setClass(CollectActivity.this,
								ProductDetailActivity.class);
						intent.putExtra("data", product);
						intent.putExtra("type", 1);
						startActivity(intent);
					}
				}
			}
		});
		onRefresh();
	}

	@Override
	public void onRefresh() {
		xlvData.setRefreshTime(DateUtil.getDateTime(new Date(System
				.currentTimeMillis())));
		isRefresh = true;
		page = 1;
		getCollectList(type, true);
	}

	@Override
	public void onLoadMore() {
		isRefresh = false;
		page++;
		getCollectList(type, true);
	}

	private void getCollectList(final String type, boolean showDialog) {
		ComonService.getInstance(context).getCollect(
				rows + "",
				page + "",
				AppContext.currentUser == null ? "" : AppContext.currentUser
						.getUserId(),
				type,
				type.equals("job") ? Job.class
						: type.equals("article") ? Message.class
								:type.equals("company")?Company.class:Product.class, showDialog,
				new CustomAsyncResponehandler() {
					public void onFinish() {
						xlvData.stopLoadMore();
						xlvData.stopRefresh();
						xlvData.setRefreshTime(DateUtil.getDateTime(new Date(
								System.currentTimeMillis())));
					};

					@SuppressWarnings("unchecked")
					@Override
					public void onSuccess(ResponeModel baseModel) {
						super.onSuccess(baseModel);
						if (baseModel != null && baseModel.isStatus()) {
							if ("job".equals(type)) {
								List<Job> jobList = (List<Job>) baseModel
										.getResultObj();
								if (jobList != null && jobList.size() >= 0) {
									if (jobList.size() == 0) {
										jobs.clear();
										jobAdapter.notifyDataSetChanged();
									}
									for (Job job : jobList) {
										job.setIsFavorite("1");
									}
									if (isRefresh) {
										jobs.clear();
										jobs.addAll(0, jobList);
									} else {
										jobs.addAll( jobList);
									}
									jobAdapter.notifyDataSetChanged();
								}
							}
							if ("article".equals(type)) {
								List<Message> messageList = (List<Message>) baseModel
										.getResultObj();
								if (messageList != null
										&& messageList.size() >= 0) {
									if (messageList.size() == 0) {
										messages.clear();
										messageAdapter.notifyDataSetChanged();
									}
									for (Message message : messageList) {
										message.setIsFavorite("1");
									}
									if (isRefresh) {
										messages.clear();
										messages.addAll(0, messageList);
									} else {
										messages.addAll(
												messageList);
									}

								}
							}
							if ("company".equals(type)) {
								List<Company> companyList = (List<Company>) baseModel
										.getResultObj();
								if (companyList != null
										&& companyList.size() >= 0) {
									if (companyList.size() == 0) {
										companys.clear();
										companyAdapter.notifyDataSetChanged();
									}
									for (Company company : companyList) {
										company.setIsFavorite("1");
									}
									if (isRefresh) {
										companys.clear();
										companys.addAll(0, companyList);
									} else {
										companys.addAll(
												companyList);
									}

								}
							}
							if ("product".equals(type)) {
								List<Product> productList = (List<Product>) baseModel
										.getResultObj();
								if (productList != null
										&& productList.size() >= 0) {
									if (productList.size() == 0) {
										products.clear();
										productListAdapter.notifyDataSetChanged();
									}
									if (isRefresh) {
										products.clear();
										products.addAll(0, productList);
									} else {
										products.addAll(
												productList);
									}
									productListAdapter.notifyDataSetChanged();
								}
							}
						}
					}
				});
	}

	@Override
	public void onResume() {
		getCollectList(type, false);
		super.onResume();
	}
}
