package com.yns.app.ui;

import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;


import com.yns.R;
import com.yns.app.AppContext;
import com.yns.util.ImageUtils;

import java.util.ArrayList;

/**
 * 图片放大界面
 * 
 * @author bin
 * @version 1.0.0
 * @created 2014-3-17
 */
public class BigImageActivity extends BaseActivity {

	private String url;
	private String[] urls;
	private ArrayList<View> pageViews;
	private ImageView[] imageViews;
	private ViewPager viewPager;
	private GuidePageAdapter guidePageAdapter;
	private boolean isLocal;
	private LinearLayout page_ll;
	private int index;// 图片再列表里的下标

	public BigImageActivity() {
		super(R.layout.activity_big_image_layout);

	}

	@Override
	public void initViews() {

		url = getIntent().getStringExtra("url");
		urls = url.split(",");
		isLocal = getIntent().getBooleanExtra("isLocal", false);
		index = getIntent().getIntExtra("index", 0);
		initView();

	}

	@Override
	public void initData() {
		// TODO Auto-generated method stub

	}

	@Override
	public void bindViews() {
		// TODO Auto-generated method stub

	}

	private void initView() {
		pageViews = new ArrayList<View>();
		imageViews = new ImageView[urls.length];
		page_ll = (LinearLayout) findViewById(R.id.page_ll);
		for (int i = 0; i < urls.length; i++) {
			View view = getLayoutInflater().inflate(
					R.layout.big_image_layout_item, null);
			final ImageView imageView = (ImageView) view
					.findViewById(R.id.big_img);
			imageView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					BigImageActivity.this.finish();
				}
			});
			if (isLocal) {// 判断是本地图片还是网络图片，true为本地图片false为网络图片
				imageView.setImageBitmap(ImageUtils.getBitmapByPath(urls[i]));
			} else {
				AppContext.setImage(urls[i], imageView, AppContext.imageOption);

			}
			pageViews.add(view);
			ImageView pageImageView = new ImageView(this);
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
					ViewGroup.LayoutParams.WRAP_CONTENT,
					ViewGroup.LayoutParams.WRAP_CONTENT);
			pageImageView.setLayoutParams(params);
			params.setMargins(20, 0, 0, 0);
			pageImageView.setImageResource(R.drawable.dot_no);
			imageViews[i] = pageImageView;
			page_ll.addView(pageImageView);
		}

		viewPager = (ViewPager) findViewById(R.id.guidePages);
		guidePageAdapter = new GuidePageAdapter();
		viewPager.setAdapter(guidePageAdapter);

		viewPager.setCurrentItem(index);
		imageViews[index].setImageResource(R.drawable.dot_yes);
		viewPager.setOnPageChangeListener(new GuidePageChangeListener());
	}

	class GuidePageAdapter extends PagerAdapter {

		@Override
		public int getCount() {
			return pageViews.size();
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == arg1;
		}

		@Override
		public int getItemPosition(Object object) {
			return super.getItemPosition(object);
		}

		@Override
		public void destroyItem(View arg0, int arg1, Object arg2) {
			((ViewPager) arg0).removeView(pageViews.get(arg1));
		}

		@Override
		public Object instantiateItem(View arg0, int arg1) {
			((ViewPager) arg0).addView(pageViews.get(arg1));
			return pageViews.get(arg1);
		}

		@Override
		public void restoreState(Parcelable arg0, ClassLoader arg1) {

		}

		@Override
		public Parcelable saveState() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void startUpdate(View arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void finishUpdate(View arg0) {
			// TODO Auto-generated method stub

		}
	}

	class GuidePageChangeListener implements OnPageChangeListener {

		@Override
		public void onPageScrollStateChanged(int arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onPageSelected(int arg0) {
			for (int i = 0; i < imageViews.length; i++) {
				if (arg0 != i) {
					imageViews[i].setImageResource(R.drawable.dot_no);
				} else {
					imageViews[i].setImageResource(R.drawable.dot_yes);
				}
			}

		}

	}

}
