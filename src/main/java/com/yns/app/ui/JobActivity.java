package com.yns.app.ui;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yns.R;
import com.yns.app.AppContext;
import com.yns.app.adapter.ServiceKindAdapter;
import com.yns.model.Advert;
import com.yns.model.HomeKind;
import com.yns.model.Job;
import com.yns.model.ResponeModel;
import com.yns.model.TradeType;
import com.yns.net.Urls;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.ComonService;
import com.yns.util.DateUtil;
import com.yns.widget.HeaderBar;
import com.yns.widget.SlideShowView;
import com.yns.widget.SlideShowView.OnBranchClickListener;
import com.yns.widget.XListView;
import com.yns.widget.XListView.IXListViewListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class JobActivity extends BaseActivity implements IXListViewListener,
		OnBranchClickListener {

	private HeaderBar headerBar;
	private SlideShowView slideShowViewService;
	private XListView xListViewService;
	private ServiceKindAdapter serviceAdapter;
	private String gongZuodidian = "";
	private String jobSort = "";
	private String gongZuoJingYan = "";
	private String workType = "全职";
	LinearLayout llMore;
	View v;
	private GridView xListViewService2;
	public JobActivity() {
		super(R.layout.activity_service_list);
	}

	@Override
	public void initViews() {
		headerBar = (HeaderBar) findViewById(R.id.title);
		slideShowViewService = (SlideShowView) findViewById(R.id.slideshowview_service);
		xListViewService = (XListView) findViewById(R.id.service_list);
		xListViewService2= (GridView) findViewById(R.id.service_list2);
		xListViewService.setPullRefreshEnable(true);
		xListViewService.setPullLoadEnable(false);
		headerBar.top_right_img2.setVisibility(View.VISIBLE);
		headerBar.top_right_img2.setImageDrawable(getResources().getDrawable(
				R.drawable.search));
		headerBar.top_right_img2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				JumpToActivityForResult(JobSearchActivity.class, 1000, false);
			}
		});
		xListViewService2.setNumColumns(3);
	}

	private void initDiff() {
		headerBar.setTitle("校园招聘");

	}

	@Override
	public void initData() {

		initDiff();

		List<HomeKind> homeKinds = new ArrayList<HomeKind>();
		// HomeKind ks1 = new HomeKind(R.drawable.icon_a1, R.string.job_1);
		HomeKind ks2 = new HomeKind(R.drawable.icon_a2, R.string.job_2);
		HomeKind ks3 = new HomeKind(R.drawable.icon_a3, R.string.job_3);
		HomeKind ks4 = new HomeKind(R.drawable.icon_a4, R.string.job_4);
		// homeKinds.add(ks1);
		homeKinds.add(ks2);
		homeKinds.add(ks3);
		homeKinds.add(ks4);
		serviceAdapter = new ServiceKindAdapter(this, homeKinds);
		xListViewService2.setAdapter(serviceAdapter);
		xListViewService.setAdapter(new ServiceKindAdapter(this, new ArrayList<HomeKind>()));
		getBanner();
	}

	@Override
	public void bindViews() {
		v = getLayoutInflater().inflate(R.layout.layout_service_foot, null);
		llMore = (LinearLayout) v.findViewById(R.id.ll_more);
		xListViewService.addFooterView(v);
		xListViewService.setXListViewListener(this);
		xListViewService2.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					final int position, long id) {
				ComonService.getInstance(context).getTradeType(
						new CustomAsyncResponehandler() {
							public void onFinish() {

							};

							@SuppressWarnings("unchecked")
							@Override
							public void onSuccess(ResponeModel baseModel) {
								super.onSuccess(baseModel);
								if (baseModel != null && baseModel.isStatus()) {
									List<TradeType> tradeTypeList = (List<TradeType>) baseModel
											.getResultObj();
									if (tradeTypeList != null
											&& tradeTypeList.size() > 0) {

										Intent intent = new Intent();
										intent.putExtra("title", "行业类别");
										intent.putExtra("data",
												(Serializable) tradeTypeList);
										intent.putExtra("tag",1);
										intent.putExtra("flag", position);
										intent.setClass(JobActivity.this,
												JobType2Activity.class);
										startActivity(intent);
									}
								}
							}
						});
			}
		});
		onRefresh();
		getJobList();
		xListViewService.setPullRefreshEnable(false);
		xListViewService.setPullLoadEnable(false);
		xListViewService.setFooterDividersEnabled(false);
	}

	@Override
	public void OnBranchClick(int position) {

	}

	@Override
	public void onRefresh() {
		xListViewService.setRefreshTime(DateUtil.getDateTime(new Date(System
				.currentTimeMillis())));
	}

	@Override
	public void onLoadMore() {

	}

	private void getBanner() {
		ComonService.getInstance(context).getBanner(10 + "", 1 + "", "index",
				new CustomAsyncResponehandler() {
					public void onFinish() {

					};

					@SuppressWarnings("unchecked")
					@Override
					public void onSuccess(ResponeModel baseModel) {
						super.onSuccess(baseModel);
						if (baseModel != null && baseModel.isStatus()) {
							List<Advert> lists = (List<Advert>) baseModel
									.getResultObj();
							if (lists != null && lists.size() > 0) {
								List<String> strList = new ArrayList<String>();
								for (Advert item : lists) {
									strList.add(Urls.IMAGE_URL + item.getPic());
								}
								slideShowViewService.setImageUris(strList);
							}
						}
					}
				});
	}

	private void getJobList() {
		ComonService.getInstance(context).getJob(
				5 + "",
				1 + "",
				AppContext.currentUser == null ? "" : AppContext.currentUser
						.getUserId(), "", workType, gongZuodidian,
				gongZuoJingYan, jobSort, -1,"", new CustomAsyncResponehandler() {
					public void onFinish() {

					};

					@SuppressWarnings("unchecked")
					@Override
					public void onSuccess(ResponeModel baseModel) {
						super.onSuccess(baseModel);
						if (baseModel != null && baseModel.isStatus()) {

							try {
								List<Job> jobList = (List<Job>) baseModel
										.getResultObj();
								if (jobList != null && jobList.size() > 0) {

									llMore.removeAllViews();

									for (Job data : jobList) {

										View convertView = getLayoutInflater()
												.inflate(R.layout.item_job,
														null);
										TextView tvTime = (TextView) convertView
												.findViewById(R.id.tv_time);
										TextView tvCompany = (TextView) convertView
												.findViewById(R.id.tv_com);
										TextView tvContent = (TextView) convertView
												.findViewById(R.id.tv_content);
										TextView tvTitle = (TextView) convertView
												.findViewById(R.id.tv_title);
										TextView tvPrice = (TextView) convertView
												.findViewById(R.id.tv_price);

										tvTime.setText(data.getCreateDate());
										tvCompany.setText(data.getDeptName());
										tvContent.setText( data.getXueLiYaoQiu() + " | " + data.getGongZuoJingYan());
										tvTitle.setText(data.getTitle());
										tvPrice.setText(data.getXinZiDaiYu());
										convertView.setTag(data);
										convertView
												.setOnClickListener(new OnClickListener() {

													@Override
													public void onClick(View v) {
														Job job = (Job) v
																.getTag();
														Intent intent = new Intent();
														intent.setClass(
																JobActivity.this,
																JobDetailActivity.class);
														intent.putExtra("job",
																job);
														startActivity(intent);
													}
												});
										llMore.addView(convertView);
									}

								}
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}
				});
	}
}
