package com.yns.app.ui;

import android.widget.TextView;

import com.yns.R;
import com.yns.model.Text;
import com.yns.widget.HeaderBar;

/**
 * @className：TextActivity.java
 * @author: lmt
 * @Function: 简单文本显示
 * @createDate: 2014-12-9 下午4:36:53
 * @update:
 */
public class TextActivity extends BaseActivity {
	private HeaderBar headerBar;
	private Text text;

	public TextActivity() {
		super(R.layout.activity_text);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void firstLoad() {
		// TODO Auto-generated method stub
		super.firstLoad();
		text = (Text) getIntent().getSerializableExtra("data");
	}

	@Override
	public void initViews() {
		// TODO Auto-generated method stub
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle(text.getTitle());

		TextView textView = (TextView) findViewById(R.id.textcontent);
		textView.setText(text.getContent());
	}

	@Override
	public void initData() {
		// TODO Auto-generated method stub

	}

	@Override
	public void bindViews() {
		// TODO Auto-generated method stub

	}

}
