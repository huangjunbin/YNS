package com.yns.app.ui;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.yns.R;
import com.yns.app.AppContext;
import com.yns.app.adapter.ShopCartAdapter;
import com.yns.model.ResponeModel;
import com.yns.model.ShopCart;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.GoodService;
import com.yns.util.DateUtil;
import com.yns.widget.HeaderBar;
import com.yns.widget.XListView;
import com.yns.widget.XListView.IXListViewListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ShopCartActivity extends BaseActivity implements IXListViewListener {
    private HeaderBar headerBar;
    private XListView xListView;
    private ShopCartAdapter adapter;
    public static List<ShopCart> dataList;
    private int page = 1;
    private int rows = 5;
    private boolean isRefresh = true;
    private int position = 0;
    private TextView allPrice;
    private Button commit;

    public ShopCartActivity() {
        super(R.layout.activity_shopcart);
    }

    @Override
    public void initViews() {
        headerBar = (HeaderBar) findViewById(R.id.title);
        allPrice = (TextView) findViewById(R.id.shopcart_textPrice);
        commit = (Button) findViewById(R.id.shopcart_btnOK);
        headerBar.setTitle("购物车");
        xListView = (XListView) findViewById(R.id.messagelist);
        xListView.setXListViewListener(this);
        xListView.setPullLoadEnable(false);
    }

    public void setAllPrice(String price) {
        allPrice.setText("￥" + price);
    }

    @Override
    public void initData() {
        position = getIntent().getIntExtra("position", 0);

        dataList = new ArrayList<ShopCart>();
        adapter = new ShopCartAdapter(this, dataList);
        xListView.setAdapter(adapter);
    }

    @Override
    public void bindViews() {
        xListView.setFooterDividersEnabled(false);
        commit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dataList == null || dataList.size() < 1) {
                    showToast("购物车为空");
                    return;
                }
                Intent intent = new Intent(ShopCartActivity.this, AddressActivity.class);
                String str = "";
                for (ShopCart item : dataList) {
                    str += item.getAppProductInfoId() + ",";
                }
                str.substring(0,str.length()-1);
                intent.putExtra("data", str);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onRefresh() {
        isRefresh = true;
        page = 1;
        getMessageList();

    }

    @Override
    public void onLoadMore() {
        isRefresh = false;
        page++;
        getMessageList();
    }

    private void getMessageList() {
        GoodService.getInstance(context).getShopCartList(
                AppContext.currentUser.getUserId(), new CustomAsyncResponehandler() {
                    public void onFinish() {
                        xListView.stopLoadMore();
                        xListView.stopRefresh();
                        xListView.setRefreshTime(DateUtil.getDateTime(new Date(
                                System.currentTimeMillis())));
                    }

                    ;

                    @SuppressWarnings("unchecked")
                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel != null && baseModel.isStatus()) {
                            List<ShopCart> ShopCart = (List<ShopCart>) baseModel
                                    .getResultObj();
                            if (ShopCart != null && ShopCart.size() > 0) {
                                if (isRefresh) {
                                    dataList.clear();
                                    dataList.addAll(0, ShopCart);
                                } else {
                                    dataList.addAll(ShopCart);
                                }
                                double all = 0.0;
                                for (ShopCart cart : dataList) {
                                    all += cart.getSalePrice() * cart.getBuyCount();
                                }
                                setAllPrice(all + "");
                                adapter.notifyDataSetChanged();
                            }
                        }
                    }
                });
    }

    @Override
    protected void onResume() {
        onRefresh();
        super.onResume();
    }
}
