package com.yns.app.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.yns.R;
import com.yns.app.AppContext;
import com.yns.app.adapter.JobAdapter;
import com.yns.model.Job;
import com.yns.model.ResponeModel;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.ComonService;
import com.yns.util.DateUtil;
import com.yns.widget.HeaderBar;
import com.yns.widget.XListView;
import com.yns.widget.XListView.IXListViewListener;

public class ApplyActivity extends BaseActivity implements IXListViewListener {

	private HeaderBar headerBar;
	private XListView xlvData;

	private JobAdapter jobAdapter;
	public static List<Job> jobs;
	private int page = 1;
	private int rows = 5;
	private boolean isRefresh = true;

	public ApplyActivity() {
		super(R.layout.activity_apply);

	}

	@Override
	public void initViews() {

		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle("申请列表");
		xlvData = (XListView) findViewById(R.id.xlv_data);
		xlvData.setPullRefreshEnable(true);
		xlvData.setPullLoadEnable(true);

	}

	@Override
	public void initData() {
		jobs = new ArrayList<Job>();
		jobAdapter = new JobAdapter(this, jobs);
		xlvData.setAdapter(jobAdapter);

	}

	@Override
	public void bindViews() {
		xlvData.setXListViewListener(this);
		xlvData.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Job job = jobs.get(position - 1);
				Intent intent = new Intent();
				intent.setClass(ApplyActivity.this, JobDetailActivity.class);
				intent.putExtra("job", job);
				startActivity(intent);
			}
		});
		onRefresh();
	}

	@Override
	public void onRefresh() {
		xlvData.setRefreshTime(DateUtil.getDateTime(new Date(System
				.currentTimeMillis())));
		isRefresh = true;
		page = 1;
		getApplyList();
	}

	@Override
	public void onLoadMore() {
		isRefresh = false;
		page++;
		getApplyList();
	}

	private void getApplyList() {
		ComonService.getInstance(context).getApply(
				rows + "",
				page + "",
				AppContext.currentUser == null ? "" : AppContext.currentUser
						.getUserId(), new CustomAsyncResponehandler() {
					public void onFinish() {
						xlvData.stopLoadMore();
						xlvData.stopRefresh();
						xlvData.setRefreshTime(DateUtil.getDateTime(new Date(
								System.currentTimeMillis())));
					};

					@SuppressWarnings("unchecked")
					@Override
					public void onSuccess(ResponeModel baseModel) {
						super.onSuccess(baseModel);
						if (baseModel != null && baseModel.isStatus()) {
							List<Job> jobList = (List<Job>) baseModel
									.getResultObj();
							if (jobList != null && jobList.size() > 0) {

								if (isRefresh) {
									jobs.clear();
									jobs.addAll(0, jobList);
								} else {
									jobs.addAll( jobList);
								}
								jobAdapter.notifyDataSetChanged();
							}
						}
					}
				});
	}
}
