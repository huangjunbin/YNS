package com.yns.app.ui;

import java.io.File;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

import com.yns.R;
import com.yns.app.AppConfig;
import com.yns.app.AppContext;
import com.yns.db.dao.UserDao;
import com.yns.model.ResponeModel;
import com.yns.model.User;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.ComonService;
import com.yns.service.UserService;
import com.yns.util.MediaUtil;
import com.yns.util.SPUtil;
import com.yns.util.UIHelper;
import com.yns.widget.CameraView;
import com.yns.widget.CircleImageView;
import com.yns.widget.HeaderBar;

/**
 * @className：PersonInfoActivity.java
 * @author: lmt
 * @Function: 个人信息
 * @createDate: 2014-12-6 下午10:48:52
 * @update:
 */
public class PersonInfoActivity extends BaseActivity implements OnClickListener {
	private String personImageName = "person.jpg";

	private boolean isInfoChanged = false;
	// ----------
	private HeaderBar headerBar;
	private CircleImageView personPhoto;
	private EditText editTextUserName, etMobile, etEmail;
	private String companyPIC = AppContext.currentUser.getHeaderPic();

	public PersonInfoActivity() {
		super(R.layout.activity_personinfo);
	}

	@Override
	public void initViews() {
		headerBar = (HeaderBar) findViewById(R.id.title);
		headerBar.setTitle(getResString(R.string.personinfo_title));
		headerBar.setRightText(getResString(R.string.save));
		personPhoto = (CircleImageView) findViewById(R.id.personinfo_photo);
		editTextUserName = (EditText) findViewById(R.id.personinfo_realname);
		etMobile = (EditText) findViewById(R.id.personinfo_mobile);
		etEmail = (EditText) findViewById(R.id.personinfo_email);
	}

	@Override
	public void initData() {
		updateView();
	}

	@Override
	public void bindViews() {
		personPhoto.setOnClickListener(this);
		editTextUserName.addTextChangedListener(textWatcher);
		etMobile.addTextChangedListener(textWatcher);
		etEmail.addTextChangedListener(textWatcher);

		headerBar.top_right_btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (isInfoChanged) {
					UserService.getInstance(context).modifUserInfo(
							AppContext.currentUser.getUserId(),

							etEmail.getText().toString(),
							etMobile.getText().toString(), companyPIC,
							editTextUserName.getText().toString(),
							new CustomAsyncResponehandler() {
								public void onSuccess(ResponeModel baseModel) {
									if (baseModel != null
											&& baseModel.isStatus()) {
										UIHelper.ShowMessage(context, "修改成功！");

										AppContext.currentUser = (User) baseModel
												.getResultObj();

										AppContext.currentUser
												.setType(AppContext.userType);

										AppContext.currentUser
												.setRemberPassword(User.REMBER_PASSWORD);

										AppContext.currentUser
												.setAutoLogin(User.AUTO_LOGIN);
										AppContext.currentUser.setAutoLogin(-1);
									}
									UserDao userDao = new UserDao(
											PersonInfoActivity.this);
									if (userDao != null) {
										userDao.delete();
										userDao.insert(AppContext.currentUser);
									}
									finish();

								};
							});

				} else {
					UIHelper.ShowMessage(context, "额，您没有修改任何信息！");
				}
			}
		});
	}

	private TextWatcher textWatcher = new TextWatcher() {

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {

		}

		@Override
		public void afterTextChanged(Editable s) {
			isInfoChanged = true;
		}
	};

	private void updateView() {
		if (AppContext.currentUser != null) {
			AppContext.setImage(AppContext.currentUser.getHeaderPic(),
					personPhoto, AppContext.memberPhotoOption);
			editTextUserName.setText(AppContext.currentUser.getUserName());
			etEmail.setText(AppContext.currentUser.getEmail());
			etMobile.setText(AppContext.currentUser.getMobile());
		}
	}

	@Override
	public void lastLoad() {
		super.lastLoad();

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.personinfo_photo:
			CameraView.showCameraView(PersonInfoActivity.this, personImageName);
			break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case MediaUtil.ALBUM:
			if (data != null || resultCode == Activity.RESULT_OK) {
				startPhotoZoom(data.getData());
			}
			break;
		case MediaUtil.PHOTO:
			if (resultCode == Activity.RESULT_OK) {
				File temp = new File(MediaUtil.ROOTPATH + File.separator
						+ AppConfig.APP_PATH + File.separator + personImageName);
				startPhotoZoom(Uri.fromFile(temp));
			}
			break;
		case MediaUtil.ZOOMPHOTO:
			if (data != null || resultCode == Activity.RESULT_OK) {
				setPicToView(data);
			}
			break;
		}

	}

	/**
	 * 裁剪图片方法实现
	 * 
	 * @param uri
	 */
	public void startPhotoZoom(Uri uri) {
		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setDataAndType(uri, "image/*");
		// 下面这个crop=true是设置在开启的Intent中设置显示的VIEW可裁剪
		intent.putExtra("crop", "true");
		// aspectX aspectY 是宽高的比例
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		// outputX outputY 是裁剪图片宽高
		intent.putExtra("outputX", 150);
		intent.putExtra("outputY", 150);
		intent.putExtra("return-data", true);
		File dir = new File((MediaUtil.ROOTPATH + File.separator
				+ AppConfig.APP_PATH + File.separator));
		if (!dir.exists())
			dir.mkdirs();

		File saveFile = new File(MediaUtil.ROOTPATH + File.separator
				+ AppConfig.APP_PATH + File.separator + personImageName);

		intent.putExtra("output", Uri.fromFile(saveFile)); // 传入目标文件
		intent.putExtra("outputFormat", "JPEG"); // 输入文件格式
		Intent it = Intent.createChooser(intent, "剪裁图片");
		startActivityForResult(it, MediaUtil.ZOOMPHOTO);
	}

	private void setPicToView(Intent data) {
		Bundle extras = data.getExtras();
		if (extras != null) {
			final Bitmap img = extras.getParcelable("data");
			ComonService.getInstance(context).uploadImg(

					new File(MediaUtil.ROOTPATH + File.separator
							+ AppConfig.APP_PATH + File.separator
							+ personImageName),
					new CustomAsyncResponehandler() {
						@Override
						public void onSuccess(ResponeModel baseModel) {
							super.onSuccess(baseModel);
							if (baseModel != null && baseModel.isStatus()) {
								personPhoto.setImageBitmap(img);
								isInfoChanged = true;

								try {
									companyPIC = new JSONObject(baseModel
											.getResult()).getString("url");
								} catch (JSONException e) {
									e.printStackTrace();
								}
								UIHelper.ShowMessage(context, "上传成功");
							}
						}
					});
		}
	}
}
