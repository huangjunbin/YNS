package com.yns.app.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yns.R;
import com.yns.app.AppContext;
import com.yns.model.Comment;
import com.yns.model.Job;
import com.yns.model.ResponeModel;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.ComonService;
import com.yns.util.UIHelper;
import com.yns.util.UMShare;
import com.yns.widget.HeaderBar;

import java.util.List;

@SuppressLint("ResourceAsColor")
public class JobDetailActivity extends BaseActivity {

    private HeaderBar bar;
    private TextView tvjob, tvCompany;
    private LinearLayout lyJob, lyCompany;
    private Job job;
    private TextView tvCName, tvCContent, tvCPrice, tvCType, tvCUrl,
            tvCLocation, tvCDetail;
    private TextView tvTitle, tvContent, tvCom, tvTime, tvPrice, tvWorkTime,
            tvNum, tvType;
    private WebView wvdetail;
    private Button btnApply, btnComment;
    private int currentTab = 0;
    private int tag = 0;
    private LinearLayout lyMore;

    public JobDetailActivity() {
        super(R.layout.activity_jobdetail);

    }

    @Override
    public void initViews() {
        tvjob = (TextView) findViewById(R.id.tv_job);
        tvCompany = (TextView) findViewById(R.id.tv_company);
        lyJob = (LinearLayout) findViewById(R.id.ly_job);
        lyCompany = (LinearLayout) findViewById(R.id.ly_comapny);
        bar = (HeaderBar) findViewById(R.id.title);
        bar.setTitle("职位详情");
        tvCName = (TextView) findViewById(R.id.tv_company_name);
        tvCContent = (TextView) findViewById(R.id.tv_company_content);
        tvCPrice = (TextView) findViewById(R.id.tv_company_num);
        tvCType = (TextView) findViewById(R.id.tv_company_type);
        tvCUrl = (TextView) findViewById(R.id.tv_company_url);
        tvCLocation = (TextView) findViewById(R.id.tv_company_locaiotn);
        tvCDetail = (TextView) findViewById(R.id.tv_company_detail);

        tvTitle = (TextView) findViewById(R.id.tv_title);
        tvContent = (TextView) findViewById(R.id.tv_content);
        tvCom = (TextView) findViewById(R.id.tv_com);
        tvTime = (TextView) findViewById(R.id.tv_time);
        tvPrice = (TextView) findViewById(R.id.tv_price);
        tvWorkTime = (TextView) findViewById(R.id.tv_worktime);
        tvNum = (TextView) findViewById(R.id.tv_num);
        tvType = (TextView) findViewById(R.id.tv_type);
        wvdetail = (WebView) findViewById(R.id.tv_work_detial);
        btnComment = (Button) findViewById(R.id.btn_comment);
        btnApply = (Button) findViewById(R.id.btn_apply);
        lyMore = (LinearLayout) findViewById(R.id.ll_more);
    }

    @Override
    public void initData() {
        job = (Job) getIntent().getSerializableExtra("job");
        tag = getIntent().getIntExtra("tag", 0);
    }

    @Override
    public void bindViews() {
        change(0);
        tvjob.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                change(0);
            }
        });
        tvCompany.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                change(1);
            }
        });
        tvCName.setText(job.getDeptName());
        tvCContent.setText(job.getXueLiYaoQiu() + " | "
                + job.getGongZuoJingYan());
        tvCPrice.setText(job.getXinZiDaiYu());
        tvCType.setText(job.getCompanyType());
        tvCUrl.setText(job.getIndustry());
        tvCLocation.setText(job.getGongZuoDiDian());
        tvCDetail.setText(job.getDescription());

        tvTitle.setText(job.getTitle());
        tvContent.setText(job.getXueLiYaoQiu() + " | "
                + job.getGongZuoJingYan());
        tvTime.setText(job.getCreateDate());
        tvCom.setText(job.getDeptName());
        tvPrice.setText(job.getXinZiDaiYu());
        tvWorkTime.setText(job.getGongZuoJingYan());
        tvNum.setText(job.getZhaoPingRenShu());
        tvType.setText(job.getWorktype());
        wvdetail.loadDataWithBaseURL(null, job.getContent(), "text/html",
                "utf-8", null);
        bar.top_right_img.setVisibility(View.VISIBLE);
        bar.top_right_img.setImageResource(R.drawable.share);
        bar.top_right_img.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "http://www.dxsapp.com/front/app_job_info.html?id=" + job.getAppJobInfoId();
                UMShare.ShareUrl(JobDetailActivity.this, url, null,
                        R.drawable.ic_launcher).openShare(JobDetailActivity.this,
                        false);
            }
        });
        bar.top_right_img2.setVisibility(View.VISIBLE);

        if ("1".equals(job.getIsFavorite())) {
            bar.top_right_img2.setImageDrawable(getResources().getDrawable(
                    R.drawable.select_star));
        } else {
            bar.top_right_img2.setImageDrawable(getResources().getDrawable(
                    R.drawable.star));
        }
        bar.top_right_img2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (AppContext.currentUser == null) {
                    JumpToActivity(LoginActivity.class, false);
                    return;
                }
                if ("0".equals(job.getIsFavorite())) {
                    addColelct(job.getAppJobInfoId(), "job");
                } else {
                    removeColelct(job.getAppJobInfoId());
                }
            }
        });
        btnApply.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (AppContext.currentUser == null) {
                    JumpToActivity(LoginActivity.class, false);
                    return;
                }
                if (currentTab == 0) {
                    applyJob(job.getAppJobInfoId());
                } else {
                    addColelct(job.getDeptId(), "company");
                }
            }
        });
        if (AppContext.userType == 2) {

            if (!job.isTopflag()) {
                btnApply.setText("设为置顶");
                btnApply.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ComonService.getInstance(context).isTopJob(AppContext.currentUser.getUserId(), job.getAppJobInfoId(),
                                new CustomAsyncResponehandler() {
                                    public void onFinish() {

                                    };
                                    @Override
                                    public void onSuccess(ResponeModel baseModel) {
                                        super.onSuccess(baseModel);
                                        if (baseModel != null && baseModel.isStatus()) {

                                        }
                                    }
                                });
                    }
                });
            } else {
                btnApply.setVisibility(View.GONE);
            }
            bar.top_right_img2.setVisibility(View.GONE);
            if (tag == 1) {
                bar.top_right_btn.setText("应聘简历");
                bar.top_right_btn.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(JobDetailActivity.this,
                                ResumeActivity.class);
                        i.putExtra("type", 2);
                        i.putExtra("jobId", job.getAppJobInfoId());
                        startActivity(i);
                    }
                });
            }

            bar.top_right_img.setVisibility(View.GONE);


        }
        btnComment.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppContext.currentUser == null) {
                    JumpToActivity(LoginActivity.class, false);
                    return;
                }
                Intent intent = new Intent(JobDetailActivity.this, JobCommentActivity.class);
                intent.putExtra("enterpriseId", job.getDeptId());
                startActivity(intent);
            }
        });
    }

    @Override
    public void lastLoad() {
        super.lastLoad();

    }

    @Override
    protected void onResume() {
        ComonService.getInstance(context).getComment(job.getDeptId(),
                new CustomAsyncResponehandler() {
                    public void onFinish() {

                    }

                    ;

                    @SuppressWarnings("unchecked")
                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel != null && baseModel.isStatus()) {
                            List<Comment> tradeTypeList = (List<Comment>) baseModel
                                    .getResultObj();

                            if (tradeTypeList != null
                                    && tradeTypeList.size() > 0) {
                                lyMore.removeAllViews();

                                for (Comment comment : tradeTypeList) {
                                    View v = getLayoutInflater().inflate(R.layout.item_job_comment, null);
                                    TextView tvTitle = (TextView) v.findViewById(R.id.tv_title);
                                    TextView tvContent = (TextView) v.findViewById(R.id.tv_content);
                                    TextView tvTime = (TextView) v.findViewById(R.id.tv_time);
                                    tvTitle.setText(comment.getNickName());
                                    tvContent.setText(comment.getContent());
                                    tvTime.setText(comment.getCreateDate());
                                    System.out.println(comment);
                                    lyMore.addView(v);
                                }
                            }
                        }
                    }
                });


        super.onResume();
    }

    public void change(int tab) {
        currentTab = tab;
        if (tab == 0) {
            lyMore.setVisibility(View.GONE);
            btnComment.setVisibility(View.GONE);
            lyJob.setVisibility(View.VISIBLE);
            lyCompany.setVisibility(View.GONE);
            tvjob.setTextColor(Color.WHITE);
            tvjob.setBackgroundColor(getResources().getColor(
                    R.color.main_title_bg));
            tvCompany.setTextColor(getResources().getColor(
                    R.color.main_title_bg));
            tvCompany.setBackgroundColor(Color.WHITE);
            if (AppContext.userType == 2) {

                if (!job.isTopflag()) {
                    btnApply.setText("设为置顶");

                } else {
                    btnApply.setVisibility(View.GONE);
                }


            } else {
                btnApply.setText("立即申请");
            }

        } else {
            lyMore.setVisibility(View.VISIBLE);
            lyJob.setVisibility(View.GONE);
            lyCompany.setVisibility(View.VISIBLE);
            tvjob.setTextColor(getResources().getColor(R.color.main_title_bg));
            tvjob.setBackgroundColor(Color.WHITE);
            tvCompany.setTextColor(Color.WHITE);
            tvCompany.setBackgroundColor(getResources().getColor(
                    R.color.main_title_bg));
            btnApply.setText("关注该公司");
            if (AppContext.userType == 2) {
                btnComment.setVisibility(View.GONE);
            } else {
                btnComment.setVisibility(View.VISIBLE);
            }
        }
    }

    private void addColelct(String id, final String type) {
        ComonService.getInstance(context).addCollect(
                AppContext.currentUser.getUserId(), id, type,
                new CustomAsyncResponehandler() {
                    public void onFinish() {

                    }

                    ;

                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel != null && baseModel.isStatus()) {
                            if ("job".equals(type)) {
                                bar.top_right_img2
                                        .setImageResource(R.drawable.select_star);
                                job.setIsFavorite("1");
                                for (Job j : JobListActivity.jobs) {
                                    if (j.getAppJobInfoId().equals(
                                            job.getAppJobInfoId())) {
                                        j.setIsFavorite("1");
                                    }
                                }
                            }
                        }
                    }
                });
    }

    private void removeColelct(String id) {
        ComonService.getInstance(context).removeCollect(
                AppContext.currentUser.getUserId(), id,
                new CustomAsyncResponehandler() {
                    public void onFinish() {

                    }

                    ;

                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel != null && baseModel.isStatus()) {
                            bar.top_right_img2
                                    .setImageResource(R.drawable.star);
                            job.setIsFavorite("0");
                            for (Job j : JobListActivity.jobs) {
                                if (j.getAppJobInfoId().equals(
                                        job.getAppJobInfoId())) {
                                    j.setIsFavorite("0");
                                }
                            }
                        }
                    }
                });
    }

    private void applyJob(String jobId) {
        ComonService.getInstance(context).applyJob(
                AppContext.currentUser.getUserId(), jobId,
                new CustomAsyncResponehandler() {
                    public void onFinish() {

                    }

                    ;

                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (!baseModel.isStatus()
                                && "没有默认简历!".equals(baseModel.getMsg())) {
                            UIHelper.showSysDialog(JobDetailActivity.this,
                                    "没有默认简历，是否添加简历",
                                    new UIHelper.OnSurePress() {

                                        @Override
                                        public void onClick(View view) {
                                            Intent intent = new Intent();
                                            intent.setClass(
                                                    JobDetailActivity.this,
                                                    ResumeAddActivity.class);
                                            startActivity(intent);
                                        }
                                    }, true);
                        }
                    }
                });
    }
}
