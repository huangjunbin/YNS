package com.yns.app;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.os.Handler;
import android.widget.ImageView;
import cn.jpush.android.api.JPushInterface;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.yns.R;
import com.yns.global.GlobalConstant;
import com.yns.model.User;
import com.yns.net.Urls;
import com.yns.util.LogUtil;
import com.yns.util.NetUtil;

/**
 * 全局应用程序类：用于保存和调用全局应用配置及访问网络数据
 * 
 * @author bin
 * @version 1.0.0
 * @created 2014-2-24
 */
public class AppContext extends Application {

	public static final int PAGE_SIZE = 20;// 默认分页大小
	public static final int YES = 1;
	public static final int NOT = 2;

	private static AppContext application;
	public static ImageLoader imageLoader;
	public static DisplayImageOptions memberPhotoOption;
	public static DisplayImageOptions imageOption;
	public static ArrayList<EventHandler> mListeners = new ArrayList<EventHandler>();

	private static final int CITY_LIST_SCUESS_HAND = 0;// 城市加载成功
	private static final int HOT_CITY_LIST_SCUESS_HAND = 1;// 热门城市加载成功
	private static final int NET_CHANGE_STATE_HAND = 2;// 网络状态改变
	public static boolean ISAllCITYGETED = false;// 是否获取了所有的城市
	public static boolean ISAllHOTCITYGETED = false;// 是否获取了所有的热门城市
	public static boolean ISNETCONNECTED = false;// 网络是否连接
	public static int CURRENTNETTYPE = 0;// 当前网络类型

	public static User currentUser;// 当前的用户
	public static int userType = 1;// 1个人 2企业

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate() {

		super.onCreate();
		LogUtil.setLogStart(true);
		application = this;
	
		imageLoader = ImageLoader.getInstance();
		imageLoader.init(ImageLoaderConfiguration.createDefault(this));
		memberPhotoOption = new DisplayImageOptions.Builder()
				.showStubImage(R.drawable.default_image)
				.showImageForEmptyUri(R.drawable.default_image)
				.showImageOnFail(R.drawable.default_image).cacheInMemory(true)
				.cacheOnDisc(true).bitmapConfig(Bitmap.Config.RGB_565).build();
		imageOption = new DisplayImageOptions.Builder()
				.showStubImage(R.drawable.img_loading_default_big)
				.showImageForEmptyUri(R.drawable.img_loading_empty_big)
				.showImageOnFail(R.drawable.img_loading_fail_big)
				.cacheInMemory(true).cacheOnDisc(true)
				.bitmapConfig(Bitmap.Config.RGB_565).build();
		// 注册网络
		IntentFilter mFilter = new IntentFilter();
		mFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
		registerReceiver(netChangeReceiver, mFilter);
	}

	public static AppContext getApplication() {
		return application;
	}

	public static void setImageFull(String url, ImageView imageView,
			DisplayImageOptions options) {
		imageLoader.displayImage(url, imageView, options);
	}

	public static void setImage(String url, ImageView imageView,
			DisplayImageOptions options) {
		imageLoader.displayImage(Urls.IMAGE_URL + url, imageView, options);
	}

	/**
	 * 检测当前系统声音是否为正常模式
	 * 
	 * @return
	 */
	public boolean isAudioNormal() {
		AudioManager mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
		return mAudioManager.getRingerMode() == AudioManager.RINGER_MODE_NORMAL;
	}

	/**
	 * 应用程序是否发出提示音
	 * 
	 * @return
	 */
	public boolean isAppSound() {
		return isAudioNormal();
	}

	BroadcastReceiver netChangeReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			CURRENTNETTYPE = NetUtil.getNetworkType(application);
			ISNETCONNECTED = NetUtil.isNetworkConnected(application);
			if (intent.getAction().equals(GlobalConstant.NET_CHANGE_ACTION)) {
				mHandler.sendEmptyMessage(NET_CHANGE_STATE_HAND);
			}
		}

	};

	/**
	 * 判断当前版本是否兼容目标版本的方法
	 * 
	 * @param VersionCode
	 * @return
	 */
	public static boolean isMethodsCompat(int VersionCode) {
		int currentVersion = android.os.Build.VERSION.SDK_INT;
		return currentVersion >= VersionCode;
	}

	/**
	 * 获取App安装包信息
	 * 
	 * @return
	 */
	public PackageInfo getPackageInfo() {
		PackageInfo info = null;
		try {
			info = getPackageManager().getPackageInfo(getPackageName(), 0);
		} catch (NameNotFoundException e) {
			e.printStackTrace(System.err);
		}
		if (info == null)
			info = new PackageInfo();
		return info;
	}

	/**
	 * 保存磁盘缓存
	 * 
	 * @param key
	 * @param value
	 * @throws IOException
	 */
	public void setDiskCache(String key, String value) throws IOException {
		FileOutputStream fos = null;
		try {
			fos = openFileOutput("cache_" + key + ".data", Context.MODE_PRIVATE);
			fos.write(value.getBytes());
			fos.flush();
		} finally {
			try {
				fos.close();
			} catch (Exception e) {
			}
		}
	}

	/**
	 * 获取磁盘缓存数据
	 * 
	 * @param key
	 * @return
	 * @throws IOException
	 */
	public String getDiskCache(String key) throws IOException {
		FileInputStream fis = null;
		try {
			fis = openFileInput("cache_" + key + ".data");
			byte[] datas = new byte[fis.available()];
			fis.read(datas);
			return new String(datas);
		} finally {
			try {
				fis.close();
			} catch (Exception e) {
			}
		}
	}

	public static abstract interface EventHandler {
		public abstract void onCityComplite();

		public abstract void onHotCityComplite();

		public abstract void onNetChange();
	}

	@SuppressLint("HandlerLeak")
	private Handler mHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case CITY_LIST_SCUESS_HAND:
				ISAllCITYGETED = true;
				if (mListeners.size() > 0)// 通知接口完成所有城市加载
					for (EventHandler handler : mListeners) {
						handler.onCityComplite();
					}
				break;
			case HOT_CITY_LIST_SCUESS_HAND:
				ISAllHOTCITYGETED = true;
				if (mListeners.size() > 0)// 通知接口完成热门城市加载
					for (EventHandler handler : mListeners) {
						handler.onHotCityComplite();
					}
				break;
			case NET_CHANGE_STATE_HAND:
				if (mListeners.size() > 0)// 通知网络状态发生改变
					for (EventHandler handler : mListeners) {
						if (handler != null)
							handler.onNetChange();
					}
			default:
				break;
			}
		}
	};
}
