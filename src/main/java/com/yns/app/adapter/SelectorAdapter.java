package com.yns.app.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yns.R;
import com.yns.app.ui.BaseActivity;
import com.yns.model.Agent;

/**
 * @className：SelectorAdapter.java
 * @author: lmt
 * @Function: 简单选择适配器
 * @createDate: 2014-12-7 下午3:37:46
 * @update:
 */
@SuppressLint({ "ViewHolder", "InflateParams" })
public class SelectorAdapter extends BaseListAdapter<Agent> {

	public SelectorAdapter(BaseActivity context, List<Agent> list) {
		super(context, list);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		convertView = mInflater.inflate(R.layout.item_selector, null);
		TextView textViewSelectoe = (TextView) convertView
				.findViewById(R.id.selector_name);
		textViewSelectoe.setText(((Agent) getItem(position)).getName());
		return convertView;
	}
}
