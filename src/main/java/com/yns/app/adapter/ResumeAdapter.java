package com.yns.app.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yns.R;
import com.yns.app.AppContext;
import com.yns.app.ui.BaseActivity;
import com.yns.app.ui.ResumeActivity;
import com.yns.app.ui.ResumeAddActivity;
import com.yns.model.ResponeModel;
import com.yns.model.Resume;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.ComonService;
import com.yns.util.UIHelper;
import com.yns.util.UIHelper.OnSurePress;

@SuppressLint("InflateParams")
public class ResumeAdapter extends BaseListAdapter<Resume> {

	public ResumeAdapter(BaseActivity context, List<Resume> list) {
		super(context, list);

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder viewHolder;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.item_resume, null);
			viewHolder.tvTime = (TextView) convertView
					.findViewById(R.id.tv_time);
			viewHolder.tvDefault = (TextView) convertView
					.findViewById(R.id.tv_default);
			viewHolder.tvEdit = (TextView) convertView
					.findViewById(R.id.tv_edit);
			viewHolder.tvTitle = (TextView) convertView
					.findViewById(R.id.tv_title);
			viewHolder.tvDelete = (TextView) convertView
					.findViewById(R.id.tv_delete);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		final Resume data = (Resume) getItem(position);

		viewHolder.tvTime.setText("创建时间:" + data.getCreateDate());

		viewHolder.tvTitle.setText(data.getName() + "("
				+ data.getGongZuoLeiXing() + ")");
		if ("false".equals(data.getDefaultFlag())) {
			viewHolder.tvDefault.setTextColor(baseActivity
					.getResColor(R.color.red));
		} else {
			viewHolder.tvDefault.setTextColor(baseActivity
					.getResColor(R.color.green));
		}
		viewHolder.tvDefault.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				UIHelper.showSysDialog(baseActivity, "确认设置为默认简历?",
						new OnSurePress() {

							@Override
							public void onClick(View view) {
								setResumeDefault(data);
							}
						}, true);
			}
		});
		viewHolder.tvDelete.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				UIHelper.showSysDialog(baseActivity, "是否删除该简历?",
						new OnSurePress() {

							@Override
							public void onClick(View view) {
								deleteResume(data);
							}
						}, true);
			}
		});
		viewHolder.tvEdit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent = new Intent();
				intent.setClass(baseActivity, ResumeAddActivity.class);
				intent.putExtra("resume", data);
				baseActivity.startActivity(intent);
			}
		});
		if (AppContext.userType == 1) {
			viewHolder.tvDefault.setVisibility(View.VISIBLE);
			viewHolder.tvEdit.setVisibility(View.VISIBLE);
			viewHolder.tvDelete.setVisibility(View.VISIBLE);
		} else {
			viewHolder.tvDefault.setVisibility(View.GONE);
			viewHolder.tvEdit.setVisibility(View.GONE);
			viewHolder.tvDelete.setVisibility(View.GONE);
		}
		return convertView;
	}

	class ViewHolder {
		TextView tvTitle, tvDefault, tvTime, tvEdit, tvDelete;
	}

	private void setResumeDefault(final Resume resume) {
		ComonService.getInstance(baseActivity).setResumeDefault(
				resume.getAppMemberResumeInfoId(),
				AppContext.currentUser == null ? "" : AppContext.currentUser
						.getUserId(), new CustomAsyncResponehandler() {
					public void onFinish() {

					};

					@Override
					public void onSuccess(ResponeModel baseModel) {
						super.onSuccess(baseModel);
						if (baseModel != null && baseModel.isStatus()) {
							resume.setDefaultFlag("true");
							for (Resume item : mList) {
								if (!item.getAppMemberResumeInfoId().equals(
										resume.getAppMemberResumeInfoId())) {
									item.setDefaultFlag("false");
								}
							}
							UIHelper.ShowMessage(baseActivity, "设置成功");
							notifyDataSetChanged();
						}
					}
				});
	}

	private void deleteResume(final Resume resume) {
		ComonService.getInstance(baseActivity).deleteResume(
				resume.getAppMemberResumeInfoId(),
				new CustomAsyncResponehandler() {
					public void onFinish() {

					};

					@Override
					public void onSuccess(ResponeModel baseModel) {
						super.onSuccess(baseModel);
						if (baseModel != null && baseModel.isStatus()) {
							mList.remove(resume);
							UIHelper.ShowMessage(baseActivity, "删除成功");
							notifyDataSetChanged();
						}
					}
				});
	}
}
