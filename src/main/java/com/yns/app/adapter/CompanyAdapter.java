package com.yns.app.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yns.R;
import com.yns.app.ui.BaseActivity;
import com.yns.model.Company;

@SuppressLint("InflateParams")
public class CompanyAdapter extends BaseListAdapter<Company> {

	public CompanyAdapter(BaseActivity context, List<Company> list) {
		super(context, list);

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder viewHolder;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.item_company, null);
			viewHolder.tvTime = (TextView) convertView
					.findViewById(R.id.tv_time);
			viewHolder.tvCompany = (TextView) convertView
					.findViewById(R.id.tv_com);
			viewHolder.tvContent = (TextView) convertView
					.findViewById(R.id.tv_content);

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		Company data = (Company) getItem(position);

		viewHolder.tvTime.setText(data.getAppMemberFavoriteInfoCreateDate());
		viewHolder.tvCompany.setText(data.getDeptName());
		viewHolder.tvContent.setText(data.getCompanyType() + " | "
				+ data.getIndustry());

		return convertView;
	}

	class ViewHolder {
		TextView tvContent, tvTime, tvCompany;
	}
}
