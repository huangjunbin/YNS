package com.yns.app.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yns.R;
import com.yns.app.ui.BaseActivity;
import com.yns.model.Record;
import com.yns.util.DateUtil;

/**
 * @className：RecordAdapter.java
 * @author: lmt
 * @Function: 充值记录适配器
 * @createDate: 2014-12-6 下午4:26:56
 * @update:
 */
@SuppressLint("InflateParams")
public class RecordAdapter extends BaseListAdapter<Record> {

	public RecordAdapter(BaseActivity context, List<Record> list) {
		super(context, list);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.item_chargerecord, null);
			viewHolder.textViewRecordTime = (TextView) convertView
					.findViewById(R.id.record_time);
			viewHolder.textViewRecordDes = (TextView) convertView
					.findViewById(R.id.record_des);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		Record data = (Record) getItem(position);

		String time = DateUtil.exchangeDate(data.getCreateDate(),
				"yyyyMMddHHmmss", "yyyy-MM-dd");
		viewHolder.textViewRecordTime.setText(time);
		viewHolder.textViewRecordDes.setText("成功充值"+data.getMoney()+"元,余额为"+data.getAllMoney()+"元");

		return convertView;
	}

	class ViewHolder {
		TextView textViewRecordTime, textViewRecordDes;
	}
}
