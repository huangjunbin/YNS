package com.yns.app.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.yns.R;
import com.yns.app.ui.BaseActivity;
import com.yns.model.Appraise;
import com.yns.util.DateUtil;

/**
 * @className：AppraiseAdapter.java
 * @author: lmt
 * @Function: 评论适配器
 * @createDate: 2014-12-11 下午6:06:48
 * @update:
 */
@SuppressLint("InflateParams")
public class AppraiseAdapter extends BaseListAdapter<Appraise> {

	public AppraiseAdapter(BaseActivity context, List<Appraise> list) {
		super(context, list);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.item_userappraise, null);
			viewHolder.textViewAppraiseName = (TextView) convertView
					.findViewById(R.id.appraise_name);
			viewHolder.textViewAppraiseTime = (TextView) convertView
					.findViewById(R.id.appraise_time);
			viewHolder.textViewAppraiseContent = (TextView) convertView
					.findViewById(R.id.appraise_content);
			viewHolder.textViewAppraiseStar = (RatingBar) convertView
					.findViewById(R.id.appraise_start);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		Appraise data = (Appraise) getItem(position);

		baseActivity.setTextView(viewHolder.textViewAppraiseName,
				data.getUserNickname(), "佚名", false);
		
		String time = "";
		if (data.getCreateDate() != null) {
			time = DateUtil.exchangeDate(data.getCreateDate(),
					"yyyyMMddHHmmss", "yyyy-MM-dd");
		}
		baseActivity.setTextView(viewHolder.textViewAppraiseTime, time, "时间异常",
				false);

		baseActivity.setTextView(viewHolder.textViewAppraiseContent,
				data.getContent(), "暂无数据", false);

		try {
			viewHolder.textViewAppraiseStar.setProgress(Integer.parseInt(data
					.getStar()));
		} catch (Exception e) {
			// TODO: handle exception
			viewHolder.textViewAppraiseStar.setProgress(0);
		}

		return convertView;
	}

	class ViewHolder {
		TextView textViewAppraiseName, textViewAppraiseTime,
				textViewAppraiseContent;
		RatingBar textViewAppraiseStar;
	}
}
