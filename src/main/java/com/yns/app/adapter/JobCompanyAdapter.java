package com.yns.app.adapter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yns.R;
import com.yns.app.ui.BaseActivity;
import com.yns.app.ui.JobAddActivity;
import com.yns.model.Job;
import com.yns.model.ResponeModel;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.ComonService;
import com.yns.util.UIHelper;
import com.yns.util.UIHelper.OnSurePress;

import java.util.List;

@SuppressLint("InflateParams")
public class JobCompanyAdapter extends BaseListAdapter<Job> {

	public   boolean isEdit=true;
	public JobCompanyAdapter(BaseActivity context, List<Job> list) {
		super(context, list);

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder viewHolder;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.item_jobcompany, null);
			viewHolder.tvTime = (TextView) convertView
					.findViewById(R.id.tv_time);
			viewHolder.tvCompany = (TextView) convertView
					.findViewById(R.id.tv_com);
			viewHolder.tvContent = (TextView) convertView
					.findViewById(R.id.tv_content);
			viewHolder.tvTitle = (TextView) convertView
					.findViewById(R.id.tv_title);
			viewHolder.tvPrice = (TextView) convertView
					.findViewById(R.id.tv_price);
			viewHolder.tvEdit = (TextView) convertView
					.findViewById(R.id.tv_edit);
			viewHolder.tvDelete = (TextView) convertView
					.findViewById(R.id.tv_delete);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		final Job data = (Job) getItem(position);

		viewHolder.tvTime.setText(data.getCreateDate());
		viewHolder.tvCompany.setText(data.getDeptName());
		viewHolder.tvContent.setText(data.getXueLiYaoQiu() + " | "
				+ data.getGongZuoJingYan());
		viewHolder.tvTitle.setText(data.getTitle());
		viewHolder.tvPrice.setText(data.getXinZiDaiYu());
		viewHolder.tvDelete.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				UIHelper.showSysDialog(baseActivity, "是否删除该职位?",
						new OnSurePress() {

							@Override
							public void onClick(View view) {
								deleteJob(data);
							}
						}, true);
			}
		});
		viewHolder.tvEdit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Intent intent = new Intent();
				intent.setClass(baseActivity, JobAddActivity.class);
				intent.putExtra("job", data);
				baseActivity.startActivity(intent);
			}
		});
		if(!isEdit){
			viewHolder.tvDelete.setVisibility(View.GONE);
			viewHolder.tvEdit.setVisibility(View.GONE);
		}
		return convertView;
	}

	private void deleteJob(final Job data) {
		ComonService.getInstance(baseActivity).deleteJob(
				data.getAppJobInfoId(), new CustomAsyncResponehandler() {
					public void onFinish() {

					};

					@Override
					public void onSuccess(ResponeModel baseModel) {
						super.onSuccess(baseModel);
						if (baseModel != null && baseModel.isStatus()) {
							mList.remove(data);
							UIHelper.ShowMessage(baseActivity, "删除成功");
							notifyDataSetChanged();
						}
					}
				});
	}

	class ViewHolder {
		TextView tvTitle, tvContent, tvTime, tvPrice, tvCompany, tvEdit,
				tvDelete;
	}
}
