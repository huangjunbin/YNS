package com.yns.app.adapter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.yns.R;
import com.yns.app.AppContext;
import com.yns.app.ui.BaseActivity;
import com.yns.app.ui.MarketAddActivity;
import com.yns.model.Market;
import com.yns.model.ResponeModel;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.ComonService;
import com.yns.util.UIHelper;
import com.yns.util.UIHelper.OnSurePress;

import java.util.List;

@SuppressLint("InflateParams")
public class MarketAdapter extends BaseListAdapter<Market> {

    private int type = 0;

    public MarketAdapter(BaseActivity context, List<Market> list, int type) {
        super(context, list);
        this.type = type;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.item_market, null);
            viewHolder.tvTime = (TextView) convertView
                    .findViewById(R.id.tv_time);
            viewHolder.tvDefault = (TextView) convertView
                    .findViewById(R.id.tv_default);
            viewHolder.tvEdit = (TextView) convertView
                    .findViewById(R.id.tv_edit);
            viewHolder.tvTitle = (TextView) convertView
                    .findViewById(R.id.tv_title);
            viewHolder.tvDelete = (TextView) convertView
                    .findViewById(R.id.tv_delete);
            viewHolder.ivPhoto = (ImageView) convertView
                    .findViewById(R.id.iv_photo);
            viewHolder.tvPrice = (TextView) convertView
                    .findViewById(R.id.tv_price);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        final Market data = (Market) getItem(position);

        viewHolder.tvTime.setText(data.getCreateDate());
        viewHolder.tvPrice.setText(data.getPrice() + "￥");
        viewHolder.tvTitle.setText(data.getTitle());
        if ("false".equals(data.getIsPublish())) {
            viewHolder.tvDefault.setTextColor(baseActivity
                    .getResColor(R.color.red));
        } else {
            viewHolder.tvDefault.setTextColor(baseActivity
                    .getResColor(R.color.green));
        }

        viewHolder.tvDelete.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                UIHelper.showSysDialog(baseActivity, "是否删除该二手信息?",
                        new OnSurePress() {

                            @Override
                            public void onClick(View view) {
                                deleteMarket(data);
                            }
                        }, true);
            }
        });
        viewHolder.tvEdit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent();
                intent.setClass(baseActivity, MarketAddActivity.class);
                intent.putExtra("market", data);
                baseActivity.startActivity(intent);
            }
        });
        if (type == 0) {
            viewHolder.tvDefault.setVisibility(View.GONE);
            viewHolder.tvEdit.setVisibility(View.VISIBLE);
            viewHolder.tvDelete.setVisibility(View.VISIBLE);
            viewHolder.ivPhoto.setVisibility(View.VISIBLE);
            viewHolder.tvTime.setVisibility(View.GONE);
            String str[] = data.getPic().split(",");
            if (str.length > 0) {
                AppContext.setImage(str[0], viewHolder.ivPhoto, AppContext.imageOption);
            }

        } else {
            viewHolder.ivPhoto.setVisibility(View.VISIBLE);
            viewHolder.tvDefault.setVisibility(View.GONE);
            viewHolder.tvEdit.setVisibility(View.GONE);
            viewHolder.tvDelete.setVisibility(View.GONE);
            viewHolder.tvTime.setVisibility(View.GONE);
            String str[] = data.getPic().split(",");
            if (str.length > 0) {
                AppContext.setImage(str[0], viewHolder.ivPhoto, AppContext.imageOption);
            }
        }
        return convertView;
    }

    class ViewHolder {
        TextView tvTitle, tvDefault, tvTime, tvEdit, tvDelete, tvPrice;
        ImageView ivPhoto;
    }


    private void deleteMarket(final Market market) {
        ComonService.getInstance(baseActivity).deleteMarket(
                market.getMarketInfoId(),
                new CustomAsyncResponehandler() {
                    public void onFinish() {

                    }

                    ;

                    @Override
                    public void onSuccess(ResponeModel baseModel) {
                        super.onSuccess(baseModel);
                        if (baseModel != null && baseModel.isStatus()) {
                            mList.remove(market);
                            UIHelper.ShowMessage(baseActivity, "删除成功");
                            notifyDataSetChanged();
                        }
                    }
                });
    }
}
