package com.yns.app.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.yns.R;
import com.yns.app.AppContext;
import com.yns.app.ui.BaseActivity;
import com.yns.model.Message;

@SuppressLint("InflateParams")
public class MessageAdapter extends BaseListAdapter<Message> {

	public MessageAdapter(BaseActivity context, List<Message> list) {
		super(context, list);

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder viewHolder;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.item_message, null);
			viewHolder.textViewMessageContent = (TextView) convertView
					.findViewById(R.id.message_content);
			viewHolder.textViewMessageTime = (TextView) convertView
					.findViewById(R.id.message_time);
			viewHolder.tvTitle = (TextView) convertView
					.findViewById(R.id.message_title);
			viewHolder.ivPhoto = (ImageView) convertView
					.findViewById(R.id.iv_photo);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		Message data = (Message) getItem(position);
		viewHolder.tvTitle.setText(data.getTitle());
		viewHolder.textViewMessageContent.setText(data.getDescription());

		viewHolder.textViewMessageTime.setText(data.getCreateDate());
		AppContext.setImage(data.getFrontCoverPic(), viewHolder.ivPhoto,
				AppContext.imageOption);
		return convertView;
	}

	class ViewHolder {
		TextView textViewMessageContent, tvTitle, textViewMessageTime;
		ImageView ivPhoto;
	}
}
