package com.yns.app.adapter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.yns.R;
import com.yns.app.ui.BaseActivity;
import com.yns.app.ui.JobActivity;
import com.yns.app.ui.MessageActivity;
import com.yns.app.ui.MessageDetailActivity;
import com.yns.app.ui.ProductListActivity;
import com.yns.app.ui.WebViewActivity;
import com.yns.model.HomeKind;
import com.yns.util.LogUtil;

import java.util.List;

/**
 * @className：HomeKindAdapter.java
 * @author: lmt
 * @Function: 首页面适配器
 * @createDate: 2014-12-2 下午1:58:44
 * @update:
 */
@SuppressLint("InflateParams")
public class HomeKindAdapter extends BaseListAdapter<HomeKind> {
	private String TAG = "HomeKindAdapter";
	private boolean isLog = true;
	private boolean isHome = true;

	public HomeKindAdapter(BaseActivity context, List<HomeKind> list) {
		super(context, list);
	}

	public HomeKindAdapter(BaseActivity context, List<HomeKind> list,
			boolean isHome) {
		super(context, list);
		this.isHome = isHome;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.item_kind, null);
			holder.icon = (ImageView) convertView.findViewById(R.id.kind_icon);
			holder.title = (TextView) convertView.findViewById(R.id.kind_des);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		HomeKind date = (HomeKind) getItem(position);
		holder.icon.setImageResource(date.getIcon_Id());
		LogUtil.d(TAG, date.getIcon_Id() + "______", isLog);
		holder.title.setText(baseActivity.getResString(date.getDes_Id()));

		initListener(convertView, position);
		return convertView;
	}

	/**
	 * 
	 * @param convertView
	 * @param position
	 */
	private void initListener(View convertView, final int position) {
		convertView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (isHome) {
					int p = 0;
					switch (position) {
					case 0:
						p = 1;
						baseActivity.startActivity(new Intent(baseActivity,
								JobActivity.class));
						return;

					case 1:
						p = 31;
						break;

					case 2:
						p = 14;
						// TODO
						break;

					case 3:
						p = 116;
						break;
					case 4:
						// TODO
						p = 19;
						break;

					case 5:
						// TODO
						p = 26;
						break;

					case 6:
						p = 1;
						baseActivity.startActivity(new Intent(baseActivity,
								ProductListActivity.class));
						return;

					case 7:
						p = 5;
						break;

					case 8:
						p = 35;
						break;

					case 9:
						p = 36;
						break;

					case 10:
						// TODO
						p = 22;
						break;

					case 11:
						p = 32;
						break;

					default:
						break;
					}
					if (p == 14 || p == 19 || p == 26 || p == 22) {
						Intent intent = new Intent();
						intent.setClass(baseActivity,
								MessageDetailActivity.class);
						if (p == 14) {
							intent.putExtra("title", "创业中心");
						}
						if (p == 19) {
							intent.putExtra("title", "技能培训");
						}
						if (p == 26) {
							intent.putExtra("title", "就业指导");
						}
						if (p == 22) {
							intent.putExtra("title", "校园风云");
						}
						intent.putExtra("position", p);
						baseActivity.startActivity(intent);
					} else {
						Intent intent = new Intent();
						intent.setClass(baseActivity, MessageActivity.class);
						intent.putExtra("position", p);
						baseActivity.startActivity(intent);
					}
				} else {
					Intent intent = new Intent();
					intent.setClass(baseActivity, WebViewActivity.class);
					if (position == 0) {
						intent.putExtra("url",
								"http://mobile.auto.sohu.com/wzcx/mainPage.at?qq-pf-to=pcqq.discussion");
						intent.putExtra("title", "违规查询");
					}

					if (position == 1) {
						intent.putExtra(
								"url",
								"http://61.144.19.120:6003/wechatmobileapp/pages/Examine2.html?openid=obp-tjthH2l1aY6V5skRsTJRA4fg&from=singlemessage&isappinstalled=0");
						intent.putExtra("title", "年度预审");
					}
					baseActivity.startActivity(intent);
				}
			}
		});
	}

	class ViewHolder {
		ImageView icon;
		TextView title;
	}
}