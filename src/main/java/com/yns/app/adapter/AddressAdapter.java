package com.yns.app.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.yns.R;
import com.yns.app.ui.AddAddressActivity;
import com.yns.model.Address;
import com.yns.model.ResponeModel;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.GoodService;

import java.util.List;


@SuppressLint("InflateParams")
public class AddressAdapter extends BaseListAdapter<Address> {

    public AddressAdapter(Activity context, List<Address> list) {
        super(context, list);
    }

    public boolean isEdit = false;
    private Address selectAddress;

    @Override
    public View getView(final int position, View convertView,
                        final ViewGroup parent) {

        final ViewHolder holder;
        final Address address = mList.get(position);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_address, null);
            holder = new ViewHolder();
            holder.name = (TextView) convertView
                    .findViewById(R.id.address_name);
            holder.address = (TextView) convertView
                    .findViewById(R.id.address_list);
            holder.detail = (Button) convertView
                    .findViewById(R.id.address_detail);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.name.setText(address.getName());
        holder.address.setText(address.getAddress());
        holder.detail.setTag(address);
        holder.detail.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(activity, AddAddressActivity.class);
                intent.putExtra("address", (Address) v.getTag());
                activity.startActivity(intent);
            }
        });

        convertView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!isEdit) {
                    holder.name.setTextColor(activity.getResources().getColor(
                            R.color.btn_not));
                    selectAddress = mList.get(position);
                    for (int i = 0; i < parent.getChildCount(); i++) {

                        if (i != position) {
                            ViewHolder view = (ViewHolder) parent.getChildAt(i)
                                    .getTag();
                            view.name.setTextColor(activity.getResources()
                                    .getColor(R.color.gray));

                        }

                    }
                }

            }
        });
        convertView.setOnLongClickListener(new OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                if (isEdit) {
                    new AlertDialog.Builder(activity)
                            .setTitle("删除收货地址")
                            .setNeutralButton("确定",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(
                                                DialogInterface dialog,
                                                int which) {
                                            GoodService.getInstance(activity).removeAddress(
                                                    address.getConsigneeInfoId(), new CustomAsyncResponehandler() {
                                                        public void onFinish() {
                                                        }

                                                        @SuppressWarnings("unchecked")
                                                        @Override
                                                        public void onSuccess(ResponeModel baseModel) {
                                                            super.onSuccess(baseModel);
                                                            if (baseModel != null && baseModel.isStatus()) {
                                                                mList.remove(address);
                                                                notifyDataSetChanged();
                                                            }
                                                        }
                                                    });
                                            dialog.dismiss();

                                        }
                                    })
                            .setPositiveButton("取消",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(
                                                DialogInterface dialog,
                                                int whichButton) {

                                            dialog.dismiss();

                                        }
                                    })

                            .show();
                }
                return false;
            }
        });
        return convertView;
    }

    public Address getSelectAddress() {
        return selectAddress;
    }

    static class ViewHolder {
        private TextView name, address;
        private Button detail;

    }
}
