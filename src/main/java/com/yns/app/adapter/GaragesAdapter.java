package com.yns.app.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yns.R;
import com.yns.app.ui.BaseActivity;
import com.yns.app.ui.GarageDetailActivity;
import com.yns.model.Garage;

/**
 * @className：GaragesAdapter.java
 * @author: lmt
 * @Function: 我的车库适配器
 * @createDate: 2014-12-9 下午2:05:56
 * @update:
 */
@SuppressLint("InflateParams")
public class GaragesAdapter extends BaseListAdapter<Garage> {

	public GaragesAdapter(BaseActivity context, List<Garage> list) {
		super(context, list);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.item_garage, null);
			viewHolder.textViewCarBrand = (TextView) convertView
					.findViewById(R.id.carbrand);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		final Garage data = (Garage) getItem(position);

		viewHolder.textViewCarBrand.setText(data.getCarBland());

		convertView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				baseActivity.JumpToActivity(GarageDetailActivity.class, data,false);
			}
		});

		return convertView;
	}

	class ViewHolder {
		TextView textViewCarBrand;
	}

}
