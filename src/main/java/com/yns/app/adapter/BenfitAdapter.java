package com.yns.app.adapter;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.alipay.Result;
import com.yns.R;
import com.yns.app.ui.BaseActivity;
import com.yns.model.Benfit;
import com.yns.util.DateUtil;
import com.yns.util.UIHelper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @className：BenfitAdapter.java
 * @author: lmt
 * @Function: 充值优惠适配器
 * @createDate: 2014-12-6 下午10:27:10
 * @update:
 */
@SuppressLint("InflateParams")
public class BenfitAdapter extends BaseListAdapter<Benfit> {
	private static final int SDK_PAY_FLAG = 1;

	private static final int SDK_CHECK_FLAG = 2;

	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case SDK_PAY_FLAG: {
				Result resultObj = new Result((String) msg.obj);
				String resultStatus = resultObj.resultStatus;

				// 判断resultStatus 为“9000”则代表支付成功，具体状态码代表含义可参考接口文档
				if (TextUtils.equals(resultStatus, "9000")) {
					Toast.makeText(baseActivity, "支付成功", Toast.LENGTH_SHORT)
							.show();
					baseActivity.finish();
					;
				} else {
					// 判断resultStatus 为非“9000”则代表可能支付失败
					// “8000”
					// 代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）
					if (TextUtils.equals(resultStatus, "8000")) {
						Toast.makeText(baseActivity, "支付结果确认中",
								Toast.LENGTH_SHORT).show();

					} else {
						Toast.makeText(baseActivity, "支付失败", Toast.LENGTH_SHORT)
								.show();

					}
				}
				break;
			}
			case SDK_CHECK_FLAG: {
				Toast.makeText(baseActivity, "检查结果为：" + msg.obj,
						Toast.LENGTH_SHORT).show();
				break;
			}
			default:
				break;
			}
		};
	};

	public BenfitAdapter(BaseActivity context, List<Benfit> list) {
		super(context, list);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.item_benfit, null);
			viewHolder.textViewBenfitDetail = (TextView) convertView
					.findViewById(R.id.benfit_detail);
			viewHolder.textViewBenfitTime = (TextView) convertView
					.findViewById(R.id.benfit_time);
			viewHolder.buttonBenfitUse = (Button) convertView
					.findViewById(R.id.benfit_use);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		final Benfit data = (Benfit) getItem(position);
		viewHolder.textViewBenfitDetail.setText(data.getBenfitDetail());
		String time = "";
		String start = DateUtil.exchangeDate(data.getStartDate(),
				"yyyyMMddHHmmss", "yyyy-MM-dd");
		String end = DateUtil.exchangeDate(data.getEndDate(), "yyyyMMddHHmmss",
				"yyyy-MM-dd");
		time = start + "至" + end;
		if ("1".equals(data.getType())) {
			viewHolder.buttonBenfitUse.setText("立即兑换");

		} else if ("2".equals(data.getType())) {
			viewHolder.buttonBenfitUse.setText("立即充值");

		}
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		Date curDate = new Date(System.currentTimeMillis());// 获取当前时间
		String str = formatter.format(curDate);
		Long c = Long.parseLong(str);
		Long s = Long.parseLong(data.getStartDate());
		Long e = Long.parseLong(data.getEndDate());
		if (c >= s && c <= e) {
			viewHolder.buttonBenfitUse.setVisibility(View.VISIBLE);
		} else {
			viewHolder.buttonBenfitUse.setVisibility(View.GONE);
		}
		viewHolder.textViewBenfitTime.setText(time);
		viewHolder.buttonBenfitUse.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if ("1".equals(data.getType())) {
					UIHelper.showSysDialog(
							baseActivity,
							"尊敬的用户",
							"您将消耗" + data.getResume() + "积分,用于兑换获得"
									+ data.getActivityMoney() + "元充值,可用于正常消费。",
							new UIHelper.OnSurePress() {

								@Override
								public void onClick(View view) {

								}
							}, false);

				} else if ("2".equals(data.getType())) {
				}
			}
		});
		return convertView;
	}

	class ViewHolder {
		TextView textViewBenfitDetail, textViewBenfitTime;
		Button buttonBenfitUse;
	}
}
