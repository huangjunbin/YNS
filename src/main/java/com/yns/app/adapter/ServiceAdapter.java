package com.yns.app.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.graphics.Paint;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.yns.R;
import com.yns.app.AppContext;
import com.yns.app.ui.BaseActivity;
import com.yns.app.ui.ProductDetailActivity;
import com.yns.model.MerchantsGood;

/**
 * @className：ServiceAdapter.java
 * @author: lmt
 * @Function: 服务列表适配器
 * @createDate: 2014-12-4 下午3:37:11
 * @update:
 */
@SuppressLint("InflateParams")
public class ServiceAdapter extends BaseListAdapter<MerchantsGood> {

	public ServiceAdapter(BaseActivity context, List<MerchantsGood> list,
			int defaultId) {
		super(context, list, defaultId);
		// TODO Auto-generated constructor stub
	}

	public ServiceAdapter(BaseActivity context, List<MerchantsGood> list) {
		super(context, list);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder viewHolder;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.item_service, null);
			viewHolder.imageViewServiceImg = (ImageView) convertView
					.findViewById(R.id.sersvice_img);
			viewHolder.textViewServiceCompany = (TextView) convertView
					.findViewById(R.id.service_company);
			viewHolder.textViewServiceArea = (TextView) convertView
					.findViewById(R.id.service_area);
			viewHolder.textViewServiceKind = (TextView) convertView
					.findViewById(R.id.service_kind);
			viewHolder.textViewServiceNowPrice = (TextView) convertView
					.findViewById(R.id.service_nowprice);
			viewHolder.textViewServiceOldPrice = (TextView) convertView
					.findViewById(R.id.service_oldprice);
			viewHolder.textViewServiceGoodRate = (TextView) convertView
					.findViewById(R.id.service_goodrate);
			viewHolder.textViewServiceDistance = (TextView) convertView
					.findViewById(R.id.service_distance);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		final MerchantsGood data = (MerchantsGood) getItem(position);
		AppContext.setImageFull(data.getLogoUrl(),
				viewHolder.imageViewServiceImg, options);
		viewHolder.textViewServiceCompany.setText(data.getMerchantsName());
		viewHolder.textViewServiceArea.setText("[" + data.getAreaName() + "]");
		viewHolder.textViewServiceKind.setText(data.getGoodsName());
		viewHolder.textViewServiceNowPrice.setText(data.getNowPrice() + "元");
		viewHolder.textViewServiceOldPrice.setText(data.getOldPrice() + "元");
		viewHolder.textViewServiceOldPrice.getPaint().setFlags(
				Paint.STRIKE_THRU_TEXT_FLAG);
		viewHolder.textViewServiceGoodRate.setText(data.getGoodComment());
		viewHolder.textViewServiceDistance.setText(data.getDistance());

		convertView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// baseActivity.JumpToActivity(DetailActivity.class, false);
				baseActivity.JumpToActivity(ProductDetailActivity.class, data, false);
			}
		});
		return convertView;
	}

	class ViewHolder {
		ImageView imageViewServiceImg;
		TextView textViewServiceCompany;
		TextView textViewServiceArea, textViewServiceKind;
		TextView textViewServiceNowPrice, textViewServiceOldPrice,
				textViewServiceGoodRate, textViewServiceDistance;
	}
}
