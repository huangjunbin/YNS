package com.yns.app.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yns.R;
import com.yns.app.ui.BaseActivity;
import com.yns.model.Job;

@SuppressLint("InflateParams")
public class JobAdapter extends BaseListAdapter<Job> {

	public JobAdapter(BaseActivity context, List<Job> list) {
		super(context, list);

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder viewHolder;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.item_job, null);
			viewHolder.tvTime = (TextView) convertView
					.findViewById(R.id.tv_time);
			viewHolder.tvCompany = (TextView) convertView
					.findViewById(R.id.tv_com);
			viewHolder.tvContent = (TextView) convertView
					.findViewById(R.id.tv_content);
			viewHolder.tvTitle = (TextView) convertView
					.findViewById(R.id.tv_title);
			viewHolder.tvPrice = (TextView) convertView
					.findViewById(R.id.tv_price);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		Job data = (Job) getItem(position);
		viewHolder.tvTime.setText(data.getCreateDate());
		viewHolder.tvCompany.setText(data.getDeptName());
		//data.getGongZuoDiDian() + " | "
		viewHolder.tvContent.setText(
				 data.getXueLiYaoQiu() + " | " + data.getGongZuoJingYan());
		viewHolder.tvTitle.setText(data.getTitle());
		viewHolder.tvPrice.setText(data.getXinZiDaiYu());
		return convertView;
	}

	class ViewHolder {
		TextView tvTitle, tvContent, tvTime, tvPrice, tvCompany;
	}
}
