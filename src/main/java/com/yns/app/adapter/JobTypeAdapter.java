package com.yns.app.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.yns.R;
import com.yns.app.ui.BaseActivity;
import com.yns.model.JobType;

@SuppressLint({ "ViewHolder", "InflateParams" })
public class JobTypeAdapter extends BaseListAdapter<JobType> {

	public JobTypeAdapter(BaseActivity context, List<JobType> list) {
		super(context, list);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		convertView = mInflater.inflate(R.layout.item_job_type, null);
		final JobType item = (JobType) getItem(position);
		TextView tvName = (TextView) convertView.findViewById(R.id.tv_name);
		tvName.setText(item.getName());
		final CheckBox cbCheck = (CheckBox) convertView
				.findViewById(R.id.cb_check);
		cbCheck.setChecked(item.isCheck());
		cbCheck.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				item.setCheck(isChecked);
				if (isChecked) {
					for (int i = 0; i < mList.size(); i++) {

						if (mList.get(i).getId().equals(item.getId())) {
							mList.get(i).setCheck(true);
						} else {
							mList.get(i).setCheck(false);
						}
					}
				}
				notifyDataSetChanged();
			}
		});
		convertView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (cbCheck.isChecked()) {
					cbCheck.setChecked(false);
				} else {
					cbCheck.setChecked(true);
				}
			}
		});
		return convertView;
	}

	public JobType getCurrentName() {
		for (int i = 0; i < mList.size(); i++) {

			if (mList.get(i).isCheck()) {
				return mList.get(i);
			}
		}
		return null;
	}
}
