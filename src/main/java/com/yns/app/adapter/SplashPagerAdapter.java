package com.yns.app.adapter;

import java.util.List;


import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;

public class SplashPagerAdapter extends PagerAdapter {
	private List<View> mList;
//	private ViewPager mViewPager;
	
	public SplashPagerAdapter(ViewPager viewPager, List<View> list) {
//		mViewPager = viewPager;
		mList = list;
	}

	@Override
	public int getCount() {
		return this.mList.size();
	}

	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		return arg0 == arg1;
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		container.addView((View)this.mList.get(position % this.mList.size()));
	    return this.mList.get(position % this.mList.size());
	}
	
	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((View)mList.get(position % this.mList.size()));
	}
}
