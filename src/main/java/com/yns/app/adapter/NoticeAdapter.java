package com.yns.app.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yns.R;
import com.yns.app.ui.BaseActivity;
import com.yns.model.Notice;

@SuppressLint("InflateParams")
public class NoticeAdapter extends BaseListAdapter<Notice> {

	public NoticeAdapter(BaseActivity context, List<Notice> list) {
		super(context, list);

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder viewHolder;
		if (convertView == null) {
			viewHolder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.item_notice, null);

			viewHolder.tvTitle = (TextView) convertView
					.findViewById(R.id.tv_title);

			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		Notice data = (Notice) getItem(position);
		String title=data.getType().equals("job")?"面试通知":(data.getType().equals("apply")?"申请通知":"系统通知");
		//job面试通知/apply申请通知/sys系统通知
		viewHolder.tvTitle.setText(title+":"+data.getTitle());
		return convertView;
	}

	class ViewHolder {
		TextView tvTitle;
	}
}
