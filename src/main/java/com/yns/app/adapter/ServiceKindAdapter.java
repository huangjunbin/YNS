package com.yns.app.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.yns.R;
import com.yns.app.ui.BaseActivity;
import com.yns.app.ui.JobListActivity;
import com.yns.model.HomeKind;
import com.yns.util.LogUtil;

/**
 * @className：HomeKindAdapter.java
 * @author: lmt
 * @Function: 首页面适配器
 * @createDate: 2014-12-2 下午1:58:44
 * @update:
 */
@SuppressLint("InflateParams")
public class ServiceKindAdapter extends BaseListAdapter<HomeKind> {
	private String TAG = "HomeKindAdapter";
	private boolean isLog = true;

	public ServiceKindAdapter(BaseActivity context, List<HomeKind> list) {
		super(context, list);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.item_kind_list, null);
			holder.icon = (ImageView) convertView.findViewById(R.id.kind_icon);
			holder.title = (TextView) convertView.findViewById(R.id.kind_des);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		HomeKind date = (HomeKind) getItem(position);
		holder.icon.setImageResource(date.getIcon_Id());
		LogUtil.d(TAG, date.getIcon_Id() + "______", isLog);
		holder.title.setText(baseActivity.getResString(date.getDes_Id()));

		return convertView;
	}

	class ViewHolder {
		ImageView icon;
		TextView title;
	}
}