package com.yns.app.adapter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Paint;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.yns.R;
import com.yns.app.AppContext;
import com.yns.app.ui.BaseActivity;
import com.yns.app.ui.LoginActivity;
import com.yns.app.ui.ProductDetailActivity;
import com.yns.model.Product;
import com.yns.model.ResponeModel;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.GoodService;

import java.util.List;

@SuppressLint("InflateParams")
public class ProductListAdapter extends BaseListAdapter<Product> {

    public ProductListAdapter(BaseActivity context, List<Product> list) {
        super(context, list);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.item_productlist, null);
            viewHolder.textViewMessageContent = (TextView) convertView
                    .findViewById(R.id.message_content);
            viewHolder.textViewMessageTime = (TextView) convertView
                    .findViewById(R.id.message_time);
            viewHolder.tvTitle = (TextView) convertView
                    .findViewById(R.id.message_title);
            viewHolder.ivPhoto = (ImageView) convertView
                    .findViewById(R.id.iv_photo);
            viewHolder.tvSalePrice = (TextView) convertView.findViewById(R.id.tv_saleprice);
            viewHolder.tvTagPrice = (TextView) convertView.findViewById(R.id.tv_tagprice);
            viewHolder.btnAdd = (Button) convertView
                    .findViewById(R.id.btn_add);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        final Product data = (Product) getItem(position);
        viewHolder.tvTitle.setText(data.getName());
        viewHolder.textViewMessageContent.setText(data.getDescription());
        viewHolder.textViewMessageTime.setText(data.getCreateDate());
        viewHolder.tvSalePrice.setText("￥" + data.getSalePrice() + "");
        viewHolder.tvTagPrice.setText("￥" + data.getTagPrice() + "");
        viewHolder.tvTagPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);

        if (data != null) {
            AppContext.setImage(data.getPic(), viewHolder.ivPhoto,
                    AppContext.imageOption);

        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent();
                intent.putExtra("data", data);
                intent.setClass(baseActivity, ProductDetailActivity.class);
                baseActivity.startActivity(intent);
            }
        });
        viewHolder.btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppContext.currentUser == null) {
                   baseActivity.JumpToActivity(LoginActivity.class, false);
                    return;
                }
                GoodService.getInstance(baseActivity).addShopCart(data.getAppProductInfoId(),
                        AppContext.currentUser.getUserId(), 1,
                        new CustomAsyncResponehandler() {
                            public void onFinish() {

                            }
                            @Override
                            public void onSuccess(ResponeModel baseModel) {
                                super.onSuccess(baseModel);
                                if (baseModel != null && baseModel.isStatus()) {
                                    baseActivity.showToast("加入购物车成功");
                                }
                            }
                        });
            }
        });
        return convertView;
    }

    class ViewHolder {
        TextView textViewMessageContent, tvTitle, textViewMessageTime, tvSalePrice, tvTagPrice;
        ImageView ivPhoto;
        Button btnAdd;
    }
}
