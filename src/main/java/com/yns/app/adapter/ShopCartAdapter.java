package com.yns.app.adapter;

import android.annotation.SuppressLint;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.yns.R;
import com.yns.app.AppContext;
import com.yns.app.ui.BaseActivity;
import com.yns.app.ui.ShopCartActivity;
import com.yns.model.ResponeModel;
import com.yns.model.ShopCart;
import com.yns.net.http.CustomAsyncResponehandler;
import com.yns.service.GoodService;

import java.util.List;

@SuppressLint("InflateParams")
public class ShopCartAdapter extends BaseListAdapter<ShopCart> {

    private ShopCartActivity sa;
    public ShopCartAdapter(BaseActivity context, List<ShopCart> list) {
        super(context, list);
        sa=(ShopCartActivity)context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.item_shopcart, null);
            viewHolder.name = (TextView) convertView.findViewById(R.id.bar_detail_name);
            viewHolder.price = (TextView) convertView.findViewById(R.id.bar_textPrice);
            viewHolder.count = (TextView) convertView.findViewById(R.id.bar_textCount);
            viewHolder.totalCount = (TextView) convertView.findViewById(R.id.bar_textTotalGood);
            viewHolder.totalPrice = (TextView) convertView.findViewById(R.id.bar_totalPrice);
            viewHolder.photo = (ImageView) convertView.findViewById(R.id.bar_detail_header_im);
            viewHolder.btnAdd = (ImageButton) convertView.findViewById(R.id.bar_btnAdd);
            viewHolder.btnRemove = (ImageButton) convertView.findViewById(R.id.bar_btnRemove);
            viewHolder.btnDelete = (ImageButton) convertView.findViewById(R.id.bar_btnDelete);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        final ShopCart data = (ShopCart) getItem(position);
        viewHolder.name.setText(data.getName());
        viewHolder.price.setText("￥" + data.getSalePrice());
        viewHolder.count.setText(data.getBuyCount()+"");
        viewHolder.totalCount.setText("共" + data.getBuyCount() + "件商品");
        viewHolder.totalPrice.setText("￥" + data.getSalePrice() * data.getBuyCount());
        AppContext.setImage(data.getPic(), viewHolder.photo,
                AppContext.imageOption);
        viewHolder.btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GoodService.getInstance(baseActivity).addShopCart(data.getAppProductInfoId(),
                        AppContext.currentUser.getUserId(), 1, new CustomAsyncResponehandler() {
                            public void onFinish() {
                            }

                            @Override
                            public void onSuccess(ResponeModel baseModel) {
                                super.onSuccess(baseModel);
                                if (baseModel != null && baseModel.isStatus()) {
                                    data.setBuyCount(data.getBuyCount() + 1);
                                    viewHolder.count.setText(data.getBuyCount() + "");
                                    viewHolder.totalCount.setText("共" + data.getBuyCount() + "件商品");
                                    viewHolder.totalPrice.setText("￥" + data.getSalePrice() * data.getBuyCount());
                                    double all=0.0;
                                    for (ShopCart cart:mList){
                                        all+=data.getSalePrice() * data.getBuyCount();
                                    }
                                    sa.setAllPrice(all+"");
                                }
                            }
                        });
            }

        });
        viewHolder.btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(data.getBuyCount()<=1){
                    baseActivity.showToast("不能小于1件");
                    return;
                }
                GoodService.getInstance(baseActivity).addShopCart(data.getAppProductInfoId(),
                        AppContext.currentUser.getUserId(),-1,new CustomAsyncResponehandler() {
                            public void onFinish() {
                            }
                            @Override
                            public void onSuccess(ResponeModel baseModel) {
                                super.onSuccess(baseModel);
                                if (baseModel != null && baseModel.isStatus()) {
                                    data.setBuyCount(data.getBuyCount()-1);
                                    viewHolder.count.setText(data.getBuyCount() + "");
                                    viewHolder.totalCount.setText("共" + data.getBuyCount() + "件商品");
                                    viewHolder.totalPrice.setText("￥" + data.getSalePrice() * data.getBuyCount());
                                    double all=0.0;
                                    for (ShopCart cart:mList){
                                        all+=data.getSalePrice() * data.getBuyCount();
                                    }
                                    sa.setAllPrice(all + "");
                                }
                            }
                        });
            }

        });
        viewHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GoodService.getInstance(baseActivity).removeShopCart(data.getAppProductInfoId(),
                        AppContext.currentUser.getUserId(),new CustomAsyncResponehandler() {
                            public void onFinish() {
                            }
                            @Override
                            public void onSuccess(ResponeModel baseModel) {
                                super.onSuccess(baseModel);
                                if (baseModel != null && baseModel.isStatus()) {
                                    baseActivity.showToast("移除成功");
                                    mList.remove(data);
                                    notifyDataSetChanged();
                                    double all=0.0;
                                    for (ShopCart cart:mList){
                                        all+=data.getSalePrice() * data.getBuyCount();
                                    }
                                    sa.setAllPrice(all + "");
                                }
                            }
                        });
            }

        });
        return convertView;
    }

    class ViewHolder {
        TextView name;
        TextView price;
        TextView count;
        TextView totalCount;
        TextView totalPrice;
        ImageView photo;
        ImageButton btnAdd;
        ImageButton btnRemove;
        ImageButton btnDelete;
    }
}
