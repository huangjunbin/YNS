package com.yns.app.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.yns.R;
import com.yns.app.AppContext;
import com.yns.app.ui.OrderDetailActivity;
import com.yns.model.OrderInfo;

import java.util.List;

public class OrderAdapter extends BaseListAdapter<OrderInfo> {

    public OrderAdapter(Activity context, List<OrderInfo> list) {
        super(context, list);
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.item_order, null);
            holder.name = (TextView) convertView.findViewById(R.id.order_detail_name);
            holder.summary = (TextView) convertView.findViewById(R.id.order_detail_summary);
            holder.price = (TextView) convertView.findViewById(R.id.order_textPrice);
            holder.count = (TextView) convertView.findViewById(R.id.order_textCount);
            holder.totalCount = (TextView) convertView.findViewById(R.id.order_textTotalGood);
            holder.totalPrice = (TextView) convertView.findViewById(R.id.order_totalPrice);
            holder.photo = (ImageView) convertView.findViewById(R.id.order_detail_header_im);
            holder.btnOK = (Button) convertView.findViewById(R.id.order_btnOK);
            holder.btnNo = (Button) convertView.findViewById(R.id.order_btnNO);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final OrderInfo data = mList.get(position);
        if (data.getOrderItems() != null && data.getOrderItems().size() > 0) {
            holder.name.setText(data.getOrderItems().get(0).getName());
            holder.summary.setText(data.getOrderItems().get(0).getDescription());
            holder.price.setText("￥" + data.getOrderItems().get(0).getSalePrice() + "");
            holder.count.setText("x" + data.getOrderItems().get(0).getBuyCount() + "");
            holder.totalPrice.setText("￥" + data.getAmount() + "");
            AppContext.setImage(data.getOrderItems().get(0).getPic(), holder.photo, AppContext.imageOption);
        }
        if ("1".equals(data.getStatus())) {
            holder.totalCount.setText("等待发货（已支付）");
            holder.btnNo.setVisibility(View.GONE);
            holder.btnOK.setVisibility(View.GONE);
        } else if ("2".equals(data.getStatus())) {
            holder.totalCount.setText("已发货（等待确认）");
            holder.btnNo.setVisibility(View.GONE);
            holder.btnOK.setVisibility(View.VISIBLE);
            holder.btnOK.setText("确认收货");
        } else if ("3".equals(data.getStatus())) {
            holder.totalCount.setText("交易完成");
            holder.btnNo.setVisibility(View.GONE);
            holder.btnOK.setVisibility(View.GONE);
        } else if ("100".equals(data.getStatus())) {
            holder.totalCount.setText("交易关闭");
            holder.btnNo.setVisibility(View.GONE);
            holder.btnOK.setVisibility(View.GONE);
        } else if ("0".equals(data.getStatus())) {
            holder.totalCount.setText("未支付");
            holder.btnNo.setVisibility(View.GONE);
            holder.btnOK.setVisibility(View.VISIBLE);

        }
        holder.btnNo.setVisibility(View.GONE);
        holder.btnOK.setVisibility(View.GONE);
        convertView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.putExtra("order", mList.get(position));
                i.putExtra("type", "1");
                i.setClass(activity, OrderDetailActivity.class);
                activity.startActivity(i);
            }
        });
        holder.btnNo.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

            }
        });
        holder.btnOK.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {


            }
        });
        return convertView;
    }

    class ViewHolder {
        TextView name;
        TextView price;
        TextView summary;
        TextView count;
        TextView totalCount;
        TextView totalPrice;
        ImageView photo;
        Button btnOK;
        Button btnNo;
    }


}