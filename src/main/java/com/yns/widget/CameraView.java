package com.yns.widget;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface.OnCancelListener;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.yns.R;
import com.yns.app.AppConfig;
import com.yns.util.MediaUtil;

public class CameraView {
	public static final int TYPE_BUTTON = 0;
	public static final int TYPE_TITLE = 1;
	public static final int TYPE_EXIT = 2;
	public static final int TYPE_CANCEL = 3;

	public interface OnAlertSelectId {
		void onClick(int whichButton);
	}

	public static void showCameraView(final Activity context,
			final String photoName) {
		showCameraView(context, "设置图像", new String[] { "相册", "拍照" }, "取消",
				null, new OnAlertSelectId() {
					@Override
					public void onClick(int whichButton) {
						// TODO Auto-generated method stub
						int mediaFlag = -1;
						if (whichButton == 1) {
							mediaFlag = MediaUtil.PHOTO;
						} else if (whichButton == 0) {
							mediaFlag = MediaUtil.ALBUM;
						}
						switch (mediaFlag) {
						case MediaUtil.ALBUM:
							MediaUtil.searhcAlbum(context, MediaUtil.ALBUM);
							break;
						case MediaUtil.PHOTO:
							MediaUtil.takePhoto(context, MediaUtil.PHOTO,
									File.separator + AppConfig.APP_PATH
											+ File.separator, photoName);
							break;
						}
					}
				});
	}

	public static Dialog showCameraView(Context con, final String title,
			String[] items, String exitOrCancel,
			OnCancelListener cancelListener, final OnAlertSelectId alertDo) {
		final Dialog dia = new Dialog(con, R.style.camera_style);
		LinearLayout lay = (LinearLayout) LayoutInflater.from(con).inflate(
				R.layout.panel_camear, null);
		lay.setMinimumWidth(100000);
		final ListView list = (ListView) lay.findViewById(R.id.content_list);
		AlertAdapter adapter = new AlertAdapter(con, title, items, exitOrCancel);
		list.setAdapter(adapter);
		list.setDividerHeight(0);
		list.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if (!(title == null || title.equals("")) && position - 1 >= 0) {
					alertDo.onClick(position - 1);
					dia.dismiss();
					list.requestFocus();
				} else {
					alertDo.onClick(position);
					dia.dismiss();
					list.requestFocus();
				}
			}
		});

		Window win = dia.getWindow();
		WindowManager.LayoutParams lp = win.getAttributes();
		lp.x = 0;
		lp.y = -10000;
		lp.gravity = Gravity.BOTTOM;

		dia.onWindowAttributesChanged(lp);
		dia.setCanceledOnTouchOutside(true);
		if (cancelListener != null) {
			dia.setOnCancelListener(cancelListener);
		}
		dia.setContentView(lay);
		dia.show();
		return dia;
	}

	public static List<String> stringToList(String[] data) {
		List<String> list = new ArrayList<String>();
		for (int i = 0; i < data.length; i++) {
			list.add(data[i]);
		}
		return list;
	}
}

class AlertAdapter extends BaseAdapter {
	// private static final String TAG = "AlertAdapter";
	private List<String> items;
	private int[] types;
	// private boolean isSpecial = false;
	private boolean isTitle = false;
	// private boolean isExit = false;
	private Context context;

	public AlertAdapter(Context context, String title, String[] items,
			String cancelOrExit) {
		if (items == null || items.length == 0) {
			this.items = new ArrayList<String>();
		} else {
			this.items = CameraView.stringToList(items);
		}
		this.types = new int[this.items.size() + 3];
		this.context = context;
		if (title != null && !title.equals("")) {
			types[0] = CameraView.TYPE_TITLE;
			this.isTitle = true;
			this.items.add(0, title);
		}

		if (cancelOrExit != null && !cancelOrExit.equals("")) {
			// this.isSpecial = true;
			types[this.items.size()] = CameraView.TYPE_CANCEL;
			this.items.add(cancelOrExit);
		}
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public Object getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public boolean isEnabled(int position) {
		if (position == 0 && isTitle) {
			return false;
		} else {
			return super.isEnabled(position);
		}
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final String textString = (String) getItem(position);
		ViewHolder holder;
		int type = types[position];

		if (convertView == null
				|| ((ViewHolder) convertView.getTag()).type != type) {
			holder = new ViewHolder();
			if (type == CameraView.TYPE_CANCEL) {
				convertView = View.inflate(context,
						R.layout.alert_dialog_menu_list_layout_cancel, null);
			} else if (type == CameraView.TYPE_BUTTON) {
				convertView = View.inflate(context,
						R.layout.alert_dialog_menu_list_layout, null);
			} else if (type == CameraView.TYPE_TITLE) {
				convertView = View.inflate(context,
						R.layout.alert_dialog_menu_list_layout_title, null);
			} else if (type == CameraView.TYPE_EXIT) {
				convertView = View.inflate(context,
						R.layout.alert_dialog_menu_list_layout_special, null);
			}
			// holder.view = (LinearLayout)
			// convertView.findViewById(R.id.popup_layout);
			holder.text = (TextView) convertView.findViewById(R.id.popup_text);
			holder.type = type;

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.text.setText(textString);
		return convertView;
	}

	static class ViewHolder {
		TextView text;
		int type;
	}
}
